// @flow
import * as React from 'react';

import msg from '../../../../../../languages';
import Button from '../../../../../buttons/Button';

import IconButton from '../../../../../../../mobile-app/components/atoms/IconButton';
import LabelWithHint from '../../../../../fields/LabelWithHint';

import { type Report } from '../../../../../../../domain/models/exp/Report';
import { type Errors } from '../..';

type Props = {
  expReport: Report,
  hintMsg?: string,
  isRequired: boolean,
  readOnly: boolean,
  errors: Errors,
  handleChangeCostCenter: () => void,
  handleClickCostCenterBtn: () => void,
  onChangeEditingExpReport: (string, any, any) => void,
};

const ROOT = 'ts-expenses__form-report-summary__form__cost-center';
export default class ReportCostCenter extends React.Component<Props> {
  componentDidMount = () => {
    const { isRequired, onChangeEditingExpReport } = this.props;
    onChangeEditingExpReport('ui.isCostCenterRequired', isRequired, false);
  };

  componentDidUpdate(prevProps: Props) {
    const { isRequired, expReport, onChangeEditingExpReport } = this.props;
    const isReportChanged = prevProps.expReport.reportId !== expReport.reportId;
    const isCostCenterChanged =
      prevProps.expReport.costCenterCode !== expReport.costCenterCode;
    const isReportTypeChanged =
      prevProps.expReport.expReportTypeId !== expReport.expReportTypeId;
    if (isReportChanged || isCostCenterChanged || isReportTypeChanged) {
      onChangeEditingExpReport('ui.isCostCenterRequired', isRequired, false);
    }
  }

  componentWillUnmount = () => {
    const { onChangeEditingExpReport } = this.props;
    onChangeEditingExpReport('ui.isCostCenterRequired', null, false);
  };

  render() {
    const { isRequired, readOnly, expReport, errors, hintMsg } = this.props;
    const isNewReport =
      expReport.scheduledDate !== undefined
        ? !expReport.scheduledDate
        : !expReport.accountingDate;
    return (
      <React.Fragment>
        <div className={`${ROOT}-input ts-text-field-container`}>
          <LabelWithHint
            text={msg().Exp_Clbl_CostCenter}
            hintMsg={(!readOnly && hintMsg) || ''}
            isRequired={isRequired}
          />

          {expReport.costCenterName ? (
            <div className={`${ROOT}-input-field`}>
              <span>{`${expReport.costCenterCode} - ${expReport.costCenterName}`}</span>
              {!readOnly && (
                <IconButton
                  icon="close-copy"
                  size="small"
                  className={`${ROOT}-input-btn--clear`}
                  onClick={this.props.handleChangeCostCenter}
                  disabled={readOnly}
                />
              )}
            </div>
          ) : (
            <Button
              className={`${ROOT}-input-btn--search`}
              onClick={this.props.handleClickCostCenterBtn}
              disabled={readOnly || isNewReport}
            >
              {msg().Com_Lbl_Select}
            </Button>
          )}
        </div>
        {errors.costCenterName && (
          <div className="input-feedback">{msg()[errors.costCenterName]}</div>
        )}
      </React.Fragment>
    );
  }
}
