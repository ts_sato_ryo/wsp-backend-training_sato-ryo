// @flow

// note: AP = Accounting Period

import React from 'react';
import { get, find } from 'lodash';

import msg from '../../../../../../../languages';
import MultiColumnsGrid from '../../../../../../MultiColumnsGrid';
import DateField from '../../../../../../fields/DateField';
import DateUtil from '../../../../../../../utils/DateUtil';
import LabelWithHint from '../../../../../../fields/LabelWithHint';
import type { AccountingPeriodList } from '../../../../../../../../domain/models/exp/AccountingPeriod';
import { type Report } from '../../../../../../../../domain/models/exp/Report';
import { type CustomHint } from '../../../../../../../../domain/models/exp/CustomHint';

type Props = {
  // ui states
  errors: {|
    accountingDate: string,
    subject: string,
  |},
  touched: {|
    records?: Array<*>,
    accountingDate: string,
    subject: string,
  |},
  // HOC props
  accountingPeriodActive: AccountingPeriodList,
  accountingPeriodInactive: AccountingPeriodList,
  accountingPeriodAll: AccountingPeriodList,
  accountingPeriodIdOriginallySelected: string,
  expReport: Report,
  customHint: CustomHint,
  onChangeAccountingPeriod: () => void,
  readOnly: boolean,
  isFinanceApproval: boolean,
  resetAccountingPeriod: () => void,
};

const ROOT = 'ts-expenses__form-report-summary__form__ap';
// this class is only used in expense request (not in expense report)
export default class AccountingPeriodAndRecordDate extends React.Component<Props> {
  format(dateStr: string) {
    return DateUtil.format(dateStr, 'YYYY/MM/DD');
  }

  buildOptionList() {
    const accountingPeriodList = this.props.readOnly
      ? this.props.accountingPeriodAll
      : this.props.accountingPeriodActive;

    if (this.props.isFinanceApproval && !this.props.readOnly) {
      const selectedInactiveAccountingPeriod = find(
        this.props.accountingPeriodInactive,
        {
          id: this.props.accountingPeriodIdOriginallySelected,
        }
      );
      if (selectedInactiveAccountingPeriod) {
        accountingPeriodList.push(selectedInactiveAccountingPeriod);
      }
    }

    const optionLists = accountingPeriodList.map<React$Element<'option'>>(
      (ap) => {
        return (
          <option key={ap.id} value={ap.id}>
            {`${this.format(ap.validDateFrom)} - ${this.format(
              ap.validDateTo
            )}`}
          </option>
        );
      }
    );

    // Need to add this to show empty accountingPeriod option when no value is selected.
    optionLists.unshift(
      <option key="default-blank-option" value="" style={{ display: 'none' }} />
    );

    return optionLists;
  }

  buildSelectedValue() {
    const selectedAccountingPeriod = find(this.props.accountingPeriodActive, {
      id: this.props.expReport.accountingPeriodId,
    });

    const accountingPeriodId = get(selectedAccountingPeriod, 'id', '');
    const recordDate = get(selectedAccountingPeriod, 'recordingDate', '');

    // reset accountingDate to empty if the selectedAccountingPeriod is no longer Active and not in approved
    if (
      !this.props.readOnly &&
      this.props.expReport.accountingDate &&
      !selectedAccountingPeriod
    ) {
      this.props.resetAccountingPeriod();
    }

    return [accountingPeriodId, recordDate];
  }

  render() {
    const [accountingPeriodId, recordDate] = this.buildSelectedValue();
    const { errors, touched, customHint, readOnly } = this.props;

    return (
      <React.Fragment>
        <MultiColumnsGrid alignments={['top', 'middle']} sizeList={[6, 6]}>
          <div className="ts-text-field-container">
            <LabelWithHint
              text={msg().Exp_Clbl_AccountingPeriod}
              hintMsg={
                (!readOnly && customHint.reportHeaderAccountingPeriod) || ''
              }
              isRequired
            />
            <select
              onChange={this.props.onChangeAccountingPeriod}
              className={`${ROOT}__select ts-select-input`}
              disabled={this.props.readOnly}
              value={accountingPeriodId}
            >
              {this.buildOptionList()}
            </select>
          </div>
          <div className="ts-text-field-container">
            <LabelWithHint
              text={msg().Exp_Clbl_RecordDate}
              hintMsg=""
              isRequired
            />
            <DateField value={this.format(recordDate)} disabled />
          </div>
        </MultiColumnsGrid>
        {errors.accountingDate && touched.accountingDate && (
          <div className="input-feedback">{msg()[errors.accountingDate]}</div>
        )}
      </React.Fragment>
    );
  }
}
