// @flow
import * as React from 'react';
import _ from 'lodash';

import TextAreaField from '../../../../../fields/TextAreaField';
import SelectField from '../../../../../fields/SelectField';
import DateField from '../../../../../fields/DateField';
import LabelWithHint from '../../../../../fields/LabelWithHint';

import msg from '../../../../../../languages';
import Button from '../../../../../buttons/Button';
import IconButton from '../../../../../../../mobile-app/components/atoms/IconButton';

import type { Report } from '../../../../../../../domain/models/exp/Report';
import {
  getExtendedItemArray,
  type ExtendedItem,
  type EISearchObj,
  type ExtendItemInfo,
} from '../../../../../../../domain/models/exp/ExtendedItem';

const ROOT = 'ts-expenses__form-report-summary__form';
type Props = {
  onChangeEditingExpReport: (string, any, any) => void,
  onClickLookupEISearch: (item: EISearchObj) => void,
  readOnly: boolean,
  expReport: Report,
  errors: any,
  touched: any,
};

export default class ReportExtendedItems extends React.Component<Props> {
  onChangeSelectField(
    e: SyntheticInputEvent<HTMLInputElement>,
    targetIndex: string
  ) {
    this.props.onChangeEditingExpReport(
      `report.extendedItemPicklist${targetIndex}Value`,
      e.target.value,
      true
    );
  }

  removeEILookup(targetIndex: string) {
    const field = `report.extendedItemLookup${targetIndex}Value`;
    this.props.onChangeEditingExpReport(field, null, true);
  }

  handleExtendedItemChange(
    e: SyntheticInputEvent<HTMLInputElement>,
    targetIndex: string
  ) {
    this.props.onChangeEditingExpReport(
      `report.extendedItemText${targetIndex}Value`,
      e.target.value,
      true
    );
  }

  handleExtendedItemDateChange(value: string, targetIndex: string) {
    this.props.onChangeEditingExpReport(
      `report.extendedItemDate${targetIndex}Value`,
      value,
      true
    );
  }

  buildOptionList(picklist: any) {
    const optionList = picklist.map((pick) => {
      return {
        value: pick.value || '',
        text: pick.label,
      };
    });

    return [{ value: '', text: msg().Exp_Lbl_PleaseSelect }, ...optionList];
  }

  renderLabel(info: ?ExtendItemInfo) {
    const text = (info && info.name) || '';
    const hintMsg = (!this.props.readOnly && info && info.description) || '';
    const isRequired = (info && info.isRequired) || false;
    return (
      <LabelWithHint text={text} hintMsg={hintMsg} isRequired={isRequired} />
    );
  }

  render() {
    const { expReport, readOnly } = this.props;
    const extendedItems = getExtendedItemArray(expReport);
    const extendedItemsWithID = extendedItems.filter((i) => i.id);

    if (!extendedItemsWithID.length) {
      return null;
    }

    return (
      <div className={`${ROOT} slds-grid slds-wrap`}>
        {extendedItemsWithID.map((extendedItem: ExtendedItem): React.Node => {
          const targetIndex = extendedItem.index;
          const type = extendedItem.info ? extendedItem.info.inputType : '';
          const extendedItemError = _.get(
            this.props.errors,
            `extendedItem${type}${targetIndex}Value`
          );
          const isExtendedItemTouched = _.get(
            this.props.touched,
            `extendedItem${type}${targetIndex}Value`
          );
          return (
            <div
              key={extendedItem.id}
              className="slds-col slds-size--3-of-12 ts-align-middle"
            >
              <div className="ts-text-field-container">
                {this.renderLabel(extendedItem.info)}
                {type === 'Text' && (
                  <TextAreaField
                    value={extendedItem.value || ''}
                    resize="none"
                    autosize
                    minRows={1}
                    maxRows={2}
                    onChange={(e) =>
                      this.handleExtendedItemChange(e, targetIndex)
                    }
                    disabled={readOnly}
                  />
                )}
                {type === 'Picklist' && (
                  <SelectField
                    className="ts-select-input"
                    onChange={(e) => this.onChangeSelectField(e, targetIndex)}
                    id={`${ROOT}-extendedItems-${extendedItem.index}`}
                    options={this.buildOptionList(
                      extendedItem.info && extendedItem.info.picklist
                    )}
                    value={extendedItem.value || ''}
                    disabled={readOnly}
                  />
                )}
                {type === 'Lookup' && (
                  <div
                    className={`${ROOT}__ei-lookup-input ts-text-field-container`}
                  >
                    {extendedItem.value ? (
                      <div className={`${ROOT}__ei-lookup-input-field`}>
                        <span>{`${extendedItem.value} - ${extendedItem.name ||
                          ''}`}</span>
                        {!readOnly && (
                          <IconButton
                            icon="close-copy"
                            size="small"
                            className={`${ROOT}__ei-lookup-input-btn--clear`}
                            onClick={() => this.removeEILookup(targetIndex)}
                            disabled={readOnly}
                          />
                        )}
                      </div>
                    ) : (
                      <Button
                        className={`${ROOT}__ei-lookup-btn--search`}
                        onClick={() =>
                          extendedItem.info &&
                          this.props.onClickLookupEISearch({
                            extendedItemLookupId: extendedItem.id,
                            extendedItemCustomId:
                              extendedItem.info.extendedItemCustomId,
                            name: extendedItem.info.name,
                            idx: targetIndex,
                            target: 'REPORT',
                            hintMsg: extendedItem.info.description,
                          })
                        }
                        disabled={readOnly}
                      >
                        {msg().Com_Lbl_Select}
                      </Button>
                    )}
                  </div>
                )}
                {type === 'Date' && (
                  <DateField
                    className={`${ROOT}__ei-date `}
                    disabled={readOnly}
                    onChange={(value) => {
                      this.handleExtendedItemDateChange(value, targetIndex);
                    }}
                    value={extendedItem.value || ''}
                  />
                )}
                {extendedItemError && isExtendedItemTouched && (
                  <div className="value">
                    <div className="input-feedback">
                      {msg()[extendedItemError]}
                    </div>
                  </div>
                )}
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}
