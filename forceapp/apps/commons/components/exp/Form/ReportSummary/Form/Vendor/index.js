// @flow
import * as React from 'react';

import msg from '../../../../../../languages';

import MultiColumnsGrid from '../../../../../MultiColumnsGrid';
import DateField from '../../../../../fields/DateField';
import Button from '../../../../../buttons/Button';
import DetailWrapper from '../../../../VendorDetail/DetailWrapper';
import IconButton from '../../../../../../../mobile-app/components/atoms/IconButton';
import LabelWithHint from '../../../../../fields/LabelWithHint';

import {
  type VendorItemList,
  VENDOR_PAYMENT_DUE_DATE_USAGE,
} from '../../../../../../../domain/models/exp/Vendor';
import { type Report } from '../../../../../../../domain/models/exp/Report';

import { type Errors } from '../..';

const ROOT = 'ts-expenses__form-report-summary__form__vendor';

type Props = {
  errors: Errors,
  expReport: Report,
  hintMsg?: string,
  isRequired: boolean,
  onChangeEditingExpReport: (string, any, any) => void,
  onClickVendorSearch: () => void,
  searchVendorDetail: (vendorId: string) => Promise<VendorItemList>,
  readOnly: boolean,
  isFinanceApproval?: boolean,
  isVendorDetailOpen: boolean,
  toggleVendorDetail: (boolean) => void,
};

export default class ReportSummaryFormVendor extends React.Component<Props> {
  componentWillMount = () => {
    const { isRequired, onChangeEditingExpReport } = this.props;
    onChangeEditingExpReport('ui.isVendorRequired', isRequired, false);
  };

  // sets a boolean for yup validation if vendor is required
  componentDidUpdate(prevProps: Props) {
    const {
      expReport,
      isRequired,
      onChangeEditingExpReport,
      toggleVendorDetail,
    } = this.props;
    const isReportChanged = prevProps.expReport.reportId !== expReport.reportId;
    const isVendorChanged = prevProps.expReport.vendorId !== expReport.vendorId;
    const isReportTypeChanged =
      prevProps.expReport.expReportTypeId !== expReport.expReportTypeId;

    if (isReportChanged || isReportTypeChanged || isVendorChanged) {
      toggleVendorDetail(false);
      onChangeEditingExpReport('ui.isVendorRequired', isRequired, false);
    }
  }

  componentWillUnmount = () => {
    // reset isVendorRequired if component is unmounting
    const { onChangeEditingExpReport } = this.props;
    onChangeEditingExpReport('ui.isVendorRequired', null, false);
  };

  onChangePaymentDate = (value: string) => {
    this.props.onChangeEditingExpReport(`report.paymentDueDate`, value, true);
  };

  handleChangeVendor = () => {
    this.props.toggleVendorDetail(false);
    this.props.onChangeEditingExpReport('report.vendorId', null, true);
    this.props.onChangeEditingExpReport('report.paymentDueDateUsage', null);
    this.props.onChangeEditingExpReport('report.paymentDueDate', null);
  };

  render() {
    const { isRequired, expReport, readOnly, errors, hintMsg } = this.props;

    const showPaymentDate =
      expReport.vendorId &&
      expReport.paymentDueDateUsage !== VENDOR_PAYMENT_DUE_DATE_USAGE.NotUsed;
    const paymentDateRequired =
      expReport.paymentDueDateUsage === VENDOR_PAYMENT_DUE_DATE_USAGE.Required;

    const vendorArea = (
      <MultiColumnsGrid className={`${ROOT}`} sizeList={[3, 3, 3, 3]}>
        <div className="ts-text-field-container">
          <LabelWithHint
            text={msg().Exp_Lbl_Vendor}
            hintMsg={(!readOnly && hintMsg) || ''}
            isRequired={isRequired}
          />
          {expReport.vendorId ? (
            <div className={`${ROOT}__vendor-input-container`}>
              <div className={`${ROOT}__vendor-input-field`}>
                <p>{expReport.vendorCode}</p>
                <span className={`${ROOT}__vendor-input-field-name`}>
                  {expReport.vendorName}
                </span>
                {!readOnly && (
                  <IconButton
                    icon="close-copy"
                    size="small"
                    className={`${ROOT}__btn-clear`}
                    onClick={this.handleChangeVendor}
                    disabled={readOnly || !expReport.vendorName}
                  />
                )}
              </div>
              <Button
                className={`${ROOT}-btn--select`}
                onClick={() => this.props.toggleVendorDetail(true)}
              >
                {msg().Exp_Btn_VendorDetail}
              </Button>
            </div>
          ) : (
            <Button
              className={`${ROOT}-btn--select`}
              onClick={this.props.onClickVendorSearch}
              disabled={readOnly}
            >
              {msg().Com_Lbl_Select}
            </Button>
          )}
          {errors.vendorId && (
            <div className="input-feedback">{msg()[errors.vendorId]}</div>
          )}
        </div>

        {showPaymentDate && (
          <div className="ts-text-field-container">
            <p className="key">
              {paymentDateRequired && <span className="is-required">*</span>}
              &nbsp;{msg().Exp_Lbl_PaymentDate}
            </p>

            <DateField
              value={expReport.paymentDueDate}
              onChange={this.onChangePaymentDate}
              disabled={readOnly}
            />

            {errors.paymentDueDate && (
              <div className="input-feedback">
                {msg()[errors.paymentDueDate]}
              </div>
            )}
          </div>
        )}
      </MultiColumnsGrid>
    );

    const vendorModal = this.props.isVendorDetailOpen ? (
      <DetailWrapper
        vendorId={expReport.vendorId}
        searchVendorDetail={this.props.searchVendorDetail}
        closePanel={() => this.props.toggleVendorDetail(false)}
        isFinanceApproval={this.props.isFinanceApproval}
      />
    ) : null;

    return (
      <>
        {vendorArea}
        {vendorModal}
      </>
    );
  }
}
