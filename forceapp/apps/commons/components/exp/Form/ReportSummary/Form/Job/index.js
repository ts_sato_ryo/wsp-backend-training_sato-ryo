// @flow
import * as React from 'react';

import msg from '../../../../../../languages';

import IconButton from '../../../../../../../mobile-app/components/atoms/IconButton';
import Button from '../../../../../buttons/Button';
import LabelWithHint from '../../../../../fields/LabelWithHint';

import { type Report } from '../../../../../../../domain/models/exp/Report';

import { type Errors } from '../..';

type Props = {
  errors: Errors,
  expReport: Report,
  hintMsg?: string,
  isRequired: boolean,
  readOnly: boolean,
  onChangeEditingExpReport: (string, any, any) => void,
  handleChangeJob: () => void,
  handleClickJobBtn: () => void,
};

const ROOT = 'ts-expenses__form-report-summary__form__job';

export default class ReportJob extends React.Component<Props> {
  componentDidMount = () => {
    const { isRequired, onChangeEditingExpReport } = this.props;
    onChangeEditingExpReport('ui.isJobRequired', isRequired, false);
  };

  componentDidUpdate(prevProps: Props) {
    const { isRequired, expReport, onChangeEditingExpReport } = this.props;
    const isReportChanged = prevProps.expReport.reportId !== expReport.reportId;
    const isCostCenterChanged =
      prevProps.expReport.costCenterCode !== expReport.costCenterCode;
    const isReportTypeChanged =
      prevProps.expReport.expReportTypeId !== expReport.expReportTypeId;
    if (isReportChanged || isCostCenterChanged || isReportTypeChanged) {
      onChangeEditingExpReport('ui.isJobRequired', isRequired, false);
    }
  }

  componentWillUnmount = () => {
    const { onChangeEditingExpReport } = this.props;
    onChangeEditingExpReport('ui.isJobRequired', null, false);
  };

  render() {
    const { isRequired, expReport, readOnly, errors, hintMsg } = this.props;
    const isNewReport =
      expReport.scheduledDate !== undefined
        ? !expReport.scheduledDate
        : !expReport.accountingDate;
    return (
      <React.Fragment>
        <div className={`${ROOT}-input ts-text-field-container`}>
          <LabelWithHint
            text={msg().Exp_Lbl_Job}
            hintMsg={(!readOnly && hintMsg) || ''}
            isRequired={isRequired}
          />
          {expReport.jobId ? (
            <div className={`${ROOT}-input-field`}>
              <span>{`${expReport.jobCode} - ${expReport.jobName}`}</span>
              {!readOnly && (
                <IconButton
                  icon="close-copy"
                  size="small"
                  className={`${ROOT}-input-btn--clear`}
                  onClick={this.props.handleChangeJob}
                  disabled={readOnly}
                />
              )}
            </div>
          ) : (
            <Button
              className={`${ROOT}-input-btn--search`}
              onClick={this.props.handleClickJobBtn}
              disabled={readOnly || isNewReport}
            >
              {msg().Com_Lbl_Select}
            </Button>
          )}
        </div>
        {errors.jobId && (
          <div className="input-feedback">{msg()[errors.jobId]}</div>
        )}
      </React.Fragment>
    );
  }
}
