// @flow
import React from 'react';

import Tooltip from '../../../../Tooltip';
import msg from '../../../../../languages';
import MultiColumnsGrid from '../../../../MultiColumnsGrid';
import TextAreaField from '../../../../fields/TextAreaField';
import LabelWithHint from '../../../../fields/LabelWithHint';

import { modes } from '../../../../../../requests-pc/modules/ui/expenses/mode';
import ReportVendor from './Vendor';
import ReportJob from './Job';
import ReportCostCenter from './CostCenter';
import ReportExtendedItems from './ExtendedItem';
import DateSelector from './DateSelector';

import { type FormikProps, type ReportSummaryFormProps } from '..';
import { type Report } from '../../../../../../domain/models/exp/Report';
import { type ExpenseReportType } from '../../../../../../domain/models/exp/expense-report-type/list';
import { type AccountingPeriod } from '../../../../../../domain/models/exp/AccountingPeriod';
import { type CustomHint } from '../../../../../../domain/models/exp/CustomHint';

import './index.scss';

const ROOT = 'ts-expenses__form-report-summary__form';

type Props = {
  // ui states
  mode: string,
  expReport: Report,
  customHint: CustomHint,
  reportTypeList: Array<ExpenseReportType>,
  inactiveReportTypeList: Array<ExpenseReportType>,
  readOnly: boolean,
  onChangeEditingExpReport: (string, any) => void,
  apActive: AccountingPeriod,
  isExpense: boolean,
  isFinanceApproval: boolean,
  toggleVendorDetail: (boolean) => void,
  isVendorDetailOpen: boolean,
  ...FormikProps,
  ...ReportSummaryFormProps,
};

class ReportSummaryForm extends React.Component<Props> {
  isMode(mode: string) {
    return this.props.mode === modes[mode];
  }

  getReportTypeOptions: any = (
    availableReportTypes: Array<ExpenseReportType>,
    includeEmptyOption: boolean
  ) => {
    const reportTypeOptions = availableReportTypes.map((reportType) => ({
      text: reportType.name,
      value: reportType.id,
    }));
    if (includeEmptyOption) {
      reportTypeOptions.unshift({
        text: '',
        value: null,
        style: { display: 'none' },
      });
    }
    return reportTypeOptions;
  };

  getReportTypeInfo = (isReadOnly: boolean) => {
    const isReportFromPreRequest =
      !!this.props.expReport.preRequestId && !isReadOnly;
    const isFinanceApproval = this.props.isFinanceApproval && !isReadOnly;
    if (isFinanceApproval) {
      return msg().Exp_Msg_ReportTypeInfoForFinanceApproval;
    } else if (isReportFromPreRequest) {
      return msg().Exp_Msg_ReportTypeInfoForExpense;
    }
    return '';
  };

  render() {
    const {
      errors,
      touched,
      readOnly,
      expReport,
      remarksLabel,
      reportTypeList,
      inactiveReportTypeList,
      isExpenseRequest,
      renderScheduledDate,
      customHint,
    } = this.props;

    const isReadOnly =
      readOnly || !(this.isMode('REPORT_EDIT') || this.isMode('REPORT_SELECT'));

    // If there is (reportTypeId and reportTypeList) OR (Editable & ReportTypeList) => For ReadOnly: if no expReportTypeID, then don't use Report Type
    const useReportType = Boolean(
      (!!expReport.expReportTypeId && reportTypeList) ||
        (reportTypeList && !isReadOnly)
    );

    let availableReportTypes = [];
    let useEmptyReportTypeInitially = false;
    const allReportTypes = [...reportTypeList, ...inactiveReportTypeList];
    const currentReportType = allReportTypes.find(
      (reportType) => reportType.id === expReport.expReportTypeId
    );

    const vendorRequirement =
      currentReportType && currentReportType.isVendorRequired;
    const isVendorVisible = vendorRequirement && vendorRequirement !== 'UNUSED';
    const isVendorRequired = vendorRequirement === 'REQUIRED';

    const isCostCenterVisible =
      currentReportType && currentReportType.isCostCenterRequired !== 'UNUSED';
    const isCostCenterRequired =
      (currentReportType &&
        currentReportType.isCostCenterRequired === 'REQUIRED') ||
      false;

    const isJobVisible =
      currentReportType && currentReportType.isJobRequired !== 'UNUSED';
    const isJobRequired =
      (currentReportType && currentReportType.isJobRequired === 'REQUIRED') ||
      false;

    if (useReportType) {
      availableReportTypes = reportTypeList.filter((reportType) => {
        return isReadOnly
          ? reportType.id === expReport.expReportTypeId
          : reportType.active;
      });

      useEmptyReportTypeInitially = !expReport.expReportTypeId;
      if (useEmptyReportTypeInitially && useReportType) {
        expReport.expReportTypeId = undefined;
      }
    }

    const lastColumnSize = renderScheduledDate ? 3 : 6;
    const firstRowSizeList = useReportType
      ? [lastColumnSize, 3, 3]
      : [lastColumnSize, 3];

    const isDisabledReportType =
      isReadOnly ||
      !!this.props.expReport.preRequestId ||
      this.props.isFinanceApproval;

    const reportType = isDisabledReportType ? (
      <TextAreaField
        resize="none"
        autosize
        minRows={1}
        maxRows={2}
        isRequired
        value={expReport.expReportTypeName || ''}
        disabled={isDisabledReportType}
      />
    ) : (
      <React.Fragment>
        <select
          onChange={this.props.handleChangeExpenseReportType}
          className="slds-select ts-select ts-select-input"
          disabled={isDisabledReportType}
          value={expReport.expReportTypeId || ''}
          required
        >
          {this.getReportTypeOptions(
            availableReportTypes,
            useEmptyReportTypeInitially
          ).map((item, index) => (
            <option
              key={useEmptyReportTypeInitially ? index - 1 : index}
              value={item.value}
              style={item.style}
            >
              {item.text}
            </option>
          ))}
        </select>
        {errors.expReportTypeId && touched.expReportTypeId && (
          <div className="input-feedback">{msg()[errors.expReportTypeId]}</div>
        )}
      </React.Fragment>
    );

    const reportTypeInfo = this.getReportTypeInfo(isReadOnly);

    return (
      <div className={`${ROOT}`}>
        <MultiColumnsGrid sizeList={firstRowSizeList}>
          <DateSelector
            errors={errors}
            readOnly={isReadOnly}
            touched={touched}
            expReport={expReport}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            isExpense={this.props.isExpense}
            isFinanceApproval={this.props.isFinanceApproval}
            apActive={this.props.apActive}
            customHint={customHint}
          />

          {useReportType && (
            <div
              className={`${ROOT}__report-type-input ts-text-field-container`}
            >
              <LabelWithHint
                text={msg().Exp_Clbl_ReportType}
                hintMsg={
                  (!isDisabledReportType &&
                    customHint.reportHeaderReportType) ||
                  ''
                }
                isRequired
              />
              {(reportTypeInfo && (
                <Tooltip align="top" content={reportTypeInfo}>
                  <div>{reportType}</div>
                </Tooltip>
              )) ||
                reportType}
            </div>
          )}

          {isExpenseRequest && (
            <div className={`${ROOT}__purpose`}>
              <LabelWithHint
                text={remarksLabel}
                hintMsg={(!isReadOnly && customHint.reportHeaderPurpose) || ''}
                isRequired
              />
              <TextAreaField
                resize="none"
                autosize
                minRows={1}
                maxRows={2}
                isRequired
                onChange={this.props.handleChangeRemarks}
                value={expReport.purpose || ''}
                disabled={isReadOnly}
              />
              {errors.purpose && touched.purpose && (
                <div className="input-feedback">{msg()[errors.purpose]}</div>
              )}
            </div>
          )}
        </MultiColumnsGrid>

        {isVendorVisible && (
          <ReportVendor
            errors={errors}
            expReport={expReport}
            hintMsg={customHint.reportHeaderVendor}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            onClickVendorSearch={this.props.onClickVendorSearch}
            searchVendorDetail={this.props.searchVendorDetail}
            isRequired={isVendorRequired}
            readOnly={isReadOnly}
            isFinanceApproval={this.props.isFinanceApproval}
            toggleVendorDetail={this.props.toggleVendorDetail}
            isVendorDetailOpen={this.props.isVendorDetailOpen}
          />
        )}

        <MultiColumnsGrid sizeList={[3, 3, 3]}>
          {isJobVisible && (
            <ReportJob
              errors={errors}
              expReport={expReport}
              hintMsg={customHint.reportHeaderJob}
              isRequired={isJobRequired}
              readOnly={isReadOnly}
              handleChangeJob={this.props.handleChangeJob}
              handleClickJobBtn={this.props.handleClickJobBtn}
              onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            />
          )}

          {isCostCenterVisible && (
            <ReportCostCenter
              errors={errors}
              expReport={expReport}
              hintMsg={customHint.reportHeaderCostCenter}
              isRequired={isCostCenterRequired}
              readOnly={isReadOnly}
              handleChangeCostCenter={this.props.handleChangeCostCenter}
              handleClickCostCenterBtn={this.props.handleClickCostCenterBtn}
              onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            />
          )}

          {/* REMARK */}
          {!isExpenseRequest && (
            <div className={`${ROOT}__remarks`}>
              <LabelWithHint
                text={remarksLabel}
                hintMsg={(!isReadOnly && customHint.reportHeaderRemarks) || ''}
              />
              <TextAreaField
                resize="none"
                autosize
                minRows={1}
                maxRows={2}
                isRequired={false}
                onChange={this.props.handleChangeRemarks}
                value={expReport.remarks || ''}
                disabled={isReadOnly}
              />
            </div>
          )}
        </MultiColumnsGrid>

        {useReportType && (
          <ReportExtendedItems
            expReport={expReport}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            onClickLookupEISearch={this.props.onClickLookupEISearch}
            readOnly={isReadOnly}
            errors={errors}
            touched={touched}
            key={expReport.expReportTypeId}
          />
        )}
      </div>
    );
  }
}

export default ReportSummaryForm;
