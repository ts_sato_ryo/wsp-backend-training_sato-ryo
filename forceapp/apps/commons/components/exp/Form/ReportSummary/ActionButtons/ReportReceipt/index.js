// @flow
import React, { useState } from 'react';
import { CSSTransition } from 'react-transition-group';

import msg from '../../../../../../languages';
import { type Report } from '../../../../../../../domain/models/exp/Report';
import LightboxModal from '../../../../../LightboxModal';
import FileUtil from '../../../../../../utils/FileUtil';

import IconAttach from '../../../../../../images/icons/attach.svg';

import './index.scss';

const ROOT = 'ts-expenses__form-report-summary__actions';

export type ReportReceiptProps = {|
  openReceiptLibraryDialog?: () => void,
  updateReport?: (Object: Object) => void,
  getFilePreview: (attachedFileVerId: string) => Promise<any>,
|};

type Props = {
  readOnly?: boolean,
  expReport: Report,
  isFinanceApproval?: boolean,
  ...ReportReceiptProps,
};

const pinIcon = <IconAttach className={`${ROOT}__attach-icon`} />;

const ReportReceipt = (props: Props) => {
  const [isFileMenuOpen, setIsFileMenuOpen] = useState(false);
  const [showImgPreview, setShowImgPreview] = useState(false);
  const [fileUrl, setFileUrl] = useState(null);

  const {
    expReport,
    openReceiptLibraryDialog,
    isFinanceApproval,
    updateReport,
    readOnly,
  } = props;

  const fileVerId = expReport.attachedFileVerId;
  const hideAllBtn =
    !expReport.useFileAttachment ||
    (isFinanceApproval && !fileVerId) ||
    (readOnly && !fileVerId);
  const hideDelete = readOnly || isFinanceApproval;

  if (hideAllBtn) {
    return null;
  }

  if (!fileVerId) {
    return (
      <button
        type="button"
        className={`slds-button slds-button--icon ${ROOT}__attach no-attached`}
        onClick={openReceiptLibraryDialog}
      >
        {pinIcon}
      </button>
    );
  } else {
    const handlePreview = () => {
      props.getFilePreview(fileVerId).then((res) => {
        const fileBody = res.payload.fileBody;
        const dataType = FileUtil.getMIMEType(res.payload.fileType);
        const url = `data:${dataType};base64,${fileBody}`;
        const fileName = FileUtil.getOriginalFileNameWithoutPrefix(
          res.payload.title
        );
        setFileUrl(url);

        const isPDF = dataType === 'application/pdf';
        if (isPDF) {
          FileUtil.downloadFile(
            url,
            expReport.attachedFileId,
            `${fileName}.pdf`,
            true
          );
        } else {
          setShowImgPreview(true);
        }
      });
    };

    const resetFileValues = () => {
      if (updateReport) {
        const updateObj = {
          attachedFileVerId: null,
          attachedFileId: null,
          attachedFileData: null,
        };
        updateReport(updateObj);
      }
    };

    return (
      <React.Fragment>
        <button
          type="button"
          className={`slds-button slds-button--icon ${ROOT}__attach attached`}
          onClick={() => setIsFileMenuOpen((prevIsOpen) => !prevIsOpen)}
          onBlur={() => {
            // use time out otherwise will block onClick dropdown item
            setTimeout(() => setIsFileMenuOpen(false), 300);
          }}
        >
          {pinIcon}
          <div className={`${ROOT}__attach-count`} />
        </button>

        {isFileMenuOpen && (
          <div className={`${ROOT}__submenu align-left`}>
            <button
              type="button"
              className={`${ROOT}__submenu-item-button ${ROOT}__preview`}
              onClick={handlePreview}
            >
              {msg().Exp_Btn_Preview}
            </button>
            {!hideDelete && (
              <button
                type="button"
                className={`${ROOT}__submenu-item-button ${ROOT}__delete`}
                onClick={resetFileValues}
              >
                {msg().Com_Btn_Delete}
              </button>
            )}
          </div>
        )}

        {showImgPreview && (
          <CSSTransition
            in={showImgPreview}
            timeout={500}
            classNames={`${ROOT}__modal-container`}
            unmountOnExit
          >
            <LightboxModal
              src={fileUrl}
              toggleLightbox={() => setShowImgPreview(false)}
            />
          </CSSTransition>
        )}
      </React.Fragment>
    );
  }
};

export default ReportReceipt;
