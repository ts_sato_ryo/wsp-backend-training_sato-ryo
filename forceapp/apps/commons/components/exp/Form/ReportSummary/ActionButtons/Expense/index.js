// @flow
import React from 'react';
import classNames from 'classnames';

import msg from '../../../../../../languages';
import Button from '../../../../../buttons/Button';
import ReportReceipt, { type ReportReceiptProps } from '../ReportReceipt';
import IconButton from '../../../../../buttons/IconButton';
import iconTitleToggle from '../../../../images/iconTitleToggle.png';
import { type Report } from '../../../../../../../domain/models/exp/Report';
import { status } from '../../../../../../../domain/modules/exp/report';
import { modes } from '../../../../../../../requests-pc/modules/ui/expenses/mode';
import './index.scss';

const ROOT = 'ts-expenses__form-report-summary__actions';

type Props = {
  expReport: Report,
  mode: string,
  isExpenseRequest?: boolean,
  isFinanceApproval?: boolean,
  onClickApprovalHistoryButton: () => void,
  readOnly: boolean,
  isExpenseRequest?: ?boolean,
  isFinanceApproval?: ?boolean,
  onClickBackButton: () => void,
  onClickCancelRequestButton: () => void,
  onClickDeleteButton: () => void,
  onClickEditHistoryButton: () => void,
  onClickPrintPageButton: () => void,
  onClickSaveButton: () => void,
  onClickSubmitButton: () => void,
  onClickCloneButton: () => void,
  readOnly: boolean,
  ...ReportReceiptProps,
};

type State = {
  isMenuOpen: boolean,
};

export default class ActionButtonsExpense extends React.Component<
  Props,
  State
> {
  state = {
    isMenuOpen: false,
  };

  isMode(mode: string) {
    return this.props.mode === modes[mode];
  }

  toggleMenu = () => {
    this.setState((prevState) => ({
      isMenuOpen: !prevState.isMenuOpen,
    }));
  };

  isSubmittableStatus = (currentStatus: ?string) => {
    switch (currentStatus) {
      case status.NOT_REQUESTED:
      case status.REJECTED:
      case status.REMOVED:
      case status.CANCELED:
        return true;
      default:
        return false;
    }
  };

  render() {
    const { expReport } = this.props;
    const reportStatus = this.props.expReport.status;
    const isCancellable =
      reportStatus !== status.APPROVED &&
      reportStatus !== status.REJECTED &&
      reportStatus !== status.CLAIMED;
    const hasReportId = expReport.reportId;
    const hasPreRequestId = expReport.preRequestId;
    const isNewPreRequest = hasPreRequestId && !hasReportId;
    const isDeleteDisabled =
      !hasReportId || !this.isSubmittableStatus(expReport.status);
    let isSaveDisabled =
      this.isMode('INITIALIZE') || this.isMode('REPORT_SELECT');
    const isSubmitDisabled =
      this.isMode('INITIALIZE') ||
      this.isMode('REPORT_EDIT') ||
      !this.isSubmittableStatus(expReport.status);
    const isCloneDisabled =
      this.isMode('REPORT_EDIT') ||
      (this.isMode('REPORT_SELECT') && !expReport.reportId);

    const menuClass = classNames(`${ROOT}__toggle`, {
      active: this.state.isMenuOpen,
      'is-pre-request': isNewPreRequest,
    });
    const preRequestClass = classNames({ 'is-pre-request': isNewPreRequest });

    if (isNewPreRequest) {
      // make sure the start claiming button is always enabled.
      isSaveDisabled = false;
    }

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__main`}>
          <ReportReceipt
            expReport={expReport}
            openReceiptLibraryDialog={this.props.openReceiptLibraryDialog}
            updateReport={this.props.updateReport}
            getFilePreview={this.props.getFilePreview}
            readOnly={this.props.readOnly}
          ></ReportReceipt>

          <Button
            className={`${ROOT}__close`}
            onClick={this.props.onClickBackButton}
          >
            {msg().Com_Btn_Close}
          </Button>
          <Button
            className={`${ROOT}__save ${preRequestClass}`}
            onClick={this.props.onClickSaveButton}
            disabled={isSaveDisabled}
          >
            {isNewPreRequest ? msg().Exp_Lbl_StartClaim : msg().Com_Btn_Save}
          </Button>
          <Button
            className={`${ROOT}__submit ${preRequestClass}`}
            onClick={this.props.onClickSubmitButton}
            disabled={isSubmitDisabled}
          >
            {msg().Exp_Btn_Submit}
          </Button>
          <IconButton
            src={iconTitleToggle}
            className={menuClass}
            onClick={this.toggleMenu}
          />
        </div>
        {this.state.isMenuOpen && (
          <div className={`${ROOT}__submenu`}>
            <button
              className={classNames(
                `${ROOT}__submenu-item-button`,
                `${ROOT}__cancel`
              )}
              onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                event.preventDefault();
                event.stopPropagation();
                this.props.onClickCancelRequestButton();
              }}
              disabled={!(this.props.readOnly && isCancellable)}
            >
              {msg().Exp_Lbl_Recall}
            </button>
            <button
              className={classNames(
                `${ROOT}__submenu-item-button`,
                `${ROOT}__approval-history`
              )}
              onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                event.preventDefault();
                event.stopPropagation();
                this.props.onClickApprovalHistoryButton();
              }}
              disabled={!this.props.expReport.requestId}
            >
              {msg().Com_Btn_ApprovalHistory}
            </button>
            {!this.props.isExpenseRequest && (
              <button
                className={classNames(
                  `${ROOT}__submenu-item-button`,
                  `${ROOT}__edit-history`
                )}
                onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                  event.preventDefault();
                  event.stopPropagation();
                  this.props.onClickEditHistoryButton();
                }}
                disabled={!this.props.expReport.requestId}
              >
                {msg().Exp_Lbl_EditHistory}
              </button>
            )}

            <button
              className={classNames(
                `${ROOT}__submenu-item-button`,
                `${ROOT}__clone`
              )}
              onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                event.preventDefault();
                event.stopPropagation();
                this.props.onClickCloneButton();
              }}
              disabled={isCloneDisabled}
            >
              {msg().Exp_Lbl_Clone}
            </button>

            <button
              className={classNames(
                `${ROOT}__submenu-item-button`,
                `${ROOT}__delete`
              )}
              onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                event.preventDefault();
                event.stopPropagation();
                this.props.onClickDeleteButton();
              }}
              disabled={isDeleteDisabled}
            >
              {msg().Com_Btn_Delete}
            </button>
            {!this.props.isExpenseRequest && !this.props.isFinanceApproval && (
              <button
                className={classNames(
                  `${ROOT}__submenu-item-button`,
                  `${ROOT}__print`
                )}
                onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                  event.preventDefault();
                  event.stopPropagation();
                  this.props.onClickPrintPageButton();
                }}
                disabled={!hasReportId}
              >
                {msg().Com_Btn_PrintPage}
              </button>
            )}
          </div>
        )}
      </div>
    );
  }
}
