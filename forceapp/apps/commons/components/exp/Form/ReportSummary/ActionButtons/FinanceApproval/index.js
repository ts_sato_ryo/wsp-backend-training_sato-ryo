// @flow
import React from 'react';
import classNames from 'classnames';

import msg from '../../../../../../languages';
import Button from '../../../../../buttons/Button';
import IconButton from '../../../../../buttons/IconButton';
import { type Report } from '../../../../../../../domain/models/exp/Report';
import iconTitleToggle from '../../../../images/iconTitleToggle.png';
import ReportReceipt from '../ReportReceipt';

import './index.scss';

const ROOT = 'ts-expenses__form-report-summary__actions';

type Props = {
  disabled?: boolean,
  isEditMode: boolean,
  expReport: Report,
  getFilePreview: (attachedFileVerId: string) => Promise<any>,
  onClickApprovalHistoryButton: () => void,
  onClickApproveButton: (string) => void,
  onClickBackButton: () => void,
  onClickEditButton: () => void,
  onClickEditHistoryButton: () => void,
  onClickRejectButton: (string) => void,
  onClickSaveButton: () => void,
};

type State = {
  isMenuOpen: boolean,
};

export default class ActionButtonsFA extends React.Component<Props, State> {
  state = {
    isMenuOpen: false,
  };

  toggleMenu = () => {
    this.setState((prevState) => ({
      isMenuOpen: !prevState.isMenuOpen,
    }));
  };

  renderApproveReject = () => {
    const menuClass = classNames(`${ROOT}__toggle`, {
      active: this.state.isMenuOpen,
    });

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__main`}>
          <ReportReceipt
            expReport={this.props.expReport}
            getFilePreview={this.props.getFilePreview}
            isFinanceApproval
          ></ReportReceipt>
          <Button
            disabled={this.props.disabled}
            className={`${ROOT}__reject`}
            onClick={this.props.onClickRejectButton}
          >
            {msg().Appr_Btn_Reject}
          </Button>
          <Button
            disabled={this.props.disabled}
            className={`${ROOT}__edit`}
            onClick={this.props.onClickEditButton}
          >
            {msg().Appr_Btn_Edit}
          </Button>
          <Button
            disabled={this.props.disabled}
            className={`${ROOT}__approve`}
            onClick={this.props.onClickApproveButton}
          >
            {msg().Appr_Btn_Approve}
          </Button>
          <IconButton
            src={iconTitleToggle}
            className={menuClass}
            onClick={this.toggleMenu}
          />
        </div>
        {this.state.isMenuOpen && (
          <div className={`${ROOT}__submenu`}>
            <button
              className={classNames(
                `${ROOT}__submenu-item-button`,
                `${ROOT}__approval-history`
              )}
              onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                event.preventDefault();
                event.stopPropagation();
                this.props.onClickApprovalHistoryButton();
              }}
            >
              {msg().Com_Btn_ApprovalHistory}
            </button>
            <button
              className={classNames(
                `${ROOT}__submenu-item-button`,
                `${ROOT}__modification-history`
              )}
              onClick={(event: SyntheticEvent<HTMLButtonElement>) => {
                event.preventDefault();
                event.stopPropagation();
                this.props.onClickEditHistoryButton();
              }}
            >
              {msg().Exp_Lbl_EditHistory}
            </button>
          </div>
        )}
      </div>
    );
  };

  renderCancelSave = () => {
    return (
      <div className={`${ROOT}__main`}>
        <Button
          className={`${ROOT}__close`}
          onClick={this.props.onClickBackButton}
        >
          {msg().Com_Btn_Cancel}
        </Button>
        <Button
          className={`${ROOT}__save is-fa`}
          onClick={this.props.onClickSaveButton}
        >
          {msg().Com_Btn_Save}
        </Button>
      </div>
    );
  };

  render() {
    return (
      <div className={ROOT}>
        {this.props.isEditMode
          ? this.renderCancelSave()
          : this.renderApproveReject()}
      </div>
    );
  }
}
