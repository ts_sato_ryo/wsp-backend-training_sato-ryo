// @flow
import React from 'react';

import { type Report } from '../../../../../../domain/models/exp/Report';
import { status } from '../../../../../../domain/modules/exp/report';
import { modes } from '../../../../../../expenses-pc/modules/ui/expenses/mode';
import ActionButtonsFA from './FinanceApproval';
import ActionButtonsExpense from './Expense';

import { type ReportReceiptProps } from './ReportReceipt';
import { type FormContainerProps } from '../..';

type Props = {
  expReport: Report,
  mode: string,
  onClickEditButton: (string) => void,
  onClickPrintPageButton: () => void,
  onClickSaveButton: () => void,
  onClickSubmitButton: () => void,
  onClickCloneButton: () => void,
  openTitle: boolean,
  readOnly: boolean,
  ...FormContainerProps,
  ...ReportReceiptProps,
};

export default class ReportSummaryActionButtons extends React.Component<Props> {
  render() {
    const { expReport, isFinanceApproval } = this.props;
    const isEditMode = this.props.mode === modes.REPORT_EDIT;
    const isDisabled =
      expReport.status === status.ACCOUNTING_AUTHORIZED ||
      expReport.status === status.ACCOUNTING_REJECTED ||
      expReport.status === status.JOURNAL_CREATED ||
      expReport.status === status.FULLY_PAID;
    return !isFinanceApproval ? (
      <ActionButtonsExpense {...this.props} />
    ) : (
      <ActionButtonsFA
        isEditMode={isEditMode}
        disabled={isDisabled}
        expReport={expReport}
        getFilePreview={this.props.getFilePreview}
        onClickApprovalHistoryButton={this.props.onClickApprovalHistoryButton}
        onClickEditHistoryButton={this.props.onClickEditHistoryButton}
        onClickApproveButton={this.props.onClickApproveButton}
        onClickEditButton={this.props.reportEdit}
        onClickRejectButton={this.props.onClickRejectButton}
        onClickBackButton={this.props.onClickBackButton}
        onClickSaveButton={this.props.onClickSaveButton}
        onClickPrintPageButton={this.props.onClickPrintPageButton}
      />
    );
  }
}
