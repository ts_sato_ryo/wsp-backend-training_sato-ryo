/* @flow */
import React from 'react';
import _ from 'lodash';

import ReportSummaryForm from './Form';
import ReportStatus from '../ReportStatus';
import PreRequestSummaryForm from '../PreRequestSummary';
import ActionButtons from './ActionButtons';

import { type CustomHint } from '../../../../../domain/models/exp/CustomHint';
import { type Record } from '../../../../../domain/models/exp/Record';
import {
  type Report,
  headerStatusLabels,
} from '../../../../../domain/models/exp/Report';
import type { VendorItemList } from '../../../../../domain/models/exp/Vendor';
import { type EISearchObj } from '../../../../../domain/models/exp/ExtendedItem';
import type { ExpenseReportType } from '../../../../../domain/models/exp/expense-report-type/list';
import { modes } from '../../../../../requests-pc/modules/ui/expenses/mode';
import { type AccountingPeriod } from '../../../../../domain/models/exp/AccountingPeriod';
import { type ReportReceiptProps } from './ActionButtons/ReportReceipt';

import msg from '../../../../languages';
import FormatUtil from '../../../../utils/FormatUtil';

import TextAreaField from '../../../fields/TextAreaField';
import IconButton from '../../../../../mobile-app/components/atoms/IconButton';

import { type CommonProps, type OverlapProps } from '../../index';
import { type FormContainerProps } from '..';

import './index.scss';

const ROOT = 'ts-expenses__form-report-summary';

export type Errors = {|
  expReportTypeId: string,
  paymentDueDate?: string,
  purpose?: string,
  records?: Array<Record>,
  subject: string,
  vendorId?: string,
  jobId?: string,
  costCenterName?: string,
|};

type Touched = {|
  accountingDate: string,
  expReportTypeId: string,
  purpose?: string,
  records?: Array<Record>,
  subject: string,
|};

export type FormikProps = {|
  touched: Touched,
  errors: Errors,
|};

export type ReportSummaryFormProps = {|
  baseCurrencyDecimal: number,
  baseCurrencySymbol: string,
  handleChangeCostCenter: () => void,
  handleChangeExpenseReportType: (e: any) => void,
  handleChangeJob: () => void,
  handleChangeRemarks: (e: any) => void,
  handleChangeSubject: (e: any) => void,
  handleClickCostCenterBtn: () => void,
  handleClickJobBtn: () => void,
  isExpense: boolean,
  isFinanceApproval: boolean,
  isExpenseRequest?: boolean,
  onClickLookupEISearch: (item: EISearchObj) => void,
  onClickVendorSearch: () => void,
  remarksLabel: string,
  renderScheduledDate: (disabled: boolean, errors: any, touched: any) => void,
  updateReport: (updateReportObject: Report) => void,
  searchVendorDetail: (vendorId: string) => Promise<VendorItemList>,
  apActive: AccountingPeriod,
|};

type ReportSummaryContainerProps = {|
  onClickEditButton: () => void,
  onClickJobButton: (?string, string) => void,
  onClickPrintPageButton: () => void,
  onClickSaveButton: () => void,
  onClickSubmitButton: () => void,
  onClickCloneButton: () => void,
  onClickTitleToggleButton?: () => void,
  openTitle: boolean,
  openTitleAction: () => any,
  reportTypeList: Array<ExpenseReportType>,
  customHint: CustomHint,
  inactiveReportTypeList: Array<ExpenseReportType>,
  ...ReportSummaryFormProps,
|};

type Props = {
  expReport: Report,
  onChangeEditingExpReport: (string, any, any) => void,
  setLinkedExpType: (?string) => void,
  readOnly: boolean,
  ...CommonProps,
  ...FormikProps,
  ...OverlapProps,
  ...FormContainerProps,
  ...ReportSummaryContainerProps,
  ...ReportReceiptProps,
};

type State = {
  needOpen: boolean,
  isVendorDetailOpen: boolean,
};

export default class ExpensesFormReportSummary extends React.Component<
  Props,
  State
> {
  state = {
    needOpen: false,
    isVendorDetailOpen: false,
  };

  // set availabe expense type based on selected report's report type
  componentDidUpdate(prevProps: Props) {
    const isReportChanged =
      prevProps.expReport.reportId !== this.props.expReport.reportId;
    const isReportTypeIdChanged =
      prevProps.expReport.expReportTypeId !==
      this.props.expReport.expReportTypeId;
    if (isReportChanged || isReportTypeIdChanged) {
      this.props.setLinkedExpType(this.props.expReport.expReportTypeId);
    }
  }

  toggleVendorDetail = (toOpen: boolean) => {
    this.setState({ isVendorDetailOpen: toOpen });
  };

  render() {
    const { readOnly, mode } = this.props;
    const isTitleOpen = this.props.openTitle ? `${ROOT}__toggle--open` : '';
    const isReadOnly =
      readOnly || !(mode === modes.REPORT_EDIT || mode === modes.REPORT_SELECT);
    const formattedTotalAmount = FormatUtil.formatNumber(
      this.props.expReport.totalAmount,
      this.props.baseCurrencyDecimal
    );

    // よくない。
    if (this.state.needOpen) {
      const errorsClone = _.cloneDeep(this.props.errors);
      delete errorsClone.records;
      const hasHeaderError = Object.keys(errorsClone).length > 0;

      if (hasHeaderError) {
        this.props.openTitleAction();
        this.setState({ needOpen: false });
      }
    }
    const amountClassName =
      this.props.expReport.totalAmount > 0
        ? `${ROOT}__header-amount-active`
        : `${ROOT}__header-amount`;

    return (
      <div className={`${ROOT}`}>
        {headerStatusLabels.indexOf(this.props.expReport.status) > -1 && (
          <ReportStatus
            isExpenseRequest={this.props.isExpenseRequest}
            isFinanceApproval={this.props.isFinanceApproval}
            departmentCode={this.props.expReport.departmentCode}
            departmentName={this.props.expReport.departmentName}
            employeeName={this.props.expReport.employeeName}
            reportStatus={this.props.expReport.status}
            reportNo={this.props.expReport.reportNo}
          />
        )}
        {this.props.expReport.preRequest && (
          <PreRequestSummaryForm
            preRequest={this.props.expReport.preRequest}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
          />
        )}
        <section className={`${ROOT}__header`}>
          <div className={`${ROOT}__toggle`}>
            <IconButton
              icon="chevrondown"
              size="medium"
              className={isTitleOpen}
              onClick={this.props.onClickTitleToggleButton}
            />

            {/* saved report -> title of the report, new report -> Exp_Lbl_NewReportCreate */}
            <TextAreaField
              className={`${ROOT}__header-input`}
              placeholder={msg().Exp_Lbl_ReportTitle}
              value={this.props.expReport.subject}
              resize="none"
              autosize
              minRows={1}
              maxRows={2}
              disabled={isReadOnly}
              onChange={this.props.handleChangeSubject}
            />

            <span className={amountClassName}>
              &nbsp;
              {`${this.props.baseCurrencySymbol} ${formattedTotalAmount}`}
            </span>

            {this.props.errors.subject && this.props.touched.subject && (
              <div className="input-feedback">
                {msg()[this.props.errors.subject]}
              </div>
            )}
          </div>

          <ActionButtons
            key={this.props.expReport.reportId}
            mode={this.props.mode}
            expReport={this.props.expReport}
            onClickBackButton={() => {
              this.toggleVendorDetail(false);
              this.props.onClickBackButton();
            }}
            onClickSaveButton={() => {
              this.setState({ needOpen: true });
              this.props.onClickSaveButton();
            }}
            onClickDeleteButton={this.props.onClickDeleteButton}
            onClickSubmitButton={() => {
              this.setState({ needOpen: true });
              this.props.onClickSubmitButton();
            }}
            onClickApprovalHistoryButton={
              this.props.onClickApprovalHistoryButton
            }
            onClickCloneButton={this.props.onClickCloneButton}
            onClickEditHistoryButton={this.props.onClickEditHistoryButton}
            onClickCancelRequestButton={this.props.onClickCancelRequestButton}
            onClickRejectButton={this.props.onClickRejectButton}
            onClickEditButton={this.props.onClickEditButton}
            onClickApproveButton={this.props.onClickApproveButton}
            readOnly={this.props.readOnly}
            openTitle={this.props.openTitle}
            isExpenseRequest={this.props.isExpenseRequest}
            isFinanceApproval={this.props.isFinanceApproval}
            errors={this.props.errors}
            reportEdit={this.props.reportEdit}
            onClickPrintPageButton={this.props.onClickPrintPageButton}
            openReceiptLibraryDialog={this.props.openReceiptLibraryDialog}
            updateReport={this.props.updateReport}
            getFilePreview={this.props.getFilePreview}
          />
        </section>
        {this.props.openTitle && (
          <ReportSummaryForm
            isExpense={this.props.isExpense}
            isFinanceApproval={this.props.isFinanceApproval}
            isExpenseRequest={this.props.isExpenseRequest}
            mode={this.props.mode}
            customHint={this.props.customHint}
            reportTypeList={this.props.reportTypeList}
            inactiveReportTypeList={this.props.inactiveReportTypeList}
            expReport={this.props.expReport}
            errors={this.props.errors}
            touched={this.props.touched}
            readOnly={this.props.readOnly}
            baseCurrencySymbol={this.props.baseCurrencySymbol}
            baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            apActive={this.props.apActive}
            onChangeEditingExpReport={this.props.onChangeEditingExpReport}
            handleChangeExpenseReportType={
              this.props.handleChangeExpenseReportType
            }
            handleChangeCostCenter={this.props.handleChangeCostCenter}
            handleChangeJob={this.props.handleChangeJob}
            handleChangeSubject={this.props.handleChangeSubject}
            handleChangeRemarks={this.props.handleChangeRemarks}
            renderScheduledDate={this.props.renderScheduledDate}
            handleClickCostCenterBtn={this.props.handleClickCostCenterBtn}
            handleClickJobBtn={this.props.handleClickJobBtn}
            updateReport={this.props.updateReport}
            remarksLabel={this.props.remarksLabel}
            onClickLookupEISearch={this.props.onClickLookupEISearch}
            onClickVendorSearch={this.props.onClickVendorSearch}
            searchVendorDetail={this.props.searchVendorDetail}
            isVendorDetailOpen={this.state.isVendorDetailOpen}
            toggleVendorDetail={this.toggleVendorDetail}
          />
        )}
      </div>
    );
  }
}
