// @flow
import React from 'react';

import msg from '../../../../languages';

import { getStatusText } from '../../../../../domain/modules/exp/report';
import { type ApprovalStatus } from '../../../../../domain/models/approval/request/Status';

import './index.scss';

const ROOT = 'ts-expenses__form-report-status';

type Props = {
  isExpenseRequest?: boolean,
  isFinanceApproval?: boolean,
  departmentCode?: string,
  departmentName?: string,
  employeeName?: string,
  reportStatus?: $Values<ApprovalStatus>,
  reportNo?: string,
};

export default class ReportStatus extends React.Component<Props> {
  getLabel = () =>
    this.props.isExpenseRequest
      ? msg().Exp_Lbl_RequestNo
      : msg().Exp_Lbl_ReportNo;

  render() {
    const {
      reportStatus,
      reportNo,
      isFinanceApproval,
      departmentCode,
      departmentName,
      employeeName,
    } = this.props;
    const status = (reportStatus && reportStatus.toLowerCase()) || 'status';
    return (
      <div className={`${ROOT}__info`}>
        {reportStatus !== undefined && (
          <div className={`${ROOT}__${status.replace(' ', '')} ${ROOT}__label`}>
            {getStatusText(reportStatus)}
          </div>
        )}
        {isFinanceApproval && (
          <div className={`${ROOT}__applicant`}>
            {departmentCode
              ? `${departmentCode} - ${departmentName || ''} : `
              : ''}
            {employeeName}
          </div>
        )}
        {reportNo && reportStatus !== 'NotRequested' && (
          <div className={`${ROOT}__report-no`}>
            {this.getLabel()} : {reportNo}
          </div>
        )}
      </div>
    );
  }
}
