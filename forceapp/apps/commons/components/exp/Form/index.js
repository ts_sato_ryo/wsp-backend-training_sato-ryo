// @flow
import React from 'react';
import { cloneDeep, get, merge, set, isEmpty } from 'lodash';

import {
  type Report,
  calcTotalAmount,
} from '../../../../domain/models/exp/Report';

import { type RecordItem as Record } from '../../../../domain/models/exp/Record';
import { type AccountingPeriod } from '../../../../domain/models/exp/AccountingPeriod';
import { type WorkingDays } from '../../../../domain/models/attendance/WorkingDays';

import Overlap from '../../Overlap';
import { status } from '../../../../domain/modules/exp/report';
import { modes } from '../../../../requests-pc/modules/ui/expenses/mode';
import { needsResetForm } from '../../../../requests-pc/models/expenses/ExpensesRequestForm';

import { type OverlapProps, type CommonProps } from '../index';

import MessageArea from './MessageArea';

import './index.scss';

const ROOT = 'ts-expenses__form';

type Values = {
  ui: {
    checkboxes: Array<number>,
    recordIdx: number,
    recordItemIdx?: number,
    recalc: boolean, // eslint-disable-line react/no-unused-prop-types
    selectedAccountingPeriod: AccountingPeriod,
    isVendorRequired?: string,
    isCostCenterRequired?: string,
    isJobRequired?: string,
    selectedRecord?: Record,
    tempSavedRecordItems?: Array<Record>,
  },
  report: Report,
};

type Containers = {|
  accountingDate: any,
  baseCurrency: any,
  dialog: any,
  foreignCurrency: any,
  recordItem: any,
  recordList: any,
  reportSummary: any,
  routeForm: any,
  suggest: any,
|};

export type FormContainerProps = {|
  isExpense?: boolean,
  isRequest?: boolean,
  isFinanceApproval?: boolean,
  availableExpType?: Array<string>,
  onClickApprovalHistoryButton: () => void,
  onClickApproveButton: () => void,
  onClickBackButton: () => void,
  onClickCancelRequestButton: () => void,
  onClickDeleteButton: () => void,
  onClickEditHistoryButton: () => void,
  onClickRejectButton: () => void,
  reportEdit: () => void,
|};

type FormikProps = {|
  errors: *,
  values: Values,
  handleSubmit: () => void,
  resetForm: (Values) => void,
  setFieldError: (string, any) => void,
  setFieldTouched: (string, {} | boolean, ?boolean) => void,
  setFieldValue: (string, any) => void,
  setTouched: ({}) => void,
  submitForm: () => void,
  touched: *,
  validateForm: () => void,
|};

type Props = {
  disabled: boolean,
  apActive: Array<AccountingPeriod>,
  checkWorkingDays: (Array<string>) => Promise<{ payload: WorkingDays }>,
  isUseAttendance?: boolean,
  ...CommonProps,
  ...Containers,
  ...OverlapProps,
  ...FormikProps,
  ...FormContainerProps,
};

export default class ExpensesForm extends React.Component<Props> {
  onChangeEditingExpReport: (string, any) => void;
  onClickSaveButton: () => void;
  onClickSubmitButton: () => void;
  recalc: (Report) => void;

  componentWillReceiveProps(nextProps: Props) {
    const isResetFormNeeded = needsResetForm(
      this.props.mode,
      nextProps.mode,
      this.props.selectedExpReport,
      nextProps.selectedExpReport
    );
    const isRecalcNeeded = nextProps.values.ui.recalc;
    // For expense and request, if temporary availableExpType is gone, set it based on saved data in redux
    if (this.props.isExpense || this.props.isRequest) {
      const uiAvialableExpType = get(nextProps, 'values.ui.availableExpType');
      if (!uiAvialableExpType && !!this.props.availableExpType) {
        this.onChangeEditingExpReport(
          'ui.availableExpType',
          this.props.availableExpType
        );
      }
    }
    if (isResetFormNeeded) {
      const selectedAccountingPeriod = this.props.values.ui
        .selectedAccountingPeriod;
      const isVendorRequired = this.props.values.ui.isVendorRequired;
      const isCostCenterRequired = this.props.values.ui.isCostCenterRequired;
      const isJobRequired = this.props.values.ui.isJobRequired;

      this.props.resetForm({
        ui: {
          isVendorRequired,
          isCostCenterRequired,
          isJobRequired,
          selectedAccountingPeriod,
          checkboxes: [],
          recordIdx: -1,
          recalc: false,
          saveMode: false,
        },
        report: nextProps.selectedExpReport,
      });
      if (this.props.isExpense || this.props.isRequest) {
        this.onChangeEditingExpReport(
          'ui.availableExpType',
          this.props.availableExpType
        );
      }

      // check working days only when report with record
      if (
        this.props.isExpense &&
        this.props.checkWorkingDays &&
        this.props.isUseAttendance &&
        nextProps.selectedExpReport.status !== 'Approved' &&
        nextProps.values.report.records.length > 0
      ) {
        const recordDates = nextProps.selectedExpReport.records.map(
          (record) => record.recordDate
        );
        const workingDays = recordDates.filter(
          (x, i, self) => self.indexOf(x) === i
        );
        this.props.checkWorkingDays(workingDays).then(({ payload }) => {
          this.onChangeEditingExpReport('ui.workingDays', payload);
        });
      } else if (nextProps.selectedExpReport.reportId) {
        this.props.validateForm();
      }
    }

    if (isRecalcNeeded) {
      this.recalc(nextProps.values.report);
    }
  }

  onChangeEditingExpReport = (key: string, value: any, touched: any) => {
    const isReportEditMode = this.props.mode === modes.REPORT_EDIT;
    const isTouchedBoolean = typeof touched === 'boolean';

    if (
      !isReportEditMode &&
      ((isTouchedBoolean && touched) || !isEmpty(touched))
    ) {
      this.props.reportEdit();
    }
    this.props.setFieldValue(key, value);

    if (touched && isTouchedBoolean) {
      this.props.setFieldTouched(key, touched, false);
    } else {
      const tmpTouched = cloneDeep(this.props.touched);
      const tmpGetTouched = get(tmpTouched, key);
      const mergedTouched = merge(tmpGetTouched, touched);
      set(tmpTouched, key, mergedTouched);
      this.props.setTouched(tmpTouched);
    }
  };

  onClickSaveReportButton = () => {
    this.props.setFieldValue('ui.saveMode', true);
    this.props.setFieldValue('ui.isRecordSave', false);
    this.props.setFieldValue('ui.submitMode', false);
    setTimeout(this.props.submitForm, 1);
  };

  onClickSaveRecordButton = () => {
    this.props.setFieldValue('ui.saveMode', true);
    this.props.setFieldValue('ui.isRecordSave', true);
    this.props.setFieldValue('ui.submitMode', false);
    setTimeout(this.props.submitForm, 1);
  };

  onClickSubmitButton = () => {
    this.props.setFieldValue('ui.saveMode', false);
    this.props.setFieldValue('ui.isRecordSave', false);
    this.props.setFieldValue('ui.submitMode', true);
    setTimeout(this.props.submitForm, 1);
  };

  // 再計算が必要な処理の集まり
  // method to recalculate amount
  recalc = (report: Report) => {
    this.props.setFieldValue('report.totalAmount', calcTotalAmount(report));
    this.props.setFieldValue('ui.recalc', false);
  };

  checkReportStatus(reportStatus: string) {
    return this.props.selectedExpReport.status === status[reportStatus];
  }

  handleFormSubmit = () => {
    return false;
  };

  render() {
    const { isFinanceApproval, values } = this.props;
    const hasReportId = values.report.reportId;
    const hasPreRequestId = values.report.preRequestId;

    const isReportEditMode = this.props.mode === modes.REPORT_EDIT;
    const isReportPending = this.checkReportStatus('PENDING');
    const isReportApproved =
      this.checkReportStatus('APPROVED') || this.checkReportStatus('CLAIMED');
    const isReportPendingOrApproved = isReportPending || isReportApproved;
    const readOnly =
      isFinanceApproval && isReportEditMode
        ? false
        : this.props.disabled || isReportPending || isReportApproved;
    const isNewReportFromPreRequest = hasPreRequestId && !hasReportId;
    const isNewReportOrRequest = !hasReportId;
    const reportSummaryReadOnly = readOnly || isNewReportFromPreRequest;
    let recordReadOnly =
      (readOnly || (isReportEditMode && !isFinanceApproval)) &&
      !isReportPendingOrApproved;

    if (isNewReportOrRequest) {
      recordReadOnly = true;
    }

    const recordItemReadOnly = isNewReportFromPreRequest ? true : readOnly;

    if (isFinanceApproval) {
      recordReadOnly = isReportEditMode;
    }
    const ReportSummary = this.props.reportSummary;
    const RecordList = this.props.recordList;
    const RecordItem = this.props.recordItem;
    const Dialog = this.props.dialog;

    // if no tempSavedRecordItems, use items[0] which is parent record
    const firstRecordItem = get(
      values,
      `report.records.${values.ui.recordIdx}.items`,
      []
    ).slice(0, 1);

    const tempSavedRecordItems =
      values.ui.tempSavedRecordItems || firstRecordItem;

    return (
      <form
        className={`${ROOT}`}
        onSubmit={() => {
          return false;
        }}
      >
        <ReportSummary
          readOnly={reportSummaryReadOnly}
          isExpense={this.props.isExpense}
          isFinanceApproval={isFinanceApproval}
          expReport={this.props.values.report}
          errors={this.props.errors.report || {}}
          touched={this.props.touched.report || {}}
          checkboxes={this.props.values.ui.checkboxes}
          recordIdx={this.props.values.ui.recordIdx}
          onChangeEditingExpReport={this.onChangeEditingExpReport}
          onClickSaveButton={this.onClickSaveReportButton}
          onClickBackButton={this.props.onClickBackButton}
          onClickDeleteButton={this.props.onClickDeleteButton}
          onClickSubmitButton={this.onClickSubmitButton}
          onClickCancelRequestButton={this.props.onClickCancelRequestButton}
          onClickApprovalHistoryButton={this.props.onClickApprovalHistoryButton}
          onClickEditHistoryButton={this.props.onClickEditHistoryButton}
          onClickRejectButton={this.props.onClickRejectButton}
          onClickApproveButton={this.props.onClickApproveButton}
          labelObject={this.props.labelObject}
          setFieldValue={this.props.setFieldValue}
          apActive={this.props.apActive}
        />
        <MessageArea
          expReport={this.props.values.report}
          errors={this.props.errors.report || {}}
          touched={this.props.touched.report || {}}
        />
        <RecordList
          readOnly={recordReadOnly}
          isNewReportFromPreRequest={isNewReportFromPreRequest}
          isReportPendingOrApproved={isReportPendingOrApproved}
          expReport={this.props.values.report}
          errors={this.props.errors.report || {}}
          touched={this.props.touched.report || {}}
          checkboxes={this.props.values.ui.checkboxes}
          recordIdx={this.props.values.ui.recordIdx}
          onChangeEditingExpReport={this.onChangeEditingExpReport}
          isFinanceApproval={this.props.isFinanceApproval}
        />
        <Overlap
          isVisible={this.props.overlap.record}
          className={`${ROOT}__record-overlap`}
        >
          {this.props.overlap.record && (
            <RecordItem
              readOnly={recordItemReadOnly}
              isExpense={this.props.isExpense}
              isFinanceApproval={isFinanceApproval}
              setFieldTouched={this.props.setFieldTouched}
              setFieldError={this.props.setFieldError}
              recalc={this.recalc}
              expReport={this.props.values.report}
              selectedRecord={this.props.values.ui.selectedRecord}
              tempSavedRecordItems={tempSavedRecordItems}
              recordIdx={this.props.values.ui.recordIdx}
              errors={this.props.errors.report || {}}
              touched={this.props.touched.report || {}}
              baseCurrency={this.props.baseCurrency}
              foreignCurrency={this.props.foreignCurrency}
              routeForm={this.props.routeForm}
              suggest={this.props.suggest}
              onChangeEditingExpReport={this.onChangeEditingExpReport}
              onClickSaveButton={this.onClickSaveRecordButton}
              onClickBackButton={this.props.onClickBackButton}
            />
          )}
        </Overlap>
        <Dialog
          setFieldError={this.props.setFieldError}
          setFieldTouched={this.props.setFieldTouched}
          expReport={this.props.values.report}
          onChangeEditingExpReport={this.onChangeEditingExpReport}
          recordIdx={this.props.values.ui.recordIdx}
          recordItemIdx={this.props.values.ui.recordItemIdx}
          touched={this.props.touched.report || {}}
          setTouched={this.props.setTouched}
          errors={this.props.errors.report || {}}
          setFieldValue={this.props.setFieldValue}
          isExpense={this.props.isExpense}
          baseCurrency={this.props.baseCurrency}
          foreignCurrency={this.props.foreignCurrency}
          tempSavedRecordItems={tempSavedRecordItems}
          isFinanceApproval={isFinanceApproval}
          validateForm={this.props.validateForm}
          recordItemReadOnly={recordItemReadOnly}
        />
      </form>
    );
  }
}
