// @flow

import React from 'react';
import classNames from 'classnames';

import msg from '../../../../languages';
import DateUtil from '../../../../utils/DateUtil';

import IconAttention from '../../../../images/icons/attention.svg';

import './Column.scss';

const ROOT = 'ts-expenses__form-message-area-column';

export type MessageAreaColumn = {
  recordDate?: string,
  expTypeName?: string,
  message: string | any,
};

type Props = {
  column: MessageAreaColumn,
  idx: number,
};

const Column = ({ column, idx }: Props) => {
  const { recordDate, expTypeName, message } = column;
  const iconType = message.match(/^[a-zA-Z]+_Warn_[a-zA-Z]+$/)
    ? 'warn'
    : 'error';
  const iconClassName = classNames(
    `${ROOT}__icon`,
    `${ROOT}__icon--${iconType}`
  );
  return (
    <div key={idx} className={ROOT}>
      <IconAttention className={iconClassName} />
      {recordDate && (
        <div className={`${ROOT}__record-date`}>
          {DateUtil.format(recordDate)}
        </div>
      )}
      {expTypeName && (
        <div className={`${ROOT}__exptype-name`}>{expTypeName}</div>
      )}
      <div className={`${ROOT}__message`}>{msg()[message]}</div>
    </div>
  );
};

export default Column;
