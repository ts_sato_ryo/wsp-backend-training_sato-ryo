// @flow
import React, { useState } from 'react';
import classNames from 'classnames';
import { isEmpty, forEach, get } from 'lodash';

import msg from '../../../../languages';
import { type Report } from '../../../../../domain/models/exp/Report';
import IconButton from '../../../buttons/IconButton';
import ImgIconChevronDown from '../../../../images/icons/chevrondown.svg';

import Column, { type MessageAreaColumn } from './Column';

import './index.scss';

const ROOT = 'ts-expenses__form-message-area';

type Props = {
  errors: *,
  expReport: Report,
};

type ViewItems = Array<MessageAreaColumn>;

const RECORD_CHECK_LIST = ['recordDate', 'receiptId', 'expTypeId', 'items'];

//
// Area to display only special errors and warn.
// Normal errors are not displayed here.
//
const MessageArea = (props: Props) => {
  const { errors, expReport } = props;
  const [isOpen, setIsOpen] = useState(false);
  const onClickHeaderIcon = () => setIsOpen(!isOpen);

  if (isEmpty(errors) || isEmpty(errors.records)) {
    return null;
  }

  // collect item for display list
  const viewItems: ViewItems = [];

  // record
  forEach(errors.records, function(record, idx) {
    const expRecord = expReport.records[idx];
    if (record && expRecord) {
      const recordDate = expRecord.recordDate || msg().Exp_Lbl_InputEmpty;
      const { expTypeName } = expRecord.items[0];
      forEach(record, function(value, key) {
        if (RECORD_CHECK_LIST.includes(key) && typeof value === 'string') {
          viewItems.push({
            recordDate,
            expTypeName,
            message: value,
          });
        }
      });
      const errorItem = get(record, 'items[0]');
      if (errorItem) {
        Object.entries(errorItem).forEach(([key, value]) => {
          if (RECORD_CHECK_LIST.includes(key)) {
            viewItems.push({
              recordDate,
              expTypeName,
              message: value,
            });
          }
        });
      }
    }
  });

  if (!viewItems.length) {
    return null;
  }

  const titleButtonClassName = classNames(`${ROOT}__title-button`, {
    [`${ROOT}__title-button--open`]: isOpen,
  });
  const headerClassName = classNames(`${ROOT}__header`, {
    [`${ROOT}__header--open`]: isOpen,
  });

  return (
    <div className={ROOT}>
      <div className={`${ROOT}__main`}>
        <div className={headerClassName}>
          <IconButton
            srcType="svg"
            fillColor="#B7423A"
            src={ImgIconChevronDown}
            className={titleButtonClassName}
            onClick={onClickHeaderIcon}
          >
            <div className={`${ROOT}__header-message`}>
              {msg().Exp_Lbl_MessageAreaTitle}
            </div>
          </IconButton>
        </div>
        {isOpen && (
          <div className={`${ROOT}__body`}>
            {viewItems.map((item, idx) => (
              <Column key={idx} column={item} idx={idx} />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default MessageArea;
