// @flow
import React from 'react';

import msg from '../../../../languages';
import MultiColumnsGrid from '../../../MultiColumnsGrid';

import type { Report } from '../../../../../domain/models/exp/Report';

import iconPreRequest from '../../../../images/Request.svg';
import './index.scss';
import FormatUtil from '../../../../utils/FormatUtil';

const ROOT = 'ts-expenses__form-pre-request-summary';

type Props = {
  preRequest: Report,
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
};

export default class PreRequestSummary extends React.Component<Props> {
  render() {
    const { preRequest, baseCurrencySymbol, baseCurrencyDecimal } = this.props;
    const formattedTotalAmount = FormatUtil.formatNumber(
      preRequest.totalAmount,
      baseCurrencyDecimal
    );
    const Icon = iconPreRequest;
    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__title`}>
          <Icon />
          <span>{msg().Appr_Btn_ExpensesPreApproval}</span>
        </div>
        <div className={`${ROOT}__body`}>
          <MultiColumnsGrid sizeList={[1, 1, 1, 6, 1, 2]}>
            <div className={`${ROOT}__label`}>
              {`${msg().Appr_Lbl_TAndERequestNumber} :`}
            </div>
            <div className={`${ROOT}__value`}>{preRequest.reportNo}</div>
            <div className={`${ROOT}__label`}>
              {`${msg().Exp_Clbl_ReportTitle} :`}
            </div>
            <div className={`${ROOT}__value`}>{preRequest.subject}</div>
            <div className={`${ROOT}__label`}>
              {`${msg().Exp_Lbl_TotalAmount} :`}
            </div>
            <div className={`${ROOT}__value`}>
              {`${baseCurrencySymbol} ${formattedTotalAmount}`}
            </div>
          </MultiColumnsGrid>
          <MultiColumnsGrid sizeList={[1, 10]}>
            <div className={`${ROOT}__label`}>
              {`${msg().Exp_Clbl_Purpose} :`}
            </div>
            <div className={`${ROOT}__value`}>{preRequest.purpose}</div>
          </MultiColumnsGrid>
        </div>
      </div>
    );
  }
}
