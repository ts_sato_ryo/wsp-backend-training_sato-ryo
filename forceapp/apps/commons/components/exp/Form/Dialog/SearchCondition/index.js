// @flow
import React from 'react';

import msg from '../../../../../languages';
import DialogFrame from '../../../../dialogs/DialogFrame';
import Button from '../../../../buttons/Button';
import TextField from '../../../../fields/TextField';

import './index.scss';

const ROOT = 'ts-expenses-modal-approval';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */

type Props = {
  comment: string,
  title: string,
  onChangeName: (string) => void,
  onClickHideDialogButton: () => void,
  // onClickMainButton: () => void,
  inputError: string,
  onClickSaveOverwriteButton: () => void,
  onClickSaveNewButton: () => void,
  selectedConditionName: string,
};

export default class SearchCondition extends React.Component<Props> {
  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    this.props.onChangeName(e.target.value);
  };

  render() {
    const isDisabledSaveNewButton =
      this.props.comment === this.props.selectedConditionName;
    const isDisabledSaveOverWriteButton =
      this.props.selectedConditionName ===
      msg().Exp_Lbl_SearchConditionApprovelreRuestList;

    return (
      <DialogFrame
        title={this.props.title}
        hide={this.props.onClickHideDialogButton}
        className={`${ROOT}__dialog-frame`}
        footer={
          <DialogFrame.Footer>
            <Button type="default" onClick={this.props.onClickHideDialogButton}>
              {msg().Com_Btn_Cancel}
            </Button>
            <Button
              type="primary"
              onClick={this.props.onClickSaveOverwriteButton}
              disabled={isDisabledSaveOverWriteButton}
            >
              {msg().Com_Btn_Save_Overwrite}
            </Button>
            <Button
              type="primary"
              onClick={this.props.onClickSaveNewButton}
              disabled={isDisabledSaveNewButton}
            >
              {msg().Com_Btn_Save_New}
            </Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__inner`}>
          <TextField onChange={this.onChange} value={this.props.comment} />
        </div>
        <div className={`${ROOT}__error`}>{this.props.inputError}</div>
      </DialogFrame>
    );
  }
}
