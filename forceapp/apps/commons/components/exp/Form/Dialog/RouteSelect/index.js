// @flow
import React from 'react';

import msg from '../../../../../languages';
import DialogFrame from '../../../../dialogs/DialogFrame';
import Button from '../../../../buttons/Button';

import type {
  Route,
  RouteItem,
} from '../../../../../../domain/models/exp/jorudan/Route';
import type { ViaList } from '../../../../../../domain/models/exp/Record';
import type { StationInfo } from '../../../../../../domain/models/exp/jorudan/Station';

import ContentsHeader from './ContentsHeader';
import RecordHeader from './RecordHeader';
import RecordBody from './RecordBody';

import './index.scss';

const ROOT = 'ts-expenses-modal-route';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */

type Props = {
  onClickHideDialogButton: () => void,
  route: Route,
  onClickRouteSelectListItem: (RouteItem) => void,
  origin: StationInfo,
  viaList: ViaList,
  arrival: StationInfo,
  seatPreference: number,
};

export default class RouteSelect extends React.Component<Props> {
  render() {
    if (!this.props.route) {
      return null;
    }
    const { routeList } = this.props.route.route;
    return (
      <DialogFrame
        title={msg().Exp_Lbl_Transit}
        hide={this.props.onClickHideDialogButton}
        className={ROOT}
        footer={
          <div className={`${ROOT}-btn-close`}>
            <Button onClick={this.props.onClickHideDialogButton}>
              {msg().Com_Btn_Close}
            </Button>
          </div>
        }
      >
        <div className={`${ROOT}-contents`}>
          <ContentsHeader
            origin={this.props.origin}
            viaList={this.props.viaList}
            arrival={this.props.arrival}
          />
          <table className={`${ROOT}-contents-route-list`}>
            <tbody>
              {routeList.map((item) => {
                return (
                  <tr key={item.key}>
                    <RecordHeader item={item} />
                    <RecordBody
                      item={item}
                      seatPreference={this.props.seatPreference}
                    />
                    <td className={`${ROOT}-contents-route-list-btn-area`}>
                      <Button
                        type="primary"
                        onClick={() => {
                          this.props.onClickRouteSelectListItem(item);
                        }}
                      >
                        {msg().Com_Btn_Select}
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </DialogFrame>
    );
  }
}
