// @flow
import React from 'react';

import ReactDropzone from 'react-dropzone';
import msg from '../../../../../../languages';
import IconReceipt from '../../../../../../images/icons/receipt.svg';
import './index.scss';

const ROOT = 'ts-expenses-modal-receipt-library-drop-zone';
const ALLOWED_MIME_TYPES = 'image/*, application/pdf';
const MAX_FILE_SIZE = 5242880;

type Props = {
  handleDropAccepted: (File[]) => void,
  handleDropRejected: () => Function,
  multiple: boolean,
  errorFileUpload: boolean,
};

const DropZoneArea = ({
  handleDropAccepted,
  handleDropRejected,
  errorFileUpload,
  multiple,
}: Props) => {
  return (
    <div className={`${ROOT}__inner`}>
      <ReactDropzone
        className={`${ROOT}__file`}
        accept={ALLOWED_MIME_TYPES}
        onDropAccepted={handleDropAccepted}
        onDropRejected={handleDropRejected}
        multiple={multiple}
        maxSize={MAX_FILE_SIZE}
      >
        <div className={`${ROOT}__file-label`}>
          <p>{msg().Exp_Lbl_DragAndDropOCR}</p>
          <IconReceipt className={`${ROOT}__button--file ts-button`} />
        </div>
        {errorFileUpload && (
          <div className={`${ROOT}__error input-feedback`}>
            {msg().Common_Err_MaxFileSize}
          </div>
        )}
      </ReactDropzone>
    </div>
  );
};

export default DropZoneArea;
