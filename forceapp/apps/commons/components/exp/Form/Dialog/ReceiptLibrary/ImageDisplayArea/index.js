// @flow
import React from 'react';
import msg from '../../../../../../languages';
import FileUtil from '../../../../../../utils/FileUtil';
import DateUtil from '../../../../../../utils/DateUtil';
import IconTrash from '../../../../../../images/icons/trash.svg';
import Spinner from '../../../../../Spinner';
import {
  type SelectedReceipt,
  OCR_STATUS,
} from '../../../../../../../domain/models/exp/Receipt';
import pdfIcon from '../../../../../../images/pdfIcon.png';
import Lightbox from '../../../../../Lightbox';
import './index.scss';

const ROOT = 'ts-expenses-modal-receipt-library-image-display';

type Props = {
  imageUrlList: Array<Object>,
  onSelectImage: () => Function,
  onClickDeleteButton: (receiptId: string, receiptFileId: string) => Function,
  dropZoneArea: any,
  selectedReceipt: SelectedReceipt,
  receiptStatus: Object,
  isReportReceipt?: boolean,
  executeOcr: (string) => void,
};

const getDisplayStatus = (status: string) => {
  const { NOT_PROCESSED, COMPLETED, IN_PROGRESS, QUEUED } = OCR_STATUS;
  switch (status) {
    case NOT_PROCESSED:
      return msg().Exp_Lbl_StatusNotScanned;
    case IN_PROGRESS:
      return msg().Exp_Lbl_StatusProcessing;
    case QUEUED:
      return msg().Exp_Lbl_StatusProcessing;
    case COMPLETED:
      return msg().Exp_Lbl_StatusScanned;
    default:
      return msg().Exp_Lbl_StatusNotScanned;
  }
};

const renderStatus = (item, receiptStatus = {}) => {
  const status = receiptStatus[item.receiptFileId] || OCR_STATUS.NOT_PROCESSED;
  return (
    <div className={`${ROOT}__status ${ROOT}__status-${status}`}>
      {getDisplayStatus(status)}
    </div>
  );
};

const getAmount = (ocrInfo) => {
  return ocrInfo && ocrInfo.result && ocrInfo.result.amount;
};

const getRecordDate = (ocrInfo) => {
  return ocrInfo && ocrInfo.result && ocrInfo.result.recordDate;
};

const ocrData = (receiptFileId, ocrInfo, executeOcr, receiptStatus) => {
  const amount = getAmount(ocrInfo);
  const recordDate = getRecordDate(ocrInfo);
  const status = receiptStatus[receiptFileId] || OCR_STATUS.NOT_PROCESSED;
  const showSpinner = status === OCR_STATUS.IN_PROGRESS;
  return (
    (ocrInfo && (
      <>
        <div className={`${ROOT}__record-date`}>
          {msg().Exp_Clbl_Date}: {DateUtil.format(recordDate)}
        </div>
        <div className={`${ROOT}__amount`}>
          {msg().Exp_Clbl_Amount}: {amount}
        </div>
      </>
    )) ||
    ((showSpinner && (
      <div className={`${ROOT}__scan-spinner`}>
        <Spinner loading priority="low" />
      </div>
    )) || (
      <div
        className={`${ROOT}__scan-msg`}
        onClick={() => executeOcr(receiptFileId)}
      >
        {msg().Exp_Lbl_ScanReceipt}
      </div>
    ))
  );
};

const getDisplayFileName = (title: string) => {
  const originalFileName =
    FileUtil.getOriginalFileNameWithoutPrefix(title) || '';
  return originalFileName.length > 20
    ? originalFileName.substring(0, 20).concat('...')
    : originalFileName;
};

const renderOverlay = (
  item,
  onClickDeleteButton,
  executeOcr,
  receiptStatus,
  isReportReceipt
) => {
  const { ocrInfo, uploadedDate = '' } = item;
  const overlayInfo = isReportReceipt
    ? getDisplayFileName(item.title)
    : ocrData(item.receiptFileId, ocrInfo, executeOcr, receiptStatus);
  return (
    <div className="after">
      <div className={`${ROOT}__ocrInfo`}>{overlayInfo}</div>
      <div className={`${ROOT}__upload-date`}>
        {uploadedDate && uploadedDate.replace(/-/g, '/')}
      </div>
      {!FileUtil.getB64FileExtension(item.fileBody, 'pdf') ? (
        <Lightbox className={`${ROOT}__lightbox`}>
          <img className="receipt-library" src={item.fileBody} alt="File" />
        </Lightbox>
      ) : (
        ''
      )}
      <IconTrash
        aria-hidden="true"
        className={`${ROOT}__delete`}
        onClick={onClickDeleteButton(item.receiptId, item.receiptFileId)}
      />
    </div>
  );
};

const ImageDisplayArea = ({
  imageUrlList,
  onSelectImage,
  onClickDeleteButton,
  dropZoneArea,
  selectedReceipt,
  receiptStatus,
  executeOcr,
  isReportReceipt,
}: Props) => {
  const showInputButton = (item) =>
    isReportReceipt ||
    receiptStatus[item.receiptFileId] === OCR_STATUS.COMPLETED;
  return (
    <div className={`${ROOT}__msg`}>
      {msg().Exp_Lbl_SelectReceiptLibrary}
      {
        <div className={`${ROOT}__list`}>
          {dropZoneArea}
          {imageUrlList.map((item) => {
            return (
              <div className={`${ROOT}__img-area`} key={item.receiptId}>
                {(item.receiptId && (
                  <div className={`${ROOT}__labels`}>
                    <label className={`${ROOT}__first`}>
                      {!isReportReceipt && renderStatus(item, receiptStatus)}
                      {FileUtil.getB64FileExtension(item.fileBody, 'pdf') ? (
                        <img
                          className={`${ROOT}__pdf-icon ${(selectedReceipt &&
                            item.receiptFileId ===
                              selectedReceipt.receiptFileId &&
                            'selected') ||
                            ''}`}
                          src={pdfIcon}
                          alt="pdf-file"
                        />
                      ) : (
                        <img
                          className={`${ROOT}__img ${(selectedReceipt &&
                            item.receiptFileId ===
                              selectedReceipt.receiptFileId &&
                            'selected') ||
                            ''}`}
                          src={item.fileBody}
                          alt="file"
                        />
                      )}
                      {showInputButton(item) && (
                        <input
                          className={`${ROOT}__input`}
                          type="radio"
                          name="receipts"
                          value={item.receiptFileId}
                          onClick={onSelectImage()}
                          checked={
                            selectedReceipt &&
                            selectedReceipt.receiptFileId === item.receiptFileId
                          }
                        />
                      )}
                    </label>
                    {renderOverlay(
                      item,
                      onClickDeleteButton,
                      executeOcr,
                      receiptStatus,
                      isReportReceipt
                    )}
                  </div>
                )) || (
                  <div className={`${ROOT}__loading`}>
                    <div className={`${ROOT}__loading-spinner`}>
                      {msg().Exp_Lbl_LoadingPreview}
                      <Spinner loading priority="low" />
                    </div>
                  </div>
                )}
              </div>
            );
          })}
        </div>
      }
    </div>
  );
};

export default ImageDisplayArea;
