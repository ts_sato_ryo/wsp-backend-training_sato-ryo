// @flow
import React from 'react';
import isEmpty from 'lodash/isEmpty';
import find from 'lodash/find';
import msg from '../../../../../languages';

import { type ReceiptList } from '../../../../../../domain/models/exp/receipt-library/list';
import {
  type SelectedReceipt,
  OCR_STATUS,
} from '../../../../../../domain/models/exp/Receipt';
import Button from '../../../../buttons/Button';
import DialogFrame from '../../../../dialogs/DialogFrame';

import DropZoneArea from './DropZoneArea';
import { type DropzoneFile } from '../../../../fields/Dropzone';
import LabelWithHint from '../../../../fields/LabelWithHint';

import ImageDisplayArea from './ImageDisplayArea';
import ProgressBar, {
  PROGRESS_STATUS,
  type ProgressBarStep,
} from '../../../../ProgressBar';
import './index.scss';
import FileUtil from '../../../../../utils/FileUtil';

const ROOT = 'ts-expenses-modal-receipt-library';
const ALLOWED_MIME_TYPES = 'image/*, application/pdf';

type Props = {
  title: string,
  hintMsg?: string,
  mainButtonTitle: string,
  receiptList: ReceiptList,
  onClickReceiptLibrayCloseButton: () => void,
  onClickSelectReceipt: (
    string | Object,
    ?string,
    ?string,
    ?string,
    ?string
  ) => void,
  getFilePreview: (string, withOcr?: boolean) => Promise<any>,
  onImageDrop: (files: File, runOCR: boolean) => Promise<any>,
  deleteReceipt: (receiptId: string, receiptFileId: string) => void,
  receiptStatus: Object,
  selectedReceipt: SelectedReceipt,
  setSelectedReceipt: (SelectedReceipt) => void,
  executeOcr: (string) => void,
  isMultiStep?: boolean,
  isReportReceipt?: boolean,
  progressBar: Array<ProgressBarStep>,
  setProgressBar: (Array<ProgressBarStep>) => void,
};

type State = {
  imageUrlList: Array<Object>,
  errorFileUpload: boolean,
};
export default class ReceiptDialog extends React.Component<Props, State> {
  state = {
    imageUrlList: [],
    errorFileUpload: false,
  };

  componentDidMount() {
    const { receiptList, selectedReceipt } = this.props;
    this.setState({ imageUrlList: Array(receiptList.length).fill({}) });
    Array.from(receiptList).map(
      (item, idx) =>
        !isEmpty(item.contentVersionId) &&
        this.props
          .getFilePreview(item.contentVersionId, true)
          .then((result) => {
            this.setState((prevState) => {
              const list = prevState.imageUrlList;
              list[idx] = result;
              return { imageUrlList: list };
            });
          })
    );
    if (this.props.isMultiStep) {
      const receitSelectionStatus = selectedReceipt.receiptFileId
        ? PROGRESS_STATUS.ACTIVE
        : PROGRESS_STATUS.SELECTED;
      const steps = [
        {
          id: '1',
          text: msg().Exp_Lbl_ReceiptSelection,
          status: receitSelectionStatus,
        },
        {
          id: '2',
          text: msg().Exp_Lbl_ExpenseTypeSelect,
          status: PROGRESS_STATUS.INACTIVE,
        },
      ];
      this.props.setProgressBar(steps);
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    if (!this.props.isReportReceipt) {
      const isIdNotProcessed = (obj) => {
        return (key) => obj[key] !== OCR_STATUS.COMPLETED;
      };
      const { receiptStatus } = this.props;
      const newReceiptStatus = nextProps.receiptStatus;
      const unprocessedId =
        Object.keys(receiptStatus).filter(isIdNotProcessed(receiptStatus)) ||
        [];
      unprocessedId.forEach((id) => {
        if (
          receiptStatus[id] !== OCR_STATUS.COMPLETED &&
          newReceiptStatus[id] === OCR_STATUS.COMPLETED
        ) {
          nextProps.getFilePreview(id, true).then((res) => {
            this.setState((prevState) => {
              const idx =
                prevState.imageUrlList.findIndex(
                  (x) => x.receiptFileId === id
                ) || 0;
              const list = prevState.imageUrlList;
              list[idx] = res;
              return { imageUrlList: list };
            });
          });
        }
      });
    }
  }

  componentWillUnmount() {
    this.setState({ imageUrlList: [] });
  }

  onClickMainButton = (imageUrlList: Array<Object>) => {
    return () => {
      const selectedId = this.props.selectedReceipt.receiptFileId;
      if (selectedId) {
        if (this.props.isMultiStep) {
          const steps = this.props.progressBar || [];
          steps[steps.length - 1].status = PROGRESS_STATUS.SELECTED;
          this.props.onClickSelectReceipt(steps);
        } else {
          const data = find(imageUrlList, { receiptFileId: selectedId }) || {};
          const dataType = data.fileBody
            ? FileUtil.getMIMEType(
                String(FileUtil.getB64FileExtension(data.fileBody, ''))
              )
            : null;
          this.props.onClickSelectReceipt(
            selectedId,
            data.receiptId,
            data.fileBody,
            data.title,
            dataType
          );
        }
      }
    };
  };

  onSelectImage = () => {
    return (e: SyntheticInputEvent<HTMLInputElement>) => {
      const receiptFileId = e.target.value;
      const data = find(this.state.imageUrlList, { receiptFileId }) || {};
      if (this.props.isMultiStep) {
        const steps = this.props.progressBar || [];
        steps[0].status = PROGRESS_STATUS.ACTIVE;
        this.props.setProgressBar(steps);
      }
      const selectedReceipt = {
        receiptFileId,
        receiptId: data.receiptId,
        receiptData: data.fileBody,
        ocrInfo: data.ocrInfo,
        dataType: data.dataType,
      };
      this.props.setSelectedReceipt(selectedReceipt);
    };
  };

  onClickDeleteButton = (receiptId: string, receiptFileId: string) => {
    return () => {
      const { imageUrlList } = this.state;
      imageUrlList.splice(
        imageUrlList.findIndex((item) => item.receiptId === receiptId),
        1
      );
      this.setState({ imageUrlList });
      this.props.deleteReceipt(receiptId, receiptFileId);
    };
  };

  handleDropAccepted = (files: DropzoneFile[]) => {
    this.setState((prevState) => {
      const prevImageUrlList = prevState.imageUrlList;
      const newImageUrlList = Array(files.length).fill({});
      return {
        imageUrlList: newImageUrlList.concat(prevImageUrlList),
        errorFileUpload: false,
      };
    });
    const runOCR = !this.props.isReportReceipt;
    Array.from(files).map((file, idx) =>
      this.props.onImageDrop(file, runOCR).then((res) => {
        this.props.getFilePreview(res.receiptFileId, true).then((result) => {
          this.setState((prevState) => {
            const list = prevState.imageUrlList;
            list[idx] = result;
            return { imageUrlList: list };
          });
        });
      })
    );
  };

  handleDropRejected = () => {
    this.setState({ errorFileUpload: true });
  };

  footer() {
    const hintText = this.props.hintMsg ? msg().Exp_Lbl_Hint : '';
    const disabled = !this.props.selectedReceipt.receiptFileId;
    return (
      <DialogFrame.Footer
        sub={
          <>
            <LabelWithHint
              text={hintText}
              hintMsg={this.props.hintMsg}
              infoAlign="left"
            />
            {this.props.isMultiStep && (
              <ProgressBar steps={this.props.progressBar} />
            )}
          </>
        }
      >
        <Button
          type="primary"
          onClick={this.onClickMainButton(this.state.imageUrlList)}
          disabled={disabled}
        >
          {this.props.mainButtonTitle}
        </Button>
      </DialogFrame.Footer>
    );
  }

  render() {
    const dropZoneArea = (
      <DropZoneArea
        accept={ALLOWED_MIME_TYPES}
        errorFileUpload={this.state.errorFileUpload}
        handleDropAccepted={this.handleDropAccepted}
        handleDropRejected={this.handleDropRejected}
        multiple={false}
      />
    );
    return (
      <DialogFrame
        title={this.props.title}
        hide={this.props.onClickReceiptLibrayCloseButton}
        className={`${ROOT}__dialog-frame`}
        footer={this.footer()}
      >
        <ImageDisplayArea
          errorFileUpload={this.state.errorFileUpload}
          imageUrlList={this.state.imageUrlList}
          onSelectImage={this.onSelectImage}
          onClickDeleteButton={this.onClickDeleteButton}
          dropZoneArea={dropZoneArea}
          receiptStatus={this.props.receiptStatus}
          selectedReceipt={this.props.selectedReceipt}
          isMultiStep={this.props.isMultiStep}
          isReportReceipt={this.props.isReportReceipt}
          executeOcr={this.props.executeOcr}
        />
      </DialogFrame>
    );
  }
}
