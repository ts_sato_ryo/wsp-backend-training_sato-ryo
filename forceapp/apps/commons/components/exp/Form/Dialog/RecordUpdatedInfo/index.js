// @flow
import React from 'react';

import isEmpty from 'lodash/isEmpty';
import TextUtil from '../../../../../utils/TextUtil';
import DateUtil from '../../../../../utils/DateUtil';
import msg from '../../../../../languages';
import Button from '../../../../buttons/Button';
import DialogFrame from '../../../../dialogs/DialogFrame';

import './index.scss';
import type { RecordUpdateInfo } from '../../../../../../domain/models/exp/Record';

const ROOT = 'ts-expenses-modal-record-update';

export type RecordUpdated = {|
  updateInfo: RecordUpdateInfo[],
|};

type Props = {
  updateInfo: RecordUpdateInfo[],
  language: string,
  onClickHideDialogButton: () => void,
};

const RecordUpdatedDialog = (props: Props) => {
  const { onClickHideDialogButton, updateInfo, language } = props;

  const listRender = () => {
    return (
      <ul className={`${ROOT}__list`}>
        {!isEmpty(updateInfo) &&
          updateInfo.map((item, idx) => {
            const localedDate =
              language === 'ja'
                ? DateUtil.customFormat(item.recordDate, 'YYYY/MM/DD')
                : DateUtil.customFormat(item.recordDate, 'MM/DD/YYYY');

            const rate = item.isForeignCurrency
              ? msg().Exp_Msg_CloneRecordsRecalcutionExchangeRate
              : msg().Exp_Msg_CloneRecordsRecalcutionTaxRate;
            const message = TextUtil.template(
              msg().Exp_Msg_CloneRecordsRecalcution,
              rate
            );
            return (
              <li
                key={idx}
              >{`${localedDate} - ${item.expenseTypeName} : ${message}`}</li>
            );
          })}
      </ul>
    );
  };

  return (
    <DialogFrame
      title={msg().Com_Lbl_Information}
      hide={onClickHideDialogButton}
      className={`${ROOT}__dialog-frame`}
      footer={
        <DialogFrame.Footer>
          <Button type="default" onClick={onClickHideDialogButton}>
            {msg().Com_Btn_Close}
          </Button>
        </DialogFrame.Footer>
      }
    >
      <div className={`${ROOT}__inner`}>
        <div className={`${ROOT}__field`}>{listRender()}</div>
      </div>
    </DialogFrame>
  );
};

export default RecordUpdatedDialog;
