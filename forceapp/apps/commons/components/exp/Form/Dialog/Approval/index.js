// @flow
import React from 'react';

import msg from '../../../../../languages';
import DialogFrame from '../../../../dialogs/DialogFrame';
import Button from '../../../../buttons/Button';
import CommentNarrowField from '../../../../fields/CommentNarrowField';

import './index.scss';

const ROOT = 'ts-expenses-modal-approval';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */

type Props = {
  comment: string,
  title: string,
  mainButtonTitle: string,
  onChangeComment: (string) => void,
  onClickHideDialogButton: () => void,
  onClickMainButton: () => void,
  photoUrl: string,
};

export default class ApprovalDialog extends React.Component<Props> {
  render() {
    return (
      <DialogFrame
        title={this.props.title}
        hide={this.props.onClickHideDialogButton}
        className={`${ROOT}__dialog-frame`}
        footer={
          <DialogFrame.Footer>
            <Button type="default" onClick={this.props.onClickHideDialogButton}>
              {msg().Com_Btn_Cancel}
            </Button>
            <Button type="primary" onClick={this.props.onClickMainButton}>
              {this.props.mainButtonTitle}
            </Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__inner`}>
          <CommentNarrowField
            onChange={this.props.onChangeComment}
            value={this.props.comment}
            maxLength={1000}
            icon={this.props.photoUrl}
          />
        </div>
      </DialogFrame>
    );
  }
}
