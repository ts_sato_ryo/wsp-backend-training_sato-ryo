// @flow
import React from 'react';

import msg from '../../../../../languages';
import MultiColumnFinder from '../../../../dialogs/MultiColumnFinder';

import type { JobList, Job } from '../../../../../../domain/models/exp/Job';

import '../../../../Modal.scss';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */

type Props = {
  isFinanceApproval: boolean,
  hintMsg?: string,
  jobList: JobList,
  jobSearchList: JobList,
  jobRecentItems: JobList,
  onClickHideDialogButton: () => void,
  onClickJobListItem: (Job) => void,
  onClickJobSearch: (string) => void,
  onClickJobSelectByCategory: () => void,
};

export default class JobSelect extends React.Component<Props> {
  render() {
    return (
      <div className="ts-modal">
        <button
          className="ts-modal__overlay"
          onClick={this.props.onClickHideDialogButton}
        />
        <div className="ts-modal__wrap" style={{ width: 865 }}>
          <div className="ts-modal__contents">
            <MultiColumnFinder
              tab={this.props.isFinanceApproval ? 'FinanceApproval' : ''}
              items={this.props.jobList}
              typeName={msg().Exp_Lbl_Job}
              showFavorites={false}
              showHistory={false}
              parentSelectable
              onClickItem={this.props.onClickJobListItem}
              onClickCloseButton={this.props.onClickHideDialogButton}
              isDefaultSearchMode
              onClickSearch={this.props.onClickJobSearch}
              searchResult={this.props.jobSearchList}
              recentItems={this.props.jobRecentItems}
              onClickSelectByCategory={this.props.onClickJobSelectByCategory}
              hintMsg={this.props.hintMsg}
            />
          </div>
        </div>
      </div>
    );
  }
}
