// @flow
import React from 'react';
import isEmpty from 'lodash/isEmpty';
import msg from '../../../../../languages';
import MultiColumnFinder from '../../../../dialogs/MultiColumnFinder';

import ExpIconInfo from '../../../../../images/icons-utility-info.svg';
import type { ProgressBarStep } from '../../../../ProgressBar';

import {
  type ExpenseType,
  type ExpenseTypeList,
} from '../../../../../../domain/models/exp/ExpenseType';
import { RECEIPT_TYPE } from '../../../../../../domain/models/exp/Record';
import { FOREIGN_CURRENCY_USAGE } from '../../../../../../domain/models/exp/foreign-currency/Currency';

import '../../../../Modal.scss';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */

type Props = {
  isFinanceApproval: boolean,
  hintMsg?: string,
  expenseTypeList: Array<*>,
  expenseTypeSearchList: ExpenseTypeList,
  expenseTypeRecentItems: ExpenseTypeList,
  onClickExpenseTypeCloseButton: () => void,
  onClickExpenseTypeItem: (ExpenseType, ?ExpenseTypeList) => void,
  onClickExpenseTypeSearch: (string) => void,
  onClickExpenseTypeSelectByCategory: () => void,
  progressBar?: Array<ProgressBarStep>,
  setProgressBar?: (Array<ProgressBarStep>) => void,
  onClickBackButton?: () => void,
};

export default class ExpenseTypeSelect extends React.Component<Props> {
  ocrFilter = (expType: ExpenseType) => {
    return (
      expType.isGroup ||
      (!expType.isGroup &&
        (expType.fileAttachment === RECEIPT_TYPE.Required ||
          expType.fileAttachment === RECEIPT_TYPE.Optional) &&
        expType.foreignCurrencyUsage === FOREIGN_CURRENCY_USAGE.NotUsed)
    );
  };

  render() {
    const {
      expenseTypeList,
      expenseTypeSearchList,
      expenseTypeRecentItems,
    } = this.props;

    let list = expenseTypeList;
    let searchlist = expenseTypeSearchList;
    let recentItemsList = expenseTypeRecentItems;

    if (!isEmpty(this.props.progressBar)) {
      list = expenseTypeList.map((x) => x.filter(this.ocrFilter)) || [];
      searchlist = expenseTypeSearchList.filter(this.ocrFilter);
      recentItemsList = expenseTypeRecentItems.filter(this.ocrFilter);
    }

    return (
      <div className="ts-modal">
        <button
          className="ts-modal__overlay"
          onClick={this.props.onClickExpenseTypeCloseButton}
        />
        <div className="ts-modal__wrap" style={{ width: 865 }}>
          <div className="ts-modal__contents">
            <MultiColumnFinder
              tab={this.props.isFinanceApproval ? 'FinanceApproval' : ''}
              items={list}
              typeName={msg().Exp_Clbl_ExpenseType}
              showFavorites={false}
              showHistory={false}
              onClickItem={this.props.onClickExpenseTypeItem}
              onClickCloseButton={this.props.onClickExpenseTypeCloseButton}
              isDefaultSearchMode
              onClickSearch={this.props.onClickExpenseTypeSearch}
              searchResult={searchlist}
              recentItems={recentItemsList}
              onClickSelectByCategory={
                this.props.onClickExpenseTypeSelectByCategory
              }
              IconInfo={ExpIconInfo}
              hintMsg={this.props.hintMsg}
              setProgressBar={this.props.setProgressBar}
              progressBar={this.props.progressBar}
              onClickBackButton={this.props.onClickBackButton}
            />
          </div>
        </div>
      </div>
    );
  }
}
