// @flow
import React from 'react';

import msg from '../../../../../languages';
import TextField from '../../../../fields/TextField';
import LabelWithHint from '../../../../fields/LabelWithHint';
import DialogFrame from '../../../../dialogs/DialogFrame';
import Button from '../../../../buttons/Button';
import FixedHeaderTable, {
  HeaderRow,
  HeaderCell,
  BodyRow,
  BodyCell,
} from '../../../../FixedHeaderTable';

import type {
  CustomEIOption,
  CustomEIOptionList,
  EISearchObj,
} from '../../../../../../domain/models/exp/ExtendedItem';

import './index.scss';
import Icon from '../../../../../../mobile-app/components/atoms/Icon';

const ROOT = 'ts-expenses-modal-lookup-dialog';

type Props = {
  extendedItemLookup: EISearchObj,
  eiRecentlyUsed: CustomEIOptionList,
  onClickSearchLookup: (
    id: string,
    query: string
  ) => Promise<CustomEIOptionList>,
  onClickCustomObjectOption: (item: CustomEIOption) => void,
  onClickHideDialogButton: () => void,
};

type State = {
  isSearch: boolean,
  keyword: string,
  records: Array<CustomEIOption>,
  hasMore: boolean,
};

export default class EILookupDialog extends React.Component<Props, State> {
  state = {
    isSearch: false,
    keyword: '',
    // false positive
    // eslint-disable-next-line react/no-unused-state
    records: [],
    hasMore: false,
  };

  onKeyPress = (e: any) => {
    const lookupId = this.props.extendedItemLookup.extendedItemCustomId;
    if (e.key === 'Enter' && lookupId) {
      e.preventDefault();
      this.props
        .onClickSearchLookup(lookupId, this.state.keyword)
        .then((res: CustomEIOptionList) =>
          this.setState({
            // false positive
            // eslint-disable-next-line react/no-unused-state
            records: res.records,
            hasMore: res.hasMore,
            isSearch: true,
          })
        );
    }
  };

  onChangeSearchField = (e: any) => {
    this.setState({
      keyword: e.target.value,
    });
  };

  render() {
    const { isSearch, keyword } = this.state;
    const items = isSearch ? this.state : this.props.eiRecentlyUsed;
    const label = isSearch
      ? msg().Exp_Lbl_SearchResult
      : msg().Exp_Lbl_RecentlyUsedItems;
    const count = items.hasMore ? '100+' : items.records.length;
    const name = this.props.extendedItemLookup.name || '';
    const title = `${name} ${msg().Exp_Lbl_Select}`;

    return (
      <DialogFrame
        title={title}
        hide={this.props.onClickHideDialogButton}
        className={`${ROOT}__dialog-frame`}
        footer={
          <DialogFrame.Footer>
            <LabelWithHint
              className={`${ROOT}__hint-msg`}
              text={msg().Exp_Lbl_Hint}
              infoAlign="left"
              hintMsg={this.props.extendedItemLookup.hintMsg || ''}
            />
            <Button type="default" onClick={this.props.onClickHideDialogButton}>
              {msg().Com_Btn_Cancel}
            </Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__inner`}>
          <div>{msg().Exp_Lbl_SearchCodeOrName}</div>

          <div className={`${ROOT}__search-field`}>
            <TextField
              className={`${ROOT}__search-field-input`}
              onKeyPress={this.onKeyPress}
              value={keyword}
              placeholder={msg().Com_Lbl_Search}
              onChange={this.onChangeSearchField}
            />

            <Icon
              className={`${ROOT}__search-btn`}
              type="search"
              color="#AFADAB"
            />
          </div>

          <div className={`${ROOT}__label`}>
            <span> {label} </span>
            <span className={`${ROOT}__count`}>
              {isSearch && `${count} ${msg().Exp_Lbl_RecordCount}`}
            </span>
          </div>
          <FixedHeaderTable
            scrollableClass={`${ROOT}__scrollable`}
            className={`${ROOT}--is-ellipsis`}
          >
            <HeaderRow>
              <HeaderCell className={`${ROOT}__code`}>
                {msg().Exp_Lbl_Code}
              </HeaderCell>
              <HeaderCell className={`${ROOT}__name`}>
                {msg().Exp_Lbl_Name}
              </HeaderCell>
            </HeaderRow>
            {items.records.map((item, idx) => {
              return (
                <BodyRow
                  key={idx}
                  className={`${ROOT}__row`}
                  onClick={() => this.props.onClickCustomObjectOption(item)}
                >
                  <BodyCell className={`${ROOT}__code`}>{item.code}</BodyCell>
                  <BodyCell className={`${ROOT}__name`}>{item.name}</BodyCell>
                </BodyRow>
              );
            })}
          </FixedHeaderTable>
          {this.state.hasMore && (
            <span className={`${ROOT}__message`}>
              {msg().Com_Lbl_TooManySearchResults}
            </span>
          )}
        </div>
      </DialogFrame>
    );
  }
}
