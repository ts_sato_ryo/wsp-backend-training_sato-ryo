// @flow
import React from 'react';

import msg from '../../../../../languages';
import MultiColumnFinder from '../../../../dialogs/MultiColumnFinder';
import type {
  CostCenterList,
  CostCenter,
} from '../../../../../../domain/models/exp/CostCenter';
import '../../../../Modal.scss';

type Props = {
  isFinanceApproval: boolean,
  hintMsg?: string,
  costCenterList: CostCenterList,
  costCenterSearchList: CostCenterList,
  costCenterRecentItems: CostCenterList,
  onClickHideDialogButton: () => void,
  onClickCostCenterListItem: (CostCenter) => void,
  onClickCostCenterSearch: (string) => void,
  onClickCostCenterSelectByCategory: () => void,
};
export default class CostCenterSelect extends React.Component<Props> {
  render() {
    return (
      <div className="ts-modal">
        <button
          className="ts-modal__overlay"
          onClick={this.props.onClickHideDialogButton}
        />
        <div className="ts-modal__wrap" style={{ width: 865 }}>
          <div className="ts-modal__contents">
            <MultiColumnFinder
              tab={this.props.isFinanceApproval ? 'FinanceApproval' : ''}
              items={this.props.costCenterList}
              typeName={msg().Exp_Clbl_CostCenter}
              showFavorites={false}
              showHistory={false}
              parentSelectable
              onClickItem={this.props.onClickCostCenterListItem}
              onClickCloseButton={this.props.onClickHideDialogButton}
              isDefaultSearchMode
              onClickSearch={this.props.onClickCostCenterSearch}
              searchResult={this.props.costCenterSearchList}
              recentItems={this.props.costCenterRecentItems}
              onClickSelectByCategory={
                this.props.onClickCostCenterSelectByCategory
              }
              hintMsg={this.props.hintMsg}
            />
          </div>
        </div>
      </div>
    );
  }
}
