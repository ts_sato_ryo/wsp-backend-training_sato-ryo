// @flow
import React from 'react';

import msg from '../../../../../languages';
import DialogFrame from '../../../../dialogs/DialogFrame';
import Button from '../../../../buttons/Button';
import TextField from '../../../../fields/TextField';

import './index.scss';

const ROOT = 'ts-expenses-modal-approval';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */

type Props = {
  title: string,
  onChangeName: (string) => void,
  onClickHideDialogButton: () => void,
  onClickDeleteSearchCondition: () => void,
  inputError: string,
  selectedConditionName: string,
};

export default class ApprovalDialog extends React.Component<Props> {
  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    this.props.onChangeName(e.target.value);
  };

  render() {
    return (
      <DialogFrame
        title={this.props.title}
        hide={this.props.onClickHideDialogButton}
        className={`${ROOT}__dialog-frame`}
        footer={
          <DialogFrame.Footer>
            <Button type="default" onClick={this.props.onClickHideDialogButton}>
              {msg().Com_Btn_Cancel}
            </Button>
            <Button
              type="destructive"
              onClick={this.props.onClickDeleteSearchCondition}
            >
              {msg().Com_Btn_Delete}
            </Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__inner`}>
          <div className={`${ROOT}__inner__condition-name`}>
            {msg().Exp_Lbl_SearchConditionName}
          </div>
          <TextField
            className={`${ROOT}__inner__value`}
            onChange={this.onChange}
            value={this.props.selectedConditionName}
            disabled
          />
        </div>
        <div className={`${ROOT}__error`}>{this.props.inputError}</div>
      </DialogFrame>
    );
  }
}
