// @flow
import React from 'react';

import msg from '../../../../../languages';
import TextField from '../../../../fields/TextField';
import LabelWithHint from '../../../../fields/LabelWithHint';
import DialogFrame from '../../../../dialogs/DialogFrame';
import Button from '../../../../buttons/Button';
import FixedHeaderTable, {
  HeaderRow,
  HeaderCell,
  BodyRow,
  BodyCell,
} from '../../../../FixedHeaderTable';

import type {
  VendorItem,
  VendorItemList,
} from '../../../../../../domain/models/exp/Vendor';

import '../ExtendedItem/index.scss';
import Icon from '../../../../../../mobile-app/components/atoms/Icon';

const ROOT = 'ts-expenses-modal-lookup-dialog';

type Props = {
  companyId: string,
  hintMsg?: string,
  onClickHideDialogButton: () => void,
  onClickVendorItem: (item: VendorItem) => void,
  onVendorSearch: (id: string, query: string) => Promise<VendorItemList>,
  vendorRecentlyUsed: VendorItemList,
};

type State = {
  isSearch: boolean,
  keyword: string,
  records: Array<VendorItem>,
  hasMore: boolean,
};

export default class VendorLookupDialog extends React.Component<Props, State> {
  state = {
    isSearch: false,
    keyword: '',
    // false positive
    // eslint-disable-next-line react/no-unused-state
    records: [],
    hasMore: false,
  };

  onKeyPress = (e: any) => {
    const { companyId } = this.props;

    if (e.key === 'Enter' && companyId) {
      e.preventDefault();
      this.props
        .onVendorSearch(companyId, this.state.keyword)
        .then((res: VendorItemList) =>
          this.setState({
            // false positive
            // eslint-disable-next-line react/no-unused-state
            records: res.records,
            hasMore: res.hasMore,
            isSearch: true,
          })
        );
    }
  };

  onChangeSearchField = (e: any) => {
    this.setState({
      keyword: e.target.value,
    });
  };

  render() {
    const { isSearch, keyword } = this.state;
    const items = isSearch ? this.state : this.props.vendorRecentlyUsed;
    const label = isSearch
      ? msg().Exp_Lbl_SearchResult
      : msg().Exp_Lbl_RecentlyUsedItems;
    const count = items.hasMore ? '100+' : items.records.length;
    const hintText = this.props.hintMsg ? msg().Exp_Lbl_Hint : '';

    return (
      <DialogFrame
        title={msg().Exp_Lbl_Vendor}
        hide={this.props.onClickHideDialogButton}
        className={`${ROOT}__dialog-frame`}
        footer={
          <DialogFrame.Footer
            sub={
              <LabelWithHint
                text={hintText}
                hintMsg={this.props.hintMsg}
                infoAlign="left"
              />
            }
          >
            <Button type="default" onClick={this.props.onClickHideDialogButton}>
              {msg().Com_Btn_Cancel}
            </Button>
          </DialogFrame.Footer>
        }
      >
        <div className={`${ROOT}__inner`}>
          <div>{msg().Exp_Lbl_SearchCodeOrName}</div>

          <div className={`${ROOT}__search-field`}>
            <TextField
              className={`${ROOT}__search-field-input`}
              onKeyPress={this.onKeyPress}
              value={keyword}
              placeholder={msg().Com_Lbl_Search}
              onChange={this.onChangeSearchField}
            />

            <Icon
              className={`${ROOT}__search-btn`}
              type="search"
              color="#AFADAB"
            />
          </div>

          <div className={`${ROOT}__label`}>
            <span> {label} </span>
            <span className={`${ROOT}__count`}>
              {isSearch && `${count} ${msg().Exp_Lbl_RecordCount}`}
            </span>
          </div>

          <FixedHeaderTable
            scrollableClass={`${ROOT}__scrollable`}
            className={`${ROOT}--is-ellipsis`}
          >
            <HeaderRow>
              <HeaderCell className={`${ROOT}__code`}>
                {msg().Exp_Lbl_Code}
              </HeaderCell>
              <HeaderCell className={`${ROOT}__name`}>
                {msg().Exp_Lbl_Name}
              </HeaderCell>
            </HeaderRow>
            {items.records.map((item, index) => {
              return (
                <BodyRow
                  key={index}
                  className={`${ROOT}__row`}
                  onClick={() => this.props.onClickVendorItem(item)}
                >
                  <BodyCell className={`${ROOT}__code`}>{item.code}</BodyCell>
                  <BodyCell className={`${ROOT}__name`}>{item.name}</BodyCell>
                </BodyRow>
              );
            })}
          </FixedHeaderTable>
          {this.state.hasMore && (
            <span className={`${ROOT}__message`}>
              {msg().Com_Lbl_TooManySearchResults}
            </span>
          )}
        </div>
      </DialogFrame>
    );
  }
}
