// @flow
import React from 'react';

import { get } from 'lodash';
import msg from '../../../../../../../languages';
import RecordDate from '../../../../RecordItem/RecordDate';
import AmountField from '../../../../../../fields/AmountField';
import SelectField from '../../../../../../fields/SelectField';
import LabelWithHint from '../../../../../../fields/LabelWithHint';
import IconButton from '../../../../../../../../mobile-app/components/atoms/IconButton';
import { type CustomHint } from '../../../../../../../../domain/models/exp/CustomHint';

import './index.scss';

type ChildRecord = {
  amount: number,
  localAmount: number,
  recordDate: string,
  expTypeId: string,
};

export type expTypeDisplay = {
  value: string,
  text: string,
};

type Props = {
  record: ChildRecord,
  expTypesDisplay: Array<expTypeDisplay>,
  index: number,
  customHint: CustomHint,
  parentRecordIdx: number,
  recordItemsNumber: number,
  currencyDecimal: number,
  touched: {
    records?: Array<*>,
  },
  errors: {
    records?: Array<*>,
  },
  useForeignCurrency: boolean,
  onRemoveBtnClick: (idx: number) => void,
  onChangeRecordDate: (idx: number, date: string) => void,
  onChangeAmountField: (idx: number, amount: number | null) => void,
  onChangeExpenseType: (expTypeId: string, recordItemIndex: number) => void,
};

const RecordItemRow = ({
  record,
  errors,
  touched,
  onRemoveBtnClick,
  onChangeRecordDate,
  onChangeAmountField,
  onChangeExpenseType,
  parentRecordIdx,
  index,
  recordItemsNumber,
  expTypesDisplay,
  currencyDecimal,
  useForeignCurrency,
  customHint,
}: Props) => {
  const ROOT = 'ts-expenses-modal-record-items__create-row';

  const targetRecord = `records.${parentRecordIdx}.items.${index}`;
  const expTypeError = get(errors, `${targetRecord}.expTypeId`);
  const isExpTypeTouched = get(touched, `${targetRecord}.expTypeId`);

  return (
    <div className={`${ROOT}`}>
      <RecordDate
        className={`${ROOT}-date`}
        recordDate={record.recordDate}
        targetRecord={targetRecord}
        hintMsg={customHint.recordDate}
        onChangeRecordDate={(selectedDate) =>
          onChangeRecordDate(index, selectedDate)
        }
        readOnly={false}
        errors={errors}
        touched={touched}
      />

      <div className={`${ROOT}-expense-type`}>
        <LabelWithHint
          text={msg().Exp_Clbl_ExpenseType}
          hintMsg={customHint.recordExpenseType}
          isRequired
        />
        <SelectField
          className={`${ROOT}-expense-type ts-select-input`}
          onChange={(e) => {
            const expTypeId = e.target.value;
            return onChangeExpenseType(expTypeId, index);
          }}
          options={expTypesDisplay}
          value={record.expTypeId || ''}
        />
        {expTypeError && isExpTypeTouched && (
          <div className="input-feedback">{msg()[expTypeError]}</div>
        )}
      </div>

      <div className={`${ROOT}-amount`}>
        <p className="key">
          <span className="is-required">*</span>
          &nbsp;{msg().Exp_Clbl_Amount}
        </p>

        <AmountField
          className="input_right-aligned"
          value={useForeignCurrency ? record.localAmount : record.amount}
          fractionDigits={currencyDecimal}
          onBlur={(amount) => onChangeAmountField(index, amount)}
        />
      </div>

      {recordItemsNumber > 2 && (
        <IconButton
          icon="close-copy"
          size="small"
          className={`${ROOT}-remove`}
          onClick={() => onRemoveBtnClick(index)}
        />
      )}
    </div>
  );
};

export default RecordItemRow;
