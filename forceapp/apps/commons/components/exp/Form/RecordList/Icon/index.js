// @flow
import React from 'react';
import _ from 'lodash';
import ImgIconAttention from '../../../../../images/icons/attention.svg';

import Tooltip from '../../../../Tooltip';
import './index.scss';

type Props = {
  idx: number,
  touched?: {},
  errors?: {},
  tooltip?: string,
  className: string,
};

export default class RecordsIcon extends React.Component<Props> {
  findPairTouched: ({}, {}, string) => void;

  findPairTouched = (errors: {}, touched: {}, path: string) => {
    let find = false;
    const self = this;
    _.forEach(errors, (error, key) => {
      if (typeof error === 'object') {
        const ret = self.findPairTouched(error, touched, `${path}${key}.`);
        if (ret) {
          find = true;
          return false;
        }
      }
      if (_.get(touched, `${path}${key}`)) {
        find = true;
        return false;
      }
      return true;
    });
    return find;
  };

  render() {
    const { errors, touched, idx, tooltip } = this.props;
    const ROOT = this.props.className;
    const recordErrors = _.get(errors, `records.${idx}`);
    const recordExpTypeError = _.get(
      errors,
      `records.${idx}.items[0].expTypeId`
    );
    const recordItemsNumberError = _.get(errors, `records.${idx}.items`);

    const recordTouched = _.get(touched, `records.${idx}`);

    let isChecked = '__hidden';
    if (
      (recordErrors &&
        recordTouched &&
        this.findPairTouched(recordErrors, recordTouched, '')) ||
      recordExpTypeError ||
      recordItemsNumberError
    ) {
      isChecked = '__appear';
    }

    return (
      <div className={`${ROOT}`}>
        {tooltip ? (
          <Tooltip align="top" content={tooltip} className={`${ROOT}__tooltip`}>
            <ImgIconAttention className={`${ROOT}${isChecked}`} />
          </Tooltip>
        ) : (
          <ImgIconAttention className={`${ROOT}${isChecked}`} />
        )}
      </div>
    );
  }
}
