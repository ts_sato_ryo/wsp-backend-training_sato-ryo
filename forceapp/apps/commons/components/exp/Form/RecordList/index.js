// @flow
import React from 'react';

import { type Report } from '../../../../../domain/models/exp/Report';

import ButtonArea from './ButtonArea';
import List from './List';

const ROOT = 'ts-expenses__form-records__list';

export type Props = {
  baseCurrencyDecimal: number,
  baseCurrencySymbol: string,
  checkboxes: Array<number>,
  expReport: Report,
  getImage: () => void,
  isExpenseRequest?: boolean,
  isReportPendingOrApproved?: boolean,
  isNewReportFromPreRequest?: boolean,
  onChangeCheckBox: (number) => void,
  onClickDeleteRecordItem: () => void,
  onClickNewRecordButton: () => void,
  onClickCloneRecordButton: () => void,
  onClickRecord: (number) => void,
  onClickOpenLibraryButton: () => void,
  openEditMenu: boolean,
  readOnly: boolean,
  recordIdx: number,
  touched: {|
    records: Array<*>,
  |},
  errors: {|
    accountingDate?: string,
    records: Array<*>,
  |},
  isFinanceApproval?: boolean,
};

export default class RecordList extends React.Component<Props> {
  renderList() {
    return (
      <List
        baseCurrencyDecimal={this.props.baseCurrencyDecimal}
        baseCurrencySymbol={this.props.baseCurrencySymbol}
        checkboxes={this.props.checkboxes}
        errors={this.props.errors}
        getImage={this.props.getImage}
        isExpenseRequest={this.props.isExpenseRequest}
        isNewReportFromPreRequest={this.props.isNewReportFromPreRequest}
        onChangeCheckBox={this.props.onChangeCheckBox}
        onClickRecord={this.props.onClickRecord}
        readOnly={this.props.readOnly}
        recordIdx={this.props.recordIdx}
        records={this.props.expReport.records}
        report={this.props.expReport}
        touched={this.props.touched}
        isFinanceApproval={this.props.isFinanceApproval}
      />
    );
  }

  render() {
    const buttonAreaReadOnly = this.props.isReportPendingOrApproved
      ? true
      : this.props.readOnly;

    return (
      <div className={ROOT}>
        <ButtonArea
          readOnly={buttonAreaReadOnly}
          checkboxes={this.props.checkboxes}
          openEditMenu={this.props.openEditMenu}
          onClickNewRecordButton={this.props.onClickNewRecordButton}
          onClickDeleteRecordItem={this.props.onClickDeleteRecordItem}
          onClickCloneRecordButton={this.props.onClickCloneRecordButton}
          errors={this.props.errors}
          isFinanceApproval={this.props.isFinanceApproval}
          isExpenseRequest={this.props.isExpenseRequest}
          onClickOpenLibraryButton={this.props.onClickOpenLibraryButton}
        />
        {this.renderList()}
      </div>
    );
  }
}
