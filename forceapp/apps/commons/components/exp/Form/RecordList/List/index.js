/* @flow */
import * as React from 'react';

import { get } from 'lodash';
import SFPopover from '../../../../SFPopover';
import msg from '../../../../../languages';

import {
  RECEIPT_TYPE,
  type Record,
  isRecordItemized,
} from '../../../../../../domain/models/exp/Record';
import { type Report } from '../../../../../../domain/models/exp/Report';
import ItemsDetail from '../ItemsDetail';

import DateUtil from '../../../../../utils/DateUtil';
import FormatUtil from '../../../../../utils/FormatUtil';

import RecordIcon from '../Icon';
import IconCheckDetail from '../../../../../images/icons/checkDetail.svg';

import './index.scss';

const ROOT = 'ts-expenses__form-records__list';
const CHECKBOX_CLASS = `${ROOT}__item__wrapbox__checkbox__input`;

type Props = {
  isExpenseRequest?: boolean,
  isNewReportFromPreRequest?: boolean,
  recordIdx: number,
  report: Report,
  records: Array<Record>,
  checkboxes: Array<number>,
  onClickRecord: (number) => void,
  onChangeCheckBox: (number) => void,
  readOnly: boolean,
  errors: {|
    accountingDate?: string,
    records: Array<Record>,
  |},
  touched: {|
    records: Array<Record>,
  |},
  baseCurrencySymbol: string,
  baseCurrencyDecimal: number,
  getImage: (void) => any,
  isFinanceApproval?: boolean,
};

export default class RecordsList extends React.Component<Props> {
  renderItem(item: Record, i: number) {
    const { report } = this.props;
    const isNewRecord = !item.recordId;
    // valid if it has preRequestId and no reportId
    const isValidRequest = report.preRequestId && !report.reportId;

    // Do not show new record in RecordList
    if (isNewRecord && !isValidRequest) {
      return null;
    }

    const isActive = i === this.props.recordIdx ? 'active' : '';
    const isChecked = this.props.checkboxes.indexOf(i) > -1;
    const hasAttachmentBody = item.receiptFileId;
    const isReceiptRequired = item.fileAttachment === RECEIPT_TYPE.Required;
    const isReceiptOptional = item.fileAttachment === RECEIPT_TYPE.Optional;
    const hasRemarks = item.items[0].remarks;
    const hasSelectedRoute = item.routeInfo && item.routeInfo.selectedRoute;
    let image = null;
    let iconWithPopover = <div className={`${ROOT}__icon-item-wrapper`} />;
    switch (item.recordType) {
      case 'General':
      case 'HotelFee':
      case 'FixedAllowanceSingle':
      case 'FixedAllowanceMulti':
        if (isReceiptRequired) {
          if (hasAttachmentBody) {
            image = this.props.getImage().receipt();
          } else {
            image = this.props
              .getImage()
              .warningReceipt(i, ROOT, this.props.errors, this.props.touched);
          }
        } else if (isReceiptOptional) {
          if (hasAttachmentBody || hasRemarks) {
            image = this.props.getImage().receipt();
          } else {
            image = this.props
              .getImage()
              .warningReceipt(i, ROOT, this.props.errors, this.props.touched);
          }
        }

        if (isRecordItemized(item.recordType)) {
          const popoverContent = (
            <ItemsDetail
              record={item}
              baseCurrencySymbol={this.props.baseCurrencySymbol}
              baseCurrencyDecimal={this.props.baseCurrencyDecimal}
            />
          );

          iconWithPopover = (
            <SFPopover
              align="top left"
              triggerAction="hover"
              hasStaticAlignment
              body={popoverContent}
              triggerClassName={`${ROOT}__icon-item-wrapper`}
            >
              <button type="button" className={`${ROOT}__icon-item-btn`}>
                <IconCheckDetail
                  className={`${ROOT}__icon-item-btn-icon`}
                  aria-hidden="true"
                />
              </button>
            </SFPopover>
          );
        }
        break;
      case 'TransitJorudanJP':
        if (hasSelectedRoute) {
          image = this.props.getImage().routeSelected();
        } else {
          image = this.props
            .getImage()
            .warningRoute(i, ROOT, this.props.errors, this.props.touched);
        }
        break;
      default:
        break;
    }

    const amount = FormatUtil.formatNumber(
      item.items[0].amount,
      this.props.baseCurrencyDecimal
    );

    const subTotal = (
      <div>
        {this.props.baseCurrencySymbol} {amount}
      </div>
    );

    let foreignTotal = null;
    if (item.items[0].useForeignCurrency) {
      const foreignSymbol = get(item, 'items.0.currencyInfo.symbol', '');
      const foreignDecimal = get(item, 'items.0.currencyInfo.decimalPlaces', 0);
      const foreignAmount = FormatUtil.formatNumber(
        item.items[0].localAmount,
        foreignDecimal
      );

      foreignTotal = (
        <div className={`${ROOT}__foreign-total`}>
          {foreignSymbol} {foreignAmount}
        </div>
      );
    }

    // enable record if isNewReport
    const recordItemBtnReadOnly = this.props.isNewReportFromPreRequest
      ? false
      : this.props.readOnly;

    const isDisabled =
      this.props.errors.accountingDate || recordItemBtnReadOnly
        ? 'is-disabled'
        : '';

    // for multiple fixed allowance, append amount option lable
    let expTypeName = item.items[0].expTypeName;
    if (
      item.recordType === 'FixedAllowanceMulti' &&
      item.items[0].fixedAllowanceOptionLabel
    ) {
      expTypeName = `${item.items[0].expTypeName} : ${item.items[0].fixedAllowanceOptionLabel}`;
    }

    return (
      <div
        key={`record${i}`}
        className={`${ROOT}__item ${isActive} ${isDisabled}`}
        onClick={(e) => {
          if (!(e.target.className === CHECKBOX_CLASS)) {
            this.props.onClickRecord(i);
          }
        }}
      >
        <RecordIcon
          idx={i}
          errors={this.props.errors}
          touched={this.props.touched}
          className={`${ROOT}__item__wrapbox__icon`}
        />
        <div className={`${ROOT}__item__wrapbox__checkbox`}>
          {!this.props.isFinanceApproval && (
            <input
              type="checkbox"
              onMouseDown={(e) => {
                e.stopPropagation();
              }}
              onChange={() => this.props.onChangeCheckBox(i)}
              checked={isChecked}
              disabled={this.props.readOnly}
              className={CHECKBOX_CLASS}
            />
          )}
        </div>

        {iconWithPopover}

        <div className={`${ROOT}__item__date`}>
          {DateUtil.format(item.recordDate)}
        </div>
        <div className={`${ROOT}__item__type`}>{expTypeName}</div>
        <div className={`${ROOT}__item__amount`}>
          {subTotal}
          {foreignTotal}
        </div>
        {this.props.isExpenseRequest ? null : (
          <div className={`${ROOT}__item__proof`}>{image}</div>
        )}
        <div className={`${ROOT}__item__remark`}>
          {item.routeInfo && item.routeInfo.origin && item.routeInfo.arrival && (
            <div className={`${ROOT}__detail__route`}>
              {item.routeInfo.origin.name}
              &nbsp;-&nbsp;
              {item.routeInfo.arrival.name}
            </div>
          )}
          {item.items[0].remarks}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__item__header`}>
          <div className={`${ROOT}__item__wrapbox__checkbox`}>
            {/* <input type="checkbox" /> */}
          </div>
          <RecordIcon className={`${ROOT}__item__wrapbox__icon`} idx={0} />
          <div className={`${ROOT}__item__sub-item`} />
          <div className={`${ROOT}__item__date`}>{msg().Exp_Clbl_Date}</div>
          <div className={`${ROOT}__item__type`}>
            {msg().Exp_Clbl_ExpenseType}
          </div>
          <div className={`${ROOT}__item__amount`}>{msg().Exp_Clbl_Amount}</div>
          {this.props.isExpenseRequest ? null : (
            <div className={`${ROOT}__item__proof`}>
              {msg().Exp_Lbl_Evidence}
            </div>
          )}
          <div className={`${ROOT}__item__remark`}>{msg().Exp_Lbl_Detail}</div>
        </div>
        {this.props.records.length === 0 ? (
          <div className={`${ROOT}__empty`}>
            {msg().Exp_Msg_CreateNewRecord}
          </div>
        ) : (
          this.props.records.map((item, i) => this.renderItem(item, i))
        )}
      </div>
    );
  }
}
