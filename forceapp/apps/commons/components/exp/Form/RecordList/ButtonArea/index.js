/* @flow */
import React from 'react';
import msg from '../../../../../languages';
import Button from '../../../../buttons/Button';
import DropDown from '../../../../fields/Dropdown';
import { NEW_RECORD_OPTIONS } from '../../../../../../domain/models/exp/Record';

import './index.scss';

const ROOT = 'ts-expenses__form-records-button-area';

type Props = {
  onClickNewRecordButton: () => void,
  readOnly: boolean,
  onClickDeleteRecordItem: () => void,
  onClickOpenLibraryButton: () => void,
  onClickCloneRecordButton: () => void,
  checkboxes: Array<number>,
  errors: {|
    accountingDate?: string,
    records: Array<*>,
  |},
  isFinanceApproval?: boolean,
  isExpenseRequest?: boolean,
};

export default class RecordListButtonArea extends React.Component<Props> {
  createRecord = (value: string) => {
    if (value === NEW_RECORD_OPTIONS.OCR_RECORD) {
      this.props.onClickOpenLibraryButton();
    } else {
      this.props.onClickNewRecordButton();
    }
  };

  render() {
    const { checkboxes, isExpenseRequest } = this.props;
    const isDeleteDisabled = this.props.readOnly || !checkboxes.length;
    const isCloneDisabled =
      this.props.readOnly || !checkboxes.length || checkboxes.length > 10;
    const readOnly = this.props.readOnly || !!this.props.errors.accountingDate;
    const dropDownOptions = [
      { label: msg().Exp_Lbl_Manual, value: NEW_RECORD_OPTIONS.MANUAL },
      {
        label: msg().Exp_Lbl_CreateRecordFromReceipt,
        value: NEW_RECORD_OPTIONS.OCR_RECORD,
      },
    ];

    const newButtonClass = isExpenseRequest ? `${ROOT}__btn-new-request` : ``;
    return (
      <div className={`${ROOT}__contents`}>
        <div className={`${ROOT}__title`}>{msg().Exp_Lbl_RecordsList}</div>

        <div className={`${ROOT}__btn-new-area`}>
          <Button
            type="secondary"
            onClick={this.props.onClickNewRecordButton}
            disabled={readOnly}
            className={`${ROOT}__btn-new ${newButtonClass}`}
          >
            {msg().Exp_Btn_NewRecord}
          </Button>
        </div>

        {!isExpenseRequest && !this.props.isFinanceApproval && (
          <DropDown
            onSelect={(item) => this.createRecord(item.value)}
            options={dropDownOptions}
            label=""
            disabled={readOnly}
          />
        )}
        <div className={`${ROOT}__btn-clone`}>
          {!this.props.isFinanceApproval && (
            <Button
              type="default"
              onClick={this.props.onClickCloneRecordButton}
              disabled={isCloneDisabled}
            >
              {msg().Exp_Lbl_Clone}
            </Button>
          )}
        </div>
        <div className={`${ROOT}__btn-delete`}>
          {!this.props.isFinanceApproval && (
            <Button
              type="destructive"
              onClick={this.props.onClickDeleteRecordItem}
              disabled={isDeleteDisabled}
            >
              {msg().Com_Btn_Delete}
            </Button>
          )}
        </div>
      </div>
    );
  }
}
