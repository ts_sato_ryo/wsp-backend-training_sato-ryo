// @flow
import React from 'react';

import Overlap from '../Overlap';
import TabNav from '../TabNav';
import msg from '../../languages';

import { type Report } from '../../../domain/models/exp/Report';
import { type Props as WorkingDaysProps } from '../../../expenses-pc/modules/ui/expenses/recordItemPane/workingDays';
import { modes } from '../../../requests-pc/modules/ui/expenses/mode';

// Pagination
import SubHeaderPager, {
  type Props as subHeaderPageProps,
} from '../../../finance-approval-pc/components/FinanceApproval/SubHeaderPager';

import './index.scss';

const ROOT = 'ts-expenses';

// Containers passed from HOC
type Containers = {|
  baseCurrency: any,
  dialog: any,
  foreignCurrency: any,
  form: any,

  recordItem: any,
  recordList: any,
  reportList: any,
  reportSummary: any,
  routeForm: any,
  suggest: any,
  workingDays: WorkingDaysProps,
  AccountingPeriodAndRecordDateContainer: any,
|};

export type OverlapProps = {|
  overlap: { report: boolean, record: boolean },
|};

export type CommonProps = {|
  labelObject: () => any,
  mode: string,
  selectedExpReport: Report,
  selectedTab: number,
  onChangeTab: () => void,
|};

type Props = {
  ...CommonProps,
  ...Containers,
  ...OverlapProps,
} & subHeaderPageProps;

export default class Expenses extends React.Component<Props> {
  isMode(mode: string) {
    return this.props.mode === modes[mode];
  }

  render() {
    const ExpensesReport = this.props.reportList;
    const ExpensesForm = this.props.form;
    const ReportSummary = this.props.reportSummary;
    const RecordList = this.props.recordList;
    const RecordItem = this.props.recordItem;
    const BaseCurrency = this.props.baseCurrency;
    const ForeignCurrency = this.props.foreignCurrency;
    const RouteForm = this.props.routeForm;
    const Suggest = this.props.suggest;
    const Dialog = this.props.dialog;

    const disabled = !(
      this.isMode('REPORT_SELECT') || this.isMode('REPORT_EDIT')
    );

    const tabConfig = [
      {
        component: <ExpensesReport filter="NotRequested" />,
        label: msg().Exp_Lbl_ActiveReport,
      },
      {
        component: <ExpensesReport filter="Approved" />,
        label: msg().Exp_Lbl_ApprovedReport,
      },
    ];

    let titleName = this.props.labelObject().reports;
    /* title label chenges according to new report / report list / saved report */
    if (this.props.overlap.report && !this.props.selectedExpReport.reportId) {
      titleName = this.props.labelObject().newReport;
    } else if (
      this.props.overlap.report &&
      this.props.selectedExpReport.reportId
    ) {
      titleName = msg().Exp_Lbl_Report;
    }

    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}-sub-header`}>
          <div className={`${ROOT}-sub-header__title`}>{titleName}</div>
          <SubHeaderPager
            overlap={this.props.overlap}
            requestTotalNum={this.props.requestTotalNum}
            currentRequestIdx={this.props.currentRequestIdx}
            onClickBackButton={this.props.onClickBackButton}
            onClickNextToRequestButton={this.props.onClickNextToRequestButton}
          />
        </div>
        <TabNav
          config={tabConfig}
          selectedTab={this.props.selectedTab}
          onChangeTab={this.props.onChangeTab}
        />
        <Overlap isVisible={this.props.overlap.report}>
          <ExpensesForm
            recordList={RecordList}
            recordItem={RecordItem}
            reportSummary={ReportSummary}
            baseCurrency={BaseCurrency}
            foreignCurrency={ForeignCurrency}
            routeForm={RouteForm}
            dialog={Dialog}
            suggest={Suggest}
            disabled={disabled}
            mode={this.props.mode}
            labelObject={this.props.labelObject}
          />
        </Overlap>
      </div>
    );
  }
}
