// @flow
import React from 'react';

import msg from '../../../languages';
import IconButton from '../../buttons/IconButton';
import iconAddBtn from '../../../images/btnAddWhite.png';

import './index.scss';

const ROOT = 'ts-expenses__header';

type Props = {
  isExpenseRequest?: boolean,
  onClickNewReportButton: () => void,
};

export default class ExpensesHeader extends React.Component<Props> {
  renderNewReportButton() {
    const label = this.props.isExpenseRequest
      ? msg().Exp_Lbl_GeneralExpense
      : msg().Exp_Clbl_NewReport;
    return (
      <IconButton
        className={`${ROOT}__btn`}
        src={iconAddBtn}
        alt="New Report"
        onClick={this.props.onClickNewReportButton}
      >
        <span>{label}</span>
      </IconButton>
    );
  }

  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__btn-container`}>
          {this.renderNewReportButton()}
        </div>
      </div>
    );
  }
}
