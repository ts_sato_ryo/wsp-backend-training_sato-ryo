import PropTypes from 'prop-types';
import React from 'react';
import './PopupWindowNavbar.scss';

const ROOT = 'commons-popup-window-navbar';

export default class PopupWindowNavbar extends React.Component {
  static get propTypes() {
    return {
      title: PropTypes.string.isRequired,
      children: PropTypes.node,
    };
  }

  render() {
    return (
      <div className={ROOT}>
        <div className={`${ROOT}__left`} />
        <div className={`${ROOT}__title`}>{this.props.title}</div>
        <div className={`${ROOT}__right`}>{this.props.children}</div>
      </div>
    );
  }
}
