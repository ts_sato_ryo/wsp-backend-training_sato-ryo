import PropTypes from 'prop-types';
import React from 'react';

import msg from '../languages';

import FormatUtil from '../utils/FormatUtil';

import TextField from './fields/TextField';

import './Tax.scss';

export default class CurrencyChild extends React.Component {
  static get propTypes() {
    return {
      readOnly: PropTypes.bool.isRequired,
      recordItemIndex: PropTypes.number.isRequired,
      baseCurrencyFractionDigits: PropTypes.number.isRequired,
      currencyId: PropTypes.string,
      itemDate: PropTypes.number,
      localAmount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      getCurrencyRateInfo: PropTypes.func.isRequired,
      onChangeRecordItemLocalAmount: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      localAmount: 0,
      amount: 0,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocussedInputLocalAmount: false,
    };
  }

  onChangeLocalAmount(event) {
    this.props.onChangeRecordItemLocalAmount(
      event.target.value,
      this.props.recordItemIndex
    );
  }

  getLocalAmount() {
    const currencyRateInfo = this.props.getCurrencyRateInfo(
      this.props.currencyId,
      this.props.itemDate
    );
    if (this.state.isFocussedInputLocalAmount) {
      return FormatUtil.convertToEditingCurrency(
        this.props.localAmount,
        currencyRateInfo.fractionDigits
      );
    } else {
      return FormatUtil.convertToDisplayingCurrency(
        this.props.localAmount,
        currencyRateInfo.fractionDigits
      );
    }
  }

  focusInputLocalAmount() {
    this.setState({
      isFocussedInputLocalAmount: true,
    });
  }

  blurInputLocalAmount() {
    this.setState({
      isFocussedInputLocalAmount: false,
    });
  }

  getTotalAmount() {
    return FormatUtil.convertToDisplayingCurrency(
      this.props.amount,
      this.props.baseCurrencyFractionDigits
    );
  }

  renderInputLocalAmount() {
    return (
      <div className="value">
        <TextField
          type="text"
          value={this.getLocalAmount()}
          onChange={(event) => this.onChangeLocalAmount(event)}
          onFocus={() => this.focusInputLocalAmount()}
          onBlur={() => this.blurInputLocalAmount()}
          disabled={this.props.readOnly}
        />
      </div>
    );
  }

  renderAmountText() {
    return (
      <div className="value">
        <TextField
          type="text"
          value={this.getTotalAmount()}
          readOnly
          disabled
        />
      </div>
    );
  }

  render() {
    return (
      <RenderCurrencyItemArea
        leftLabel={msg().Exp_Lbl_LocalAmount}
        leftArea={this.renderInputLocalAmount()}
        rightLabel={msg().Exp_Lbl_Amount}
        rightArea={this.renderAmountText()}
      />
    );
  }
}

class RenderCurrencyItemArea extends React.Component {
  static get propTypes() {
    return {
      leftLabel: PropTypes.node.isRequired,
      leftArea: PropTypes.node.isRequired,
      rightLabel: PropTypes.node.isRequired,
      rightArea: PropTypes.node.isRequired,
    };
  }

  renderLocalAmountLabel() {
    return (
      <div className="slds-col slds-size--3-of-12 slds-align-middle">
        <div className="key">&nbsp;{this.props.leftLabel}</div>
        <div className="separate">:</div>
      </div>
    );
  }

  renderLocalAmount() {
    return (
      <div className="slds-col slds-size--3-of-12 slds-align-middle">
        {this.props.leftArea}
      </div>
    );
  }

  renderAmountLabel(column) {
    return (
      <div className={`slds-col slds-size--${column}-of-12 slds-align-middle`}>
        <div className="key">&nbsp;{this.props.rightLabel}</div>
        <div className="separate">:</div>
      </div>
    );
  }

  renderAmount() {
    return (
      <div className="slds-col slds-size--3-of-12 slds-align-middle right">
        {this.props.rightArea}
      </div>
    );
  }

  render() {
    return (
      <div className="slds-grid">
        {this.renderLocalAmountLabel()}
        {this.renderLocalAmount()}
        <div className="slds-col slds-size--1-of-12 slds-align-middle" />
        {this.renderAmountLabel(2)}
        {this.renderAmount()}
      </div>
    );
  }
}
