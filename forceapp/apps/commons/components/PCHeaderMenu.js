import PropTypes from 'prop-types';
import React from 'react';

import './PCHeaderMenu.scss';

export default class PCHeaderMenu extends React.Component {
  static get propTypes() {
    return {
      closeHeaderMenu: PropTypes.func,
      openSelectEmployee: PropTypes.func,
      isOpenHeaderMenu: PropTypes.bool,
      menuStyle: PropTypes.object,
    };
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  closeHeaderMenu() {
    this.props.closeHeaderMenu();
  }

  openSelectEmployee() {
    this.props.closeHeaderMenu();
    this.props.openSelectEmployee();
  }

  render() {
    return (
      <div>
        {(() => {
          if (this.props.isOpenHeaderMenu) {
            return (
              <div className="ts-header-menu">
                <div
                  className="ts-header-menu__overlay"
                  onClick={() => this.closeHeaderMenu()}
                />
                <div
                  className="ts-header-menu__wrap"
                  style={this.props.menuStyle}
                >
                  <div className="ts-header-menu__header">
                    <div className="ts-header-menu__header__row">
                      <img
                        className="ts-header-menu__icon-baloon"
                        src={require('../images/POP_Icon001.png')}
                      />
                      <span className="ts-header-menu__header__title">
                        &nbsp;Menu
                      </span>
                      <img
                        onClick={() => this.closeHeaderMenu()}
                        className="ts-header-menu__icon-close ts-header-menu__pointer"
                        src={require('../images/POP_btn_Close.png')}
                      />
                    </div>
                  </div>
                  <div className="ts-header-menu__body">
                    <div className="ts-header-menu__body__applications">
                      <div className="ts-header-menu__body__item">
                        <img
                          className="ts-header-menu__icon-question"
                          src={require('../images/icon_question.png')}
                        />
                        <span
                          className="ts-header-menu__pointer"
                          onClick={() => this.openSelectEmployee()}
                        >
                          &nbsp;Request as delegate...
                        </span>
                      </div>
                      <div className="ts-header-menu__body__item">
                        <img
                          className="ts-header-menu__icon-question"
                          src={require('../images/icon_question.png')}
                        />
                        <span>&nbsp;Detail Information...</span>
                      </div>
                      <div className="ts-header-menu__body__item">
                        <img
                          className="ts-header-menu__icon-question"
                          src={require('../images/icon_question.png')}
                        />
                        <span className="ts-header-menu__pointer">
                          &nbsp;Sign Out...
                        </span>
                      </div>
                      <div className="ts-header-menu__body__item">
                        <img
                          className="ts-header-menu__icon-question"
                          src={require('../images/icon_question.png')}
                        />
                        <span>&nbsp;Personal Settings...</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          } else {
            return '';
          }
        })()}
      </div>
    );
  }
}
