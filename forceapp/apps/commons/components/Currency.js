import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';

import msg from '../languages';

import FormatUtil from '../utils/FormatUtil';
import ObjectUtil from '../utils/ObjectUtil';

import TextField from './fields/TextField';

import './Currency.scss';
import ImgEditOn from '../images/btnEditOn.png';
import ImgEditOff from '../images/btnEditOff.png';

/**
 * @deprecated fieldsets/CurrencyFieldSet を使うこと。
 * (preapprovals-pc で参照されてるため残しているが、後々削除する)
 */
export default class Currency extends React.Component {
  static get propTypes() {
    return {
      caller: PropTypes.string,
      readOnly: PropTypes.bool.isRequired,
      baseCurrencyCode: PropTypes.string.isRequired,
      baseCurrencyFractionDigits: PropTypes.number.isRequired,
      useForeignCurrency: PropTypes.bool,
      currencyList: PropTypes.array.isRequired,
      totalAmount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      hasMultipleRecordItems: PropTypes.bool,
      currencyId: PropTypes.string,
      isEditingCurrencyRate: PropTypes.bool,
      currencyRate: PropTypes.number,
      originalCurrencyRate: PropTypes.number,
      localAmount: PropTypes.number,
      itemDate: PropTypes.number,
      onChangeFormParam: PropTypes.func,
      getCurrencyRateInfo: PropTypes.func.isRequired,
      onChangeCurrencyRateEditMode: PropTypes.func.isRequired,
      onChangeRecordItemLocalAmount: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      hasMultipleRecordItems: false,
      isEditingCurrencyRate: false,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocussedInputTotalAmount: false,
      isFocussedInputLocalAmount: false,
    };

    this.onChangeCurrencyRateEditMode = this.onChangeCurrencyRateEditMode.bind(
      this
    );
    this.onChangeCurrencyRate = this.onChangeCurrencyRate.bind(this);
    this.onChangeLocalAmount = this.onChangeLocalAmount.bind(this);
    this.onChangeTotalAmount = this.onChangeTotalAmount.bind(this);
  }

  onChangeTotalAmount(event) {
    this.props.onChangeFormParam('amount', event);
  }

  onChangeCurrencyRateEditMode(isEditing) {
    this.props.onChangeCurrencyRateEditMode(isEditing);
  }

  onChangeCurrencyRate(e) {
    this.props.onChangeFormParam('currencyRate', e);
  }

  onChangeLocalAmount(e) {
    this.props.onChangeRecordItemLocalAmount(e.target.value, 0);
  }

  isSelected(e) {
    this.props.onChangeFormParam('currencyId', e);
  }

  denyEditToTotalAmount() {
    if (this.props.hasMultipleRecordItems) {
      return true;
    }
    return this.props.useForeignCurrency;
  }

  getTotalAmount() {
    if (this.state.isFocussedInputTotalAmount) {
      return FormatUtil.convertToEditingCurrency(
        this.props.totalAmount,
        this.props.baseCurrencyFractionDigits
      );
    } else {
      return FormatUtil.convertToDisplayingCurrency(
        this.props.totalAmount,
        this.props.baseCurrencyFractionDigits
      );
    }
  }

  focusInputTotalAmount() {
    this.setState({
      isFocussedInputTotalAmount: true,
    });
  }

  blurInputTotalAmount() {
    this.setState({
      isFocussedInputTotalAmount: false,
    });
  }

  getLocalAmount() {
    const currencyRateInfo = this.props.getCurrencyRateInfo(
      this.props.currencyId,
      this.props.itemDate
    );
    if (this.state.isFocussedInputLocalAmount) {
      return FormatUtil.convertToEditingCurrency(
        this.props.localAmount,
        currencyRateInfo.fractionDigits
      );
    } else {
      return FormatUtil.convertToDisplayingCurrency(
        this.props.localAmount,
        currencyRateInfo.fractionDigits
      );
    }
  }

  focusInputLocalAmount() {
    this.setState({
      isFocussedInputLocalAmount: true,
    });
  }

  blurInputLocalAmount() {
    this.setState({
      isFocussedInputLocalAmount: false,
    });
  }

  isRateModified() {
    return this.props.currencyRate !== this.props.originalCurrencyRate;
  }

  renderCurrencyText() {
    return (
      <TextField type="text" value={this.props.baseCurrencyCode} readOnly />
    );
  }

  renderSelectCurrency() {
    // 通貨一覧から有効期間外の通貨を除外し、アルファベット順にソート
    // TODO: 本処理の共通化（CurrencyUtilへ）を検討する
    const selectableCurrencyList = this.props.currencyList
      .filter((item) => {
        const from = ObjectUtil.getOrDefault(item, 'validFrom', '0000-01-01');
        const to = ObjectUtil.getOrDefault(item, 'validTo', '9999-12-31');
        return (
          this.props.itemDate === null ||
          moment(this.props.itemDate).isBetween(from, to, null, '[]')
        );
      })
      .sort();

    const selectTypeList = selectableCurrencyList.map((item, i) => (
      <option key={i} value={item.id}>
        {item.code}
      </option>
    ));

    return (
      <div className="value">
        <select
          value={this.props.currencyId}
          onChange={(event) => this.isSelected(event)}
          disabled={this.props.readOnly}
        >
          {selectTypeList}
        </select>
      </div>
    );
  }

  renderInputTotalAmount() {
    return (
      <input
        type="text"
        disabled={
          this.props.readOnly ||
          this.denyEditToTotalAmount() ||
          this.props.useForeignCurrency
        }
        value={this.getTotalAmount()}
        onChange={this.onChangeTotalAmount}
        onFocus={() => this.focusInputTotalAmount()}
        onBlur={() => this.blurInputTotalAmount()}
      />
    );
  }

  renderInputCurrencyRate() {
    const isEditing = this.props.isEditingCurrencyRate || this.isRateModified();
    const imgEdit = isEditing ? ImgEditOn : ImgEditOff;
    const imgEditAlt = isEditing ? 'ImgEditOn' : 'ImgEditOff';

    // 編集ボタン
    const currencyRateEditButton = this.props.readOnly ? null : (
      <div className="ts-tax-auto">
        <img
          src={imgEdit}
          onClick={() => this.onChangeCurrencyRateEditMode(!isEditing)}
          alt={imgEditAlt}
        />
      </div>
    );

    // 通貨ペア表記
    const currencyRateInfo = this.props.getCurrencyRateInfo(
      this.props.currencyId,
      this.props.itemDate
    );
    let currencyPairText;
    if (currencyRateInfo.useInverseRate) {
      currencyPairText = `(${currencyRateInfo.currencyCode}/${
        this.props.baseCurrencyCode
      })`;
    } else {
      currencyPairText = `(${this.props.baseCurrencyCode}/${
        currencyRateInfo.currencyCode
      })`;
    }

    return (
      <div className="slds-grid">
        <div className="slds-col slds-size--7-of-12 slds-align-middle">
          <div className="value">
            <TextField
              type="text"
              className={
                this.props.readOnly && this.isRateModified()
                  ? 'ts-currency--modified'
                  : ''
              }
              value={this.props.currencyRate}
              disabled={
                this.props.readOnly ||
                !this.props.useForeignCurrency ||
                !isEditing
              }
              onChange={this.onChangeCurrencyRate}
            />
            {currencyRateEditButton}
          </div>
        </div>
        <div className="slds-col slds-size--5-of-12 slds-align-middle center">
          {currencyPairText}
        </div>
      </div>
    );
  }

  renderInputLocalAmount() {
    return (
      <input
        type="text"
        value={this.getLocalAmount()}
        disabled={
          this.props.readOnly ||
          !this.props.useForeignCurrency ||
          this.props.hasMultipleRecordItems
        }
        onChange={this.onChangeLocalAmount}
        onFocus={() => this.focusInputLocalAmount()}
        onBlur={() => this.blurInputLocalAmount()}
      />
    );
  }

  render() {
    const expAmountLabel = this.props.hasMultipleRecordItems
      ? msg().Exp_Lbl_TotalAmount
      : msg().Exp_Lbl_Amount;
    const expInputAmountLabel = this.props.useForeignCurrency
      ? msg().Exp_Lbl_LocalAmount
      : expAmountLabel;
    const currencyLabelMap = {
      expenses: {
        leftLabel: msg().Exp_Lbl_Currency,
        rightLabel: expInputAmountLabel,
      },
      applications: {
        leftLabel: msg().Exp_Lbl_Currency,
        rightLabel: msg().Exp_Lbl_Amount,
      },
    };
    return (
      <div>
        <RenderPriceArea
          leftLabel={currencyLabelMap[this.props.caller].leftLabel}
          leftArea={
            this.props.useForeignCurrency
              ? this.renderSelectCurrency()
              : this.renderCurrencyText()
          }
          rightLabel={currencyLabelMap[this.props.caller].rightLabel}
          rightArea={
            this.props.useForeignCurrency
              ? this.renderInputLocalAmount()
              : this.renderInputTotalAmount()
          }
        />
        {(() => {
          if (this.props.useForeignCurrency) {
            return (
              <RenderPriceArea
                leftLabel={msg().Exp_Lbl_Rate}
                leftArea={this.renderInputCurrencyRate()}
                rightLabel={expAmountLabel}
                rightArea={this.renderInputTotalAmount()}
              />
            );
          }
          return <div />;
        })()}
      </div>
    );
  }
}

class RenderPriceArea extends React.Component {
  static get propTypes() {
    return {
      leftLabel: PropTypes.string,
      leftArea: PropTypes.string,
      rightLabel: PropTypes.string,
      rightArea: PropTypes.string,
    };
  }

  static get defaultProps() {
    return {
      leftLabel: '',
      leftArea: '',
      rightLabel: '',
      rightArea: '',
    };
  }

  render() {
    return (
      <div className="slds-grid">
        <div className="slds-col slds-size--6-of-12 slds-align-middle">
          <div className="slds-grid">
            <div className="slds-col slds-size--3-of-12 slds-align-middle">
              <div className="key">{this.props.leftLabel}</div>
            </div>
            <div className="slds-col slds-size--9-of-12 slds-align-middle">
              {this.props.leftArea}
            </div>
          </div>
        </div>
        <div className="slds-col slds-size--5-of-12 slds-align-middle">
          <div className="slds-grid">
            <div className="slds-col slds-size--4-of-12 slds-align-middle right">
              <div className="key">{this.props.rightLabel}</div>
            </div>
            <div className="slds-col slds-size--8-of-12 slds-align-middle">
              <div className="value">{this.props.rightArea}</div>
            </div>
          </div>
        </div>
        <div className="slds-col slds-size--1-of-12 slds-align-middle">
          &nbsp;
        </div>
      </div>
    );
  }
}
