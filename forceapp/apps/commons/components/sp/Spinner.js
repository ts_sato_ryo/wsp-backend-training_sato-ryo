import PropTypes from 'prop-types';
import React from 'react';

import './Spinner.scss';

export default class Spinner extends React.Component {
  static get propTypes() {
    return {
      loading: PropTypes.bool.isRequired,
    };
  }

  render() {
    if (this.props.loading) {
      return (
        <div className="ts-spinner">
          <div className="ts-spinner--medium">
            <img src="/resource/slds/assets/images/spinners/slds_spinner.gif" alt="Loading..." />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
