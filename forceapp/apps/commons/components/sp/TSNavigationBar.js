import React from 'react';
import SLDSChevronLeftIcon from '@salesforce-ux/design-system/assets/icons/utility/chevronleft.svg';

import animationType from '../../constants/animationType';
import msg from '../../languages';

import SPViewBase from './SPViewBase';

import './TSNavigationBar.scss';

/**
 * SPのナビゲーションバーを表示するためのclass
 * 指定されたanimationのタイプで出力するナビゲーションバーの形式を変える
 */
export default class TSNavigationBar extends SPViewBase {
  componentWillMount() {
    const views = this.navigator.state.views;
    this.animation = views[views.length - 1].animation;
  }

  // NOTE: 戻るボタンの処理は各画面で共通となるため、
  // ViewBaseを継承してpopFromNavigatorを直接呼び出せるようにしている
  // FIXME : 本当にこの仕様で問題ないか改めて検討する。
  back() {
    if (this.props.back) {
      this.props.back();
    } else {
      this.popFromNavigator();
    }
  }

  renderItemLeft() {
    if (this.animation === animationType.HORIZONTAL) {
      return (
        <div className="ts-navigation-bar__item ts-navigation-bar__item-left">
          <SLDSChevronLeftIcon
            aria-hidden="true"
            className="ts-navigation-bar__item__icon slds-icon"
            onClick={() => this.back(this)}
          />
        </div>
      );
    }
    return null;
  }

  renderItemRight() {
    if (this.animation === animationType.HORIZONTAL) {
      return (
        <div className="ts-navigation-bar__item ts-navigation-bar__item-right">
          {this.props.rightItem}
        </div>
      );
    } else if (this.animation === animationType.VERTICAL) {
      return (
        <div className="ts-navigation-bar__item ts-navigation-bar__item-right">
          <button
            className="slds-button slds-button--icon-inverse ts-navigation-bar__item__button"
            onClick={() => this.back(this)}
          >
            {msg().Com_Btn_Close}
          </button>
        </div>
      );
    }
    return null;
  }

  render() {
    let marginLeft = 40;
    let marginRight = 60;

    if (this.animation === animationType.HORIZONTAL) {
      marginRight = 10;
      if (this.props.rightItemWidth) {
        marginRight = parseInt(this.props.rightItemWidth) + 24;
      }
    } else if (this.animation === animationType.VERTICAL) {
      marginLeft = 10;
    }

    const divStyle = {
      marginLeft: `${marginLeft}px`,
      marginRight: `${marginRight}px`,
    };

    const navigationBarStyle = {
      backgroundColor: this.navigator.navigationBarColor,
    };

    return (
      <div className="ts-navigation-bar slds" style={navigationBarStyle}>
        {this.renderItemLeft()}
        <div
          className="ts-navigation-bar__item ts-navigation-bar__item-center"
          style={divStyle}
        >
          {this.props.title || this.props.children}
        </div>
        {this.renderItemRight()}
      </div>
    );
  }
}
