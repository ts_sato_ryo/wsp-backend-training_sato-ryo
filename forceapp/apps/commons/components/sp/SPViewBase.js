import PropTypes from 'prop-types';
import React from 'react';

/**
 * 画面(View) のベースクラス
 * 画面遷移用のメソッドが実装されており、すべての View はこのクラスを継承して作成する
 */
export default class SPViewBase extends React.Component {
  static get contextTypes() {
    return {
      navigator: PropTypes.object,
    };
  }

  get navigator() {
    return this.context.navigator;
  }

  /**
   * 次ページの情報を受け取る
   * 詳細はTSNavigatorのpush参照
   * @param {object} options
   *    view:遷移先、animetion:アニメーション種類、props：次画面に引き継ぐprops
   */
  pushToNavigator(options) {
    this.context.navigator.push(options);
  }

  /**
   * 前ページへ移動する際、直前のコンテナー情報を取得する
   * 詳細はTSNavigatorのpop参照
   */
  popFromNavigator() {
    this.context.navigator.pop();
  }

  /**
   * 指定したページへ直接移動する
   * 詳細はTSNavigatorのpopToView参照
   * @param {object} index n回目に遷移したページ
   */
  popToView(index) {
    this.context.navigator.popToView(index);
  }

  render() {
    return null;
  }
}
