import PropTypes from 'prop-types';
import React from 'react';

import './SPHeader.scss';

export default class SPHeader extends React.Component {
  static get propTypes() {
    return {
      title: PropTypes.string.isRequired,
      backgroundColor: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      headerContent: PropTypes.object,
      rightNavigationItem: PropTypes.object,
    };
  }

  static get defaultProps() {
    return {
      headerContent: null,
      rightNavigationItem: null,
    };
  }

  render() {
    const style = {
      backgroundColor: this.props.backgroundColor,
    };
    return (
      <div className="ts-header slds">
        <div className="ts-header__title">
          <div className="ts-header__title-area">
            <div className="ts-header__title-area-main">{this.props.title}</div>
            <div className="ts-header__title-area-main-right">
              {this.props.rightNavigationItem}
            </div>
          </div>
          <div className="ts-header__line" style={style} />
          <div className="ts-header__circle" style={style}>
            <img
              className="ts-header__icon"
              src={this.props.icon}
            />
          </div>
        </div>
        <div className="ts-header__sub">
          {this.props.headerContent}
        </div>
      </div>
    );
  }
}
