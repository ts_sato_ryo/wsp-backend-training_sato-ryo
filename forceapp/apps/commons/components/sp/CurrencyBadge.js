import React from 'react';

import './CurrencyBadge.scss';

/**
 * 通貨バッジ
 * FIXME: 本来は通貨情報が受け取り、それにふさわしい通貨コードを表示するようなコンポーネントになる
 */
export default class CurrencyBadge extends React.Component {
  render() {
    return (
      <div className="ts-currency-badge">
        SGD
      </div>
    );
  }
}
