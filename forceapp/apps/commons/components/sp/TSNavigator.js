import PropTypes from 'prop-types';
import React from 'react';
import { CSSTransition as ReactCSSTransitionGroup } from 'react-transition-group';

import './TSNavigator.scss';

/**
 * ページ遷移を実行するクラス
 * SPViewBaseを通じて各メソッドを呼び出す
 */
export default class TSNavigator extends React.Component {
  static get propTypes() {
    return {
      rootView: PropTypes.func.isRequired,
    };
  }

  static get childContextTypes() {
    return {
      navigator: PropTypes.object,
    };
  }

  constructor(props) {
    super(props);

    /**
     * @property views : pushしたページ情報
     * @property transition : push or pop
     */
    this.state = {
      rootView: this.props.rootView,
      views: [],
      navigationBarColor: '#1D689A',
      index: 0,
    };
  }

  getChildContext() {
    return { navigator: this };
  }

  push(options) {
    this.state.views.push({
      component: options.view,
      animation: options.animation,
      props: options.props,
    });
    this.setState((prevState) => ({
      views: prevState.views,
      index: prevState.index + 1,
    }));
  }

  pop() {
    this.setState(
      (prevState) => ({
        views: prevState.views,
        index: prevState.index - 1,
      }),
      () => {
        this.state.views.pop();
      }
    );
  }

  popToView(index) {
    this.setState(
      (prevState) => ({
        views: prevState.views,
        index: this.index,
      }),
      () => {
        const cnt = this.state.views.length - index;
        for (let i = 0; i < cnt; i++) {
          this.state.views.pop();
        }
      }
    );
  }

  get navigationBarColor() {
    return this.state.navigationBarColor;
  }

  set navigationBarColor(value) {
    this.setState({ navigationBarColor: value });
  }

  render() {
    const RootView = this.state.rootView;

    // pushされたページをdiv(1ページ相当)要素として
    // 上部に重ねていくことでページ遷移を実現する
    const navi = this.state.views.map((item, i) => {
      const Component = item.component;
      const transitionAppear = true;
      return (
        <ReactCSSTransitionGroup
          key={`RCSSTG-${i}`}
          classNames={`ts-${item.animation}`}
          timeout={{ enter: 300, exit: 300 }}
          appear={transitionAppear}
          component="div"
        >
          <div>
            {i >= this.state.index ? null : (
              <Component key={`stackedView-${i}`} {...item.props} />
            )}
          </div>
        </ReactCSSTransitionGroup>
      );
    });

    return (
      <div className="ts-navigator">
        <RootView key="rootView" />
        {navi}
      </div>
    );
  }
}
