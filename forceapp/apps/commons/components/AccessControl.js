// @flow

import * as React from 'react';

import {
  isPermissionSatisfied,
  type TotalTestConditions,
  type DynamicTestConditions,
} from '../../domain/models/access-control/Permission';

export type Props = {|
  ...TotalTestConditions,

  // FIXME: <AccessControl {...conditions}> で済ませたい／Flowの型エラーを解決できず別のpropにした
  conditions?: DynamicTestConditions,

  children: React.Node,
|};

const AccessControl = (props: Props): React.Element<*> => {
  const {
    children: _children,
    conditions: conditionsHash,
    ...totalConditions
  } = props;
  return (
    <React.Fragment>
      {/* FIXME: appear to have issue of spread operator on Flow.  */}
      {/* It looks check for consistency with all types of union */}
      {/* $FlowFixMe v0.101 */}
      {isPermissionSatisfied({ ...totalConditions, ...(conditionsHash || {}) })
        ? props.children
        : null}
    </React.Fragment>
  );
};

export default AccessControl;
