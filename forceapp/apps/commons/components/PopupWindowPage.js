import PropTypes from 'prop-types';
import React from 'react';

import './PopupWindowPage.scss';

const ROOT = 'commons-popup-window-page';

export default class PopupWindowPage extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
    };
  }

  render() {
    return <div className={ROOT}>{this.props.children}</div>;
  }
}
