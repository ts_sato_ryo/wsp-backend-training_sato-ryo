import PropTypes from 'prop-types';
import React from 'react';

export default class TSIconButton extends React.Component {
  static get propTypes() {
    return {
      buttonClassName: PropTypes.string.isRequired,
      iconClassName: PropTypes.string.isRequired,
      imagePath: PropTypes.string.isRequired,
      buttonText: PropTypes.string.isRequired,
      disabled: PropTypes.bool,
      onClick: PropTypes.func,
    };
  }

  render() {
    return (
      <div className={this.props.buttonClassName}>
        <button
          onClick={this.props.onClick}
          disabled={this.props.disabled}
        >
          <img
            className={this.props.iconClassName}
            src={this.props.imagePath}
          />
          {this.props.buttonText}
        </button>
      </div>
    );
  }
}

