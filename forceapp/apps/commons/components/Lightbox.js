import React from 'react';
import { CSSTransition } from 'react-transition-group';
import LightboxModal from './LightboxModal';
import iconZoom from '../images-pre-optimized/iconZoom.png';
import msg from '../languages';
import './Lightbox.scss';

const ROOT = 'lightbox';

export default class Lightbox extends React.Component {
  state = {
    showLightbox: false,
  };

  toggleLightbox = () => {
    this.setState((prevState) => ({
      showLightbox: !prevState.showLightbox,
    }));
  };

  render() {
    const { showLightbox } = this.state;
    return (
      <div className={`${ROOT}`}>
        <a
          className={`${ROOT}__thumbnail-container`}
          onClick={this.toggleLightbox}
          tabIndex={-1}
        >
          <div className={`${ROOT}__thumbnail-hover`}>
            <img className="ts-icon__button-image" src={iconZoom} alt="zoom" />
            <p className={`${ROOT}__thumbnail-hover__msg`}>
              {msg().Exp_Lbl_ClickToZoom}
            </p>
          </div>
          {this.props.children}
        </a>
        <CSSTransition
          in={showLightbox}
          timeout={500}
          classNames={`${ROOT}__modal-container`}
          unmountOnExit
        >
          <LightboxModal
            src={this.props.children.props.src}
            toggleLightbox={this.toggleLightbox}
          />
        </CSSTransition>
      </div>
    );
  }
}
