// @flow

import * as React from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import type { Column } from './Util';
import { buildColumnCss, ellipsisCss } from './Util';

import './BodyRow.scss';

const ROOT = 'commons-grid-body-row';
const HANDLE_KEYPRESS_CODE = ['Enter', ' '];

type Props = {|
  data: Object,
  columns: Column[],
  selected: boolean,
  isBrowsing: boolean,
  onChangeRowSelection: (
    { id: string, checked: boolean },
    e: SyntheticInputEvent<Element>
  ) => void,
  id: string,
  ellipsis: boolean,
  onClickRow: (id: string, SyntheticKeyboardEvent<Element>) => void,
|};

export default class BodyRow extends React.Component<Props> {
  onChange: (SyntheticInputEvent<Element>) => void;
  onClickRow: (SyntheticKeyboardEvent<Element>) => void;
  renderItem: (Column) => React.Element<*>;
  onClickRowByKey: (SyntheticKeyboardEvent<Element>) => void;

  constructor(props: Props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.onClickRow = this.onClickRow.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.onClickRowByKey = this.onClickRowByKey.bind(this);
  }

  onChange(e: SyntheticInputEvent<Element>) {
    this.props.onChangeRowSelection(
      {
        id: this.props.id,
        checked: e.target.checked,
      },
      e
    );
  }

  onClickLabel(e: SyntheticInputEvent<Element>) {
    // 行選択時に詳細移動してしまうことをを防ぐ
    e.stopPropagation();
    // フォーカスが背後の行へ移ることを防ぐ
    e.target.focus();
  }

  onClickRow(e: SyntheticKeyboardEvent<Element>) {
    this.props.onClickRow(this.props.id, e);
  }

  onClickRowByKey(e: SyntheticKeyboardEvent<Element>) {
    if (HANDLE_KEYPRESS_CODE.includes(e.key)) {
      e.preventDefault();
      this.props.onClickRow(this.props.id, e);
    }
  }

  onKeyPressLabel(e: SyntheticEvent<Element>) {
    e.stopPropagation();
  }

  getValue(data: Object, key: string | string[]): any {
    if (
      _.isArray(key) &&
      Array.isArray(key) /* flowtype に key を string[] として推論させる */
    ) {
      const value = {};
      key.forEach((k) => {
        value[k] = data[k];
      });

      return value;
    } else {
      return data[key];
    }
  }

  renderItem(column: Column): React.Element<*> {
    const cssClass = classNames(`${ROOT}__cell`, {
      [`${ROOT}__cell--addon`]: column.isAddon,
    });

    const style = buildColumnCss(column, this.props.ellipsis);

    const innerStyle = this.props.ellipsis
      ? Object.assign({}, ellipsisCss)
      : {};

    const value = this.getValue(this.props.data, column.key);

    let content;
    if (column.formatter) {
      if (column.extraProps) {
        content = (
          <column.formatter
            value={value}
            {...column.extraProps}
            data={this.props.data}
          />
        );
      } else {
        content = <column.formatter value={value} data={this.props.data} />;
      }
    } else {
      content = value;
    }

    return (
      <div key={column.name} className={cssClass} role="gridcell" style={style}>
        <div style={innerStyle} className={`${ROOT}__cell-inner`}>
          {content}
        </div>
      </div>
    );
  }

  render() {
    const cssClass = classNames(ROOT, {
      [`${ROOT}--is-browsing`]: this.props.isBrowsing,
    });

    let rowButtonRole = 'button';
    let rowButtonTabIndex = 0;
    let onClickRow = this.onClickRow;
    let onClickRowByKey = this.onClickRowByKey;

    // 選択済み申請の場合はクリック対象から外す
    if (this.props.isBrowsing) {
      rowButtonTabIndex = null;
      rowButtonRole = '';
      onClickRow = null;
      onClickRowByKey = null;
    }

    return (
      <div className={cssClass} role="row">
        <div
          className={`${ROOT}__selection`}
          role={rowButtonRole}
          tabIndex={rowButtonTabIndex}
          onClick={onClickRow}
          onKeyPress={onClickRowByKey}
        >
          <div
            className={`${ROOT}__cell ${ROOT}__cell--select`}
            role="gridcell"
          >
            <label
              className={`${ROOT}__input-wrapper`}
              onClick={this.onClickLabel}
              onKeyPress={this.onKeyPressLabel}
            >
              <input
                type="checkbox"
                onChange={this.onChange}
                checked={this.props.selected}
              />
            </label>
          </div>

          {this.props.columns.map(this.renderItem)}
        </div>
      </div>
    );
  }
}
