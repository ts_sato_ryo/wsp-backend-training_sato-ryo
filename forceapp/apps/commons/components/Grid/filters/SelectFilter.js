// @flow
import React from 'react';

import SelectField from '../../fields/SelectField';
import msg from '../../../languages';

type Props = {
  value: string,
  options: Array<string | { text: string, value: any }>,
  onChange: (string) => void,
};

export default class DateFilter extends React.Component<Props> {
  onChange: $PropertyType<DateFilter, 'onChange'>;

  constructor(props: Props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e: SyntheticEvent<HTMLSelectElement>) {
    this.props.onChange(e.currentTarget.value);
  }

  render() {
    const options = this.props.options.map((text) =>
      typeof text === 'string' ? { text, value: text } : text
    );
    options.unshift({ value: '', text: msg().Com_Sel_All });

    return (
      <SelectField
        options={options}
        value={this.props.value}
        onChange={this.onChange}
      />
    );
  }
}
