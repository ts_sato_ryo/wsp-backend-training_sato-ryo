// @flow
import React from 'react';

import TextField from '../../fields/TextField';

import msg from '../../../languages';

type Props = {
  value: string,
  onChange: (string) => void,
};

export default class TextFilter extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange = (_e: *, value: string) => {
    this.props.onChange(value);
  };

  render() {
    return (
      <TextField
        value={this.props.value}
        onChange={this.onChange}
        placeholder={msg().Com_Lbl_Search}
      />
    );
  }
}
