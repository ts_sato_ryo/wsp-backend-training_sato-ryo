// @flow
import React from 'react';

import DateField from '../../fields/DateField';
import msg from '../../../languages';

const ROOT = 'commons-grid-filters-date-filter';

type Props = {
  value: string,
  onChange: (string) => void,
};

export default class DateFilter extends React.Component<Props> {
  render() {
    return (
      <div className={ROOT}>
        <DateField
          value={this.props.value}
          onChange={this.props.onChange}
          placeholder={msg().Com_Lbl_Search}
        />
      </div>
    );
  }
}
