import React from 'react';
import PropTypes from 'prop-types';

import DateUtil from '../../../utils/DateUtil';

export default class DateYMD extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
  };

  render() {
    return <span>{DateUtil.formatYMD(this.props.value)}</span>;
  }
}
