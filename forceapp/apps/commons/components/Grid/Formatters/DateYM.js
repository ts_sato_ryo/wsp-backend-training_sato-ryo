import React from 'react';
import PropTypes from 'prop-types';

import DateUtil from '../../../utils/DateUtil';

export default class DateYM extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
  };

  render() {
    return <span>{DateUtil.formatYM(this.props.value)}</span>;
  }
}
