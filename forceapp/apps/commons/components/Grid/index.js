// @flow

import * as React from 'react';
import classNames from 'classnames';

import BodyRow from './BodyRow';
import MessageBoard from '../MessageBoard';
import { type Column, buildColumnCss, ellipsisCss } from './Util';

import './index.scss';

import imgIconDoneCircle from '../../images/iconDoneCircle.png';

const ROOT = 'commons-grid';

type Props = {|
  columns: Column[], // eslint-disable-line react/no-unused-prop-types
  data: Array<Object>,
  idKey: string,
  selected: string[],
  browseId: string,
  onChangeRowSelection: Function,
  onClickRow: (id: string, event: SyntheticEvent<Element>) => void,
  ellipsis?: boolean,
  useFilter?: boolean,
  emptyMessage?: string,
|};

type State = {
  columns: Column[],
};

const COLUMN_DEFAULT = {
  selected: false,
  width: 'auto',
  expand: true,
  formatter: null,
};

const COLUMN_FOR_ICON = {
  name: '\u2001', // 1em space
  width: '50',
  shrink: true,
  expand: true,
  selected: false,
  isAddon: true,
};

const buildColumn = (columns: Column[]): Column[] => {
  const columnsWithIconColumn: Column[] = [];
  columns.forEach((column) => {
    if (column.addon) {
      columnsWithIconColumn.push({
        ...COLUMN_FOR_ICON,
        key: '',
        formatter: (props) => (
          <div className={`${ROOT}__addon addon`}>
            {column.addon ? <column.addon {...props} /> : null}
          </div>
        ),
      });
    }

    columnsWithIconColumn.push({
      ...COLUMN_DEFAULT,
      ...column,
    });
  });

  return columnsWithIconColumn;
};

export default class Grid extends React.Component<Props, State> {
  renderRow: $PropertyType<Grid, 'renderRow'>;
  isSelected: $PropertyType<Grid, 'isSelected'>;
  onClickRow: $PropertyType<Grid, 'onClickRow'>;

  static defaultProps = {
    ellipsis: false,
  };

  static getDerivedStateFromProps(props: Props, _state: State) {
    return {
      columns: buildColumn(props.columns),
    };
  }

  constructor(props: Props) {
    super(props);

    this.state = Grid.getDerivedStateFromProps(props, this.state);

    this.renderRow = this.renderRow.bind(this);
    this.isSelected = this.isSelected.bind(this);
    this.onClickRow = this.onClickRow.bind(this);
  }

  onClickLabel(e: SyntheticInputEvent<Element>) {
    // 行選択時に詳細移動してしまうことをを防ぐ
    e.stopPropagation();
    // フォーカスが背後の行へ移ることを防ぐ
    e.target.focus();
  }

  onClickRow(id: string, event: SyntheticEvent<Element>): void {
    this.props.onClickRow(id, event);
  }

  isSelected(id: string): boolean {
    return this.props.selected.includes(id);
  }

  isBrowsing(id: string): boolean {
    return this.props.browseId === id;
  }

  renderRow(rowData: { [string]: string }): React.Element<*> {
    const id = rowData[this.props.idKey];

    return (
      <BodyRow
        ellipsis={this.props.ellipsis || Grid.defaultProps.ellipsis}
        columns={this.state.columns}
        data={rowData}
        key={id}
        id={id}
        onChangeRowSelection={this.props.onChangeRowSelection}
        selected={this.isSelected(id)}
        isBrowsing={this.isBrowsing(id)}
        onClickRow={this.onClickRow}
      />
    );
  }

  renderBody() {
    if (this.props.data.length > 0) {
      return this.props.data.map<React.Element<typeof BodyRow>>(this.renderRow);
    } else {
      return (
        <MessageBoard
          message={this.props.emptyMessage || ''}
          iconSrc={imgIconDoneCircle}
        />
      );
    }
  }

  renderHeaderItem() {
    return this.state.columns.map<React.Element<'div'>>((column) => {
      let css = buildColumnCss(column, this.props.ellipsis);

      if (!column.isAddon && this.props.ellipsis) {
        css = Object.assign({}, css, ellipsisCss);
      }

      const className = classNames(
        `${ROOT}__cell-head`,
        column.cssModifier ? `${ROOT}__cell-head--${column.cssModifier}` : null
      );

      return (
        <div
          key={column.name}
          style={css}
          className={className}
          role="columnheader"
        >
          {column.name}
        </div>
      );
    });
  }

  renderFilterItem() {
    return this.state.columns.map<React.Element<'div'>>((column) => {
      let css = buildColumnCss(column, this.props.ellipsis);

      if (!column.isAddon && this.props.ellipsis) {
        css = Object.assign({}, css, ellipsisCss);
      }

      const className = classNames(
        `${ROOT}__cell-filter`,
        column.cssModifier
          ? `${ROOT}__cell-filter--${column.cssModifier}`
          : null
      );

      return (
        <div key={column.name} style={css} className={className}>
          {column.renderFilter ? column.renderFilter() : null}
        </div>
      );
    });
  }

  render() {
    const { useFilter } = this.props;

    return (
      <div className={`${ROOT}`} role="grid">
        <div className={`${ROOT}__head`}>
          <div className={`${ROOT}__head-row`} role="row">
            <div
              className={`${ROOT}__cell-head ${ROOT}__cell-head--checkbox`}
              role="columnheader"
            >
              <label
                className={`${ROOT}__input-wrapper`}
                onClick={this.onClickLabel}
              >
                {/* <input type="checkbox" /> */}
              </label>
            </div>

            {this.renderHeaderItem()}
          </div>

          {useFilter ? (
            <div className={`${ROOT}__filter-row`} role="row">
              <div
                className={`${ROOT}__cell-filter ${ROOT}__cell-filter--checkbox`}
              />

              {this.renderFilterItem()}
            </div>
          ) : null}
        </div>

        <div
          className={classNames(`${ROOT}__body`, {
            [`${ROOT}__body--use-filter`]: useFilter,
          })}
        >
          {this.renderBody()}
        </div>
      </div>
    );
  }
}
