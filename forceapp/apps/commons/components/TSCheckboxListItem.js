import PropTypes from 'prop-types';
import React from 'react';

export default class TSCheckboxListItem extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      isChecked: PropTypes.bool,
      isSelected: PropTypes.bool.isRequired,
      onClickContents: PropTypes.func.isRequired,
      onChange: PropTypes.func.isRequired,
    };
  }

  makeContentItemClassName() {
    let contentItemClassName = 'ts-expenses-reports__contents__item';
    if (this.props.isSelected) {
      contentItemClassName = `${contentItemClassName} active`;
    }
    return contentItemClassName;
  }

  render() {
    return (
      <div>
        <div
          className={`slds-grid ${this.makeContentItemClassName()} ts-expenses-reports__pointer`}
          onClick={() => this.props.onClickContents()}
        >
          <div className="slds-col--bump-right slds-align-middle slds-size--1-of-12">
            <div className="ts-expenses-reports__contents__item__checkbox">
              <input
                type="checkbox"
                onChange={this.props.onChange}
                checked={this.props.isChecked}
              />
            </div>
          </div>
          {this.props.children}
        </div>

        <div className="slds-grid">
          <div className="slds-col slds-align-middle slds-size--1-of-12" />
          <div className="slds-col slds-align-middle slds-size--11-of-12 ts-expenses-reports__separate" />
        </div>
      </div>
    );
  }
}

