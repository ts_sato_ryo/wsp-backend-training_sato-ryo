import PropTypes from 'prop-types';
import React from 'react';
import './PCHeader.scss';

/**
 * @deprecated
 */
export default class PCHeader extends React.Component {
  static get propTypes() {
    return {
      headerContent: PropTypes.element,
      icon: PropTypes.string,
    };
  }

  // TODO: 「ユーザー切り替え」や「お知らせ」を実装する際に復活させる
  // renderInformation() {
  //   return (
  //     <div className="ts-header__infos">
  //       <div className="ts-header__infos-info">
  //         <img
  //           className=""
  //           src={require('../images/HeaderIconBubble.png')}
  //         />
  //         <span className="ts-header__infos-badge-frame" />
  //         <span className="ts-header__infos-badge">4</span>
  //       </div>
  //       <div className="ts-header__infos-alert">
  //         <img
  //           className=""
  //           src={require('../images/HeaderIconApprovals.png')}
  //         />
  //         <span className="ts-header__infos-badge-frame" />
  //         <span className="ts-header__infos-badge">4</span>
  //       </div>
  //       <div className="ts-header__infos-help">
  //         <img
  //           className=""
  //           src={require('../images/heddaIcon_Info.png')}
  //         />
  //       </div>
  //       <div className="ts-header__infos-dairi">
  //         <img
  //           className=""
  //           src={require('../images/heddaIcon_Dairi.png')}
  //         />
  //       </div>
  //     </div>
  //   );
  // }

  render() {
    return (
      <div className="ts-header slds">
        <div className="ts-header__contents">
          <div className="ts-header__main">{this.props.headerContent}</div>

          {/* this.renderInformation() */}
        </div>

        <div className="ts-header__icon">
          <img src={this.props.icon} />
        </div>
      </div>
    );
  }
}
