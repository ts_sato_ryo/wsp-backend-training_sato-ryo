// @flow

// @TODO Integrate with Core (Design System) componments

import * as React from 'react';
import { CSSTransition } from 'react-transition-group';

import IconButton from './buttons/IconButton';

import './Toast.scss';

import ImgIconAttention from '../../core/assets/icons/attention.svg';
import ImgIconCheck from '../images/icons/check.svg';
import ImgIconClose from '../images/icons/close.svg';

const ROOT = 'commons-toast';

export type Props = $ReadOnly<{|
  variant?: 'success' | 'warning' | 'error',
  message: string,
  isShow: boolean,
  onClick: () => void,
  onExit: () => void,
|}>;

const Icons = {
  success: ImgIconCheck,
  warning: ImgIconAttention,
  error: ImgIconAttention,
};

export default class Toast extends React.PureComponent<Props> {
  render() {
    const variant = this.props.variant || 'success';
    const Icon = Icons[variant];
    return (
      <CSSTransition
        in={this.props.isShow}
        mountOnEnter
        unmountOnExit
        appear
        timeout={1000}
        classNames={`${ROOT}__animation`}
        onExited={() => this.props.onExit()}
      >
        <div className={`${ROOT} ${ROOT}--${variant}`}>
          <div className={`${ROOT}__content`}>
            <div className={`${ROOT}__icon`}>
              <Icon width="24" height="24" />
            </div>
            <p className={`${ROOT}__message`}>{this.props.message}</p>
            <div className={`${ROOT}__close_button_container`}>
              <IconButton
                srcType="svg"
                src={ImgIconClose}
                onClick={this.props.onClick}
              />
            </div>
          </div>
        </div>
      </CSSTransition>
    );
  }
}
