import PropTypes from 'prop-types';
import React from 'react';
import { CSSTransition as ReactCSSTransitionGroup } from 'react-transition-group';

import './Modal.scss';

export default class Modal extends React.Component {
  static get propTypes() {
    return {
      onClosed: PropTypes.func.isRequired,
      isOpen: PropTypes.bool.isRequired,
      component: PropTypes.object,
      modalStyle: PropTypes.object,
    };
  }

  constructor(props) {
    super(props);
    this.modalClose = this.modalClose.bind(this);
  }

  modalClose() {
    this.props.onClosed();
  }

  modalRef() {
    return 'tsmodalwrap';
  }

  render() {
    if (this.props.isOpen) {
      return (
        <ReactCSSTransitionGroup
          classNames="ts-modal-pc"
          timeout={{ enter: 300, exit: 0 }}
          component="div"
        >
          <div className="ts-modal">
            <div className="ts-modal__overlay" onClick={this.modalClose} />
            <div
              className="ts-modal__wrap"
              style={this.props.modalStyle}
              ref={(ref) => {
                this.modalRef = ref;
              }}
            >
              <div className="ts-modal__contents">{this.props.component}</div>
            </div>
          </div>
        </ReactCSSTransitionGroup>
      );
    }
    return <div />;
  }
}
