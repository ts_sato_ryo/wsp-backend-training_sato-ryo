// @flow

import * as React from 'react';
import isNil from 'lodash/isNil';
import flatten from 'lodash/flatten';
import moment from 'moment';

import type { BaseEvent } from '../../../models/DailySummary/BaseEvent';

import DateUtil from '../../../utils/DateUtil';
import ImgPopClose from '../../../images/popClose.png';

import './index.scss';
import EventItem from './EventItem';

const ROOT = 'commons-popups-event-list-popup';

const CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT = 'teamspirit';

type Props = $ReadOnly<{
  style?: Object,
  targetDate: moment,
  events: BaseEvent[] | BaseEvent[][],
  isOpened: boolean,
  onClickClose: () => void,
  onClickEvent?: (BaseEvent, SyntheticMouseEvent<HTMLElement>) => void,
}>;

const isVisibleEvent = (
  event: BaseEvent,
  startDate: moment,
  endDate: moment
): boolean => {
  return (
    (event.start.isSameOrAfter(startDate) &&
      event.start.isSameOrBefore(endDate)) ||
    (event.end.isSameOrAfter(startDate) && event.end.isSameOrBefore(endDate)) ||
    (event.start.isSameOrBefore(startDate) && event.end.isSameOrAfter(endDate))
  );
};

const EventListPopup = (props: Props) => {
  // Remove timezone
  const startDate = moment(props.targetDate.valueOf()).startOf('day');
  const endDate = startDate.clone().endOf('day');

  return (
    <div className={ROOT} style={props.style}>
      {props.isOpened && (
        <>
          <div
            className="ts-event-list__overlay"
            onClick={props.onClickClose}
          />
          <div className={`${ROOT}__wrap`}>
            <div className={`${ROOT}__title`}>
              {DateUtil.format(props.targetDate.valueOf(), 'LL')}
              <img
                onClick={props.onClickClose}
                className={`${ROOT}__close-icon`}
                src={ImgPopClose}
                alt="close"
              />
            </div>
            <div className={`${ROOT}__body`}>
              {flatten([...props.events])
                .filter(
                  (event) =>
                    !isNil(event) && isVisibleEvent(event, startDate, endDate)
                )
                .sort((left, right) =>
                  left.start.isAfter(right.start) ? 0 : 1
                )
                .map((event) => (
                  <EventItem
                    className={`${ROOT}__event-item`}
                    event={event}
                    isHyperlink={
                      event.createdServiceBy !==
                      CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT
                    }
                    showTime={
                      event.start.isSameOrAfter(startDate) && !event.isAllDay
                    }
                    onClick={(domEvent: SyntheticMouseEvent<HTMLElement>) => {
                      if (props.onClickEvent) {
                        props.onClickEvent(event, domEvent);
                      }
                    }}
                  />
                ))}
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default EventListPopup;
