// @flow

import * as React from 'react';

type Props = {
  className: string,
  onClick?: (SyntheticMouseEvent<HTMLElement>) => void,
  children: React.Node,
};

export default ({ className, onClick, children }: Props) => {
  return onClick ? (
    <div className={className} onClick={onClick} role="button" tabIndex={0}>
      {children}
    </div>
  ) : (
    <div className={className}>{children}</div>
  );
};
