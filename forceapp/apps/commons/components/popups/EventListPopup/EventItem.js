// @flow

import React from 'react';
import classNames from 'classnames';

import type { BaseEvent } from '../../../models/DailySummary/BaseEvent';

import ClickableBlock from './ClickableBlock';
import msg from '../../../languages';
import ImgIconHasJob from '../../../images/iconHasJob.png';
import ImgStsCalGaisyutu from '../../../images/sts_Cal_gaisyutu.png';

import './EventItem.scss';

const ROOT = 'commons-popups-event-list-popup-event-item';

type Props = $ReadOnly<{|
  className?: string,
  isHyperlink: boolean,
  showTime: boolean,
  event: BaseEvent,
  onClick?: (SyntheticMouseEvent<HTMLElement>) => void,
|}>;

export default ({
  isHyperlink,
  onClick,
  showTime,
  event,
  className,
}: Props) => {
  const isButton = !isHyperlink && onClick !== null;
  return (
    <ClickableBlock
      className={classNames(ROOT, className, {
        hyperlink: isHyperlink,
        button: isButton,
        'all-day': event.layout.containsAllDay,
      })}
      onClick={isButton ? onClick : undefined}
    >
      <div className={`${ROOT}__title`}>
        {event.job.id && (
          <img
            className={`${ROOT}__icon`}
            src={ImgIconHasJob}
            alt={event.job.name}
          />
        )}
        {showTime && (
          <span className={`${ROOT}__time`}>{event.start.format('HH:mm')}</span>
        )}
        {event.title || msg().Cal_Lbl_NoTitle}
      </div>
      {event.isOuting && (
        <img
          className={`${ROOT}__icon`}
          src={ImgStsCalGaisyutu}
          alt={msg().Cal_Lbl_OutOf}
        />
      )}
    </ClickableBlock>
  );
};
