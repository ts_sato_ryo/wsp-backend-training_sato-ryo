import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../../languages';

import BaseCurrencyFieldSet from './BaseCurrencyFieldSet';
import ForeignCurrencyFieldSet from './ForeignCurrencyFieldSet';

import './index.scss';

// TODO: スタイルが expenses-pc/components/ExpRecordItemPane.scss から分離できていない
export default class CurrencyFieldSet extends React.Component {
  /* eslint-disable react/no-unused-prop-types */
  static get propTypes() {
    return {
      baseCurrencyCode: PropTypes.string.isRequired,
      baseCurrencyFractionDigits: PropTypes.number.isRequired,
      totalAmount: PropTypes.number.isRequired,
      onChangeFormParam: PropTypes.func.isRequired,
      onChangeCurrencyRateEditMode: PropTypes.func.isRequired,
      onChangeRecordItemLocalAmount: PropTypes.func.isRequired,

      readOnly: PropTypes.bool,
      useForeignCurrency: PropTypes.bool,
      isEditingCurrencyRate: PropTypes.bool,
      hasMultipleRecordItems: PropTypes.bool,
      // FIXME: ForeignCurrencyFieldSet 専用だが、isRequired にするか精査した方がいい
      getCurrencyRateInfo: PropTypes.func,
      localAmount: PropTypes.number,
      currencyId: PropTypes.string,
      currencyList: PropTypes.array,
      currencyRate: PropTypes.number,
      originalCurrencyRate: PropTypes.number,
      itemDate: PropTypes.number,
    };
  }
  /* eslint-enable react/no-unused-prop-types */

  static get defaultProps() {
    return {
      readOnly: false,
      hasMultipleRecordItems: false,
      isEditingCurrencyRate: false,
      currencyRate: 0,
      originalCurrencyRate: 0,
    };
  }

  render() {
    const amountLabel = this.props.hasMultipleRecordItems
      ? msg().Exp_Lbl_TotalAmount
      : msg().Exp_Lbl_Amount;

    // FIXME: ...props での渡し方はやめたい
    return (
      <div>
        {this.props.useForeignCurrency
          ? <ForeignCurrencyFieldSet
            baseCurrencyCode={this.props.baseCurrencyCode}
            amountLabel={amountLabel}
            onChangeLocalAmount={
              (e, value) => this.props.onChangeRecordItemLocalAmount(value, 0)
            }

            {...this.props}
          />
          : <BaseCurrencyFieldSet
            currencyCode={this.props.baseCurrencyCode}
            amountLabel={amountLabel}
            onChangeTotalAmount={(e, value) => this.props.onChangeFormParam('amount', value)}

            {...this.props}
          />
        }
      </div>
    );
  }
}
