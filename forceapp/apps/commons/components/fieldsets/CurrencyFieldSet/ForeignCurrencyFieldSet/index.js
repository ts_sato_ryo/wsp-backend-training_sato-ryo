import PropTypes from 'prop-types';
import React from 'react';

import AmountField from '../../../fields/AmountField';
import CurrencyUtil from '../../../../utils/CurrencyUtil';
import ImgEditOff from '../../../../images/btnEditOff.png';
import ImgEditOn from '../../../../images/btnEditOn.png';
import msg from '../../../../languages';


// レート項目の小数桁(暫定)
const RATE_FRACTION_DIGITS = 6;

export default class ForeignCurrencyFieldSet extends React.Component {
  static get propTypes() {
    return {
      amountLabel: PropTypes.string.isRequired,
      baseCurrencyCode: PropTypes.string.isRequired,
      baseCurrencyFractionDigits: PropTypes.number.isRequired,
      currencyList: PropTypes.arrayOf(PropTypes.object).isRequired,
      currencyId: PropTypes.string.isRequired,
      itemDate: PropTypes.number.isRequired,
      localAmount: PropTypes.number.isRequired,
      totalAmount: PropTypes.number.isRequired,
      onChangeFormParam: PropTypes.func.isRequired,
      getCurrencyRateInfo: PropTypes.func.isRequired,
      onChangeCurrencyRateEditMode: PropTypes.func.isRequired,
      onChangeLocalAmount: PropTypes.func.isRequired,

      readOnly: PropTypes.bool,
      hasMultipleRecordItems: PropTypes.bool,
      isEditingCurrencyRate: PropTypes.bool,
      currencyRate: PropTypes.number,
      originalCurrencyRate: PropTypes.number,
    };
  }

  static get defaultProps() {
    return {
      readOnly: false,
      hasMultipleRecordItems: false,
      isEditingCurrencyRate: false,
      currencyRate: 0,
      originalCurrencyRate: 0,
    };
  }

  isRateModified() {
    return this.props.currencyRate !== this.props.originalCurrencyRate;
  }

  renderInputLocalAmount() {
    return (
      <AmountField
        value={this.props.localAmount}
        disabled={this.props.readOnly || this.props.hasMultipleRecordItems}
        onBlur={this.props.onChangeLocalAmount}
      />
    );
  }

  renderInputCurrencyRate() {
    const isEditing = this.props.isEditingCurrencyRate || this.isRateModified();
    const imgEdit = isEditing ? ImgEditOn : ImgEditOff;
    const imgEditAlt = isEditing ? 'ImgEditOn' : 'ImgEditOff';

    // 編集ボタン
    const currencyRateEditButton = this.props.readOnly ? null : (
      <div className="ts-tax-auto">
        <img
          src={imgEdit}
          onClick={() => this.props.onChangeCurrencyRateEditMode(!isEditing)}
          alt={imgEditAlt}
        />
      </div>);

    // 通貨ペア表記
    const currencyRateInfo = this.props.getCurrencyRateInfo(this.props.currencyId, this.props.itemDate);
    let currencyPairText;
    if (currencyRateInfo.useInverseRate) {
      currencyPairText = `(${currencyRateInfo.currencyCode}/${this.props.baseCurrencyCode})`;
    } else {
      currencyPairText = `(${this.props.baseCurrencyCode}/${currencyRateInfo.currencyCode})`;
    }

    return (
      <div className="slds-grid">
        <div className="slds-col slds-size--7-of-12 slds-align-middle">
          <div className="value">
            <AmountField
              className={this.props.readOnly && this.isRateModified() ? 'ts-currency--modified' : ''}
              value={this.props.currencyRate}
              fractionDigits={RATE_FRACTION_DIGITS}
              disabled={this.props.readOnly || !isEditing}
              onBlur={(e, value) => this.props.onChangeFormParam('currencyRate', value)}
            />
            {currencyRateEditButton}
          </div>
        </div>
        <div className="slds-col slds-size--5-of-12 slds-align-middle center">
          {currencyPairText}
        </div>
      </div>
    );
  }

  renderSelectCurrency() {
    // 通貨一覧から有効期間外の通貨を除外し、アルファベット順にソート
    const activeCurrencyList = CurrencyUtil.findActiveCurrencyList(
      this.props.currencyList, this.props.itemDate
    ).sort();

    return (
      <div className="value">
        <select
          value={this.props.currencyId}
          onChange={(e) => this.props.onChangeFormParam('currencyId', e.target.value)}
          disabled={this.props.readOnly}
        >
          {activeCurrencyList.map((item, i) => (
            <option key={i} value={item.id} >{item.code}</option>
          ))}
        </select>
      </div>
    );
  }

  render() {
    return (
      <div>
        <div className="slds-grid">
          <div className="slds-col slds-size--6-of-12 slds-align-middle">
            <div className="slds-grid">
              <div className="slds-col slds-size--3-of-12 slds-align-middle">
                <div className="key">
                  {msg().Exp_Lbl_Currency}
                </div>
              </div>
              <div className="slds-col slds-size--9-of-12 slds-align-middle">
                {this.renderSelectCurrency()}
              </div>
            </div>
          </div>
          <div className="slds-col slds-size--5-of-12 slds-align-middle">
            <div className="slds-grid">
              <div className="slds-col slds-size--4-of-12 slds-align-middle right">
                <div className="key">
                  {msg().Exp_Lbl_LocalAmount}
                </div>
              </div>
              <div className="slds-col slds-size--8-of-12 slds-align-middle">
                <div className="value">
                  {this.renderInputLocalAmount()}
                </div>
              </div>
            </div>
          </div>
          <div className="slds-col slds-size--1-of-12 slds-align-middle">&nbsp;</div>
        </div>

        <div className="slds-grid">
          <div className="slds-col slds-size--6-of-12 slds-align-middle">
            <div className="slds-grid">
              <div className="slds-col slds-size--3-of-12 slds-align-middle">
                <div className="key">
                  {msg().Exp_Lbl_Rate}
                </div>
              </div>
              <div className="slds-col slds-size--9-of-12 slds-align-middle">
                {this.renderInputCurrencyRate()}
              </div>
            </div>
          </div>
          <div className="slds-col slds-size--5-of-12 slds-align-middle">
            <div className="slds-grid">
              <div className="slds-col slds-size--4-of-12 slds-align-middle right">
                <div className="key">
                  {this.props.amountLabel}
                </div>
              </div>
              <div className="slds-col slds-size--8-of-12 slds-align-middle">
                <div className="value">
                  <AmountField
                    disabled
                    value={this.props.totalAmount}
                    fractionDigits={this.props.baseCurrencyFractionDigits}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="slds-col slds-size--1-of-12 slds-align-middle">&nbsp;</div>
        </div>
      </div>
    );
  }
}
