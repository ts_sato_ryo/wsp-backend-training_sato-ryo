import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../../languages';

import TextField from '../../fields/TextField';
import AmountField from '../../fields/AmountField';

/** 基準通貨の金額項目セット */
export default class BaseCurrencyFieldSet extends React.Component {
  static get propTypes() {
    return {
      amountLabel: PropTypes.string.isRequired,
      baseCurrencyCode: PropTypes.string.isRequired,
      baseCurrencyFractionDigits: PropTypes.number.isRequired,
      totalAmount: PropTypes.number.isRequired,
      onChangeTotalAmount: PropTypes.func.isRequired,

      readOnly: PropTypes.bool,
      hasMultipleRecordItems: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      readOnly: false,
      hasMultipleRecordItems: false,
    };
  }

  render() {
    return (
      <div className="slds-grid">
        <div className="slds-col slds-size--6-of-12 slds-align-middle">
          <div className="slds-grid">
            <div className="slds-col slds-size--3-of-12 slds-align-middle">
              <div className="key">{msg().Exp_Lbl_Currency}</div>
            </div>
            <div className="slds-col slds-size--9-of-12 slds-align-middle">
              <TextField value={this.props.baseCurrencyCode} readOnly />
            </div>
          </div>
        </div>
        <div className="slds-col slds-size--5-of-12 slds-align-middle">
          <div className="slds-grid">
            <div className="slds-col slds-size--4-of-12 slds-align-middle right">
              <div className="key">
                {this.props.amountLabel}
              </div>
            </div>
            <div className="slds-col slds-size--8-of-12 slds-align-middle">
              <div className="value">
                <AmountField
                  disabled={this.props.readOnly || this.props.hasMultipleRecordItems}
                  value={this.props.totalAmount}
                  fractionDigits={this.props.baseCurrencyFractionDigits}
                  onBlur={this.props.onChangeTotalAmount}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="slds-col slds-size--1-of-12 slds-align-middle">
          &nbsp;
        </div>
      </div>
    );
  }
}
