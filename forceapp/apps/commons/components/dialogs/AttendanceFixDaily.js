import PropTypes from 'prop-types';
import React from 'react';

import DialogListLayout from './layouts/DialogListLayout';
import Button from '../buttons/Button';
import HorizontalLayout from '../fields/layouts/HorizontalLayout';
import Label from '../fields/Label';
import CommentFieldIconOutside from '../fields/CommentFieldIconOutside';
import AttendanceGraphNarrow from '../../../timesheet-pc/components/graphs/AttendanceGraphNarrow';

import imgIconAttendance from '../../../timesheet-pc/images/iconAttendance.png';
import imgBtnCloseDialog from '../../images/btnCloseDialog.png';
import imgIconStatusNormalFace from '../../../timesheet-pc/images/iconStatusNormalFace.png';

import './AttendanceFixDaily.scss';

// dummy
import ImgSamplePhoto001 from '../../images/Sample_photo001.png';

export default class attendanceApplyForDnt extends React.Component {
  static get propTypes() {
    return {
      hideDialog: PropTypes.func.Requied,
    };
  }

  render() {
    return (
      <div className="dialog__inner attendance-fix-daily">
        <button className="dialog__close" onClick={this.props.hideDialog}>
          <img src={imgBtnCloseDialog} alt="close" />
        </button>
        <div className="dialog__header">
          <div className="dialog__header-title">
            <img
              className="dialog__header-title-icon"
              src={imgIconAttendance}
              alt=""
              aria-hidden="true"
            />日次確定申請
          </div>

          <div className="dialog__header-subInfo">
            <time className="attendance-fix-daily__date">
              2017年5月9日&#xA0;火曜日
            </time>
          </div>
        </div>
        <div className="dialog__body attendance-fix-daily">
          <div className="attendance-fix-daily__graph">
            <AttendanceGraphNarrow />
          </div>

          <div className="attendance-fix-daily__list">
            <DialogListLayout>
              <DialogListLayout.Item>
                <HorizontalLayout>
                  <HorizontalLayout.Label cols={4} required>
                    出退社
                  </HorizontalLayout.Label>
                  <HorizontalLayout.Body
                    className="attendance-fix-daily__list-text"
                    cols={8}
                  >
                    10:00<img
                      className="attendance-fix-daily__list-icon"
                      src={imgIconStatusNormalFace}
                      alt="good"
                    />
                    〜 19:00<img
                      className="attendance-fix-daily__list-icon"
                      src={imgIconStatusNormalFace}
                      alt="good"
                    />
                  </HorizontalLayout.Body>
                </HorizontalLayout>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <HorizontalLayout>
                  <HorizontalLayout.Label cols={4} required>
                    休憩時間
                  </HorizontalLayout.Label>
                  <HorizontalLayout.Body
                    className="attendance-fix-daily__list-text"
                    cols={8}
                  >
                    01:00
                  </HorizontalLayout.Body>
                </HorizontalLayout>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <HorizontalLayout>
                  <HorizontalLayout.Label cols={4} required>
                    総労働時間
                  </HorizontalLayout.Label>
                  <HorizontalLayout.Body
                    className="attendance-fix-daily__list-text"
                    cols={8}
                  >
                    21:00
                  </HorizontalLayout.Body>
                </HorizontalLayout>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <HorizontalLayout>
                  <HorizontalLayout.Label cols={4} required>
                    実労働時間
                  </HorizontalLayout.Label>
                  <HorizontalLayout.Body
                    className="attendance-fix-daily__list-text"
                    cols={8}
                  >
                    08:00
                  </HorizontalLayout.Body>
                </HorizontalLayout>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <Label
                  className="attendance-fix-daily__comment-bubble-label"
                  text="備考"
                  require
                  labelCols="4"
                  childCols="8"
                >
                  <CommentFieldIconOutside
                    icon={ImgSamplePhoto001}
                    id="item5"
                  />
                </Label>
              </DialogListLayout.Item>
            </DialogListLayout>
          </div>
        </div>

        <div className="dialog__footer">
          <div className="dialog__footer-container">
            <Button
              className="attendance-fix-daily__button"
              onClick={this.props.hideDialog}
            >
              キャンセル
            </Button>
            <Button
              className="attendance-fix-daily__button"
              type="primary"
              onClick={this.props.hideDialog}
            >
              承&#xA0;認
            </Button>
          </div>

          <div className="dialog__footer-container">
            <a>承認履歴</a>
          </div>
        </div>
      </div>
    );
  }
}
