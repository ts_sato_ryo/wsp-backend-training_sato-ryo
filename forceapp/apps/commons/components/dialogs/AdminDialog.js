import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../languages';

import Button from '../buttons/Button';
// import FormatUtil from '../../utils/FormatUtil';
// import ObjectUtil from '../../utils/ObjectUtil';

import './AdminDialog.scss';

/**
 * 経路の検索結果を表示する
 */
export default class AdminDialog extends React.Component {
  static get propTypes() {
    return {
      onClickCloseButton: PropTypes.func.isRequired,
      children: PropTypes.array.isRequired,
    };
  }

  render() {
    return (
      <div className="ts-admin-dialog">
        <div className="slds-grid ts-admin-dialog__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-admin-dialog__header-title">管理画面</div>
          </div>
        </div>

        <div className="ts-admin-dialog__body">
          <div className="ts-admin-dialog__result">{this.props.children}</div>
        </div>
        <div className="slds-grid ts-admin-dialog__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <Button
              className="ts-admin-dialog__btn-close"
              onClick={this.props.onClickCloseButton}
            >
              {msg().Com_Btn_Close}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
