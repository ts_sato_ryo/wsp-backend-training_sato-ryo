import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../languages';
import DateUtil from '../../utils/DateUtil';

import DateField from '../fields/DateField';
import Button from '../buttons/Button';

import './PaymentDateSettingDialog.scss';

/**
 * 精算確定画面で使用
 * 支払日設定ダイアログ
 * Dialogコンポーネントからimportして使われる
 */
export default class PaymentDateSettingDialog extends React.Component {
  static get propTypes() {
    return {
      onClickCloseButton: PropTypes.func.isRequired,
      onClickSettingButton: PropTypes.func.isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.state = { paymentDate: DateUtil.format(Date.now(), 'YYYY-MM-DD') };
    this.setPaymentDate = this.setPaymentDate.bind(this);
  }

  setPaymentDate(paymentDate) {
    const isBeforeToday = DateUtil.isBeforeToday(paymentDate);
    // FIXME: 過去日の判定結果をどう表現するか決まったら実装再開
    if (isBeforeToday) {
      console.log('[Debug] 設定された日付は過去日です。');
    }
    this.setState({ paymentDate });
  }

  render() {
    return (
      <div className="ts-payment-date-setting-dialog slds">
        <div className="slds-grid ts-payment-date-setting-dialog__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-payment-date-setting-dialog__header-title">
              {msg().Exp_Btn_PaymentDateSetting}
            </div>
          </div>
        </div>

        <div className="ts-payment-date-setting-dialog__body-wrap">
          <div className="ts-payment-date-setting-dialog__body">
            <div className="slds-grid ts-payment-date-setting-dialog__body-info">
              <div className="slds-col slds-size--12-of-12 slds-align-middle">
                {msg().Exp_Msg_PaymentDateOwnExpense}
              </div>
            </div>

            <div className="ts-payment-date-setting-dialog__body-input">
              <div className="slds-grid">
                <div className="slds-col slds-size--1-of-12" />
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_DateOfPayment}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--4-of-12 slds-align-middle">
                  <div className="value">
                    <DateField
                      value={this.state.paymentDate}
                      minDays={0}
                      onChange={this.setPaymentDate}
                    />
                  </div>
                </div>
                <div className="slds-col slds-size--4-of-12" />
              </div>
            </div>
          </div>
        </div>

        <div className="slds-grid ts-payment-date-setting-dialog__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <Button
              className="ts-payment-date-setting-dialog__btn-close"
              onClick={() => this.props.onClickCloseButton()}
            >
              {msg().Com_Btn_Cancel}
            </Button>
            <Button
              type="primary"
              className="ts-payment-date-setting-dialog__btn-submit"
              onClick={() =>
                this.props.onClickSettingButton(this.state.paymentDate)
              }
            >
              {msg().Com_Btn_Change}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
