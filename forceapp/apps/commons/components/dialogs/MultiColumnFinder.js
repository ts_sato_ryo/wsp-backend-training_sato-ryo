// @flow

import * as React from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import msg from '../../languages';
import Button from '../buttons/Button';
import ButtonGroups from '../buttons/ButtonGroups';
import Tooltip from '../Tooltip';
import LabelWithHint from '../fields/LabelWithHint';
import ProgressBar, { type ProgressBarStep } from '../ProgressBar';
import './MultiColumnFinder.scss';

import ImgBtnFavoriteOn from '../../images/btnFavorite_on.png';
import ImgBtnFavoriteOff from '../../images/btnFavorite_off.png';
import ImgIconArrowGrey from '../../images/iconArrowGrey.png';
import ImgIconInfo from '../../images/iconInfo2.png';
import ImgBtnCloseDialog from '../../images/btnCloseDialog.png';
import ArrowRight from '../../images/arrowRight.svg';

import TextField from '../fields/TextField';
import FixedHeaderTable, { BodyRow, BodyCell } from '../FixedHeaderTable';
import Icon from '../../../mobile-app/components/atoms/Icon';

const ROOT = 'ts-multi-column-finder';

/**
 *  The `parentSelectable` type
 */
type Props = {
  tab?: string,
  items: Array<*>,
  typeName: string,
  hintMsg?: string,
  showFavorites?: boolean,
  showHistory?: boolean,
  parentSelectable?: boolean,
  onClickItem: (*, ?(*[])) => void,
  onClickCloseButton: Function,
  isDefaultSearchMode?: boolean,
  searchResult: *[],
  recentItems: *[],
  onClickSearch: (string) => void,
  onClickSelectByCategory: () => void,
  IconInfo?: string,
  progressBar?: Array<ProgressBarStep>,
  onClickBackButton?: () => void,
};

type Mode = 'directory' | 'history' | 'favorite' | 'search';

type State = {
  mode: Mode,
  nest: string[],
  keyword: string,
  searchViewLabel: string,
};

const withTooltip = (tooltipContent: React.Node, align?: string) => (
  element: React.Element<*>
) => (
  <Tooltip
    align={align || 'top'}
    content={tooltipContent}
    className="slds-col slds-align-middle slds-size--10-of-12"
  >
    {element}
  </Tooltip>
);

/**
 * マルチカラムファインダーダイアログ
 * Dialogコンポーネントからimportして使われる
 */
export default class MultiColumnFinder extends React.Component<Props, State> {
  tsExpenseTypeContents: HTMLDivElement | null;

  onClickGroupItem: Function;

  static get defaultProps() {
    return {
      showFavorites: false,
      showHistory: false,
      isDefaultSearchMode: false,
      searchResult: [],
      recentItems: [],
      progressBar: [],
      setProgressBar: () => {},
      onClickSearch: () => {},
      onClickSelectByCategory: () => {},
      onClickBackButton: () => {},
    };
  }

  constructor(props: Props) {
    super(props);

    this.state = {
      mode: this.props.isDefaultSearchMode ? 'search' : 'directory',
      // 各階層で選ばれているItemのID, 添え字がそのままに階層を示す
      // [ 0: 'id-1', 1: 'id-2' ... ]
      nest: [],
      keyword: '',
      searchViewLabel:
        this.props.tab !== 'FinanceApproval'
          ? msg().Exp_Lbl_RecentlyUsedItems
          : '',
    };

    this.onClickGroupItem = this.onClickGroupItem.bind(this);
  }

  /**
   *  お気に入り状態の変更
   */
  onClickFavoriteButton() {
    // TODO: お気に入り状態を変更するactionの呼び出し
    // TODO: その後リフレッシュ？
  }

  /**
   *  選択タイプ変更
   */
  onChangeMode(mode: Mode) {
    // TODO: 該当モードに対応するデータの取得
    this.setState({
      mode,
    });
  }

  /**
   * グループが選択された場合、選択されたIDと階層の深さを記憶してイベントハンドラを呼ぶ
   */
  onClickGroupItem(selectedNest: *, item: *, items: *[]) {
    this.setState((prevState) => {
      let nest = _.cloneDeep(prevState.nest);
      // クリックされた階層以降の選択状態はいらないのでクリア
      if (nest === 0) {
        nest = [];
      } else {
        nest = nest.slice(0, selectedNest);
      }

      // 選択されたグループを記憶
      nest.push(item.id);

      return {
        nest,
      };
    });

    this.props.onClickItem(item, items);
  }

  onPressEnter(e: SyntheticKeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.props.onClickSearch(e.currentTarget.value);
      this.setState({ searchViewLabel: msg().Exp_Lbl_SearchResult });
    }
  }

  styleActive(mode: Mode): 'active' | '' {
    return this.state.mode === mode ? 'active' : '';
  }

  getHierarchyDisplay(levels: string[]) {
    const rowClass = `${ROOT}__parent-row`;
    const displayArr = [];
    const tooltipArr = [];
    const lastIdx = levels.length - 1;
    const rightIcon = (
      <img
        src={ImgIconArrowGrey}
        className={`${ROOT}__parent-row-icon-next slds-icon`}
      />
    );
    if (lastIdx < 3) {
      levels.forEach((x) => {
        displayArr.push(x, rightIcon);
      });
    } else {
      displayArr.push(
        levels[0],
        rightIcon,
        '...',
        rightIcon,
        levels[lastIdx],
        rightIcon
      );
      levels.forEach((x) => {
        tooltipArr.push(x, ' > ');
      });
    }
    displayArr.pop();
    tooltipArr.pop();
    let innerClass = '';
    const renderStr = displayArr.reverse().map((item, idx) => {
      innerClass =
        idx % 2 || item === '...' ? `${rowClass}-seperator` : `${rowClass}-col`;
      return <span className={innerClass}>{item}</span>;
    });
    return tooltipArr.length ? (
      withTooltip(<div>{tooltipArr.reverse().join('')}</div>, 'bottom')(
        <div className={rowClass}>{renderStr}</div>
      )
    ) : (
      <div className={rowClass}>{renderStr}</div>
    );
  }

  renderCloseIcon() {
    return (
      <button
        type="button"
        className={`${ROOT}__close`}
        onClick={this.props.onClickCloseButton}
      >
        <img src={ImgBtnCloseDialog} alt="close" />
      </button>
    );
  }

  renderHeader() {
    return (
      <div className="slds-grid ts-multi-column-finder__header">
        <div className="slds-col slds-size--12-of-12 slds-align-middle">
          <div className="ts-multi-column-finder__header__left">
            <div className="ts-multi-column-finder__header__title">
              {this.props.typeName} {msg().Exp_Lbl_Select}
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderTabs() {
    if (!this.props.showFavorites && !this.props.showHistory) {
      return null;
    }

    return (
      <div className="slds-grid ts-multi-column-finder__subheader">
        <div className="slds-col slds-size--2-of-12 slds-align-middle">
          <div
            className={`
            ts-multi-column-finder__subheader-title ts-multi-column-finder__pointer
            ${this.styleActive('directory')}`}
            onClick={() => this.onChangeMode('directory')}
          >
            {this.props.typeName} {msg().Exp_Btn_FromList}
          </div>
        </div>
        {this.props.showFavorites ? (
          <div className="slds-col slds-size--2-of-12 slds-align-middle">
            <div
              className={`
            ts-multi-column-finder__subheader-title ts-multi-column-finder__pointer
            ${this.styleActive('favorite')}`}
              onClick={() => this.onChangeMode('favorite')}
            >
              <img
                className="ts-multi-column-finder__icon-header-fav"
                src={ImgBtnFavoriteOn}
              />
              {msg().Exp_Btn_FromFavorite}
            </div>
          </div>
        ) : null}
        {this.props.showHistory ? (
          <div className="slds-col slds-size--2-of-12 slds-align-middle">
            <div
              className={`
            ts-multi-column-finder__subheader-title ts-multi-column-finder__pointer
            ${this.styleActive('history')}`}
              onClick={() => this.onChangeMode('history')}
            >
              {msg().Exp_Btn_FromHistory}
            </div>
          </div>
        ) : null}
        <div className="slds-col slds-size--6-of-12 slds-align-middle">
          &nbsp;
        </div>
      </div>
    );
  }

  renderButtonGroups() {
    const { items, onClickSelectByCategory } = this.props;
    const { mode } = this.state;
    const btnTypeSearch = mode === 'search' ? 'primary' : 'default';
    const btnTypeDirectory = mode === 'directory' ? 'primary' : 'default';
    const onChange = (activeMode) => {
      this.setState({
        mode: activeMode,
      });
      if (activeMode === 'directory' && items.length === 0) {
        onClickSelectByCategory();
      }
    };
    return (
      this.props.isDefaultSearchMode && (
        <div className="ts-multi-column-finder__btn-grp">
          <ButtonGroups>
            <Button type={btnTypeSearch} onClick={() => onChange('search')}>
              {msg().Exp_Lbl_SearchSelect}
            </Button>
            <Button
              type={btnTypeDirectory}
              onClick={() => onChange('directory')}
            >
              {msg().Exp_Lbl_SelectFromCategory}
            </Button>
          </ButtonGroups>
        </div>
      )
    );
  }

  renderSelectableItem(item: *) {
    const cssNoIcon = this.props.showFavorites
      ? null
      : 'ts-multi-column-finder__item__no-icon';

    const { IconInfo } = this.props;

    return (
      <div
        className="ts-multi-column-finder__item slds-grid ts-multi-column-finder__pointer"
        key={item.id}
        onClick={() => this.props.onClickItem(item)}
      >
        <div className={cssNoIcon}>
          {item.description
            ? withTooltip(<div>{item.name}</div>)(
                <div className="ts-multi-column-finder__text">
                  <div className="ts-multi-column-finder__text-main">
                    {item.code}
                  </div>
                  <div className="ts-multi-column-finder__text-sub">
                    <div aria-label={item.description}>
                      {withTooltip(item.description)(
                        IconInfo ? (
                          <IconInfo className="ts-multi-column-finder__icon-exp-type-info slds-icon" />
                        ) : (
                          <img
                            src={ImgIconInfo}
                            className="ts-multi-column-finder__icon-info slds-icon"
                          />
                        )
                      )}
                      {item.name}
                    </div>
                  </div>
                </div>
              )
            : withTooltip(<div>{item.name}</div>)(
                <div className="ts-multi-column-finder__text">
                  <div className="ts-multi-column-finder__text-code">
                    {item.code}
                  </div>
                  <div className="ts-multi-column-finder__text-name">
                    {item.name}
                  </div>
                </div>
              )}
        </div>
        {this.props.showFavorites && (
          <div className="slds-col slds-align-middle slds-size--1-of-12">
            <img
              role="presentation"
              className="ts-multi-column-finder__icon-fav"
              src={item.isFavorite ? ImgBtnFavoriteOn : ImgBtnFavoriteOff}
              onClick={() => this.onClickFavoriteButton()}
            />
          </div>
        )}
      </div>
    );
  }

  renderGroupItem(item: *, columnIndex: number, isSelectable: boolean) {
    const isSelected =
      !_.isEmpty(this.state.nest[columnIndex]) &&
      this.state.nest[columnIndex] === item.id;

    const rowClass = classNames(
      'slds-grid',
      'ts-multi-column-finder__item-group',
      'ts-multi-column-finder__pointer',
      { 'ts-multi-column-finder__item-selectable-group--selected': isSelected }
    );

    const iconClass = classNames(
      'slds-col slds-align-middle slds-size--2-of-12',
      isSelectable
        ? 'ts-multi-column-finder__show-child-btn-selectable'
        : 'ts-multi-column-finder__show-child-btn',
      { 'ts-multi-column-finder__item-group--selected': isSelected }
    );

    const selectableClass = isSelectable
      ? 'ts-multi-column-finder__item-group__selectable-item'
      : 'ts-multi-column-finder__item-group__non-selectable-item';

    const itemClass = classNames(
      'slds-col slds-grid slds-align-middle slds-size--10-of-12 ',
      selectableClass
    );

    const { IconInfo } = this.props;

    return (
      <div className={rowClass} key={item.id}>
        <div className={itemClass} onClick={() => this.props.onClickItem(item)}>
          {item.description
            ? withTooltip(<div>{item.name}</div>)(
                <div className="ts-multi-column-finder__text">
                  <div className="ts-multi-column-finder__text-main">
                    {item.code}
                  </div>
                  <div className="ts-multi-column-finder__text-sub">
                    <div aria-label={item.description}>
                      {withTooltip(item.description)(
                        IconInfo ? (
                          <IconInfo className="ts-multi-column-finder__icon-exp-type-info  slds-icon" />
                        ) : (
                          <img
                            src={ImgIconInfo}
                            className="ts-multi-column-finder__icon-info slds-icon"
                          />
                        )
                      )}
                      {item.name}
                    </div>
                  </div>
                </div>
              )
            : withTooltip(<div>{item.name}</div>)(
                <div className="ts-multi-column-finder__text">
                  <div className="ts-multi-column-finder__text-main">
                    {item.code}
                  </div>
                  <div className="ts-multi-column-finder__text-sub">
                    <div aria-label={item.description}>{item.name}</div>
                  </div>
                </div>
              )}
        </div>
        <div
          className={iconClass}
          onClick={() => {
            const items = this.props.items.slice(0, columnIndex + 1);
            this.onClickGroupItem(columnIndex, item, items);
          }}
        >
          <ArrowRight
            aria-hidden="true"
            className="ts-multi-column-finder__icon-next slds-icon"
          />
        </div>
      </div>
    );
  }

  renderSearchAndSelectView() {
    const rowClass = `${ROOT}__row`;
    const isSearch = this.state.searchViewLabel === msg().Exp_Lbl_SearchResult;
    const recentItems =
      this.props.tab !== 'FinanceApproval' ? this.props.recentItems : [];
    const items = isSearch ? this.props.searchResult : recentItems;
    return (
      this.state.mode === 'search' && (
        <div className={`${ROOT}__search-area`}>
          <div className={`${ROOT}__search-field`}>
            <div className={`${ROOT}__search-field-label`}>
              {msg().Exp_Lbl_SearchCodeOrName}
            </div>
            <TextField
              className={`${ROOT}__search-field-input`}
              onKeyPress={(e) => this.onPressEnter(e)}
              value={this.state.keyword || ''}
              placeholder={msg().Com_Lbl_Search}
              onChange={(e) => this.setState({ keyword: e.target.value })}
            />
            <Icon
              className={`${ROOT}__search-btn`}
              type="search"
              color="#AFADAB"
            />
          </div>
          <div className={`${ROOT}__search-result`}>
            <span className={`${ROOT}__search-result-label`}>
              {this.state.searchViewLabel}
            </span>
            <span className={`${ROOT}__search-result-count`}>
              {isSearch && `${items.length} ${msg().Exp_Lbl_RecordCount}`}
            </span>
            {(items.length > 0 || isSearch) && (
              <FixedHeaderTable
                scrollableClass={`${ROOT}__scrollable`}
                className={`${ROOT}--is-ellipsis`}
              >
                {items.map((item, idx) => {
                  return (
                    <BodyRow
                      key={idx}
                      className={rowClass}
                      onClick={() => this.props.onClickItem(item)}
                    >
                      <BodyCell className={`${ROOT}__code`}>
                        <div className={`${ROOT}__name`}>
                          {item.code} - {item.name}
                        </div>
                        <div className={`${ROOT}__parents`}>
                          {item.hierarchyParentNameList &&
                            this.getHierarchyDisplay(
                              item.hierarchyParentNameList
                            )}
                        </div>
                      </BodyCell>
                    </BodyRow>
                  );
                })}
              </FixedHeaderTable>
            )}
          </div>
        </div>
      )
    );
  }

  renderDirectoryView() {
    return (
      <div className="ts-multi-column-finder__wrap">
        <div
          className="slds-grid ts-multi-column-finder__contents"
          ref={(c) => {
            this.tsExpenseTypeContents = c;
          }}
        >
          <div className="slds-grid">
            {this.props.items.map((itemList, columnIndex) => {
              return (
                <div
                  className="ts-multi-column-finder__column"
                  key={columnIndex}
                >
                  {Array.isArray(itemList)
                    ? itemList.map((item) => {
                        if (item.hasChildren || item.isGroup) {
                          if (this.props.parentSelectable) {
                            return this.renderGroupItem(
                              item,
                              columnIndex,
                              true
                            );
                          } else {
                            return this.renderGroupItem(
                              item,
                              columnIndex,
                              false
                            );
                          }
                        }
                        return this.renderSelectableItem(item);
                      })
                    : null}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }

  renderMainContent() {
    return this.state.mode === 'search'
      ? this.renderSearchAndSelectView()
      : this.renderDirectoryView();
  }

  renderFooter() {
    return _.isEmpty(this.props.progressBar)
      ? this.renderFooterSingletep()
      : this.renderFooterMultiStep();
  }

  renderFooterSingletep() {
    const hintText = this.props.hintMsg ? msg().Exp_Lbl_Hint : '';
    return (
      <div className="slds-grid ts-multi-column-finder__footer">
        <div
          className={`slds-col slds-size--12-of-12 slds-align-middle ${ROOT}__footer-area`}
        >
          <LabelWithHint
            text={hintText}
            hintMsg={this.props.hintMsg}
            hintAlign="top"
            infoAlign="left"
          />
          <Button
            className="ts-multi-column-finder__footer-btn ts-multi-column-finder__footer-btn-cancel"
            onClick={this.props.onClickCloseButton}
          >
            {msg().Com_Btn_Cancel}
          </Button>
        </div>
      </div>
    );
  }

  renderFooterMultiStep() {
    const hintText = this.props.hintMsg ? msg().Exp_Lbl_Hint : '';
    return (
      <div className="slds-grid ts-multi-column-finder__footer">
        <div
          className={`slds-col slds-size--12-of-12 slds-align-middle ${ROOT}__footer-area ${ROOT}__footer-multi`}
        >
          <Button
            className="ts-multi-column-finder__footer-btn "
            onClick={this.props.onClickBackButton}
          >
            {msg().Com_Lbl_Back}
          </Button>
          <ProgressBar steps={this.props.progressBar || []} />
          <LabelWithHint
            text={hintText}
            hintMsg={this.props.hintMsg}
            hintAlign="top"
            infoAlign="left"
          />
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="ts-multi-column-finder slds">
        {this.renderCloseIcon()}
        {this.renderHeader()}
        {this.renderTabs()}
        {this.renderButtonGroups()}
        {this.renderMainContent()}
        {this.renderFooter()}
      </div>
    );
  }
}
