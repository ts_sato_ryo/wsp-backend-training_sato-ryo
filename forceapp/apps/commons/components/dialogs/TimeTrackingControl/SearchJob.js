import React from 'react';

import IconButton from '../../buttons/IconButton';

import ImgBtnSearchInput from '../../../images/btnSearchInput.png';

import './SearchJob.scss';

export default class SearchJob extends React.Component {
  render() {
    return (
      <div className="time-tracking-control-search-job">
        <button className="time-tracking-control-search-job__frequency slds-button slds-button--neutral">よく使うプロジェクト</button>

        <div className="time-tracking-control-search-job__search-container">
          <IconButton className="time-tracking-control-search-job__serach-icon" src={ImgBtnSearchInput} alt="" />
          <input type="text" className="time-tracking-control-search-job__search slds-input" />
        </div>
      </div>
    );
  }
}
