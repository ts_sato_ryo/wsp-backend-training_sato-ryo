import PropTypes from 'prop-types';
import React from 'react';

import TableRow from './TableRow';
import IconButton from '../../buttons/IconButton';

import ImgBtnAdd from '../../../images/btn_add.png';

import './Table.scss';
import './TableGraphScale.scss';

export default class Table extends React.Component {
  static get propTypes() {
    return {
      onChangeGraph: PropTypes.func.isRequired,
      graphData: PropTypes.array.isRequired,
    };
  }

  render() {
    return (
      <table className="time-tracking-control-table">
        <thead>
          <tr className="time-tracking-control-table__head-row">
            <th className="time-tracking-control-table__col--checkbox">
              <input type="checkbox" />
            </th>
            <th className="time-tracking-control-table__col--number">
              #
            </th>
            <th className="time-tracking-control-table__col--task">
              タスク
            </th>
            <th className="time-tracking-control-table__col--graph time-tracking-control-table-graph-scale">
              <span className="time-tracking-control-table-graph-scale__head-big">0</span>
              <span className="time-tracking-control-table-graph-scale__head">1</span>
              <span className="time-tracking-control-table-graph-scale__head">2</span>
              <span className="time-tracking-control-table-graph-scale__head">3</span>
              <span className="time-tracking-control-table-graph-scale__head">4</span>
              <span className="time-tracking-control-table-graph-scale__head-big">5</span>
              <span className="time-tracking-control-table-graph-scale__head">6</span>
              <span className="time-tracking-control-table-graph-scale__head">7</span>
              <span className="time-tracking-control-table-graph-scale__head">8</span>
              <span className="time-tracking-control-table-graph-scale__head">9</span>
              <span className="time-tracking-control-table-graph-scale__head-big">10</span>
            </th>
            <th className="time-tracking-control-table__col--time">
              時間
            </th>
            <th className="time-tracking-control-table__col--way">
              入力
            </th>
            <th className="time-tracking-control-table__col--remarks">
              備考
            </th>
          </tr>
        </thead>

        <tbody>
          <TableRow barColor="#0083b6" ratio={this.props.graphData[0].value} linkBarColor="#00658c" onChangeGraph={this.props.onChangeGraph(0)} />
          <TableRow barColor="#00a3df" ratio={this.props.graphData[1].value} linkBarColor="#007dab" onChangeGraph={this.props.onChangeGraph(1)} />
          <TableRow barColor="#6fbeda" ratio={this.props.graphData[2].value} linkBarColor="#5592a8" onChangeGraph={this.props.onChangeGraph(2)} />
          <TableRow barColor="#79cca6" ratio={this.props.graphData[3].value} linkBarColor="#5d9d80" onChangeGraph={this.props.onChangeGraph(3)} />
        </tbody>

        <tfoot>
          <tr className="time-tracking-control-table__foot">
            <td colSpan="4">
              <IconButton src={ImgBtnAdd} alt="Add" />
            </td>
            <td colSpan="3">
              08:01（入力合計）
            </td>
          </tr>
        </tfoot>
      </table>
    );
  }
}
