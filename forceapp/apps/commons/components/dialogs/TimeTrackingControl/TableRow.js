import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../buttons/Button';
import ButtonGroups from '../../buttons/ButtonGroups';
import TimeTrackingRangeField from '../../fields/TimeTrackingRangeField';

import ImgBtnIconRangeClock from '../../../images/btnIconRangeClock.png';
import ImgBtnIconRangeGraph from '../../../images/btnIconRangeGraph.png';

const INPUT_CLOCK = 'INPUT_CLOCK';
const INPUT_GRAPH = 'INPUT_GRAPH';

export default class TableRow extends React.Component {
  static get propTypes() {
    return {
      // 工数表示用
      barColor: PropTypes.string.isRequired,
      // 連携済み工数表示用
      linkBarColor: PropTypes.string.isRequired,
      ratio: PropTypes.number.isRequired,
      onChangeGraph: PropTypes.func.isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      inputMode: INPUT_CLOCK,
    };

    this.onClickBtnClock = this.onClickBtnClock.bind(this);
    this.onClickBtnGraph = this.onClickBtnGraph.bind(this);
  }

  onClickBtnClock() {
    this.setState({
      inputMode: INPUT_CLOCK,
    });
  }

  onClickBtnGraph() {
    this.setState({
      inputMode: INPUT_GRAPH,
    });
  }

  render() {
    const btnClockType = this.state.inputMode === INPUT_CLOCK ? 'primary' : '';
    const btnGraphType = this.state.inputMode === INPUT_GRAPH ? 'primary' : '';

    let ImgIconPick = null;
    if (this.state.inputMode === INPUT_CLOCK) {
      ImgIconPick = ImgBtnIconRangeClock;
    } else {
      ImgIconPick = ImgBtnIconRangeGraph;
    }

    return (
      <tr className="time-tracking-control-table__body-row">
        <td className="time-tracking-control-table__col--checkbox">
          <input type="checkbox" />
        </td>
        <td className="time-tracking-control-table__col--number">
          1
        </td>
        <td className="time-tracking-control-table__col--task">
          <div className="time-tracking-control-table-task__name">Aプロジェクト</div>
          <div className="time-tracking-control-table-task__category">【受注後活動】運用開始前サポート</div>
        </td>
        <td className="time-tracking-control-table__col--graph">
          <TimeTrackingRangeField barColor={this.props.barColor} linkBarColor={this.props.linkBarColor} ratio={this.props.ratio} linkRatio={70} onChange={this.props.onChangeGraph} iconSrc={ImgIconPick} />
        </td>
        <td className="time-tracking-control-table__col--time">
          <input className="slds-input" />
        </td>
        <td className="time-tracking-control-table__col--way">
          <ButtonGroups>
            <Button type={btnClockType} onClick={this.onClickBtnClock}>C</Button>
            <Button type={btnGraphType} onClick={this.onClickBtnGraph}>B</Button>
          </ButtonGroups>
        </td>
        <td className="time-tracking-control-table__col--remarks">
          <input type="text" className="slds-input" />
        </td>
        <td />
      </tr>
    );
  }
}
