import PropTypes from 'prop-types';
import React from 'react';

import Button from '../../buttons/Button';
import HorizontalLayout from '../../fields/layouts/HorizontalLayout';
import Label from '../../fields/Label';
import CommentFieldIconOutside from '../../fields/CommentFieldIconOutside';
import HorizontalBarGraph from '../../graphs/HorizontalBarGraph';
import SerachJob from './SearchJob';
import Table from './Table';

import ImgBtnCloseDialog from '../../../images/btnCloseDialog.png';
import ImgBtnManHour from '../../../../timesheet-pc/images/btnManHour.png';

import './index.scss';

// dummy
import ImgSamplePhoto001 from '../../../images/Sample_photo001.png';

const graphDummy = [
  { value: 70, color: '#0083b6' },
  { value: 70, color: '#00a3df' },
  { value: 70, color: '#6fbeda' },
  { value: 70, color: '#79cca6' },
];

export default class TimeTrackingControl extends React.Component {
  static get propTypes() {
    return {
      hideDialog: PropTypes.func.isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      graphData: graphDummy,
    };

    this.onChangeGraph = this.onChangeGraph.bind(this);
  }

  onChangeGraph(index) {
    return (value) => {
      this.setState((prevState) => {
        const data = [].concat(prevState.graphData);
        data[index].value = value;

        return {
          graphData: data,
        };
      });
    };
  }

  render() {
    return (
      <div className="dialog__inner time-tracking-control">
        <button className="dialog__close" onClick={this.props.hideDialog}>
          <img src={ImgBtnCloseDialog} alt="close" />
        </button>
        <div className="dialog__header">
          <div className="dialog__header-title">
            <img
              className="dialog__header-title-icon"
              src={ImgBtnManHour}
              alt=""
              aria-hidden="true"
            />
            工数実績入力
          </div>

          <div className="dialog__header-subInfo">
            &#9664;&#xA0;
            <time className="attendance-fix-daily__date">
              2017年05月09日&#xA0;火曜日
            </time>
            &#xA0;&#9654;
          </div>
        </div>
        <div className="dialog__body">
          <div className="time-tracking-control__info">
            <HorizontalLayout>
              <HorizontalLayout.Label cols={2}>工数実績</HorizontalLayout.Label>

              <HorizontalLayout.Body cols={10}>
                <HorizontalBarGraph
                  className="time-tracking-control__info-graph"
                  data={this.state.graphData}
                />
                <span className="time-tracking-control__info-graph-time-text">
                  08:01(実労働時間)
                </span>
              </HorizontalLayout.Body>
            </HorizontalLayout>

            <Label
              className="time-tracking-control__info-report"
              text="作業報告"
              labelCols={2}
              childCols={10}
            >
              <CommentFieldIconOutside
                icon={ImgSamplePhoto001}
                id="some-comment-id"
              />
            </Label>

            <div className="time-tracking-control__info-action">
              <Button
                className="time-tracking-control__info-action-button"
                type="secondary"
              >
                新規追加
              </Button>

              <SerachJob />
            </div>
          </div>

          <div className="time-tracking-control__input">
            <Table
              graphData={this.state.graphData}
              onChangeGraph={this.onChangeGraph}
            />
          </div>
        </div>

        <div className="dialog__footer">
          <div className="dialog__footer-container">
            <Button
              className="attendance-fix-daily__button"
              onClick={this.props.hideDialog}
            >
              キャンセル
            </Button>
            <Button
              className="attendance-fix-daily__button"
              type="primary"
              onClick={this.props.hideDialog}
            >
              登&#xA0;録
            </Button>
          </div>

          <div className="dialog__footer-container">
            <a href="">インポート</a>
            &#xA0;
            <a href="">エクスポート</a>
          </div>
        </div>
      </div>
    );
  }
}
