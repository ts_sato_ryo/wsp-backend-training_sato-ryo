import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../languages';

import Label from '../fields/Label';
import Button from '../buttons/Button';

import './PayMethodSettingDialog.scss';

/**
 * 精算確定画面で使用
 * 支払方法設定ダイアログ
 * Dialogコンポーネントからimportして使われる
 */
export default class PayMethodSettingDialog extends React.Component {
  static get propTypes() {
    return {
      onClickCloseButton: PropTypes.func.isRequired,
    };
  }

  render() {
    return (
      <div className="ts-paymethod-setting-dialog slds">
        <div className="slds-grid ts-paymethod-setting-dialog__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-paymethod-setting-dialog__header-title">
              {msg().Exp_Btn_PayMethodSetting}
            </div>
          </div>
        </div>
        <div className="ts-paymethod-setting-dialog__body-wrap">
          <div className="ts-paymethod-setting-dialog__body">
            <div className="ts-paymethod-setting-dialog__body-input">
              <div className="slds-grid">
                <div className="slds-col slds-size--1-of-12" />
                <div className="slds-col slds-size--9-of-12 slds-align-middle">
                  <Label text="支払日">
                    {/* TODO: 実装時に適切な id を付与する */}
                    <select>
                      {/* TODO: バックエンドと繋ぎこむ際に適切なデータを入れる */}
                      <option>test 1</option>
                      <option>test 2</option>
                    </select>
                  </Label>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>
              <div className="slds-grid">
                <div className="slds-col slds-size--1-of-12" />
                <div className="slds-col slds-size--9-of-12 slds-align-middle">
                  <Label text="振込元口座">
                    <input id="some-input" type="text" className="slds-input" />
                  </Label>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>
            </div>
          </div>
        </div>
        <div className="slds-grid ts-paymethod-setting-dialog__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <Button
              className="ts-paymethod-setting-dialog__btn-close"
              onClick={() => this.props.onClickCloseButton()}
            >
              {msg().Com_Btn_Cancel}
            </Button>
            <Button
              type="primary"
              className="ts-paymethod-setting-dialog__btn-submit"
            >
              {msg().Com_Btn_Change}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
