import React from 'react';
import PropTypes from 'prop-types';

import './Footer.scss';

const ROOT = 'commons-dialog-frame-footer';

/**
 * 右から順にエレメントが並んでいくシンプルなフッターレイアウトを提供する
 * デザインによってばらつきが激しいためレイアウトを分離した
 */
export default class Footer extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    // 削除ボタンなど副次的な要素の描画
    // 左端に描画される
    sub: PropTypes.node,
  };

  static defaultProps = {
    sub: null,
  };

  render() {
    return (
      <div className={`slds-grid ${ROOT}`}>
        <div className={`slds-grow ${ROOT}__sub`}>
          {this.props.sub ? this.props.sub : null}
        </div>

        <div className={`${ROOT}__inner`}>{this.props.children}</div>
      </div>
    );
  }
}
