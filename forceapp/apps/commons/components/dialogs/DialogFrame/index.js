import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import Footer from './Footer';

import './index.scss';

import imgBtnCloseDialog from '../../../images/btnCloseDialog.png';

export const Z_INDEX_DEFAULT = 5100000;

const ROOT = 'commons-dialog-frame';

class DialogFrame extends React.Component {
  static get propTypes() {
    return {
      title: PropTypes.string,
      titleIcon: PropTypes.string,
      children: PropTypes.node.isRequired,
      hide: PropTypes.func.isRequired,
      className: PropTypes.string.isRequired,
      // ヘッダーに副次的に表示するエレメント
      // 右に寄せて配置される
      headerSub: PropTypes.element,
      zIndex: PropTypes.number,
      // Footerに表示する要素を通す、何もなかった場合はFooterそのものを描画しない
      initialFocus: PropTypes.string,
      footer: PropTypes.node,
      withoutCloseButton: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      titleIcon: '',
      className: '',
      headerSub: null,
      zIndex: Z_INDEX_DEFAULT,
      withoutCloseButton: false,
    };
  }

  constructor(props) {
    super(props);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderContents = this.renderContents.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  componentDidMount() {
    this.prevFocusElement = document.activeElement;
    this.focusInitial();
  }

  componentWillUnmount() {
    if (this.prevFocusElement) {
      this.prevFocusElement.focus();
    }
  }

  // フォーカス移動
  focusInitial() {
    // IDの指定がなければcloseボタンにフォーカスを付与
    if (this.props.initialFocus) {
      document.getElementById(this.props.initialFocus).focus();
    } else if (this.closeButton) {
      this.closeButton.focus();
    }
  }

  renderHeaderSub() {
    if (this.props.headerSub) {
      return (
        <div className={`${ROOT}__header-sub`}>{this.props.headerSub}</div>
      );
    } else {
      return null;
    }
  }

  renderHeader() {
    return (
      <div
        className={`slds-grid slds-grid--vertical-align-center ${ROOT}__header`}
      >
        {this.props.titleIcon ? (
          <div className={`${ROOT}__header-icon`}>
            <img
              className={`${ROOT}__icon-title`}
              src={this.props.titleIcon}
              alt=""
            />
          </div>
        ) : null}
        <div className={`slds-grow ${ROOT}__header-title`}>
          {this.props.title}
        </div>

        {this.renderHeaderSub()}
      </div>
    );
  }

  renderContents() {
    return (
      <div className={`${ROOT}__wrap`}>
        <div className={`${ROOT}__contents`}>{this.props.children}</div>
      </div>
    );
  }

  renderFooter() {
    if (!this.props.footer) {
      return null;
    }

    return <div className={`${ROOT}__footer`}>{this.props.footer}</div>;
  }

  render() {
    const cssClass = classNames(ROOT, 'slds', this.props.className, {
      [`${ROOT}--no-footer`]: !this.props.footer,
    });

    // TODO: TransitionGroup ＆ zIndexつける要素は、TransitionGroupの出力要素？
    return (
      <div className={`${ROOT}__wrapper`} style={{ zIndex: this.props.zIndex }}>
        <div className={`${ROOT}__overlay`} onClick={this.props.hide} />
        <div className={cssClass} role="dialog">
          {this.props.withoutCloseButton ? null : (
            <button
              type="button"
              className={`${ROOT}__close`}
              onClick={this.props.hide}
              ref={(closeButton) => {
                this.closeButton = closeButton;
              }}
            >
              <img src={imgBtnCloseDialog} alt="close" />
            </button>
          )}
          {this.props.title && this.renderHeader()}
          {this.renderContents()}
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

DialogFrame.Footer = Footer;

export default DialogFrame;
