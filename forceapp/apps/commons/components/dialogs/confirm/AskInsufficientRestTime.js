// @flow
import React from 'react';

import { type CustomConfirmDialogComponent } from '../ConfirmDialog';
import msg from '../../../languages';
import TextUtil from '../../../utils/TextUtil';

type Params = {
  insufficientRestTime: number,
  targetDate?: ?string,
};

const AskFillDailyRestTime: CustomConfirmDialogComponent<Params> = (props) => {
  const { ConfirmDialog, params } = props;
  return (
    <ConfirmDialog okButtonLabel={msg().Com_Lbl_SaveRestHours}>
      {TextUtil.template(
        msg().Com_Msg_InsufficientRestTime,
        params.insufficientRestTime
      )}
    </ConfirmDialog>
  );
};

export default AskFillDailyRestTime;
