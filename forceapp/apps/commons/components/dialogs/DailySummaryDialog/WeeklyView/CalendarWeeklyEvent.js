// @flow

import React from 'react';
import _ from 'lodash';
import classNames from 'classnames';

import type { BaseEvent } from '../../../../models/DailySummary/BaseEvent';
import { CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT } from './constants/event';
import ImgIconHasJob from '../../../../images/iconHasJob.png';
import ImgStsCalGaisyutu from '../../../../images/sts_Cal_gaisyutu.png';
import msg from '../../../../languages';

import './CalendarWeeklyEvent.scss';

type Props = $ReadOnly<{|
  event: BaseEvent,
  openEventEdit: ?(BaseEvent, SyntheticEvent<HTMLDivElement>) => void,
|}>;

/**
 * 週ビューに表示される 1 件の予定
 */
export default class CalendarWeeklyEvent extends React.Component<Props> {
  openEventEdit: (
    selectedEvent: BaseEvent,
    mouseEvent: SyntheticEvent<HTMLDivElement>
  ) => void;

  static get defaultProps() {
    return {
      openEventEdit: null,
    };
  }

  constructor(props: Props) {
    super(props);

    this.openEventEdit = this.openEventEdit.bind(this);
  }

  openEventEdit(
    selectedEvent: BaseEvent,
    mouseEvent: SyntheticEvent<HTMLDivElement>
  ) {
    // FIXME: ここで stopPropagation() しないといけないかどうかは要検証。
    // 不要ならこのメソッド自体不要
    mouseEvent.stopPropagation();
    if (this.props.openEventEdit) {
      this.props.openEventEdit(selectedEvent, mouseEvent);
    }
  }

  stopMouseEventPropagation(mouseEvent: SyntheticEvent<HTMLDivElement>) {
    mouseEvent.stopPropagation();
  }

  renderJobStatus() {
    if (this.props.event.job.id) {
      return (
        <img
          className="ts-calendar-weekly-event__job-status"
          src={ImgIconHasJob}
          alt={this.props.event.job.name}
        />
      );
    } else {
      return null;
    }
  }

  renderIcons() {
    const icons = [];

    if (this.props.event.isOuting) {
      icons.push(
        <img
          className="ts-calendar-weekly-event__icon ts-calendar-weekly-event__icon--outing"
          src={ImgStsCalGaisyutu}
          alt={msg().Cal_Lbl_OutOf}
        />
      );
    }

    return icons;
  }

  render() {
    const divStyle = {
      top: this.props.event.layout.startMinutesOfDay,
      left: `${String(this.props.event.left)}%`,
      width: `${String(this.props.event.width)}%`,
      height:
        this.props.event.layout.endMinutesOfDay -
        this.props.event.layout.startMinutesOfDay,
    };

    const eventTitle = this.props.event.title || msg().Cal_Lbl_NoTitle;

    const isWide =
      this.props.event.layout.endMinutesOfDay -
        this.props.event.layout.startMinutesOfDay >
      30;
    const hasLink =
      !_.isEmpty(this.props.event.createdServiceBy) &&
      this.props.event.createdServiceBy !==
        CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT;
    const allowEdit = !hasLink && this.props.openEventEdit;

    const moduleCss = classNames(
      'ts-calendar-weekly-event',
      { 'ts-calendar-weekly-event--narrow': !isWide },
      {
        'ts-calendar-weekly-event--no-clickable': !allowEdit,
      }
    );

    const contentCss = classNames('ts-calendar-weekly-event__content', {
      'ts-calendar-weekly-event__content--hasLink': hasLink,
    });

    const props = {
      className: moduleCss,
      style: divStyle,
      role: undefined,
      tabIndex: undefined,
      onClick: undefined,
    };

    // Make the event clickable if it were provided by WSP,
    // and openEventEdit prop were provided.
    if (allowEdit) {
      props.onClick = (mouseEvent) =>
        this.openEventEdit(this.props.event, mouseEvent);
      props.role = 'button';
      props.tabIndex = 0;
    } else {
      props.onClick = this.stopMouseEventPropagation;
    }

    // FIXME onClickだけでなくonKeyPressにも必要
    return (
      <div {...props}>
        {(() => {
          if (isWide) {
            return (
              <dl className={contentCss}>
                <dt className="ts-calendar-weekly-event__time">
                  {this.renderJobStatus()}
                  {this.props.event.start.format('HH:mm')} 〜{' '}
                  {this.props.event.end.format('HH:mm')}
                  <div className="ts-calendar-weekly-event__icon-container">
                    {this.renderIcons()}
                  </div>
                </dt>
                <dt className="ts-calendar-weekly-event__title">
                  {eventTitle}
                </dt>
              </dl>
            );
          } else {
            // 30分以下の予定、細長表示
            return (
              <dl className={contentCss}>
                <dt className="ts-calendar-weekly-event__title ts-calendar-weekly-event__title--narrow">
                  {this.renderJobStatus()}
                  {this.props.event.start.format('HH:mm')}〜 {eventTitle}
                  <div className="ts-calendar-weekly-event__icon-container">
                    {this.renderIcons()}
                  </div>
                </dt>
              </dl>
            );
          }
        })()}
      </div>
    );
  }
}
