// @flow

import React from 'react';

import './List.scss';
import './GoalList.scss';

export default class Class extends React.Component<{}> {
  render() {
    return (
      <div className="planner-day-main-plan-list">
        <div className="planner-day-main-plan-list__summary">
          <span className="planner-day-main-plan-list__summary-title">
            合計
          </span>
          <span className="planner-day-main-plan-list__summary-value">
            03:00
          </span>
        </div>

        <ul className="planner-day-main-plan-list__items">
          <li className="planner-day-main-plan-list__item planner-day-main-plan-goal-list">
            <div className="planner-day-main-plan-goal-list__item">
              新規法人10社への営業提案
            </div>
            <div className="planner-day-main-plan-goal-list__time">03:00</div>
          </li>

          <li className="planner-day-main-plan-list__item planner-day-main-plan-goal-list">
            <div className="planner-day-main-plan-goal-list__item">
              キーマン20名との食事
            </div>
            <div className="planner-day-main-plan-goal-list__time">00:00</div>
          </li>
        </ul>
      </div>
    );
  }
}
