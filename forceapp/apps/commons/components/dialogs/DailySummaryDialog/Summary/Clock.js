/* @flow */
import React from 'react';
import moment from 'moment';
import type Moment from 'moment';

import msg from '../../../../languages';

import './Clock.scss';

export type Props = {};

type State = { currentTime: ?Moment };

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-clock';

export default class Clock extends React.Component<Props, State> {
  onTick: () => void;
  timer: ?IntervalID;

  constructor(props: Props) {
    super(props);
    this.state = {
      currentTime: null,
    };
    this.onTick = this.onTick.bind(this);
  }

  componentDidMount() {
    this.onTick();
    this.timer = setInterval(this.onTick, 60 * 1000);
  }

  componentWillUnmount() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  onTick() {
    this.setState({ currentTime: moment() });
  }

  render() {
    const { currentTime } = this.state;
    return (
      <div className={ROOT}>
        <span>{msg().Cal_Lbl_CurrentTime}: </span>
        {currentTime !== null && currentTime !== undefined
          ? currentTime.format('HH')
          : null}
        <span>:</span>
        {currentTime !== null && currentTime !== undefined
          ? currentTime.format('mm')
          : null}
      </div>
    );
  }
}
