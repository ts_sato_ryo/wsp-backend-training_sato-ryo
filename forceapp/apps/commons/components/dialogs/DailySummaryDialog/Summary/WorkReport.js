// @flow

import React from 'react';

import './WorkReport.scss';

import ImgIconBurned from '../../../../images/iconBurned.png';
import ImgIconDiamond from '../../../../images/iconDiamond.png';
import ImgIconInProgress from '../../../../images/iconInProgress.png';

export default class Class extends React.Component<{}> {
  render() {
    return (
      <div className="planner-day-main-plan-work-report">
        <ul className="planner-day-main-plan-work-report__items">
          <li className="planner-day-main-plan-work-report__item">
            <div className="planner-day-main-plan-work-report__item-text">
              総労働時間
              <br />
              9:31h
            </div>
            <div className="planner-day-main-plan-work-report__item-graph">
              <svg className="planner-day-main-plan-work-report__item-graph-svg">
                <rect x="0" y="0" width="84%" fill="#4f90db" key="1" />
                <rect x="84%" y="0" width="16%" fill="#ca6b5a" key="2" />
              </svg>
            </div>
            <div className="planner-day-main-plan-work-report__item-detail">
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--blue">
                勤務
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                8:00
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--red">
                残業
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                1:31
              </span>
            </div>
          </li>

          <li className="planner-day-main-plan-work-report__item">
            <div className="planner-day-main-plan-work-report__item-text">
              Quadrant
            </div>
            <div className="planner-day-main-plan-work-report__item-graph">
              <svg className="planner-day-main-plan-work-report__item-graph-svg">
                <rect x="0%" y="0%" width="42%" fill="#b5dc86" key="2" />
                <rect x="42%" y="0%" width="27%" fill="#ca6b5a" key="3" />
                <rect x="69%" y="0%" width="4%" fill="#84799b" key="5" />
              </svg>
            </div>
            <div className="planner-day-main-plan-work-report__item-detail">
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--yellow-green">
                <img src={ImgIconDiamond} alt="Diamond" />
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                04:00
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--red">
                <img src={ImgIconBurned} alt="Burned" />
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                02:40
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--purple">
                <img src={ImgIconInProgress} alt="InProgress" />
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                00:21
              </span>
            </div>
          </li>

          <li className="planner-day-main-plan-work-report__item">
            <div className="planner-day-main-plan-work-report__item-text">
              直間比率
            </div>
            <div className="planner-day-main-plan-work-report__item-graph">
              <svg className="planner-day-main-plan-work-report__item-graph-svg">
                <rect x="0%" y="0%" width="100%" fill="#4f90db" key="6" />
              </svg>
            </div>
            <div className="planner-day-main-plan-work-report__item-detail">
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--blue">
                直接
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                9:31
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-name planner-day-main-plan-work-report__item-detail-name--grey">
                間接
              </span>
              <span className="planner-day-main-plan-work-report__item-detail-time">
                0:00
              </span>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}
