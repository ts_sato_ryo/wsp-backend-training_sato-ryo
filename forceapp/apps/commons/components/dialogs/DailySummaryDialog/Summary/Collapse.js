// @flow

import * as React from 'react';
import { CSSTransition as ReactCSSTransitionGroup } from 'react-transition-group';

import IconButton from '../../../buttons/IconButton';

import './Collapse.scss';

import ImgBtnDetailOpen from '../../../../images/btnDetailOpen.png';
import ImgBtnDetailClose from '../../../../images/btnDetailClose.png';

const ROOT = 'planner-day-main-plan-collapse';

type Props = $ReadOnly<{|
  children: React.Element<any>,
  header: string,
  isCollapsed: boolean,
  summary?: React.Node,
|}>;

type State = {|
  isCollapsed: boolean,
|};

export default class Collapse extends React.Component<Props, State> {
  onClickToggle: () => void;
  onKeypressToggle: (e: SyntheticKeyboardEvent<HTMLAnchorElement>) => void;

  static get defaultProps() {
    return {
      isCollapsed: false,
    };
  }

  constructor(props: Props) {
    super(props);

    this.state = {
      isCollapsed: this.props.isCollapsed,
    };

    this.onClickToggle = this.onClickToggle.bind(this);
    this.onKeypressToggle = this.onKeypressToggle.bind(this);
  }

  onClickToggle() {
    this.toggleCollapse();
  }

  onKeypressToggle(e: SyntheticKeyboardEvent<HTMLAnchorElement>) {
    if (e.key === 'Enter') {
      this.toggleCollapse();
    }
  }

  toggleCollapse() {
    this.setState((prevState) => ({
      isCollapsed: !prevState.isCollapsed,
    }));
  }

  renderChildren() {
    if (this.state.isCollapsed) {
      return null;
    } else {
      return (
        <div className="planner-day-main-plan-collapse__children">
          {this.props.children}
        </div>
      );
    }
  }

  render() {
    const btnImage = this.state.isCollapsed
      ? ImgBtnDetailOpen
      : ImgBtnDetailClose;

    return (
      <div className={ROOT}>
        <div className={`${ROOT}__header`}>
          <IconButton
            onClick={this.onClickToggle}
            src={btnImage}
            className={`${ROOT}__header-button`}
            alt="collapse"
          />
          <a
            onClick={this.onClickToggle}
            onKeyPress={this.onKeypressToggle}
            className={`${ROOT}__header-anchor`}
          >
            {this.props.header}
          </a>

          <div className={`${ROOT}__summary`}>{this.props.summary}</div>
        </div>

        <ReactCSSTransitionGroup
          classNames="planner-day-main-plan-collapse"
          timeout={{ enter: 300, exit: 300 }}
        >
          <div>{this.renderChildren()}</div>
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}
