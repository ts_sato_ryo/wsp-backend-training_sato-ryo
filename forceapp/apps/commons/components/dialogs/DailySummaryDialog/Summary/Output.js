// @flow

import React from 'react';

import TextAreaField from '../../../fields/TextAreaField';

type Props = $ReadOnly<{|
  editOutput: (string) => void,
  value: string,
|}>;

export default class Output extends React.Component<Props> {
  onChange: (e: SyntheticInputEvent<HTMLElement>) => void;

  constructor(props: Props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange(e: SyntheticInputEvent<HTMLElement>) {
    this.props.editOutput(e.target.value);
  }

  render() {
    return <TextAreaField onChange={this.onChange} value={this.props.value} />;
  }
}
