// @flow

import React from 'react';

import TextAreaField from '../../../fields/TextAreaField';

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-note';

type Props = $ReadOnly<{|
  isLocked: boolean,
  value: string,
  editNote: (string) => void,
|}>;

export default class Note extends React.Component<Props> {
  onChange: (e: SyntheticInputEvent<HTMLElement>) => void;

  constructor(props: Props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange(e: SyntheticInputEvent<HTMLElement>) {
    this.props.editNote(e.target.value);
  }

  render() {
    return (
      <TextAreaField
        className={ROOT}
        value={this.props.value}
        onChange={this.onChange}
        readOnly={this.props.isLocked}
      />
    );
  }
}
