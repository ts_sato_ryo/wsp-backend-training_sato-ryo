// @flow

import React from 'react';

import msg from '../../../../languages';

import './OutputSummary.scss';

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-output-summary';

type Props = $ReadOnly<{|
  output: string,
|}>;

export default class OutputSummary extends React.Component<Props> {
  render() {
    const lineArray = this.props.output.split(/\r\n|\r|\n/);
    const lineFiltered = lineArray.filter((line) => {
      return line !== '';
    });
    const lines = this.props.output !== '' ? lineFiltered.length : 0;

    return (
      <span className={ROOT}>
        <span className={`${ROOT}__label`}>{msg().Cal_Lbl_OutputNumber}</span>
        <span className={`${ROOT}__value`}>
          {lines}
          {msg().Cal_Lbl_OutputUnit}
        </span>
      </span>
    );
  }
}
