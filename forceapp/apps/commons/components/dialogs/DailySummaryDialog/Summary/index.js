// @flow

import * as React from 'react';

import type { DailySummaryTask } from '../../../../../domain/models/time-management/DailySummaryTask';
import type { WorkCategory } from '../../../../../domain/models/time-tracking/WorkCategory';
import msg from '../../../../languages';
import Collapse from './Collapse';
import Note from './Note';
import TrackingList from './TrackingList';
import SummaryGraph from './TrackingList/SummaryGraph';
import TimestampComment from './TimestampComment';
import Clock from './Clock';
import Button from '../../../buttons/Button';

import ImgIconPrimarySave from '../../../../images/iconPrimarySave.png';
import imgIconStampTime from '../../../../images/iconStampTime.png';

import './index.scss';

type Props = $ReadOnly<{|
  defaultJobId?: string,
  isEnableEndStamp?: boolean,
  isLocked: boolean,
  isTemporaryWorkTime: boolean,
  isProxyMode: ?boolean,
  note: string,
  realWorkTime: number,
  taskList: DailySummaryTask[],
  timestampComment: string,
  workCategoryLists: { [jobId: string]: WorkCategory[] },

  showJobSelectDialog: (SyntheticEvent<Button>) => void,
  saveDailySummary: () => void,
  saveDailySummaryAndRecordEndTimestamp: () => void,
  deleteTask: (index: number) => void,
  editTask: (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ) => void,
  editTaskTime: (
    index: number,
    prop: 'taskTime' | 'ratio' | 'volume',
    value: number
  ) => void,
  toggleDirectInput: (index: number) => void,
  editNote: (value: string) => void,
  editTimestampComment: (string) => void,
  hideDialog: (SyntheticEvent<HTMLElement>) => void,
  reorderTaskList: (Object[]) => void,
|}>;

export default class Summary extends React.Component<Props> {
  onSubmit: (e: SyntheticEvent<HTMLFormElement>) => void;

  constructor(props: Props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e: SyntheticEvent<HTMLFormElement>) {
    e.preventDefault();

    (this.props.isEnableEndStamp
      ? this.props.saveDailySummaryAndRecordEndTimestamp
      : this.props.saveDailySummary)();
  }

  render() {
    return (
      <div className="planner-day-main-plan">
        <form onSubmit={this.onSubmit}>
          <Collapse header={msg().Cal_Lbl_WorkReport}>
            <Note
              editNote={this.props.editNote}
              value={this.props.note}
              isLocked={this.props.isLocked}
            />
          </Collapse>

          <Collapse
            header={msg().Cal_Lbl_WorkTimeInput}
            summary={
              <SummaryGraph
                taskList={this.props.taskList}
                realWorkTime={this.props.realWorkTime}
                isTemporaryWorkTime={this.props.isTemporaryWorkTime}
              />
            }
          >
            <TrackingList
              showJobSelectDialog={this.props.showJobSelectDialog}
              taskList={this.props.taskList}
              workCategoryLists={this.props.workCategoryLists}
              defaultJobId={this.props.defaultJobId}
              deleteTask={this.props.deleteTask}
              editTask={this.props.editTask}
              editTaskTime={this.props.editTaskTime}
              toggleDirectInput={this.props.toggleDirectInput}
              reorderTaskList={this.props.reorderTaskList}
              isLocked={this.props.isLocked}
            />
          </Collapse>

          {/* TODO: Uncomment below to show expense application form */}
          {/*
          <Collapse header={msg().Cal_Lbl_Expense}>
            <ExpRecordList />
          </Collapse>
          */}

          {/* TODO: Uncomment below to show work reporting form */}
          {/*
          <Collapse header={msg().Cal_Lbl_WorkReport}>
            <WorkReport />
          </Collapse>
          *}

          {/* TODO: Uncomment below to show the related goals */}
          {/*
          <Collapse header={msg().Cal_Lbl_RelatedTarget}>
            <GoalList />
          </Collapse>
          */}

          {/*
          <Collapse
            header={msg().Cal_Lbl_Output}
            summary={<OutputSummary output={this.props.output} />}
          >
            <Output
              value={this.props.output}
              editOutput={this.props.editOutput}
            />
          </Collapse>
          */}

          {!this.props.isLocked &&
          !this.props.isProxyMode &&
          this.props.isEnableEndStamp ? (
            <TimestampComment
              value={this.props.timestampComment}
              onChange={this.props.editTimestampComment}
            />
          ) : null}

          <div className="planner-day-main-plan__footer">
            <Button
              className="planner-day-main-plan__footer-button"
              onClick={this.props.hideDialog}
            >
              {msg().Com_Btn_Cancel}
            </Button>
            {!this.props.isProxyMode &&
              (!this.props.isLocked && this.props.isEnableEndStamp ? (
                [
                  <Button
                    key="save_button"
                    type="default"
                    disabled={this.props.isLocked}
                    className="planner-day-main-plan__footer-button"
                    onClick={this.props.saveDailySummary}
                  >
                    {msg().Com_Btn_Submit}
                  </Button>,
                  <Clock key="timestamp_clock" />,
                  <Button
                    key="record_end_timestamp_and_save_button"
                    type="primary"
                    disabled={this.props.isLocked}
                    className="planner-day-main-plan__footer-button planner-day-main-plan__save-and-record-end-timestamp-button"
                    submit
                  >
                    <img
                      role="presentation"
                      src={imgIconStampTime}
                      className="planner-day-main-plan__footer-button-icon"
                      style={{ marginTop: '-6px', marginRight: '6px' }}
                    />
                    {msg().Cal_Btn_SaveAndRecordEndTimestamp}
                  </Button>,
                ]
              ) : (
                <Button
                  key="save_button"
                  type="primary"
                  disabled={this.props.isLocked}
                  className="planner-day-main-plan__footer-button"
                  submit
                >
                  <img
                    role="presentation"
                    src={ImgIconPrimarySave}
                    className="planner-day-main-plan__footer-button-icon"
                    style={{ marginTop: '-2px', marginRight: '8px' }}
                  />
                  {msg().Com_Btn_Submit}
                </Button>
              ))}
          </div>
        </form>
      </div>
    );
  }
}
