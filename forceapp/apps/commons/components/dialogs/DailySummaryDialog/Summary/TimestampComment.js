/* @flow */
import React from 'react';

import msg from '../../../../languages';

import './TimestampComment.scss';

export type Props = {
  value: string,
  onChange: (string) => void,
};

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-timestamp-comment';

export default class TimestampComment extends React.Component<Props> {
  onChange: (SyntheticEvent<HTMLTextAreaElement>) => void;

  constructor(props: Props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event: SyntheticInputEvent<HTMLTextAreaElement>) {
    event.preventDefault();
    this.props.onChange(event.target.value);
  }

  render() {
    return (
      <div className={ROOT}>
        <label
          htmlFor="daily_summary_dialog_timestamp_comment"
          className={`${ROOT}__label`}
        >
          {msg().Cal_Lbl_EndTimestampComment}
        </label>
        <textarea
          id="daily_summary_dialog_timestamp_comment"
          value={this.props.value}
          className={`ts-textarea-field slds-input ${ROOT}__textarea`}
          onChange={this.onChange}
        />
      </div>
    );
  }
}
