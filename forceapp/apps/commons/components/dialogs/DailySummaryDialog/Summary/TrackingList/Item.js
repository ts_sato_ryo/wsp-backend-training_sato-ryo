// @flow

import * as React from 'react';
import classNames from 'classnames';
import _ from 'lodash';
import defaultTo from 'lodash/defaultTo';

import type { DailySummaryTask } from '../../../../../../domain/models/time-management/DailySummaryTask';
import type { WorkCategory } from '../../../../../../domain/models/time-tracking/WorkCategory';
import Tooltip from '../../../../Tooltip';
import msg from '../../../../../languages';
import ReorderbleListItem from '../../../../ReorderbleListItem';
import IconButton from '../../../../buttons/IconButton';
import AttTimeField from '../../../../fields/AttTimeField';
import Dropdown, { type Option } from '../../../../fields/Dropdown';
import TimeTrackingRangeField from '../../../../fields/TimeTrackingRangeField';
import SingleToggleButton from '../../../../buttons/SingleToggleButton';
import TimeUtil from '../../../../../utils/TimeUtil';
import ObjectUtil from '../../../../../utils/ObjectUtil';

import './Item.scss';

import ImgBtnTaskDelete from '../../../../../images/btnTaskDelete.png';
import ImgBtnTaskCopy from '../../../../../images/btnTaskCopy.png';
import ImgIconChargeJob from '../../../../../images/iconChargeJob.png';
import ImgBtnRatioOn from '../../../../../images/btnRatioOn.png';
import ImgBtnRatioOff from '../../../../../images/btnRatioOff.png';

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-tracking-item';

type Props = $ReadOnly<{|
  hasDefaultJobInTrackingList: boolean,
  index: number,
  isDefaultJob: boolean,
  isLocked: boolean,
  task: DailySummaryTask,
  workCategoryList: WorkCategory[],

  deleteTask: (index: number) => void,
  editTask: (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ) => void,
  editTaskTime: (
    index: number,
    prop: 'taskTime' | 'ratio' | 'volume',
    value: number
  ) => void,
  toggleDirectInput: (index: number) => void,
|}>;

export default class Item extends React.Component<Props> {
  onChangeTaskGraph: (value: number) => void;
  onClickToggleDirectInput: () => void;
  onClickDeleteButton: () => void;

  constructor(props: Props) {
    super(props);

    this.onChangeTaskGraph = this.onChangeTaskGraph.bind(this);
    this.onClickDeleteButton = this.onClickDeleteButton.bind(this);
    this.onClickToggleDirectInput = this.onClickToggleDirectInput.bind(this);
  }

  onChangeTask(index: number, prop: string) {
    return (option: Option<$Values<DailySummaryTask>>) => {
      this.props.editTask(index, prop, option.value);
    };
  }

  onChangeTaskGraph(value: number) {
    const prop = this.props.task.isDirectInput
      ? ('taskTime': 'taskTime')
      : ('volume': 'volume');
    this.props.editTaskTime(this.props.index, prop, value);
  }

  onBlurTaskTime(index: number) {
    return (value: string) => {
      this.props.editTaskTime(
        index,
        'taskTime',
        TimeUtil.toMinutes(value) || 0
      );
    };
  }

  onClickToggleDirectInput() {
    this.props.toggleDirectInput(this.props.index);
  }

  onClickDeleteButton() {
    this.props.deleteTask(this.props.index);
  }

  convertForDropdown(workCategoryList: WorkCategory[]): Option<mixed>[] {
    const emptyItem = { type: 'item', label: '', value: null };
    const converted = workCategoryList.map((workCategory) => {
      return {
        type: 'item',
        label: workCategory.name,
        value: workCategory.id,
      };
    });

    return [emptyItem, ...converted];
  }

  zeroPadding(value: number) {
    return `00${value}`.slice(-2);
  }

  buildRangeValue() {
    const rangeValue = this.props.task.isDirectInput
      ? this.props.task.taskTime
      : this.props.task.volume;
    return _.isNil(rangeValue) ? 0 : rangeValue;
  }

  buildTaskTimeValue() {
    return TimeUtil.toHHmm(this.buildRangeValue());
  }

  /**
   * 比率計算の場合、taskTimeをTimeTrackingRangeFieldへ設定する
   */
  buildBarRatio() {
    if (this.props.task.isDirectInput) {
      return null;
    } else {
      return ObjectUtil.getOrDefault(this.props.task, 'taskTime', 0);
    }
  }

  /**
   * 比率入力かつ、工数時間が未確定（null）の際は透明にする
   */
  buildBarColor() {
    if (this.props.task.isDirectInput) {
      return this.props.task.color.base;
    } else {
      const color = _.isNil(this.props.task.taskTime)
        ? 'transparent'
        : this.props.task.color.base;

      return color;
    }
  }

  getTaskTimeValue() {
    const { task } = this.props;
    if (task.isDirectInput) {
      return this.buildTaskTimeValue();
    } else {
      let value;
      if (
        _.isNil(task.taskTime) &&
        !_.isNil(task.volume) &&
        task.volume !== undefined &&
        task.volume !== null &&
        task.volume > 0
      ) {
        value = '* *';
      } else if (!_.isNil(task.taskTime)) {
        value = TimeUtil.toHHmm(this.props.task.taskTime);
      } else {
        value = '-';
      }

      return value;
    }
  }

  renderReadOnlyTimeInput() {
    const value = this.getTaskTimeValue();

    return (
      <span className={`${ROOT}__time-input-field slds-input slds-input_bare`}>
        {value}
      </span>
    );
  }

  renderTimeInput() {
    const value = this.getTaskTimeValue();

    const { task } = this.props;
    if (task.isDirectInput) {
      return (
        <AttTimeField
          className={`${ROOT}__time-input-field`}
          onBlur={this.onBlurTaskTime(this.props.index)}
          value={value}
          onChange={this.onChangeTaskGraph}
        />
      );
    } else {
      return (
        <span
          className={`${ROOT}__time-input-field slds-input slds-input_bare`}
        >
          {value}
        </span>
      );
    }
  }

  renderIsDirectCharged() {
    if (this.props.task.isDirectCharged) {
      return <img src={ImgIconChargeJob} alt={msg().Cal_Lbl_DirectCharge} />;
    } else {
      return null;
    }
  }

  render() {
    const { task } = this.props;
    const convertedworkCategoryList: Option<mixed>[] = this.convertForDropdown(
      this.props.workCategoryList
    );

    const className = classNames({
      [ROOT]: true,
      [`${ROOT}--locked`]: this.props.isLocked,
    });

    return (
      <ReorderbleListItem
        className={className}
        index={this.props.index}
        itemId={task.id}
        useHandle
        isDragDisabled={this.props.isLocked}
      >
        {(dragHandleProps) => (
          <div className={`${ROOT}__inner`}>
            <div className={`${ROOT}__drag-handle`}>
              <div
                className={`${ROOT}__drag-handle-bar`}
                {...dragHandleProps}
              />
            </div>

            <div className={`${ROOT}__number`}>{this.props.index + 1}</div>

            <div className={`${ROOT}__contents`}>
              <div className={`${ROOT}__contents-row`}>
                <div className={`${ROOT}__task`}>
                  <div
                    className={`slds-grid slds-grid--vertical-align-center ${ROOT}__task-metadata-upper`}
                  >
                    <div className={`${ROOT}__task-id`}>
                      {this.props.task.jobCode}
                    </div>
                    {this.props.isDefaultJob ? (
                      <Tooltip
                        align="bottom"
                        content={msg().Cal_Msg_DefaultJobDescription}
                      >
                        <div
                          className={`${ROOT}__task-default-job-indicator`}
                          aria-label={msg().Cal_Msg_DefaultJobDescription}
                        >
                          {msg().Com_Lbl_DefaultJobShort}
                        </div>
                      </Tooltip>
                    ) : null}
                  </div>

                  <div className={`${ROOT}__task-metadata-lower`}>
                    <div className={`${ROOT}__task-name`}>
                      {this.props.task.jobName}
                    </div>
                  </div>

                  {/* Remove the Working Category if the item is the Default Job */}
                  {!this.props.isDefaultJob ? (
                    <div className={`${ROOT}__work-category`}>
                      <Dropdown
                        options={convertedworkCategoryList}
                        className={`${ROOT}__input`}
                        value={defaultTo(this.props.task.workCategoryId, null)}
                        onSelect={this.onChangeTask(
                          this.props.index,
                          'workCategoryId'
                        )}
                        disabled={this.props.isLocked}
                      />
                    </div>
                  ) : null}
                </div>

                {/* TODO Remove this */}
                <div className={`${ROOT}__spacer`} />

                <div className={`${ROOT}__charge`}>
                  {this.renderIsDirectCharged()}
                </div>

                <div className={`${ROOT}__graph`}>
                  <TimeTrackingRangeField
                    barColor={this.buildBarColor()}
                    linkBarColor={this.props.task.color.linked}
                    ratio={this.buildRangeValue()}
                    barRatio={this.buildBarRatio()}
                    linkRatio={this.props.task.eventTaskTime}
                    width={200}
                    onChange={this.onChangeTaskGraph}
                    isDirectInputMode={this.props.task.isDirectInput}
                    ratioLabel={
                      this.props.task.ratio === null ? 0 : this.props.task.ratio
                    }
                    readOnly={this.props.isLocked}
                  />
                </div>

                <div className={`${ROOT}__time-input`}>
                  {this.props.isLocked
                    ? this.renderReadOnlyTimeInput()
                    : this.renderTimeInput()}
                </div>

                <div className={`${ROOT}__tools`}>
                  {/* Remove the toggle button if the item is the Default Job */}
                  {!this.props.hasDefaultJobInTrackingList ? (
                    <SingleToggleButton
                      src={ImgBtnTaskCopy}
                      iconSrcActive={ImgBtnRatioOn}
                      iconSrcDisabled={ImgBtnRatioOff}
                      hasBorder={false}
                      value={!this.props.task.isDirectInput}
                      onClick={this.onClickToggleDirectInput}
                      disabled={this.props.isLocked}
                    />
                  ) : null}
                  {/* Uncomment below to show the button to copy task */}
                  {/*
              <IconButton src={ImgBtnTaskCopy} disabled={this.props.isLocked} />
              */}
                  {/* Remove the delete button if the item is the Default Job */}
                  {!this.props.isDefaultJob ? (
                    <IconButton
                      src={ImgBtnTaskDelete}
                      onClick={this.onClickDeleteButton}
                      disabled={this.props.isLocked}
                    />
                  ) : null}
                </div>
              </div>

              <div className={`${ROOT}__contents-row`}>
                {/* Uncomment below to show remarks */}
                {/*
            <div className={`${ROOT}__remark`}>
              <TextField className={`${ROOT}__input`} readOnly={this.props.isLocked} />
            </div>
            */}
              </div>
            </div>
          </div>
        )}
      </ReorderbleListItem>
    );
  }
}
