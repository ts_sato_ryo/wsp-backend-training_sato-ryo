// @flow

import * as React from 'react';
import _ from 'lodash';

import type { DailySummaryTask } from '../../../../../../domain/models/time-management/DailySummaryTask';
import type { WorkCategory } from '../../../../../../domain/models/time-tracking/WorkCategory';
import msg from '../../../../../languages';
import Header from './Header';
import Item from './Item';

import ReorderbleList from '../../../../ReorderbleList';
import Button from '../../../../buttons/Button';

import './index.scss';

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-tracking';

type Props = $ReadOnly<{|
  taskList: DailySummaryTask[],
  workCategoryLists: { [jobId: string]: WorkCategory[] },
  defaultJobId?: string,
  isLocked: boolean,

  showJobSelectDialog: (SyntheticEvent<Button>) => void,
  deleteTask: (index: number) => void,
  editTask: (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ) => void,
  editTaskTime: (
    index: number,
    prop: 'taskTime' | 'ratio' | 'volume',
    value: number
  ) => void,
  toggleDirectInput: (index: number) => void,
  reorderTaskList: (Object[]) => void,
|}>;

type State = {|
  hasDefaultJob: boolean,
|};

export default class TrackingList extends React.Component<Props, State> {
  renderItem: (task: DailySummaryTask, i: number) => void;

  static initialState = {
    hasDefaultJob: false,
  };

  constructor(props: Props) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
    this.state = this.updateState(TrackingList.initialState, props);
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState((prevState) => this.updateState(prevState, nextProps));
  }

  updateState(oldState: State, nextProps: Props) {
    return {
      ...oldState,
      hasDefaultJob:
        !!nextProps.defaultJobId &&
        nextProps.taskList.some(
          (task) =>
            task.jobId === nextProps.defaultJobId &&
            task.workCategoryId === null &&
            !task.isDirectInput
        ),
    };
  }

  renderItem(task: DailySummaryTask, i: number) {
    // 作業分類を取り出し
    let workCategoryList = [];
    if (!_.isEmpty(this.props.workCategoryLists[task.jobId])) {
      workCategoryList = this.props.workCategoryLists[task.jobId];
    }

    return (
      <Item
        key={task.id}
        task={task}
        index={i}
        workCategoryList={workCategoryList}
        isDefaultJob={
          !!this.props.defaultJobId &&
          task.jobId === this.props.defaultJobId &&
          task.workCategoryId === null &&
          !task.isDirectInput
        }
        hasDefaultJobInTrackingList={this.state.hasDefaultJob}
        deleteTask={this.props.deleteTask}
        editTask={this.props.editTask}
        editTaskTime={this.props.editTaskTime}
        toggleDirectInput={this.props.toggleDirectInput}
        isLocked={this.props.isLocked}
      />
    );
  }

  render() {
    return (
      <div className={`${ROOT}`}>
        <div className={`${ROOT}__buttons`}>
          <Button
            type="secondary"
            onClick={this.props.showJobSelectDialog}
            disabled={this.props.isLocked}
          >
            {msg().Com_Btn_AddNew}
          </Button>
        </div>

        <div className={`${ROOT}__list`}>
          <Header />

          <ReorderbleList
            className={`${ROOT}__items`}
            list={this.props.taskList}
            onDragEnd={this.props.reorderTaskList}
            isDropDisabled={this.props.isLocked}
          >
            {this.renderItem}
          </ReorderbleList>
        </div>
      </div>
    );
  }
}
