// @flow

import * as React from 'react';
import _ from 'lodash';

import type { DailySummaryTask } from '../../../../../../domain/models/time-management/DailySummaryTask';
import HorizontalBarGraph from '../../../../graphs/HorizontalBarGraph';
import msg from '../../../../../languages';
import TimeUtil from '../../../../../utils/TimeUtil';

import './SummaryGraph.scss';

const ROOT =
  'commons-dialogs-daily-summary-dialog-summary-tracking-list-summary-graph';

type Props = $ReadOnly<{|
  taskList: DailySummaryTask[],
  // 実労働時間が入っていない場合はnullが設定されている
  isTemporaryWorkTime: boolean,
  realWorkTime: number,
|}>;

export default class SummaryGraph extends React.Component<Props> {
  parseInt(str: ?string | number) {
    if (!_.isNumber(str)) {
      return 0;
    }

    return parseInt(str);
  }

  sumTaskListTime() {
    return this.props.taskList.reduce(
      (prev: number, task: DailySummaryTask) => {
        return prev + this.parseInt(task.taskTime);
      },
      0
    );
  }

  extractGraphData() {
    return this.props.taskList.map<{ value: number, color: string }>(
      (task: DailySummaryTask) => {
        return {
          value: this.parseInt(task.taskTime),
          color: task.color.base,
        };
      }
    );
  }

  renderRealWorkTime() {
    if (this.props.realWorkTime) {
      return (
        <span>
          &nbsp;/&nbsp;
          <span className={`${ROOT}__actual`}>
            {TimeUtil.toHHmm(this.props.realWorkTime)}
            {this.props.isTemporaryWorkTime
              ? ` (${msg().Cal_Lbl_TemporaryWorkHours})`
              : ` (${msg().Cal_Lbl_ActualWorkingHours})`}
          </span>
        </span>
      );
    } else {
      return null;
    }
  }

  render() {
    const sumTaskTime = this.sumTaskListTime();
    const graphData = this.extractGraphData();

    if (sumTaskTime < this.props.realWorkTime) {
      graphData.push({
        value: this.props.realWorkTime - sumTaskTime,
        color: 'transparent',
      });
    }

    return (
      <div className={`${ROOT}`}>
        <HorizontalBarGraph className={`${ROOT}__graph`} data={graphData} />

        {TimeUtil.toHHmm(sumTaskTime)}
        {this.renderRealWorkTime()}
      </div>
    );
  }
}
