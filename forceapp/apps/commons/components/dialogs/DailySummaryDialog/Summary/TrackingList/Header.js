// @flow

import React from 'react';

import msg from '../../../../../languages';

import './Header.scss';

const ROOT = 'commons-dialogs-daily-summary-dialog-summary-tracking-header';

// 現状ロジックをもたない
export default class Class extends React.Component<{}> {
  render() {
    return (
      <header className={`${ROOT}`}>
        <div className={`${ROOT}__head ${ROOT}__head--number`}>#</div>
        <div className={`${ROOT}__head ${ROOT}__head--task`}>
          {msg().Com_Lbl_Job}/{msg().Cal_Lbl_WorkCategory}
        </div>
        <div className={`${ROOT}__head ${ROOT}__head--charge`}>
          {msg().Cal_Lbl_DirectCharge}
        </div>
        <div className={`${ROOT}__head ${ROOT}__head--graph`}>
          <span className={`${ROOT}__scale ${ROOT}__scale--big`}>0</span>
          <span className={`${ROOT}__scale`}>1</span>
          <span className={`${ROOT}__scale`}>2</span>
          <span className={`${ROOT}__scale`}>3</span>
          <span className={`${ROOT}__scale`}>4</span>
          <span className={`${ROOT}__scale ${ROOT}__scale--big`}>5</span>
          <span className={`${ROOT}__scale`}>6</span>
          <span className={`${ROOT}__scale`}>7</span>
          <span className={`${ROOT}__scale`}>8</span>
          <span className={`${ROOT}__scale`}>9</span>
          <span className={`${ROOT}__scale ${ROOT}__scale--big`}>10</span>
        </div>
        <div className={`${ROOT}__head ${ROOT}__head--time`}>
          {msg().Cal_Lbl_Time}
        </div>
      </header>
    );
  }
}
