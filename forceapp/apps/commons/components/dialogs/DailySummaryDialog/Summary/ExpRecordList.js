// @flow

import React from 'react';

import './List.scss';
import './ExpRecordList.scss';

/**
 * 経費精算リスト
 */
export default class ExpRecordList extends React.Component<{}> {
  render() {
    return (
      <div className="planner-day-main-plan-list">
        <div className="planner-day-main-plan-list__summary">
          <span className="planner-day-main-plan-list__summary-title">
            経費申請合計
          </span>
          <span className="planner-day-main-plan-list__summary-value">
            ￥1,120
          </span>
        </div>

        <ul className="planner-day-main-plan-list__items">
          <li className="planner-day-main-plan-list__item planner-day-main-plan-exp-record-list">
            <div className="planner-day-main-plan-exp-record-list__item">
              交通費
            </div>
            <div className="planner-day-main-plan-exp-record-list__content">
              京橋(東京) ⇒ 国際展示場 (IC運賃)
              <br />
              （経路：京橋(東京)→新橋→有明(東京)→国際展...
            </div>
            <div className="planner-day-main-plan-exp-record-list__amount">
              560
            </div>
          </li>

          <li className="planner-day-main-plan-list__item planner-day-main-plan-exp-record-list">
            <div className="planner-day-main-plan-exp-record-list__item">
              交通費
            </div>
            <div className="planner-day-main-plan-exp-record-list__content">
              京橋(東京) ⇒ 国際展示場 (IC運賃)
              <br />
              （経路：京橋(東京)→新橋→有明(東京)→国際展...
            </div>
            <div className="planner-day-main-plan-exp-record-list__amount">
              560
            </div>
          </li>
        </ul>
      </div>
    );
  }
}
