// @flow

import * as React from 'react';
import moment from 'moment';

import type { DailySummaryTask } from '../../../../domain/models/time-management/DailySummaryTask';
import type { WorkCategory } from '../../../../domain/models/time-tracking/WorkCategory';
import type { BaseEvent } from '../../../models/DailySummary/BaseEvent';
import type { Job } from '../../../../domain/models/time-tracking/Job';
import type { Permission } from '../../../../domain/models/access-control/Permission';
import DateUtil from '../../../utils/DateUtil';
import Calendar from './Calendar';
import DialogFrame from '../DialogFrame';
import Button from '../../buttons/Button';
import Summary from './Summary';
import JobSelectDialog from './JobSelectDialog';
import msg from '../../../languages';

import './index.scss';

const ROOT = 'commons-dialogs-daily-summary-dialog';

type Props = $ReadOnly<{|
  /**
   * Delegated login or not
   */
  isProxyMode?: boolean,

  /**
   * Permission
   */
  userPermission?: Permission,

  note: string,
  timestampComment: string,
  taskList: DailySummaryTask[],
  workCategoryLists: { [jobId: string]: WorkCategory[] },
  defaultJobId: string,
  isLocked: boolean,
  isTemporaryWorkTime: boolean,
  realWorkTime: number,
  selectedDay: moment,
  isEnableEndStamp?: boolean,
  opening?: boolean,
  isOpeningJobSelectDialog: boolean,
  jobTree: { ...Job, isGroup: boolean }[][],

  events: BaseEvent[],
  openEventListPopup: (
    e: SyntheticMouseEvent<HTMLButtonElement>,
    moment,
    BaseEvent[]
  ) => void,

  moveDay: (moment) => void,
  showJobSelectDialog: (SyntheticEvent<Button>) => void,
  saveDailySummary: () => void,
  saveDailySummaryAndRecordEndTimestamp: () => void,
  hideDialog: (SyntheticEvent<HTMLElement>) => void,
  deleteTask: (index: number) => void,
  editTask: (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ) => void,
  editTaskTime: (
    index: number,
    prop: 'taskTime' | 'ratio' | 'volume',
    value: number
  ) => void,
  toggleDirectInput: (index: number) => void,
  editNote: (value: string) => void,
  editTimestampComment: (string) => void,
  reorderTaskList: (Object[]) => void,
  closeJobSelectDialog: () => void,
  fetchJobTree: (selectedItem: Job, existingItems: ?(Job[][])) => void,
|}>;

export default class DailySummaryDialog extends React.Component<Props> {
  onClickSelectorDayButton: (number: number) => void;

  constructor(props: Props) {
    super(props);

    this.onClickSelectorDayButton = this.onClickSelectorDayButton.bind(this);
  }

  onClickSelectorDayButton(number: number) {
    return () => {
      const baseDate = this.props.selectedDay.clone();
      this.props.moveDay(baseDate.add(number, 'days'));
    };
  }

  getSelectedDayStr() {
    if (this.props.selectedDay) {
      // FIXME フォーマットを確認中で暫定対応
      return DateUtil.format(this.props.selectedDay.valueOf(), 'L (ddd)');
    } else {
      return '';
    }
  }

  renderDateSelector() {
    return (
      <div className={`${ROOT}__date-selector`}>
        <Button
          className={`${ROOT}__date-selector-prev-button`}
          type="text"
          onClick={this.onClickSelectorDayButton(-1)}
          aria-label="prev"
        >
          &#9664;
        </Button>
        <time className={`${ROOT}__date-selector-time`}>
          {this.getSelectedDayStr()}
        </time>
        <Button
          className={`${ROOT}__date-selector-next-button`}
          type="text"
          onClick={this.onClickSelectorDayButton(1)}
          aria-label="next"
        >
          &#9654;
        </Button>
      </div>
    );
  }

  render() {
    if (!this.props.opening) {
      return null;
    }

    return (
      <>
        <JobSelectDialog
          jobTree={this.props.jobTree}
          opening={this.props.isOpeningJobSelectDialog}
          onClickItem={this.props.fetchJobTree}
          onClickClose={this.props.closeJobSelectDialog}
        />
        <DialogFrame
          title={msg().Cal_Lbl_DailySummary}
          showFooter={false}
          className={`${ROOT}`}
          headerSub={this.renderDateSelector()}
          hide={this.props.hideDialog}
        >
          <div className={`${ROOT}__contents`}>
            <div className={`${ROOT}__calender`}>
              <Calendar
                day={this.props.selectedDay}
                events={this.props.events}
                openEventListPopup={this.props.openEventListPopup}
              />
            </div>
            <div className={`${ROOT}__summary`}>
              <div className={`${ROOT}__summary-scrollable-wrapper`}>
                <Summary
                  showJobSelectDialog={this.props.showJobSelectDialog}
                  saveDailySummary={this.props.saveDailySummary}
                  saveDailySummaryAndRecordEndTimestamp={
                    this.props.saveDailySummaryAndRecordEndTimestamp
                  }
                  note={this.props.note}
                  timestampComment={this.props.timestampComment}
                  taskList={this.props.taskList}
                  workCategoryLists={this.props.workCategoryLists}
                  defaultJobId={this.props.defaultJobId}
                  deleteTask={this.props.deleteTask}
                  editTask={this.props.editTask}
                  editTaskTime={this.props.editTaskTime}
                  toggleDirectInput={this.props.toggleDirectInput}
                  reorderTaskList={this.props.reorderTaskList}
                  realWorkTime={this.props.realWorkTime}
                  isTemporaryWorkTime={this.props.isTemporaryWorkTime}
                  hideDialog={this.props.hideDialog}
                  editNote={this.props.editNote}
                  editTimestampComment={this.props.editTimestampComment}
                  isLocked={this.props.isLocked}
                  isEnableEndStamp={this.props.isEnableEndStamp}
                  isProxyMode={this.props.isProxyMode}
                />
              </div>
            </div>
          </div>
        </DialogFrame>
      </>
    );
  }
}
