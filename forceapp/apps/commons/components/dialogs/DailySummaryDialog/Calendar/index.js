// @flow

import * as React from 'react';
import moment from 'moment';

import type { BaseEvent } from '../../../../models/DailySummary/BaseEvent';
import CalendarWeeklyEvent from '../WeeklyView/CalendarWeeklyEvent';
import AllDayEvent from '../WeeklyView/AllDayEvent';
import EventListPopupButton from './EventListPopupButton';
import { addPositionAndWidth } from '../../../../models/DailySummary/BaseEvent';
import msg from '../../../../languages';

import './index.scss';
import '../WeeklyView/index.scss';
import '../WeeklyView/CalendarWeeklyEvent.scss';

const ROOT = 'commons-daily-summary-dialog-calendar';

type Props = $ReadOnly<{|
  day: moment,
  events: BaseEvent[],
  openEventListPopup: (
    e: SyntheticMouseEvent<HTMLButtonElement>,
    moment,
    BaseEvent[]
  ) => void,
|}>;

type State = {|
  events: BaseEvent[],
  allDayEvents: BaseEvent[],
|};

export default class Calendar extends React.Component<Props, State> {
  scrollableRef: Object;
  openEventListPopup: (e: SyntheticMouseEvent<HTMLButtonElement>) => void;

  constructor(props: Props) {
    super(props);

    this.openEventListPopup = this.openEventListPopup.bind(this);
    this.state = {
      ...this.getInitialState(props),
    };
  }

  getInitialState(props: Props): State {
    const normalEvents = props.events.filter((event) => {
      return !this.isAllDayEvent(event);
    });
    const allDayEvents = props.events.filter((event) => {
      return this.isAllDayEvent(event);
    });

    return {
      events: addPositionAndWidth(normalEvents, 100),
      allDayEvents,
    };
  }

  componentDidMount() {
    // Set the initial position to 7:00 AM
    this.scrollableRef.scrollTop = (7 * 60 * 2) / 3;
  }

  componentWillReceiveProps(nextProps: Props) {
    const prevAllDayEventsNum = this.state.allDayEvents.length;
    this.setState({
      ...this.getInitialState(nextProps),
    });

    const allDayEventsNum = this.state.allDayEvents.length;

    if (prevAllDayEventsNum !== allDayEventsNum) {
      window.setTimeout(() => {
        if (this.scrollableRef) {
          this.scrollableRef.scrollTop = (7 * 60 * 2) / 3;
        }
      }, 10);
    }
  }

  /**
   * 終日として表示する予定か？
   * 24時間以上の日またぎ予定もtrueとなるため注意
   * @param {object} event - 予定オブジェクト
   */
  isAllDayEvent(event: BaseEvent) {
    return event.isAllDay || event.layout.containsAllDay;
  }

  openEventListPopup(e: SyntheticMouseEvent<HTMLButtonElement>) {
    this.props.openEventListPopup(e, this.props.day, this.props.events);
  }

  renderAllDayEventPopupButton() {
    const numOmittedEvents = this.state.allDayEvents.length - 5;
    /* assert(0 < numOmittedEvents) */

    return (
      <div className={`${ROOT}__all-day-events-popup-button`}>
        <div className={`${ROOT}__all-day-events-popup-button-wrapper`}>
          <EventListPopupButton
            events={this.props.events}
            targetDay={this.props.day}
            numberOfHiddenEvents={numOmittedEvents}
          />
        </div>
      </div>
    );
  }

  renderAllDayEvents() {
    return (
      <div className={`${ROOT}__all-day-events-events-wrapper`}>
        <table className={`${ROOT}__all-day-events-events`}>
          <tbody>
            {this.state.allDayEvents.length > 0 ? (
              this.state.allDayEvents.slice(0, 5).map((event, i) => (
                <tr>
                  <AllDayEvent key={i} day={this.props.day} event={event} />
                </tr>
              ))
            ) : (
              <tr>
                {/* Insert a empty cell to show a border */}
                <td className={`${ROOT}__all-day-event--empty`} />
              </tr>
            )}
          </tbody>
        </table>
        {this.state.allDayEvents.length > 5
          ? this.renderAllDayEventPopupButton()
          : null}
      </div>
    );
  }

  renderAllDayEventsWrapper() {
    return (
      <div className={`${ROOT}__all-day-events-wrapper`}>
        <div className={`${ROOT}__all-day-events-label`}>
          {msg().Cal_Lbl_AllDay}
        </div>
        {this.renderAllDayEvents()}
      </div>
    );
  }

  renderNonAllDayEventsIndicators() {
    return (
      <div className={`${ROOT}__non-all-day-events-indicators`}>
        {new Array(24)
          .fill(0)
          .map((_, hour) => hour)
          .map((hour) => `0${String(hour)}:00`.slice(-5))
          .map((s, hour) => (
            <div
              key={String(hour)}
              className={`${ROOT}__non-all-day-events-indicator`}
            >
              {s}
            </div>
          ))}
      </div>
    );
  }

  renderNonAllDayEvents() {
    return (
      <div className={`${ROOT}__non-all-day-events`}>
        {/* FIXME set a flexible width to each event in hacky way */}
        {this.state.events.map((event, i) => (
          <CalendarWeeklyEvent key={i} event={event} />
        ))}
      </div>
    );
  }

  renderNonAllDayEventsGrid() {
    return (
      <div className={`${ROOT}__non-all-day-events-grid`}>
        {new Array(24)
          .fill(0)
          .map((_, hour) => hour)
          .map((hour) => (
            <div
              key={String(hour)}
              className={`${ROOT}__non-all-day-events-grid-cell`}
            >
              <div className={`${ROOT}__non-all-day-events-grid-divider`} />
            </div>
          ))}
        {this.renderNonAllDayEvents()}
      </div>
    );
  }

  renderNonAllDayEventsWrapper() {
    return (
      <div
        ref={(ref) => {
          if (ref) {
            this.scrollableRef = ref;
          }
        }}
        className={`${ROOT}__non-all-day-events-wrapper`}
      >
        {this.renderNonAllDayEventsIndicators()}
        {this.renderNonAllDayEventsGrid()}
      </div>
    );
  }

  render() {
    return (
      <div className={ROOT}>
        {this.renderAllDayEventsWrapper()}
        {this.renderNonAllDayEventsWrapper()}
      </div>
    );
  }
}
