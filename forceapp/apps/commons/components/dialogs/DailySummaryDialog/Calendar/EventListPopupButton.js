// @flow

import React, { useState } from 'react';
import moment from 'moment';

import type { BaseEvent } from '../../../../models/DailySummary/BaseEvent';
import EventListPopup from '../../../popups/EventListPopup';
import Button from '../../../buttons/Button';
import msg from '../../../../languages';

import './EventListPopupButton.scss';

const ROOT = 'commons-daily-summary-dialog-calendar-event-list-popup';

type Props = $ReadOnly<{|
  events: BaseEvent[],
  numberOfHiddenEvents: number,
  targetDay: moment,
|}>;

const Component = (props: Props) => {
  const [isOpened, setIsOpened] = useState(false);

  const allDayEvents = props.events.filter((event) => {
    return event.layout.containsAllDay === true;
  });

  return (
    <div className={ROOT}>
      <Button
        type="text"
        className={`${ROOT}__all-day-events-popup-button`}
        onClick={() => setIsOpened(true)}
      >
        {`${msg().Cal_Lbl_MoreEvents1} ${props.numberOfHiddenEvents} ${
          msg().Cal_Lbl_MoreEvents2
        }`}
      </Button>
      <EventListPopup
        targetDate={props.targetDay}
        events={allDayEvents}
        isOpened={isOpened}
        onClickClose={() => setIsOpened(false)}
      />
    </div>
  );
};

Component.display = 'EventListPopupButton';

export default Component;
