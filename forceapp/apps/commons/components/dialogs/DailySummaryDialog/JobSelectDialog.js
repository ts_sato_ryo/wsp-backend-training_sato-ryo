// @flow

import React from 'react';

import msg from '../../../languages';
import Modal from '../../Modal';
import MultiColumnFinder from '../MultiColumnFinder';
import type { Job } from '../../../../domain/models/time-tracking/Job';

import './JobSelectDialog.scss';

const ROOT = 'commons-dialogs-daily-summary-dialog-job-select-dialog';

type Props = $ReadOnly<{|
  jobTree: { ...Job, isGroup: boolean }[][],
  opening: boolean,

  onClickItem: (selectedItem: Job, existingItems: ?(Job[][])) => void,
  onClickClose: () => void,
|}>;

const JobSelectDialog = (props: Props) => {
  return (
    <div className={ROOT}>
      <Modal
        onClosed={props.onClickClose}
        isOpen={props.opening}
        component={
          <MultiColumnFinder
            items={props.jobTree}
            typeName={msg().Com_Lbl_Job}
            showFavorites={false}
            showHistory={false}
            parentSelectable
            onClickItem={props.onClickItem}
            onClickCloseButton={props.onClickClose}
          />
        }
      />
    </div>
  );
};

JobSelectDialog.displayName = 'JobSelectDialog';

export default JobSelectDialog;
