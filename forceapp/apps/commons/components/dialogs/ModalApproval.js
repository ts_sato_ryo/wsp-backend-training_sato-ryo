import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../languages';

import InputComment from '../CommentInput';

import Button from '../buttons/Button';

import './ModalApproval.scss';

/**
 * 申請ダイアログ
 * Dialogコンポーネントからimportして使われる
 */
export default class Approval extends React.Component {
  static get propTypes() {
    return {
      onModalClosed: PropTypes.func.isRequired,
      onSubmitApproval: PropTypes.func.isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      comment: '',
    };

    this.onCommentChanged = this.onCommentChanged.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onClose() {
    this.props.onModalClosed();
  }

  onSubmit() {
    this.props.onModalClosed();
    this.props.onSubmitApproval(this.state.comment);
  }

  onCommentChanged(comment) {
    this.setState({
      comment,
    });
  }

  render() {
    return (
      <div className="ts-modal-approval slds">
        <div className="slds-grid ts-modal-approval__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-modal-approval__header__left">
              <div className="ts-modal-approval__header__title">
                {msg().Exp_Lbl_Request}
              </div>
            </div>
          </div>
        </div>

        <div className="ts-modal-approval__contents">
          <div className="ts-modal-approval__wrap">
            <div className="slds-grid ts-modal-approval__input">
              <div className="slds-col slds-size--2-of-12 slds-align-middle">
                <div className="ts-modal-approval__input-title">
                  {msg().Exp_Lbl_RequestTarget}
                </div>
                <div className="ts-modal-approval__input-separate">:</div>
              </div>
              <div className="slds-col slds-size--10-of-12 slds-align-middle">
                <div className="ts-modal-approval__input-radio">
                  <label className="ts-modal-approval__input-text">
                    <input type="radio" value="all" name="approvalType" />
                    &nbsp;{msg().Com_Sel_All}
                  </label>
                  <label className="ts-modal-approval__input-text">
                    <input type="radio" value="selected" name="approvalType" />
                    &nbsp;{msg().Com_Sel_SelectedOnly}
                  </label>
                </div>
              </div>
            </div>

            <div className="slds-grid ts-modal-approval__input">
              <div className="slds-col slds-size--12-of-12 slds-align-middle">
                <InputComment
                  comment={this.state.comment}
                  photoUrl={require('../../images/Sample_photo001.png')}
                  onCommentChanged={this.onCommentChanged}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="slds-grid ts-modal-approval__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-modal-approval__btn-cancel">
              <Button onClick={this.onClose}>{msg().Com_Btn_Cancel}</Button>
            </div>

            <div className="ts-modal-approval__btn-approval">
              <Button type="primary" onClick={this.onSubmit}>
                {msg().Com_Btn_Request}
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
