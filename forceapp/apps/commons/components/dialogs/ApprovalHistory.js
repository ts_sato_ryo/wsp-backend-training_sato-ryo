import PropTypes from 'prop-types';
import React from 'react';
import moment from 'moment';
import msg from '../../languages';

import Button from '../buttons/Button';

import './ApprovalHistory.scss';

/**
 * 申請履歴ダイアログ
 * Dialogコンポーネントからimportして使われる
 */
export default class ApprovalHistory extends React.Component {
  static get propTypes() {
    return {
      onClickCloseButton: PropTypes.func.isRequired,
      approvalHistoryList: PropTypes.array.isRequired,
    };
  }

  render() {
    return (
      <div className="ts-modal-approval-history slds">
        <div className="slds-grid ts-modal-approval-history__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-modal-approval-history__header-icon">
              <img src={require('../../images/iconApprovalHistory.png')} />
            </div>
            <div className="ts-modal-approval-history__header-title">
              {msg().Appr_Lbl_ApprovalHistory}
            </div>
          </div>
        </div>

        <div className="ts-modal-approval-history__body">
          <table className="ts-modal-approval-history__table">
            <thead>
              <tr>
                <th className="step">{msg().Appr_Lbl_Step}</th>
                <th className="createdDate">{msg().Exp_Lbl_Date}</th>
                <th className="status">{msg().Appr_Lbl_Situation}</th>
                <th className="originalActor">
                  {msg().Appr_Lbl_OriginalActor}
                </th>
                <th className="actor">{msg().Appr_Lbl_Actor}</th>
                <th>{msg().Appr_Lbl_Comments}</th>
              </tr>
            </thead>
            <tbody>
              {this.props.approvalHistoryList.map((item) => {
                const createdDate = moment(item.createdDate).format(
                  'YYYY/MM/DD'
                );
                return (
                  <tr>
                    <td>{item.step}</td>
                    <td>{createdDate}</td>
                    <td>{item.status}</td>
                    <td>{item.originalActor.name}</td>
                    <td>
                      <img
                        src={item.actor.photoUrl}
                        className="ts-modal-approval-history__icon-actor"
                      />
                      <span>{item.actor.name}</span>
                    </td>
                    <td className="left">{item.comment}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        <div className="slds-grid ts-modal-approval-history__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <Button
              className="ts-modal-approval-history__btn-close"
              onClick={() => this.props.onClickCloseButton()}
            >
              {msg().Com_Btn_Close}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
