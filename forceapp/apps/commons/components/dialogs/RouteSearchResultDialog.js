import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../languages';

import Button from '../buttons/Button';
import FormatUtil from '../../utils/FormatUtil';
import ObjectUtil from '../../utils/ObjectUtil';

import RouteTransportationIcon from '../Route/RouteTransportationIcon';

import ImgIconRouteVia from '../../images/iconRouteVia.png';
import ImgIconRouteViaTwin from '../../images/iconRouteViaTwin.png';
import ImgIconYasui from '../../images/iconYasui.png';
import ImgIconHayai from '../../images/iconHayai.png';
import ImgIconRaku from '../../images/iconRaku.png';

import './RouteSearchResultDialog.scss';

/**
 * 経路の検索結果を表示する
 */
export default class RouteSearchResultDialog extends React.Component {
  static get propTypes() {
    return {
      onClickCloseButton: PropTypes.func.isRequired,
      onClickSelectRouteButton: PropTypes.func.isRequired,
      searchedRoute: PropTypes.object.isRequired,
    };
  }

  renderExpressAmount(path, expressMapItem) {
    // TODO:画面からのパラメータを元に優先席順の初期値を決定
    // const seatPreference = 2;

    if (expressMapItem.fee === 0 && expressMapItem.green === 0) {
      return null;
    }

    return (
      <select>
        <option>
          指定席(+￥{FormatUtil.convertToIntegerString(expressMapItem.fee)})
        </option>
        <option>通常料金</option>
        <option>
          グリーン席(+￥
          {FormatUtil.convertToIntegerString(expressMapItem.green)})
        </option>
      </select>
    );
  }

  renderStation(path, lineType, isFrom) {
    const isVia = lineType !== '';
    let lineClassName = 'ts-route-dialog__result-route__map-line';
    if (lineType) {
      lineClassName += ` ${lineClassName}--${lineType}`;
    }

    return (
      <li>
        <div className="ts-route-dialog__result-route__title">
          <p className="ts-route-dialog__result-route__title-station">
            {isFrom ? path.fromName : path.toName}
          </p>
        </div>
        <div className="ts-route-dialog__result-route__map">
          <span className={lineClassName} />
          {isVia ? <img src={ImgIconRouteVia} alt="" /> : null}
          {!isVia && path.transfer ? (
            <img src={ImgIconRouteViaTwin} alt="" />
          ) : null}
        </div>
      </li>
    );
  }

  renderTransration(path, lastFareKey, fare, expressMapItem) {
    // 前回と異なる運行会社か
    const isAnotherCom = lastFareKey !== path.fareKey;
    // 特急料金はあるか
    const hasExpress = isAnotherCom && expressMapItem;

    return (
      <li>
        <div className="ts-route-dialog__result-route__title">
          <p className="ts-route-dialog__result-route__title-transportation">
            <RouteTransportationIcon lineType={path.lineType} />
            {path.lineName}
          </p>
        </div>
        <div className="ts-route-dialog__result-route__map">
          <p className="ts-route-dialog__result-route__map-fare">
            {isAnotherCom
              ? `￥${FormatUtil.convertToIntegerString(fare)}`
              : '( → )'}
          </p>
          {hasExpress ? this.renderExpressAmount(path, expressMapItem) : ''}
          <span className="ts-route-dialog__result-route__map-line" />
        </div>
      </li>
    );
  }

  // NOTE:折り返しを考慮し、出発駅と次の駅までの経路を1セットとして表示する
  renderRoute(routeItem) {
    const { pathList, fareMap, expressMap } = routeItem;
    const result = pathList.map((path, idx, array) => {
      const lineType = idx === 0 ? 'right' : '';
      const lastFareKey = idx === 0 ? '' : array[idx - 1].fareKey;
      const fare = fareMap[path.fareKey].fare;
      const expressMapItem = ObjectUtil.getOrDefault(
        expressMap,
        path.expressKey,
        null
      );

      return (
        <li key={path.key}>
          <ul>
            {this.renderStation(path, lineType, true)}
            {this.renderTransration(path, lastFareKey, fare, expressMapItem)}
          </ul>
        </li>
      );
    });
    // 最後の路線情報から到着駅の情報を表示
    // FIXME: key値の"last"については再考する
    result.push(
      <li key="last">
        <ul>
          {this.renderStation(pathList[pathList.length - 1], 'left', false)}
        </ul>
      </li>
    );
    return result;
  }

  renderResultTableBody(item) {
    return (
      <tr key={item.key}>
        <td className="ts-route-dialog__result-title">
          <div className="ts-route-dialog__result-title__amount">
            ￥{FormatUtil.convertToIntegerString(item.cost)}
          </div>
          <div className="ts-route-dialog__result-title__amount--round-trip">
            (往復:￥{FormatUtil.convertToIntegerString(item.roundTripCost)})
          </div>
          <ul className="ts-route-dialog__result-title__icon">
            {item.status.isCheapest ? (
              <li>
                <img src={ImgIconYasui} alt="安" />
              </li>
            ) : null}
            {item.status.isEarliest ? (
              <li>
                <img src={ImgIconHayai} alt="早" />
              </li>
            ) : null}
            {item.status.isMinTransfer ? (
              <li>
                <img src={ImgIconRaku} alt="楽" />
              </li>
            ) : null}
          </ul>
        </td>
        <td className="ts-route-dialog__result-route">
          <ul>{this.renderRoute(item)}</ul>
        </td>
        <td className="ts-route-dialog__result-btn-area">
          <Button
            type="primary"
            onClick={() => {
              this.props.onClickSelectRouteButton(item);
            }}
          >
            選択
          </Button>
        </td>
      </tr>
    );
  }

  render() {
    if (!this.props.searchedRoute.route) {
      return null;
    }
    const { routeList } = this.props.searchedRoute.route;

    return (
      <div className="ts-route-dialog">
        <div className="slds-grid ts-route-dialog__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-route-dialog__header-title">路線検索</div>
          </div>
        </div>

        <div className="ts-route-dialog__body">
          <div className="ts-route-dialog__result">
            <table>
              <tbody>
                {routeList.map((item) => {
                  return this.renderResultTableBody(item);
                })}
              </tbody>
            </table>
          </div>
        </div>
        <div className="slds-grid ts-route-dialog__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <Button
              className="ts-route-dialog__btn-close"
              onClick={this.props.onClickCloseButton}
            >
              {msg().Com_Btn_Close}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
