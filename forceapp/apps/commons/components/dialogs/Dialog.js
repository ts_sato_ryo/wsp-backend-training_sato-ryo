import PropTypes from 'prop-types';
import React from 'react';
import { CSSTransition as ReactCSSTransitionGroup } from 'react-transition-group';

import msg from '../../languages';

import MultiColumnFinder from './MultiColumnFinder';
import ModalApproval from './ModalApproval';
import ApprovalHistory from './ApprovalHistory';
import PaymentDateSettingDialog from './PaymentDateSettingDialog';
import PayMethodSettingDialog from './PayMethodSettingDialog';
import ExpReportListSearchDialog from './ExpReportListSearchDialog';
import AttendanceFixDaily from './AttendanceFixDaily';
import AttendanceApplicationVacation from './AttendanceApplicationVacation';
import ApprovalList from './ApprovalList';
import TimeTrackingControl from './TimeTrackingControl';
import RouteSearchResultDialog from './RouteSearchResultDialog';
import AdminDialog from './AdminDialog';

import './Dialog.scss';

/**
 * 共通コンポーネント Dialog
 * モーダルの表示制御およびwrap用レイアウトを提供する
 * props.dialog.typeを元に内容を出し分ける
 */
export default class Dialog extends React.Component {
  static get propTypes() {
    return {
      style: PropTypes.object.isRequired,
      contents: PropTypes.array.isRequired,
      dialog: PropTypes.object.isRequired,
      dialogTypeList: PropTypes.object.isRequired,
      approvalHistoryList: PropTypes.array.isRequired,
      costCenterList: PropTypes.array.isRequired,
      expTypeList: PropTypes.array.isRequired,
      jobList: PropTypes.array.isRequired,
      searchedRoute: PropTypes.object.isRequired,
      hideDialog: PropTypes.func.isRequired,
      onClickExpTypeItem: PropTypes.func,
      onSubmitReport: PropTypes.func,
      onClickCostCenterItem: PropTypes.func,
      onClickJobItem: PropTypes.func,
      onClickSettingButton: PropTypes.func,
      onClickSelectRouteButton: PropTypes.func,
    };
  }

  renderDialogBody() {
    switch (this.props.dialog.type) {
      case this.props.dialogTypeList.ADD_RECORD:
        return (
          <MultiColumnFinder
            items={this.props.expTypeList}
            typeName={msg().Exp_Lbl_ExpenseType}
            showFavorites={false}
            showHistory={false}
            onClickItem={this.props.onClickExpTypeItem}
            onClickCloseButton={this.props.hideDialog}
          />
        );
      case this.props.dialogTypeList.APPROVAL_HISTORY:
        return (
          <ApprovalHistory
            approvalHistoryList={this.props.approvalHistoryList}
            onClickCloseButton={this.props.hideDialog}
          />
        );
      case this.props.dialogTypeList.APPROVAL_REQUEST:
        return (
          <ModalApproval
            onModalClosed={this.props.hideDialog}
            onSubmitApproval={this.props.onSubmitReport}
          />
        );
      case this.props.dialogTypeList.COST_CENTER_FINDER:
        return (
          <MultiColumnFinder
            items={this.props.costCenterList}
            typeName={msg().Exp_Lbl_CostCenter}
            showFavorites={false}
            showHistory={false}
            onClickItem={this.props.onClickCostCenterItem}
            onClickCloseButton={this.props.hideDialog}
          />
        );
      case this.props.dialogTypeList.EXP_TYPE_FINDER:
        return (
          <MultiColumnFinder
            items={this.props.expTypeList}
            typeName={msg().Exp_Lbl_ExpenseType}
            showFavorites={false}
            showHistory={false}
            onClickItem={this.props.onClickExpTypeItem}
            onClickCloseButton={this.props.hideDialog}
          />
        );
      case this.props.dialogTypeList.JOB_FINDER:
        return (
          <MultiColumnFinder
            items={this.props.jobList}
            typeName={msg().Com_Lbl_Job}
            showFavorites={false}
            showHistory={false}
            onClickItem={this.props.onClickJobItem}
            onClickCloseButton={this.props.hideDialog}
          />
        );
      case this.props.dialogTypeList.PAYMENT_DATE_SETTING:
        return (
          <PaymentDateSettingDialog
            onClickCloseButton={this.props.hideDialog}
            onClickSettingButton={this.props.onClickSettingButton}
          />
        );
      case this.props.dialogTypeList.PAY_METHOD_SETTING:
        return (
          <PayMethodSettingDialog
            onClickCloseButton={this.props.hideDialog}
            onClickSettingButton={this.props.onClickSettingButton}
          />
        );
      case this.props.dialogTypeList.SEARCH_EXP_REPORT:
        return (
          <ExpReportListSearchDialog
            onClickCloseButton={this.props.hideDialog}
          />
        );
      case this.props.dialogTypeList.ATTENDANCE_FIX_DAILY:
        return <AttendanceFixDaily hideDialog={this.props.hideDialog} />;
      case this.props.dialogTypeList.ATTENDANCE_APPLICATION_VACATION:
        return (
          <AttendanceApplicationVacation hideDialog={this.props.hideDialog} />
        );
      case this.props.dialogTypeList.APPROVAL_LIST:
        return <ApprovalList hideDialog={this.props.hideDialog} />;
      case this.props.dialogTypeList.TIME_TRACKING_CONTROL:
        return <TimeTrackingControl hideDialog={this.props.hideDialog} />;
      case this.props.dialogTypeList.ROUTE_SEARCH_RESULT:
        return (
          <RouteSearchResultDialog
            onClickCloseButton={this.hideDialog}
            searchedRoute={this.props.searchedRoute}
            onClickSelectRouteButton={this.props.onClickSelectRouteButton}
          />
        );
      case this.props.dialogTypeList.ADMIN:
        return (
          <AdminDialog onClickCloseButton={this.props.hideDialog}>
            {this.props.contents}
          </AdminDialog>
        );
      default:
        return null;
    }
  }

  render() {
    if (!this.props.dialog.show) {
      return null;
    }
    return (
      <ReactCSSTransitionGroup
        classNames="dialog-pc"
        timeout={{ enter: 300, exit: 0 }}
        component="div"
      >
        <div className="dialog">
          <div className="dialog__overlay" onClick={this.props.hideDialog} />
          <div className="dialog__wrap" style={this.props.style}>
            <div className="dialog__contents">{this.renderDialogBody()}</div>
          </div>
        </div>
      </ReactCSSTransitionGroup>
    );
  }
}
