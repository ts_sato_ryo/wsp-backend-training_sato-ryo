import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import DialogListItemLayout from './DialogListItemLayout';

import './DialogListLayout.scss';

export default class DialogListLayout extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      className: PropTypes.string,
    };
  }

  render() {
    const clsnm = classNames(
      this.props.className,
      'ts-dialog-list-layout',
    );

    return (
      <ul className={clsnm}>
        {this.props.children}
      </ul>
    );
  }
}

DialogListLayout.Item = DialogListItemLayout;
