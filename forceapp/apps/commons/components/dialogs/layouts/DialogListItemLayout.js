import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import './DialogListLayout.scss';

export default class DialogListItemLayout extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      className: PropTypes.string,
    };
  }

  render() {
    const clsnm = classNames(
      this.props.className,
      'ts-dialog-list-layout__item',
    );

    return (
      <li className={clsnm}>
        {this.props.children}
      </li>
    );
  }
}
