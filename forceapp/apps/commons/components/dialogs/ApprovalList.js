import PropTypes from 'prop-types';
import React from 'react';

import Button from '../buttons/Button';

import imgIconApplicationVacation from '../../../timesheet-pc/images/iconApplicationVacation.png';
import ImgBtnCloseDialog from '../../images/btnCloseDialog.png';
import ImgPartsIconExportCSV from '../../../timesheet-pc/images/partsIconExportCSV.png';

import ImgIconStatusApprovedNoPhoto from '../../images/iconStatusApprovedNoPhoto.png';
import ImgIconStatusSubmit from '../../images/iconStatusSubmit.png';
import ImgSampleIcon01 from '../../../timesheet-pc/images/imgSampleIcon01.jpg';
import ImgSampleIcon02 from '../../../timesheet-pc/images/imgSampleIcon02.jpg';

import './ApprovalList.scss';

export default class ApprovalList extends React.Component {
  static get propTypes() {
    return {
      hideDialog: PropTypes.func.Requied,
    };
  }

  render() {
    return (
      <div className="dialog__inner approval-list">
        <button className="dialog__close" onClick={this.props.hideDialog}>
          <img src={ImgBtnCloseDialog} alt="close" />
        </button>
        <div className="dialog__header">
          <div className="dialog__header-title">
            <img
              className="dialog__header-title-icon"
              src={imgIconApplicationVacation}
              alt=""
              aria-hidden="true"
            />勤怠申請一覧
          </div>

          <div className="dialog__header-subInfo">
            <div className="ts-header-status">
              <span className="ts-header-status__title">休暇日数</span>
              <span className="ts-header-status__value tmst-value-frame tmst-value-frame--align-right tmst-value-frame--color-primary">
                10
              </span>
              <span className="ts-header-status__title">消化日数</span>
              <span className="ts-header-status__value tmst-value-frame tmst-value-frame--align-right tmst-value-frame--color-primary">
                10
              </span>
              <span className="ts-header-status__title">休暇予定数</span>
              <span className="ts-header-status__value tmst-value-frame tmst-value-frame--align-right tmst-value-frame--color-alert">
                3
              </span>
            </div>
          </div>
        </div>
        <div className="dialog__body approval-list">
          <div className="approval-list-item-list">
            <div className="approval-list-item-list__search">
              <div className="approval-list-item-list__search-conditions">
                <select
                  className="slds-select approval-list-item-list__search-select"
                  name=""
                  id=""
                >
                  <option value="">すべて</option>
                </select>

                <input
                  type="text"
                  className="slds-input approval-list-item-list__search-text-field"
                />

                <Button className="approval-list-item-list__search-button">
                  詳細
                </Button>
              </div>

              <ul className="approval-list-item-list__pager approval-list-item-list__pager--disabled">
                <li className="approval-list-item-list__pager-item approval-list-item-list__pager-item--prev">
                  Previous
                </li>
                <li className="approval-list-item-list__pager-item approval-list-item-list__pager-item--next">
                  Next
                </li>
              </ul>
            </div>

            <div className="approval-list-item-list__body">
              <div className="approval-list-item-table atndc-modal-item-table">
                <table className="approval-list-item-table__header">
                  <tr className="approval-list-item-table__row">
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--status">
                      状態
                    </td>
                    <td className="approval-list-item-table__col approval-list-item-table__col--highlight atndc-modal-item-table__col--application-date">
                      申請日
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--start">
                      開始
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--end">
                      終了
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--identification">
                      申請種別
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--applicant">
                      承認者
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--approval-date">
                      承認日
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--paid-holidays">
                      有給残日数
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--remarks">
                      備考
                    </td>
                  </tr>
                </table>

                <table className="approval-list-item-table__body">
                  <tr className="approval-list-item-table__row">
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--status">
                      <img src={ImgIconStatusApprovedNoPhoto} alt="承認" />
                    </td>
                    <td className="approval-list-item-table__col approval-list-item-table__col--highlight atndc-modal-item-table__col--application-date">
                      2017/01/05
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--start">
                      2017/04/15
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--end">
                      2017/04/20
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--identification">
                      年次有給休暇
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--applicant">
                      <img
                        className="atndc-modal-item-table__col-applicant-img"
                        src={ImgSampleIcon02}
                        aria-hidden="true"
                        alt=""
                      />承認太郎
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--approval-date">
                      2017/04/05
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--paid-holidays">
                      9
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--remarks" />
                  </tr>
                </table>

                <table className="approval-list-item-table__body">
                  <tr className="approval-list-item-table__row">
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--status">
                      <img src={ImgIconStatusSubmit} alt="承認" />
                    </td>
                    <td className="approval-list-item-table__col approval-list-item-table__col--highlight atndc-modal-item-table__col--application-date">
                      2017/01/05
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--start">
                      2017/04/15
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--end" />
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--identification">
                      残業申請
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--applicant">
                      <img
                        className="atndc-modal-item-table__col-applicant-img"
                        src={ImgSampleIcon01}
                        aria-hidden="true"
                        alt=""
                      />承認太郎
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--approval-date" />
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--paid-holidays">
                      10
                    </td>
                    <td className="approval-list-item-table__col atndc-modal-item-table__col--remarks">
                      備考
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div className="dialog__footer">
          <div className="dialog__footer-container">
            <Button
              className="approval-list__button"
              onClick={this.props.hideDialog}
            >
              <img src={ImgPartsIconExportCSV} alt="CSV" /> エクスポート
            </Button>
            <Button
              className="approval-list__button"
              onClick={this.props.hideDialog}
            >
              閉じる
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
