// @flow

import * as React from 'react';
import className from 'classnames';

import DialogFrame from './DialogFrame';
import Button from '../buttons/Button';
import msg from '../../languages';
import iconConfirm from '../../images/iconConfirm.png';

import './ConfirmDialog.scss';

const ROOT = 'commons-dialogs-confirm-dialog';

type Props = {
  onClickOk: () => void,
  onClickCancel: () => void,
  children: ?React.Node,
  okButtonLabel?: ?string,
  cancelButtonLabel?: ?string,
  className?: $npm$classnames$Classes | Array<$npm$classnames$Classes>,
};

const ConfirmDialog = (props: Props) => {
  const children =
    Array.isArray(props.children) && typeof props.children[0] === 'string'
      ? props.children.join('\n')
      : props.children;

  return (
    <DialogFrame
      title={msg().Com_Lbl_Confirm}
      className={className(ROOT, props.className)}
      zIndex={5999999}
      initialFocus={`${ROOT}__cancel-button`}
      footer={
        <DialogFrame.Footer>
          <Button id={`${ROOT}__cancel-button`} onClick={props.onClickCancel}>
            {props.cancelButtonLabel || msg().Com_Btn_Cancel}
          </Button>
          <Button
            id={`${ROOT}__ok-button`}
            type="primary"
            onClick={props.onClickOk}
          >
            {props.okButtonLabel || msg().Com_Btn_Ok}
          </Button>
        </DialogFrame.Footer>
      }
      hide={() => {}}
      withoutCloseButton
    >
      <div className={`${ROOT}__message`}>
        <div className={`${ROOT}__icon`}>
          <img src={iconConfirm} alt="INFO" />
        </div>
        <div className={`${ROOT}__content`}>
          {typeof children === 'string' ? <p>{children}</p> : children}
        </div>
      </div>
    </DialogFrame>
  );
};
export default ConfirmDialog;

type ContentProps = {|
  children: React$Node,
  okButtonLabel?: ?string,
  cancelButtonLabel?: ?string,
  className?: $npm$classnames$Classes | Array<$npm$classnames$Classes>,
|};

export const bindHandlerToConfirmDialog = (handlerProps: {|
  onClickOk: () => void,
  onClickCancel: () => void,
|}) => (contentProps: ContentProps) => {
  return (
    <ConfirmDialog
      onClickOk={handlerProps.onClickOk}
      onClickCancel={handlerProps.onClickCancel}
      okButtonLabel={contentProps.okButtonLabel}
      cancelButtonLabel={contentProps.cancelButtonLabel}
      className={contentProps.className}
    >
      {contentProps.children}
    </ConfirmDialog>
  );
};

export type CustomConfirmDialogComponent<
  T,
  C = {
    (props: ContentProps, context: any): React$Element<typeof ConfirmDialog>,
    displayName?: ?string,
    propTypes?: $Shape<{ [_: $Keys<ContentProps>]: any }>,
    contextTypes?: any,
  }
> = {
  (
    props: {
      ConfirmDialog: C,
      params: T,
    },
    context: any
  ): React$Element<C>,
  displayName?: ?string,
  propTypes?: $Shape<{ [_: 'ConfirmDialog' | 'params']: any }>,
  contextTypes?: any,
};
