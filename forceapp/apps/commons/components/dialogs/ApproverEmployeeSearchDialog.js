// @flow
import * as React from 'react';
import msg from '../../languages';
import ProxyEmployeeSelectDialog from '../../../../widgets/dialogs/ProxyEmployeeSelectDialog/components/ProxyEmployeeSelectDialog';

export default function ApproverEmployeeSearchDialog(props: *) {
  return (
    <ProxyEmployeeSelectDialog
      {...props}
      title={msg().Com_Lbl_SearchApproverEmployee}
    />
  );
}
