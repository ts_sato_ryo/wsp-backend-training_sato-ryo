import PropTypes from 'prop-types';
import React from 'react';

import msg from '../../languages';
import Button from '../buttons/Button';

import './ExpReportListSearchDialog.scss';

/**
 * 精算確定画面で使用
 * 経費申請検索用ダイアログ
 * Dialogコンポーネントからimportして使われる
 */
export default class ExpReportListSearchDialog extends React.Component {
  static get propTypes() {
    return {
      onClickCloseButton: PropTypes.func.isRequired,
    };
  }

  render() {
    return (
      <div className="ts-search-dialog slds">
        <div className="slds-grid ts-search-dialog__header">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <div className="ts-search-dialog__header-title">
              {msg().Exp_Lbl_ExpenseReportSearch}
            </div>
          </div>
        </div>

        <div className="ts-search-dialog__body-wrap">
          <div className="ts-search-dialog__body">
            <div className="ts-search-dialog__body-input">
              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Com_Lbl_Status}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--4-of-12 slds-align-middle">
                  <div className="value">
                    <select>
                      <option>{msg().Com_Sel_StatusApproved}</option>
                      <option>{msg().Exp_Lbl_ApprovedOption}</option>
                      <option>{msg().Exp_Lbl_Finalized}</option>
                    </select>
                  </div>
                </div>
                <div className="slds-col slds-size--5-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_ApplicationDate}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--1-of-12 slds-align-middle">
                  <div className="inner-separate">〜</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_DateOfUse}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--1-of-12 slds-align-middle">
                  <div className="inner-separate">〜</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_Department}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--4-of-12 slds-align-middle">
                  <div className="value">
                    <select>
                      <option />
                    </select>
                  </div>
                </div>
                <div className="slds-col slds-size--5-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle" />
                <div className="slds-col slds-size--9-of-12 slds-align-middle">
                  <div className="value">
                    <input id="departmentCheckbox" type="checkbox" />
                    <label htmlFor="departmentCheckbox">
                      &nbsp;{msg().Exp_Msg_SearchByDepartment}
                    </label>
                  </div>
                </div>
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_EmployeeCodeFull}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--6-of-12 slds-align-middle" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_EmployeeName}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--5-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--4-of-12 slds-align-middle" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_RequestNo}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--6-of-12 slds-align-middle" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Appr_Lbl_Title}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--9-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">
                    {msg().Exp_Lbl_PersonalAmountTransferred}
                  </div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--1-of-12 slds-align-middle">
                  <div className="inner-separate">〜</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Appr_Lbl_TAndERequestNumber}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--6-of-12 slds-align-middle" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_PreapprovalSubject}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--9-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_PreApplication}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--4-of-12 slds-align-middle">
                  <div className="value">
                    <select>
                      <option />
                      <option>{msg().Exp_Sel_DomesticTravel}</option>
                      <option>{msg().Exp_Sel_OverseasTravel}</option>
                      <option>{msg().Exp_Sel_Entertainment}</option>
                      <option>{msg().Exp_Sel_GeneralExpense}</option>
                      <option>{msg().Exp_Sel_CashAdvance}</option>
                    </select>
                  </div>
                </div>
                <div className="slds-col slds-size--5-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">
                    {msg().Exp_Lbl_ProvisionalPaymentAmount}
                  </div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--1-of-12 slds-align-middle">
                  <div className="inner-separate">〜</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>

              <div className="slds-grid">
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="key">{msg().Exp_Lbl_ApprovalDate}</div>
                  <div className="separate">:</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--1-of-12 slds-align-middle">
                  <div className="inner-separate">〜</div>
                </div>
                <div className="slds-col slds-size--3-of-12 slds-align-middle">
                  <div className="value">
                    <input type="text" />
                  </div>
                </div>
                <div className="slds-col slds-size--2-of-12" />
              </div>
            </div>
          </div>
        </div>

        <div className="slds-grid ts-search-dialog__footer">
          <div className="slds-col slds-size--12-of-12 slds-align-middle">
            <Button
              type="primary"
              className="ts-search-dialog__btn-search"
              onClick={() => this.props.onClickCloseButton()}
            >
              {msg().Exp_Btn_Search}
            </Button>
            <Button
              className="ts-search-dialog__btn-close"
              onClick={() => this.props.onClickCloseButton()}
            >
              {msg().Com_Btn_Cancel}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
