import PropTypes from 'prop-types';
import React from 'react';

import DialogListLayout from './layouts/DialogListLayout';
import Button from '../buttons/Button';
import HorizontalLayout from '../fields/layouts/HorizontalLayout';
import Label from '../fields/Label';
import TextField from '../fields/TextField';
import CommentFieldIconOutside from '../fields/CommentFieldIconOutside';
import DateRangeField from '../fields/DateRangeField';
import SelectField from '../fields/SelectField';

import imgIconApplicationVacation from '../../../timesheet-pc/images/iconApplicationVacation.png';
import imgBtnCloseDialog from '../../images/btnCloseDialog.png';

import './AttendanceApplicationVacation.scss';

// dummy
import ImgSamplePhoto001 from '../../images/Sample_photo001.png';

export default class AttendanceApplicationVacation extends React.Component {
  static get propTypes() {
    return {
      hideDialog: PropTypes.func.Requied,
    };
  }

  render() {
    return (
      <div className="dialog__inner attendance-application-vacation">
        <button className="dialog__close" onClick={this.props.hideDialog}>
          <img src={imgBtnCloseDialog} alt="close" />
        </button>
        <div className="dialog__header">
          <div className="dialog__header-title">
            <img
              className="dialog__header-title-icon"
              src={imgIconApplicationVacation}
              alt=""
              aria-hidden="true"
            />休暇申請
          </div>

          <div className="dialog__header-subInfo">
            <time className="attendance-application-vacation__date">
              2017年05月09日&#xA0;火曜日
            </time>
          </div>
        </div>
        <div className="dialog__body attendance-application-vacation">
          <div className="attendance-application-vacation__list">
            <DialogListLayout>
              <DialogListLayout.Item>
                <Label text="休暇取得" labelCols="4" childCols="8">
                  <SelectField
                    id="attendance-application-vacation__list-select"
                    options={[
                      { value: '年次有給休暇', text: '年次有給休暇' },
                      { value: '午前半休', text: '午前半休' },
                      { value: '午後半休', text: '午後半休' },
                      { value: '時間単位有休', text: '時間単位有休' },
                      { value: '代休', text: '代休' },
                      { value: '午前半代休', text: '午前半代休' },
                      { value: '午後半代休', text: '午後半代休' },
                      { value: '慶弔休暇', text: '慶弔休暇' },
                      { value: '育児休暇', text: '育児休暇' },
                      { value: '欠勤', text: '欠勤' },
                    ]}
                  />
                </Label>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <HorizontalLayout>
                  <HorizontalLayout.Label cols={4} required>
                    有給日数
                  </HorizontalLayout.Label>
                  <HorizontalLayout.Body
                    className="attendance-application-vacation__list-text"
                    cols={8}
                  >
                    0日
                  </HorizontalLayout.Body>
                </HorizontalLayout>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <Label text="期間" labelCols="4" childCols="8">
                  <DateRangeField id="item3" required />
                </Label>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <Label text="連絡先" labelCols="4" childCols="8">
                  <TextField id="item3" required />
                </Label>
              </DialogListLayout.Item>

              <DialogListLayout.Item>
                <Label
                  className="attendance-application-vacation__list-remarks"
                  text="備考"
                  require
                  labelCols="4"
                  childCols="8"
                >
                  <CommentFieldIconOutside
                    icon={ImgSamplePhoto001}
                    id="item5"
                  />
                </Label>
              </DialogListLayout.Item>
            </DialogListLayout>
          </div>
        </div>

        <div className="dialog__footer">
          <div className="dialog__footer-container">
            <Button
              className="attendance-application-vacation__button"
              onClick={this.props.hideDialog}
            >
              キャンセル
            </Button>
            <Button
              className="attendance-application-vacation__button"
              type="primary"
              onClick={this.props.hideDialog}
            >
              承&#xA0;認
            </Button>
          </div>

          <div className="dialog__footer-container">
            <a>承認履歴</a>
          </div>
        </div>
      </div>
    );
  }
}
