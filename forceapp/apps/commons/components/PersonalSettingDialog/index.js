/* @flow */
import React from 'react';
import classNames from 'classnames';

import { type PersonalSetting } from '../../modules/personalSetting';
import { type PersonalSettingGroup } from '../../constants/personalSettingGroup';
import { type UserSetting } from '../../../domain/models/UserSetting';
import Button from '../buttons/Button';
import DialogFrame from '../dialogs/DialogFrame';
import TimeTrackingFormPane from './TimeTrackingFormPane';
import msg from '../../languages';

import './index.scss';

const ROOT = 'commons-personal-setting-dialog';

type MenuItemProps = {
  isSelected: boolean,
  children: React$Node,
};

const MenuItem = (props: MenuItemProps) => (
  <button
    type="text"
    className={classNames(`${ROOT}__menu-item`, {
      [`${ROOT}__menu-item--inactive`]: !props.isSelected,
      [`${ROOT}__menu-item--active`]: props.isSelected,
    })}
  >
    {props.children}
  </button>
);

export type Props = {
  selectedGroup: PersonalSettingGroup,
  newPersonalSetting: PersonalSetting,
  userSetting: UserSetting,
  onChangeFormItem: (SyntheticEvent<HTMLElement>) => void,
  onClickSaveButton: (SyntheticEvent<HTMLFormElement>) => void,
  onClickCloseButton: (SyntheticEvent<HTMLElement>) => void,
  onClickSelectDefaultJobButton: (SyntheticEvent<HTMLButtonElement>) => void,
  onClickResetDefaultJobButton: () => void,
};

export default class PersonalSettingDialog extends React.Component<Props> {
  hasAvailableSettings(): boolean {
    return this.props.userSetting.useWorkTime;
  }

  renderMenuPane() {
    return (
      <div className={`${ROOT}__menu-pane`}>
        <MenuItem isSelected>{msg().Com_Btn_TimeTracking}</MenuItem>
      </div>
    );
  }

  renderFormPane() {
    const { defaultJob } = this.props.newPersonalSetting;

    switch (this.props.selectedGroup) {
      case 'TIME_TRACKING':
        return (
          this.props.userSetting.useWorkTime && (
            <>
              <TimeTrackingFormPane
                defaultJobName={defaultJob && defaultJob.name}
                onChangeFormItem={this.props.onChangeFormItem}
                onClickSaveButton={this.props.onClickSaveButton}
                onClickSelectDefaultJobButton={
                  this.props.onClickSelectDefaultJobButton
                }
                onClickResetDefaultJobButton={
                  this.props.onClickResetDefaultJobButton
                }
              />
            </>
          )
        );

      default:
        return null;
    }
  }

  renderNoSettingPane() {
    return <div className={`${ROOT}__message`}>{msg().Com_Lbl_NoSetting}</div>;
  }

  renderFooter() {
    return (
      <DialogFrame.Footer>
        <Button type="default" onClick={this.props.onClickCloseButton}>
          {msg().Com_Btn_Close}
        </Button>
        {this.hasAvailableSettings() && (
          <Button type="primary" onClick={this.props.onClickSaveButton}>
            {msg().Com_Btn_Save}
          </Button>
        )}
      </DialogFrame.Footer>
    );
  }

  render() {
    return (
      <DialogFrame
        title={msg().Com_Lbl_PersonalSetting}
        footer={this.renderFooter()}
        className={ROOT}
        hide={this.props.onClickCloseButton}
      >
        <main className={`slds-grid ${ROOT}__body`}>
          {this.hasAvailableSettings() ? (
            <>
              {this.renderMenuPane()}
              <div className={`${ROOT}__body-form`}>
                {this.renderFormPane()}
              </div>
            </>
          ) : (
            this.renderNoSettingPane()
          )}
        </main>
      </DialogFrame>
    );
  }
}
