/* @flow */
import React from 'react';

import Button from '../buttons/Button';
import IconButton from '../buttons/IconButton';

import msg from '../../languages';

import ImgBtnDeleteVia from '../../images/btnDeleteVia.png';

import './TimeTrackingFormPane.scss';

const ROOT = 'commons-personal-setting-dialog-time-tracking-form-pane';

type Props = {
  defaultJobName: ?string,
  onClickSelectDefaultJobButton: (SyntheticEvent<HTMLButtonElement>) => void,
  onClickResetDefaultJobButton: () => void,
};

export default class TimeTrackingFormPane extends React.Component<Props> {
  render() {
    return (
      <div className={`slds-grow ${ROOT}`}>
        <div className={`slds-grid slds-grid--vertical ${ROOT}__form`}>
          <div className={`slds-grow ${ROOT}__form-inner`}>
            <label className={`${ROOT}__label`}>
              {msg().Com_Lbl_DefaultJob}
            </label>
            {this.props.defaultJobName !== null &&
            this.props.defaultJobName !== undefined ? (
              <div className={`${ROOT}__default-job`}>
                <div className={`${ROOT}__default-job-name`}>
                  {this.props.defaultJobName}
                </div>
                <div className={`${ROOT}__default-job-reset-button-container`}>
                  <IconButton
                    className={`${ROOT}__default-job-reset-button`}
                    src={ImgBtnDeleteVia}
                    onClick={this.props.onClickResetDefaultJobButton}
                  />
                </div>
              </div>
            ) : (
              <div className={`${ROOT}__default-job-select-button-container`}>
                <Button
                  className={`${ROOT}__select-default-job-button`}
                  onClick={this.props.onClickSelectDefaultJobButton}
                >
                  {msg().Com_Btn_Select}
                </Button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
