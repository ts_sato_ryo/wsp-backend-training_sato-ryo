/* @flow */
import React from 'react';

import MultiColumnFinder from './dialogs/MultiColumnFinder';
import msg from '../languages';

type Job = {
  id: string,
  code: string,
  name: string,
  parentId: ?string,
  hasChildren: boolean,
  isDirectCharged: boolean,
  selectabledTimeTrack: boolean,
};

export type Props = {
  jobList: Job[],
  onClickItem: (Job, ?(Job[])) => void,
  onClickCloseButton: () => void,
};

export default class JobSelectDialog extends React.Component<Props> {
  render() {
    return (
      <div className="ts-modal">
        {/* eslint-disable jsx-a11y/no-static-element-interactions */}
        <div
          className="ts-modal__overlay"
          onClick={this.props.onClickCloseButton}
        />
        {/* eslint-enable jsx-a11y/no-static-element-interactions */}
        <div className="ts-modal__wrap" style={{ width: 865 }}>
          <div className="ts-modal__contents">
            <MultiColumnFinder
              items={this.props.jobList}
              typeName={msg().Com_Lbl_Job}
              showFavorites={false}
              showHistory={false}
              parentSelectable
              onClickItem={this.props.onClickItem}
              onClickCloseButton={(_) => this.props.onClickCloseButton()}
            />
          </div>
        </div>
      </div>
    );
  }
}
