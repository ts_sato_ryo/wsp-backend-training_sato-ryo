import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import uuidV4 from 'uuid/v4';
import SLDSSwitchIcon from '@salesforce-ux/design-system/assets/icons/utility/switch.svg';

import './ExpandableSection.scss';

const ROOT = 'commons-expandable-section';

export default class ExpandableSection extends React.Component {
  static propTypes = {
    expanded: PropTypes.bool,
    summary: PropTypes.oneOfType([PropTypes.node, PropTypes.func]).isRequired,
    summaryLevel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    containerClassName: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.string,
    ]),
    className: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
      PropTypes.string,
    ]),
    onToggle: PropTypes.func,
    children: PropTypes.node,
  };

  static defaultProps = {
    summaryLevel: 3,
  };

  constructor(props) {
    super();
    this.onToggle = this.onToggle.bind(this);
    this.state = {
      id: uuidV4(),
      expanded: !!props.expanded,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.expanded !== nextProps.expanded) {
      this.setState({
        expanded: nextProps.expanded,
      });
    }
  }

  onToggle() {
    this.setState((prevState) => ({ expanded: !prevState.expanded }));

    if (this.props.onToggle) {
      this.props.onToggle();
    }
  }

  render() {
    const Summary = `h${this.props.summaryLevel}`;

    return (
      <section
        className={classNames(
          ROOT,
          {
            [`${ROOT}--expanded`]: this.state.expanded,
          },
          this.props.containerClassName
        )}
      >
        <div className={`${ROOT}__summary`}>
          <Summary className={`${ROOT}__summary-heading`}>
            <button
              type="button"
              className={`${ROOT}__summary-action`}
              aria-controls={this.state.id}
              aria-expanded={this.state.expanded}
              onClick={this.onToggle}
            >
              <SLDSSwitchIcon
                className={`slds-button__icon slds-button__icon--left ${ROOT}__summary-action-icon`}
                aria-hidden="true"
              />
              {typeof this.props.summary === 'function'
                ? this.props.summary(this.state.expanded)
                : this.props.summary}
            </button>
          </Summary>
        </div>
        <div
          id={this.state.id}
          className={classNames(
            `${ROOT}__content`,
            'slds-accordion__content',
            this.props.className
          )}
          aria-hidden={this.state.expanded}
        >
          {this.props.children}
        </div>
      </section>
    );
  }
}
