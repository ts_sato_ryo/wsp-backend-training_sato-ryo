import PropTypes from 'prop-types';
import React from 'react';
import Textarea from 'react-textarea-autosize';

import msg from '../languages';
import DateField from './fields/DateField';
import DateUtil from '../utils/DateUtil';
import ObjectUtil from '../utils/ObjectUtil';
import recordType from '../constants/recordType';

import RouteContainer from '../containers/RouteContainer';
import IconButton from './buttons/IconButton';

import './RecordItemArea.scss';
import TaxChild from './TaxChild';
import CurrencyChild from './CurrencyChild';

/**
 * 一般経費のフォームのうち、内訳の有無で内容が変わる部分
 *
 * FIXME: GeneralExpense の肥大化に伴い無理やり切り出したため、
 * スタイルがこのコンポーネント内で閉じるようにできていない。
 * そのため "require" などのクラスは親のスタイルが適用されている状態。
 */
export class MultiRecordItemArea extends React.Component {
  static get propTypes() {
    return {
      expRecord: PropTypes.object.isRequired,
      readOnly: PropTypes.bool.isRequired,
      useForeignCurrency: PropTypes.bool.isRequired,
      baseCurrencyFractionDigits: PropTypes.number.isRequired,
      getCurrencyRateInfo: PropTypes.func.isRequired,
      onClickJobFinderButton: PropTypes.func.isRequired,
      onClickExpTypeFinderButton: PropTypes.func.isRequired,
      onClickCostCenterFinderButton: PropTypes.func.isRequired,
      onClickRemoveButton: PropTypes.func.isRequired,
      onChangeFormParam: PropTypes.func.isRequired,
      onChangeRecordItemTaxValues: PropTypes.func,
      onChangeRecordItemLocalAmount: PropTypes.func,
    };
  }

  onClickRemoveButton(index) {
    if (this.props.readOnly) {
      return;
    }

    this.props.onClickRemoveButton(index);
  }

  renderExpRecordItemArea(item, i) {
    const controlButtonName = 'Close';
    // FIXME: 暫定的に key には index を設定している。
    //        item.id にすると内訳を追加した際に id を持たない item が作られて warning が出るため。
    //        理想的には内訳追加時に temporary id を付与したほうがよい
    return (
      <div className="ts-record-item__contents__form__list" key={i}>
        <div className="ts-record-item__contents__form__list__header">
          <div className="slds-col slds-size--1-of-12 slds-align-middle">
            <div className="ts-record-item__contents__form__list__controll-btn">
              <IconButton
                src={require(`../../commons/images/btnDetail${controlButtonName}.png`)}
              />
            </div>
          </div>
          <div className="slds-grid">
            <div className="slds-col slds-size--5-of-12 slds-align-middle">
              <div className="key">
                {msg().Exp_Lbl_RecordItems} &nbsp; {i + 1}
              </div>
            </div>
            <div className="slds-col slds-size--6-of-12 slds-align-middle">
              <label className="value">
                <input type="checkbox" disabled={this.props.readOnly} />
                &nbsp;{msg().Exp_Lbl_PrivateUse}
              </label>
            </div>
            <div className="slds-col slds-size--1-of-12 slds-align-middle">
              <IconButton
                src={require('../../commons/images/iconClose.png')}
                onClick={() => this.onClickRemoveButton(i)}
              />
            </div>
          </div>
        </div>

        <div className="slds-grid">
          <div className="require">*</div>
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            <div className="key">&nbsp;{msg().Exp_Lbl_ExpenseType}</div>
            <div className="separate">:</div>
          </div>
          <div className="slds-col slds-size--4-of-12 slds-align-middle">
            <div className="value">
              <select disabled={this.props.readOnly}>
                <option>{item.expTypeName}</option>
              </select>
            </div>
          </div>
          <div className="slds-col slds-size--1-of-12 slds-align-middle">
            <div className="search">
              <IconButton
                onClick={() =>
                  this.props.onClickExpTypeFinderButton(
                    i,
                    this.props.useForeignCurrency
                  )
                }
                disabled={this.props.readOnly}
                src={require('../../commons/images/btnSearch.png')}
              />
            </div>
          </div>
          <div className="slds-col slds-size--4-of-12 slds-align-middle" />
        </div>

        {(() => {
          if (!this.props.useForeignCurrency) {
            return (
              <TaxChild
                amount={this.props.expRecord.expRecordItemList[i].amount}
                amountWithoutTax={
                  this.props.expRecord.expRecordItemList[i].amountWithoutTax
                }
                expTaxTypeList={
                  this.props.expRecord.expRecordItemList[i].expTaxTypeList || []
                }
                taxTypeId={this.props.expRecord.expRecordItemList[i].taxTypeId}
                taxType={this.props.expRecord.taxType}
                onChangeRecordItemTaxValues={
                  this.props.onChangeRecordItemTaxValues
                }
                recordItemIndex={i}
              />
            );
          }
          return null;
        })()}

        {(() => {
          if (this.props.useForeignCurrency) {
            return (
              <CurrencyChild
                readOnly={this.props.readOnly}
                baseCurrencyFractionDigits={
                  this.props.baseCurrencyFractionDigits
                }
                currencyId={this.props.expRecord.currencyId}
                itemDate={this.props.expRecord.expRecordDate}
                localAmount={
                  this.props.expRecord.expRecordItemList[i].localAmount
                }
                amount={this.props.expRecord.expRecordItemList[i].amount}
                getCurrencyRateInfo={this.props.getCurrencyRateInfo}
                onChangeRecordItemLocalAmount={
                  this.props.onChangeRecordItemLocalAmount
                }
                recordItemIndex={i}
              />
            );
          }
          return null;
        })()}

        <div className="slds-grid">
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            <div className="key">&nbsp;{msg().Com_Lbl_Job}</div>
            <div className="separate">:</div>
          </div>
          <div className="slds-col slds-size--8-of-12 slds-align-middle">
            <div className="value">
              <select disabled={this.props.readOnly}>
                <option>{item.jobName}</option>
              </select>
            </div>
          </div>
          <div className="slds-col slds-size--1-of-12 slds-align-middle">
            <div className="search">
              <IconButton
                onClick={() => this.props.onClickJobFinderButton(i)}
                disabled={this.props.readOnly}
                src={require('../../commons/images/btnSearch.png')}
              />
            </div>
          </div>
        </div>

        <div className="slds-grid">
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            <div className="key">&nbsp;{msg().Exp_Lbl_CostCenter}</div>
            <div className="separate">:</div>
          </div>
          <div className="slds-col slds-size--8-of-12 slds-align-middle">
            <div className="value">
              <select disabled={this.props.readOnly}>
                <option>{item.costCenterName}</option>
              </select>
            </div>
          </div>
          <div className="slds-col slds-size--1-of-12 slds-align-middle">
            <div className="search">
              <IconButton
                onClick={() => this.props.onClickCostCenterFinderButton(i)}
                disabled={this.props.readOnly}
                src={require('../../commons/images/btnSearch.png')}
              />
            </div>
          </div>
        </div>

        <TwoColumnsGrid
          gridSizeList={[3, 9]}
          gridLabelName={msg().Exp_Lbl_Summary}
        >
          <Textarea minRows={2} disabled={this.props.readOnly} />
        </TwoColumnsGrid>
      </div>
    );
  }

  render() {
    const trimmedParam = {
      expRecordDate: ObjectUtil.getOrEmpty(
        this.props.expRecord,
        'expRecordDate'
      ),
      note: ObjectUtil.getOrEmpty(this.props.expRecord, 'note'),
    };

    return (
      <div className="ts-record-item__contents__form__wrap">
        <div className="ts-record-item__contents__form__common">
          <ThreeColumnsGrid
            gridSizeList={[3, 4, 5]}
            gridLabelName={msg().Exp_Lbl_Date}
            required
          >
            <DateField
              value={DateUtil.format(trimmedParam.expRecordDate, 'YYYY-MM-DD')}
              onChange={(value) =>
                this.props.onChangeFormParam(
                  'expRecordDate',
                  DateUtil.toUnixMsec(value)
                )
              }
              disabled={this.props.readOnly}
            />
          </ThreeColumnsGrid>

          <ThreeColumnsGrid
            gridSizeList={[3, 5, 4]}
            gridLabelName={msg().Exp_Lbl_PaymentType}
          >
            <select disabled={this.props.readOnly}>
              <option>{msg().Exp_Lbl_AdvancesPaid}</option>
            </select>
          </ThreeColumnsGrid>

          <TwoColumnsGrid
            gridSizeList={[3, 9]}
            gridLabelName={msg().Exp_Lbl_Summary}
          >
            <Textarea
              minRows={2}
              value={trimmedParam.note}
              onChange={(event) =>
                this.props.onChangeFormParam('note', event.target.value)
              }
              disabled={this.props.readOnly}
            />
          </TwoColumnsGrid>
        </div>

        {this.props.expRecord.expRecordItemList.map((item, i) => {
          return this.renderExpRecordItemArea(item, i);
        })}
      </div>
    );
  }
}

export class SingleRecordItemArea extends React.Component {
  static get propTypes() {
    return {
      expRecord: PropTypes.object.isRequired,
      readOnly: PropTypes.bool.isRequired,
      onClickJobFinderButton: PropTypes.func.isRequired,
      onClickExpTypeFinderButton: PropTypes.func.isRequired,
      onClickCostCenterFinderButton: PropTypes.func.isRequired,
      onClickRouteFinderButton: PropTypes.func.isRequired,
      onChangeFormParam: PropTypes.func.isRequired,
    };
  }

  renderPaymentType() {
    if (
      this.props.expRecord.expRecordItemList[0].expRecordType ===
      recordType.TRANSIT
    ) {
      return null;
    }

    return (
      <ThreeColumnsGrid
        gridSizeList={[3, 5, 4]}
        gridLabelName={msg().Exp_Lbl_PaymentType}
      >
        <select>
          <option>{msg().Exp_Lbl_AdvancesPaid}</option>
        </select>
      </ThreeColumnsGrid>
    );
  }

  renderRoute() {
    if (
      this.props.expRecord.expRecordItemList[0].expRecordType !==
      recordType.TRANSIT
    ) {
      return null;
    }

    return (
      <RouteContainer
        expRecordId={this.props.expRecord.id}
        routeInfo={this.props.expRecord.routeInfo}
        onClickRouteFinderButton={this.props.onClickRouteFinderButton}
      />
    );
  }

  render() {
    const expRecordItem = this.props.expRecord.expRecordItemList[0];
    const trimmedParam = {
      expRecordDate: ObjectUtil.getOrEmpty(
        this.props.expRecord,
        'expRecordDate'
      ),
      note: ObjectUtil.getOrEmpty(this.props.expRecord, 'note'),
    };
    let formCommonClassName = 'ts-record-item__contents__form__common';
    if (
      this.props.expRecord.expRecordItemList[0].expRecordType ===
      recordType.TRANSIT
    ) {
      formCommonClassName += '--no-border';
    }

    return (
      <div className="ts-record-item__contents__form__wrap">
        <div className={formCommonClassName}>
          <ThreeColumnsGrid
            gridSizeList={[3, 4, 5]}
            gridLabelName={msg().Exp_Lbl_Date}
            required
          >
            <DateField
              value={DateUtil.format(trimmedParam.expRecordDate, 'YYYY-MM-DD')}
              onChange={(value) =>
                this.props.onChangeFormParam(
                  'expRecordDate',
                  DateUtil.toUnixMsec(value)
                )
              }
              disabled={this.props.readOnly}
            />
          </ThreeColumnsGrid>

          <div className="slds-grid">
            <div className="require">*</div>
            <div className="slds-col slds-size--3-of-12 slds-align-middle">
              <div className="key">&nbsp;{msg().Exp_Lbl_ExpenseType}</div>
              <div className="separate">:</div>
            </div>
            <div className="slds-col slds-size--8-of-12 slds-align-middle">
              <div className="value">
                <select disabled={this.props.readOnly}>
                  <option>{expRecordItem.expTypeName}</option>
                </select>
              </div>
            </div>
            <div className="slds-col slds-size--1-of-12 slds-align-middle">
              <div className="search">
                <IconButton
                  onClick={() => this.props.onClickExpTypeFinderButton(0)}
                  disabled={this.props.readOnly}
                  src={require('../../commons/images/btnSearch.png')}
                />
              </div>
            </div>
          </div>
          {this.renderPaymentType()}
          {this.renderRoute()}
          <div className="slds-grid">
            <div className="slds-col slds-size--3-of-12 slds-align-middle">
              <div className="key">&nbsp;{msg().Com_Lbl_Job}</div>
              <div className="separate">:</div>
            </div>
            <div className="slds-col slds-size--8-of-12 slds-align-middle">
              <div className="value">
                <select disabled={this.props.readOnly}>
                  <option>{expRecordItem.jobName}</option>
                </select>
              </div>
            </div>
            <div className="slds-col slds-size--1-of-12 slds-align-middle">
              <div className="search">
                <IconButton
                  onClick={() => this.props.onClickJobFinderButton(0)}
                  disabled={this.props.readOnly}
                  src={require('../../commons/images/btnSearch.png')}
                />
              </div>
            </div>
          </div>

          <div className="slds-grid">
            <div className="slds-col slds-size--3-of-12 slds-align-middle">
              <div className="key">&nbsp;{msg().Exp_Lbl_CostCenter}</div>
              <div className="separate">:</div>
            </div>
            <div className="slds-col slds-size--8-of-12 slds-align-middle">
              <div className="value">
                <select disabled={this.props.readOnly}>
                  <option>{expRecordItem.costCenterName}</option>
                </select>
              </div>
            </div>
            <div className="slds-col slds-size--1-of-12 slds-align-middle">
              <div className="search">
                <IconButton
                  onClick={() => this.props.onClickCostCenterFinderButton(0)}
                  disabled={this.props.readOnly}
                  src={require('../../commons/images/btnSearch.png')}
                />
              </div>
            </div>
          </div>

          <TwoColumnsGrid
            gridSizeList={[3, 9]}
            gridLabelName={msg().Exp_Lbl_Summary}
          >
            <Textarea
              minRows={2}
              value={trimmedParam.note}
              onChange={(event) =>
                this.props.onChangeFormParam('note', event.target.value)
              }
              disabled={this.props.readOnly}
            />
          </TwoColumnsGrid>
        </div>
      </div>
    );
  }
}

class TwoColumnsGrid extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      gridSizeList: PropTypes.array.isRequired,
      gridLabelName: PropTypes.string.isRequired,
    };
  }

  render() {
    return (
      <div className="slds-grid">
        <div
          className={`slds-col slds-size--${
            this.props.gridSizeList[0]
          }-of-12 slds-align-middle`}
        >
          <div className="key">&nbsp;{this.props.gridLabelName}</div>
          <div className="separate">:</div>
        </div>
        <div
          className={`slds-col slds-size--${
            this.props.gridSizeList[1]
          }-of-12 slds-align-middle`}
        >
          <div className="value">{this.props.children}</div>
        </div>
      </div>
    );
  }
}

class ThreeColumnsGrid extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      gridSizeList: PropTypes.array.isRequired,
      gridLabelName: PropTypes.string.isRequired,
      optionalNode: PropTypes.node,
      required: PropTypes.bool,
    };
  }

  render() {
    return (
      <div className="slds-grid">
        {this.props.required ? <div className="require">*</div> : null}
        <div
          className={`slds-col slds-size--${
            this.props.gridSizeList[0]
          }-of-12 slds-align-middle`}
        >
          <div className="key">&nbsp;{this.props.gridLabelName}</div>
          <div className="separate">:</div>
        </div>
        <div
          className={`slds-col slds-size--${
            this.props.gridSizeList[1]
          }-of-12 slds-align-middle`}
        >
          <div className="value">{this.props.children}</div>
        </div>
        <div
          className={`slds-col slds-size--${
            this.props.gridSizeList[2]
          }-of-12 slds-align-middle`}
        >
          {this.props.optionalNode}
        </div>
      </div>
    );
  }
}
