// @flow
import React from 'react';
import { Rnd, type Props as defaultProps } from 'react-rnd';

import './DraggableResizable.scss';

const DraggableResizable = (props: defaultProps) => {
  return <Rnd {...props}>{props.children}</Rnd>;
};

export default DraggableResizable;
