import PropTypes from 'prop-types';
import React from 'react';

import msg from '../languages';
import FormatUtil from '../utils/FormatUtil';
import TaxUtil from '../utils/TaxUtil';
import TAX_TYPE from '../constants/taxType';
import IconButton from './buttons/IconButton';

import TextField from './fields/TextField';

import './Tax.scss';
import ImgEditOn from '../images/btnEditOn.png';
import ImgEditOff from '../images/btnEditOff.png';

export default class Tax extends React.Component {
  static get propTypes() {
    return {
      readOnly: PropTypes.bool.isRequired,
      totalAmount: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      taxRate1: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      taxRate2: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      gstVat1: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      gstVat2: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      amountWithoutTax: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]).isRequired,
      recordItemIndex: PropTypes.number.isRequired,
      // FIXME: taxTypeとtaxTypeIdという項目が存在し、taxTypeという単語がかぶっているため「taxType」の単語をオブジェクトのスキーマレベルで再定義するか検討する（GENIE-1886）
      // 税額が自動計算かどうかを表す「TaxType」として定義している。「税の計算が自動計算かどうか」は明細単位で指定するので ExpRecord__c.TaxType__c という項目が存在する。
      // 税区分マスタのオブジェクトを表すものは「TaxTypeId」として定義している。「税区分」は内訳単位で指定するので ExpRecordItem__c.TaxTypeId__c という項目が存在する。
      taxTypeId: PropTypes.string,
      taxType: PropTypes.string.isRequired,
      hasMultipleRecordItems: PropTypes.bool.isRequired,
      expTaxTypeList: PropTypes.array.isRequired,
      onChangeGstVat: PropTypes.func.isRequired,
      onChangeTaxType: PropTypes.func.isRequired,
      onChangeTaxTypeAuto: PropTypes.func.isRequired,
      onChangeTaxRate: PropTypes.func.isRequired,
      onChangeTotalAmount: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      taxTypeId: '',
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      taxTypeList: [
        {
          type: msg().Exp_Sel_TotalAmount,
          rate: null,
          taxType: TAX_TYPE.TOTAL,
        },
        {
          type: msg().Exp_Sel_EachBreakdown,
          rate: null,
          taxType: TAX_TYPE.PER_RECORD_ITEM,
        },
        {
          type: msg().Exp_Sel_Nontaxable,
          rate: 0,
          taxType: TAX_TYPE.NONTAXABLE,
        },
      ],
      totalAmount: FormatUtil.convertToIntegerString(this.props.totalAmount),
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.totalAmount !== nextProps.totalAmount) {
      this.setState({
        totalAmount: nextProps.totalAmount,
      });

      // 内訳がない場合のみ更新処理を走らせる
      if (!nextProps.hasMultipleRecordItems) {
        this.props.onChangeTotalAmount(
          nextProps.totalAmount,
          nextProps.taxRate1,
          nextProps.gstVat1,
          nextProps.taxType,
          nextProps.recordItemIndex
        );
      }
    }
  }

  onChangeTaxType(e) {
    const selectedTaxType = this.state.taxTypeList[e.target.selectedIndex];
    this.props.onChangeTaxType(selectedTaxType.taxType);
  }

  onChangeTaxRate(e) {
    const taxTypeId = e.target.value;
    const selectedExpTax = TaxUtil.findSelectedExpTax(
      this.props.expTaxTypeList,
      taxTypeId
    );
    this.props.onChangeTaxRate(
      this.state.totalAmount,
      selectedExpTax.rate,
      taxTypeId,
      this.props.gstVat1,
      this.props.taxType,
      this.props.recordItemIndex
    );
  }

  onChangeGstVat(e, row) {
    // 税額1の値を更新したら税率1の値を使用し、税額2の値を更新したら税率2の値を使用する
    const taxRate = row === 1 ? this.props.taxRate1 : this.props.taxRate2;
    this.props.onChangeGstVat(
      this.state.totalAmount,
      taxRate,
      e.target.value,
      this.props.taxType,
      this.props.recordItemIndex,
      row
    );
  }

  onChangeTaxTypeAuto(taxType) {
    this.props.onChangeTaxTypeAuto(
      this.state.totalAmount,
      this.props.gstVat1,
      taxType,
      this.props.taxRate1,
      this.props.recordItemIndex
    );
  }

  /**
   * 税率が0または税種別「課税なし」が選択された場合にTrueを返す関数
   * @param  {Number}  taxRate 税率
   * @param  {String}  taxType 税種別
   * @return {Boolean}         税率が0または税種別「課税なし」が選択された場合にTrue、それ以外はFalse
   */
  isNontaxableSelected(taxRate, taxType) {
    return (
      (taxRate !== null && taxRate <= 0) || TaxUtil.isTaxTypeNonTaxable(taxType)
    );
  }

  renderSelectTaxType() {
    const selectTypeList = this.state.taxTypeList.map((item, i) => (
      <option key={i} value={item.taxType}>
        {item.type}
      </option>
    ));

    return (
      <div className="value">
        <select
          value={this.props.taxType}
          onChange={(event) => this.onChangeTaxType(event)}
          disabled={this.props.readOnly}
        >
          {selectTypeList}
        </select>
      </div>
    );
  }

  renderSelectTaxRate() {
    const selectTypeList = TaxUtil.setInitialExpTaxLabel(
      this.props.expTaxTypeList
    ).map((item) => (
      <option key={item.id} value={item.id}>
        {item.label}
      </option>
    ));

    return (
      <div className="value">
        <select
          value={this.props.taxTypeId}
          onChange={(event) => this.onChangeTaxRate(event)}
          disabled={this.props.readOnly || this.props.hasMultipleRecordItems}
        >
          {selectTypeList}
        </select>
      </div>
    );
  }

  // 内訳有の場合は税率1の値をreadOnlyで表示
  renderDisabledTaxRate1() {
    return (
      <input
        type="text"
        value={FormatUtil.convertToDisplayingPercent(this.props.taxRate1)}
        readOnly
      />
    );
  }

  // 内訳有の場合は税率2の値をreadOnlyで表示
  renderDisabledTaxRate2() {
    return (
      <input
        type="text"
        value={FormatUtil.convertToDisplayingPercent(this.props.taxRate2)}
        readOnly
      />
    );
  }

  renderInputGstVat() {
    // 税額手入力可否にて処理を切り替え
    // Total : 編集可能 それ以外: 編集不可
    const isEditable = TaxUtil.isTaxTypeTotal(this.props.taxType);
    const imgEdit = isEditable ? ImgEditOn : ImgEditOff;
    const taxType = isEditable ? TAX_TYPE.PER_RECORD_ITEM : TAX_TYPE.TOTAL;
    const imgEditAlt = isEditable ? 'ImgEditOn' : 'ImgEditOff';

    // 内訳有の場合は税額自動入力の切り替えボタンは非表示とする
    const editableTaxTypeAutoArea =
      this.props.hasMultipleRecordItems || this.props.readOnly ? null : (
        <div className="ts-tax-auto">
          <IconButton
            src={imgEdit}
            onClick={() => this.onChangeTaxTypeAuto(taxType)}
            alt={imgEditAlt}
          />
        </div>
      );

    return (
      <div className="value">
        <TextField
          type="text"
          className={
            this.props.readOnly && isEditable ? 'ts-currency--modified' : ''
          }
          value={FormatUtil.convertToIntegerString(this.props.gstVat1)}
          onChange={(event) => this.onChangeGstVat(event, 1)}
          disabled={this.props.readOnly || !isEditable}
        />
        {editableTaxTypeAutoArea}
      </div>
    );
  }

  renderInputGstVat2() {
    // 税額手入力可否にて処理を切り替え
    // 1 : 編集可能 それ以外: 編集不可
    const isEditable = TaxUtil.isTaxTypeTotal(this.props.taxType);

    return (
      <div className="value">
        <input
          type="text"
          value={FormatUtil.convertToIntegerString(this.props.gstVat2)}
          onChange={(event) => this.onChangeGstVat(event, 2)}
          disabled={!isEditable}
        />
      </div>
    );
  }

  renderInputAmountWithoutTax() {
    return (
      <input
        type="text"
        disabled
        value={FormatUtil.convertToIntegerString(this.props.amountWithoutTax)}
      />
    );
  }

  render() {
    return (
      <div>
        <RenderTaxArea
          leftLabel={msg().Exp_Lbl_Gst}
          leftArea={this.renderSelectTaxType()}
          centerLabel={msg().Exp_Lbl_TaxRate}
          centerArea={this.renderSelectTaxRate()}
          rightLabel={msg().Exp_Lbl_GstAmount}
          rightArea={this.renderInputGstVat()}
          renderDisabledTaxRate1={this.renderDisabledTaxRate1()}
          renderDisabledTaxRate2={this.renderDisabledTaxRate2()}
          renderGstVat2Area={this.renderInputGstVat2()}
          isNontaxableSelected={this.isNontaxableSelected(
            this.props.taxRate1,
            this.props.taxType
          )}
          hasMultipleRecordItems={this.props.hasMultipleRecordItems}
          hasMultipleTaxRecord={TaxUtil.hasMultipleTaxRecord(
            this.props.expTaxTypeList
          )}
        />
        {/* eslint-disable consistent-return */}
        {(() => {
          if (
            !this.isNontaxableSelected(this.props.taxRate1, this.props.taxType)
          ) {
            return (
              <RenderAmountWithoutTaxArea
                rightLabel={msg().Exp_Lbl_WithoutTax}
                rightArea={this.renderInputAmountWithoutTax()}
              />
            );
          }
        })()}
        {/* eslint-enable consistent-return */}
      </div>
    );
  }
}

class RenderTaxArea extends React.Component {
  static get propTypes() {
    return {
      leftLabel: PropTypes.node.isRequired,
      leftArea: PropTypes.node.isRequired,
      centerLabel: PropTypes.node.isRequired,
      centerArea: PropTypes.node.isRequired,
      rightLabel: PropTypes.node.isRequired,
      rightArea: PropTypes.node.isRequired,
      renderDisabledTaxRate1: PropTypes.node.isRequired,
      renderDisabledTaxRate2: PropTypes.node.isRequired,
      renderGstVat2Area: PropTypes.node.isRequired,
      isNontaxableSelected: PropTypes.bool.isRequired,
      hasMultipleRecordItems: PropTypes.bool.isRequired,
      hasMultipleTaxRecord: PropTypes.bool.isRequired,
    };
  }

  renderTaxRateLabel(column, rowLabel = null) {
    return (
      <div className={`slds-col slds-size--${column}-of-12 slds-align-middle`}>
        <div className="key">
          {this.props.centerLabel}
          {rowLabel}
        </div>
      </div>
    );
  }

  renderTaxRate(column, rowLabel) {
    if (!rowLabel) {
      return (
        <div
          className={`slds-col slds-size--${column}-of-12 slds-align-middle`}
        >
          {this.props.centerArea}
        </div>
      );
    } else if (rowLabel === '1') {
      return (
        <div
          className={`slds-col slds-size--${column}-of-12 slds-align-middle`}
        >
          {this.props.renderDisabledTaxRate1}
        </div>
      );
    } else {
      return (
        <div
          className={`slds-col slds-size--${column}-of-12 slds-align-middle`}
        >
          {this.props.renderDisabledTaxRate2}
        </div>
      );
    }
  }

  renderTaxRateArea(rowLabel = null) {
    // FIXME: renderTaxRateArea が引数つきで呼ばれている箇所がないので、null しか渡されてこない?
    // 内訳なし時
    if (!this.props.hasMultipleRecordItems) {
      return (
        <div className="slds-col slds-size--4-of-12 slds-align-middle">
          <div className="slds-grid">
            {this.renderTaxRateLabel(5)}
            {this.renderTaxRate(7)}
          </div>
        </div>
      );
    } else {
      // 内訳あり時
      if (this.props.isNontaxableSelected) {
        // 課税なし時
        return null;
      }
      // 課税あり時
      return (
        <div className="slds-col slds-size--3-of-12 slds-align-middle">
          <div className="slds-grid">
            {this.renderTaxRateLabel(4, rowLabel)}
            {this.renderTaxRate(8, rowLabel)}
          </div>
        </div>
      );
    }
  }

  renderGstVatLabel(column, rowLabel = null) {
    return (
      <div
        className={`slds-col slds-size--${column}-of-12 slds-align-middle right`}
      >
        <div className="key">
          {this.props.rightLabel}
          {rowLabel}
        </div>
      </div>
    );
  }

  renderGstVat(column, rowLabel) {
    // 内訳複数行で1行目に表示する行と2行目に表示する行で表示する値を分ける
    if (!rowLabel || rowLabel === '1') {
      return (
        <div
          className={`slds-col slds-size--${column}-of-12 slds-align-middle`}
        >
          {this.props.rightArea}
        </div>
      );
    } else {
      return (
        <div
          className={`slds-col slds-size--${column}-of-12 slds-align-middle`}
        >
          {this.props.renderGstVat2Area}
        </div>
      );
    }
  }

  renderGstVatArea(rowLabel = null) {
    // 内訳なしの場合
    if (!this.props.hasMultipleRecordItems) {
      if (this.props.isNontaxableSelected) {
        // 課税なしの場合
        return <div className="slds-col slds-size--5-of-12" />;
      }
      // 課税ありの場合
      return (
        <div className="slds-col slds-size--5-of-12 slds-align-middle">
          <div className="slds-grid">
            {this.renderGstVatLabel(4)}
            {this.renderGstVat(8)}
          </div>
        </div>
      );
    } else {
      // 内訳ありの場合
      if (this.props.isNontaxableSelected) {
        // 課税なしの場合
        return <div className="slds-col slds-size--7-of-12" />;
      }
      // 課税ありの場合
      return (
        <div className="slds-col slds-size--4-of-12 slds-align-middle">
          <div className="slds-grid">
            {this.renderGstVatLabel(4, rowLabel)}
            {this.renderGstVat(8, rowLabel)}
          </div>
        </div>
      );
    }
  }

  renderTaxTypeLabel() {
    return (
      <div className="slds-col slds-size--5-of-12 slds-align-middle">
        <div className="key">{this.props.leftLabel}</div>
      </div>
    );
  }

  renderTaxType() {
    return (
      <div className="slds-col slds-size--7-of-12 slds-align-middle">
        {this.props.leftArea}
      </div>
    );
  }

  renderTaxTypeArea() {
    // 内訳なしの場合
    if (!this.props.hasMultipleRecordItems) {
      return null;
    }

    return (
      <div className="slds-col slds-size--4-of-12 slds-align-middle">
        <div className="slds-grid">
          {this.renderTaxTypeLabel()}
          {this.renderTaxType()}
        </div>
      </div>
    );
  }

  render() {
    // 内訳があり、かつ選択可能な税率のうち税率が0%より大きいものが複数ある場合は税率を複数行表示する
    if (this.props.hasMultipleRecordItems && this.props.hasMultipleTaxRecord) {
      return (
        <div>
          <div className="slds-grid">
            {this.renderTaxTypeArea()}
            {this.renderTaxRateArea()}
            {this.renderGstVatArea()}
            <div className="slds-col slds-size--1-of-12" />
          </div>
          <div className="slds-grid">
            <div className="slds-col slds-size--4-of-12" />
            {this.renderTaxRateArea()}
            {this.renderGstVatArea()}
            <div className="slds-col slds-size--1-of-12" />
          </div>
        </div>
      );
    }
    return (
      <div className="slds-grid">
        {this.renderTaxTypeArea()}
        {this.renderTaxRateArea()}
        <div className="slds-col slds-size--2-of-12" />
        {this.renderGstVatArea()}
        <div className="slds-col slds-size--1-of-12" />
      </div>
    );
  }
}

class RenderAmountWithoutTaxArea extends React.Component {
  static get propTypes() {
    return {
      rightLabel: PropTypes.string,
      rightArea: PropTypes.node,
    };
  }

  static get defaultProps() {
    return {
      rightLabel: '',
      rightArea: null,
    };
  }

  render() {
    return (
      <div className="slds-grid">
        <div className="slds-col slds-size--6-of-12" />
        <div className="slds-col slds-size--5-of-12 slds-align-middle">
          <div className="slds-grid">
            <div className="slds-col slds-size--4-of-12 slds-align-middle right">
              <div className="key">{this.props.rightLabel}</div>
            </div>
            <div className="slds-col slds-size--8-of-12 slds-align-middle">
              <div className="value">{this.props.rightArea}</div>
            </div>
          </div>
        </div>
        <div className="slds-col slds-size--1-of-12" />
      </div>
    );
  }
}
