import PropTypes from 'prop-types';
import React from 'react';

/**
 * ボタングループス - 共通コンポーネント
 * このコンポーネントでボタンをラップするとまとまった一つのボタンのように
 * スタイリングされる
 */
export default class ButtonGroups extends React.Component {
  static get propTypes() {
    return {
      children: PropTypes.node.isRequired,
    };
  }

  render() {
    return (
      <div className="ts-button-group" role="group">
        {this.props.children}
      </div>
    );
  }
}
