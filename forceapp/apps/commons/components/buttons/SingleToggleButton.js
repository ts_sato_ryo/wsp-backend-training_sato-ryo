import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import Button from './Button';
import IconButton from './IconButton';

import './SingleToggleButton.scss';

const ROOT = 'commons-buttons-single-toggle-button';

/**
 * シングルトグルボタン
 * FIXME: デモ用実装
 * トグルでボタンのprimary/secondaryと画像を切り替えできる
 * 文字は想定できていない、画像を差し替えだと製造コストがかかるので再検討したい
 */
export default class SingleToggleButton extends React.Component {
  static get propTypes() {
    return {
      value: PropTypes.bool.isRequired,
      iconSrcActive: PropTypes.string.isRequired,
      // FIXME disabledは命名がおかしい
      iconSrcDisabled: PropTypes.string.isRequired,
      onClick: PropTypes.func.isRequired,
      alt: PropTypes.string,
      className: PropTypes.string,
      hasBorder: PropTypes.bool,
      disabled: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      alt: '',
      className: '',
      hasBorder: true,
      disabled: false,
    };
  }

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    this.props.onClick(e);
  }

  render() {
    const image = this.props.value
      ? this.props.iconSrcActive
      : this.props.iconSrcDisabled;

    const cssClass = classNames(ROOT, this.props.className, {
      [`${ROOT}--active`]: this.props.value,
    });

    if (this.props.hasBorder) {
      return (
        <Button
          className={cssClass}
          type="default"
          onClick={this.onClick}
          disabled={this.props.disabled}
        >
          <img src={image} alt={this.props.alt} />
        </Button>
      );
    } else {
      return (
        <IconButton
          className={this.props.className}
          src={image}
          alt={this.props.alt}
          onClick={this.onClick}
          disabled={this.props.disabled}
        />
      );
    }
  }
}
