import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import './Button.scss';

/**
 * ボタン - 共通コンポーネント
 */

const ROOT = 'ts-button';
export default class Button extends React.Component {
  static get propTypes() {
    return {
      type: PropTypes.oneOf([
        'outline-default',
        'default',
        'primary',
        'secondary',
        'destructive',
        'text',
      ]),
      iconSrc: PropTypes.string,
      iconAlt: PropTypes.string,
      iconAlign: PropTypes.oneOf(['left', 'right']),
      className: PropTypes.string,
      onClick: PropTypes.func,
      children: PropTypes.node.isRequired,
      submit: PropTypes.bool,
      value: PropTypes.string,
      disabled: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      type: 'default',
      iconAlign: 'left',
      submit: false,
      value: '',
      disabled: false,
    };
  }

  constructor(props) {
    super(props);

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    const { onClick } = this.props;

    if (onClick) {
      onClick(e);
    }
  }

  renderIcon() {
    const { iconSrc, iconAlt, iconAlign } = this.props;

    const iconClass = classNames(`${ROOT}__icon`, {
      [`${ROOT}__icon--right`]: iconAlign === 'right',
    });

    return <img className={iconClass} src={iconSrc} alt={iconAlt} />;
  }

  render() {
    const {
      className,
      children,
      type,
      iconSrc,
      iconAlign,
      submit,
      ...props
    } = this.props;

    // button elementに渡すとwarningとなる
    delete props.iconAlt;

    const btnClassNames = classNames(
      className,
      ROOT,
      type ? `${ROOT}--${type}` : ''
    );

    const buttonType = submit ? 'submit' : 'button';

    return (
      <button
        type={buttonType}
        className={btnClassNames}
        onClick={this.onClick}
        {...props}
        value={this.props.value}
        disabled={this.props.disabled}
      >
        <div className={`${ROOT}__contents`}>
          {iconSrc && iconAlign === 'left' ? this.renderIcon() : null}
          <span className={`${ROOT}__text`}>{children}</span>
          {iconSrc && iconAlign === 'right' ? this.renderIcon() : null}
        </div>
      </button>
    );
  }
}
