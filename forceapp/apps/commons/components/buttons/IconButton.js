// @flow
import React, { type Node } from 'react';
import classNames from 'classnames';

import './IconButton.scss';

/**
 * アイコンボタン - 共通コンポーネント
 */

type Props = {
  src: string,
  alt?: string,
  className?: string,
  srcType?: 'default' | 'svg',
  children?: Node,
  testId?: string,
  onClick?: (SyntheticEvent<HTMLButtonElement>) => void,
  fillColor?: string,
};

const ROOT = 'ts-icon-button';

export default class IconButton extends React.Component<Props> {
  static defaultProps = {
    alt: '',
    fillColor: '',
  };

  onClick(e: SyntheticEvent<HTMLButtonElement>) {
    const { onClick } = this.props;

    if (onClick) {
      onClick(e);
    }
  }

  render() {
    const {
      className,
      src,
      srcType,
      fillColor,
      alt,
      children,
      ...props
    } = this.props;

    const btnClassNames = classNames(className, ROOT);
    const SvgIcon = srcType === 'svg' ? src : '';

    return (
      <button
        type="button"
        className={btnClassNames}
        onClick={this.onClick}
        {...props}
      >
        {srcType === 'svg' ? (
          <SvgIcon style={{ fill: fillColor }} aria-hidden="true" />
        ) : (
          <img className={`${ROOT}__image`} src={src} alt={alt} />
        )}
        {children}
      </button>
    );
  }
}
