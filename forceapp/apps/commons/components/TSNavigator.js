import PropTypes from 'prop-types';
import React from 'react';
import { CSSTransition as ReactCSSTransitionGroup } from 'react-transition-group';
import './TSNavigator.scss';

// TODO: アニメーション名を定数として書き出し

export default class TSNavigator extends React.Component {
  static get propTypes() {
    return {
      rootView: PropTypes.object,
    };
  }

  static get childContextTypes() {
    return {
      navigator: PropTypes.object,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      rootView: this.props.rootView,
      views: [],
      navigationBarColor: '#1D689A',
      index: 0,
    };
  }

  getChildContext() {
    return { navigator: this };
  }

  push(options) {
    this.state.views.push({
      component: options.view,
      animation: options.animation,
      props: options.props,
    });
    this.setState((prevState) => ({
      views: prevState.views,
      index: prevState.index + 1,
    }));
  }

  pop() {
    this.setState(
      (prevState) => ({
        views: prevState.views,
        index: prevState.index - 1,
      }),
      () => {
        this.state.views.pop();
      }
    );
  }

  popToView(index) {
    this.setState(
      (prevState) => ({
        views: prevState.views,
        index,
      }),
      () => {
        const cnt = this.state.views.length - index;
        for (let i = 0; i < cnt; i++) {
          this.state.views.pop();
        }
      }
    );
  }

  get navigationBarColor() {
    return this.state.navigationBarColor;
  }

  set navigationBarColor(value) {
    this.setState({ navigationBarColor: value });
  }

  render() {
    const views = this.state.views;
    const RootView = this.state.rootView;

    return (
      <div className="ts-navigator">
        <RootView key="rootView" />
        {views.map((item, i) => {
          const Component = item.component;

          return (
            <ReactCSSTransitionGroup
              classNames={`ts-${item.animation}`}
              timeout={{ enter: 300, exit: 300 }}
              component="div"
            >
              <div>
                {(() => {
                  if (i >= this.state.index) {
                    return null;
                  } else {
                    return (
                      <Component key={`stackedView-${i}`} {...item.props} />
                    );
                  }
                })()}
              </div>
            </ReactCSSTransitionGroup>
          );
        })}
      </div>
    );
  }
}
