/* @flow */
import { bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as React from 'react';
import moment from 'moment';

import { fetchJobListByParent } from '../actions/jobList';
import {
  hide,
  save,
  selectDefaultJobItem,
} from '../modules/personalSettingDialog';
import { show as showJobSelectDialog } from '../modules/jobSelectDialog';
import JobSelectDialogContainer from './JobSelectDialogContainer';
import PersonalSettingDialog, {
  type Props as PersonalSettingDialogProps,
} from '../components/PersonalSettingDialog';

type Job = {
  id: string,
  code: string,
  name: string,
  parentId: ?string,
  hasChildren: boolean,
  isDirectCharged: boolean,
  selectabledTimeTrack: boolean,
};

const mapStateToProps = (state: Object) => ({
  isVisible: state.common.personalSettingDialog.isVisible,
  selectedGroup: state.common.personalSettingDialog.selectedGroup,
  newPersonalSetting: state.common.personalSettingDialog.newPersonalSetting,
  userSetting: state.common.userSetting,
  originalPersonalSetting: state.common.personalSetting,
});

const mapDispatchToProps = (dispatch: Dispatch<Object>) =>
  bindActionCreators(
    {
      onClickSaveButton: save,
      onClickCloseButton: hide,
      onClickSelectDefaultJobButton: showJobSelectDialog,
      onSelectDefaultJobItem: selectDefaultJobItem,
      fetchJobListByParent,
    },
    dispatch
  );

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickSaveButton: () => {
    dispatchProps.onClickSaveButton(stateProps.newPersonalSetting);
  },
  onClickCloseButton: () => {
    dispatchProps.onClickCloseButton();
  },
  onClickSelectDefaultJobButton: () => {
    dispatchProps.fetchJobListByParent(moment().format('YYYY-MM-DD'), null, []);
    dispatchProps.onClickSelectDefaultJobButton();
  },
  onClickResetDefaultJobButton: () => {
    dispatchProps.onSelectDefaultJobItem(null);
  },
});

type Props = PersonalSettingDialogProps & {
  isVisible: boolean,
  onSelectDefaultJobItem: (Job, ?(Job[])) => void,
};

class PersonalSettingDialogContainer extends React.Component<Props> {
  render() {
    return this.props.isVisible ? (
      <div>
        <PersonalSettingDialog
          selectedGroup="TIME_TRACKING"
          newPersonalSetting={this.props.newPersonalSetting}
          userSetting={this.props.userSetting}
          onChangeFormItem={this.props.onChangeFormItem}
          onClickSaveButton={this.props.onClickSaveButton}
          onClickCloseButton={this.props.onClickCloseButton}
          onClickSelectDefaultJobButton={
            this.props.onClickSelectDefaultJobButton
          }
          onClickResetDefaultJobButton={this.props.onClickResetDefaultJobButton}
        />
        <JobSelectDialogContainer
          onSelectItem={this.props.onSelectDefaultJobItem}
        />
      </div>
    ) : null;
  }
}

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(
  PersonalSettingDialogContainer
): React.ComponentType<Object>): React.ComponentType<Object>);
