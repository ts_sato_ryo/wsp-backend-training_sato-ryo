/* @flow */
import { bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';
import * as React from 'react';
import moment from 'moment';

import { clearJobList, fetchJobListByParent } from '../actions/jobList';
import { hide } from '../modules/jobSelectDialog';
import JobSelectDialog, {
  type Props as JobSelectDialogProps,
} from '../components/JobSelectDialog';

type Job = {
  id: string,
  code: string,
  name: string,
  parentId: ?string,
  hasChildren: boolean,
  isDirectCharged: boolean,
  selectabledTimeTrack: boolean,
};

const mapStateToProps = (state) => {
  return {
    isVisible: state.common.jobSelectDialog.isVisible,
    jobList: state.common.jobList,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
  ...bindActionCreators(
    {
      fetchJobListByParent,
      clearJobList,
    },
    dispatch
  ),
  // GENIE-7961
  // dispatch が action を返すようになった為に、bindActionCreators を使うと
  // () => void 型ではなくて () => HideDialogAction 型の関数になってしまう。
  // presentational component が action の詳細を知る必要はない為、
  // () => void 型を修正するのではなく、マッピングを工夫する必要がある。
  onClickCloseButton: () => {
    dispatch(hide());
  },
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  onClickItem: (selectedItem: Job, existingItems: ?(Job[])) => {
    if (selectedItem.hasChildren && existingItems !== undefined) {
      dispatchProps.fetchJobListByParent(
        moment().format('YYYY-MM-DD'),
        selectedItem,
        existingItems
      );
    } else {
      ownProps.onSelectItem(selectedItem);
      dispatchProps.clearJobList();
      dispatchProps.onClickCloseButton();
    }
  },
});

type Props = JobSelectDialogProps & {
  isVisible: boolean,
};

class JobSelectDialogContainer extends React.Component<Props> {
  render() {
    return this.props.isVisible ? (
      <JobSelectDialog
        jobList={this.props.jobList}
        onClickItem={this.props.onClickItem}
        onClickCloseButton={this.props.onClickCloseButton}
      />
    ) : null;
  }
}

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(JobSelectDialogContainer): React.ComponentType<
  *
>): React.ComponentType<Object>);
