import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import StampWidget from '../../components/widgets/StampWidget';
import * as stampWidgetActions from '../../action-dispatchers/StampWidget';

const mapStateToProps = (state, ownProps) => ({
  language: state.common.userSetting.language, // NOTE: 言語適用タイミングの検知
  isEnableStartStamp: state.common.stampWidget.isEnableStartStamp,
  isEnableEndStamp: state.common.stampWidget.isEnableEndStamp,
  isEnableRestartStamp: state.common.stampWidget.isEnableRestartStamp,
  mode: state.common.stampWidget.mode,
  message: state.common.stampWidget.message,
  className: ownProps.className,
});

function mapDispatchToProps(dispatch, ownProps) {
  return bindActionCreators(
    {
      onDidMount:
        ownProps.onDidMount !== undefined
          ? ownProps.onDidMount
          : stampWidgetActions.initDailyStampTime,
      onClickStampButton: (stampWidget) =>
        stampWidgetActions.submitStamp(stampWidget, {
          withGlobalLoading: ownProps.withGlobalLoading,
          onStampSuccess: ownProps.onStampSuccess,
        }),
      onChangeMessage: (message) => stampWidgetActions.updateMessage(message),
      onClickModeButton: (clockType) =>
        stampWidgetActions.switchClockType(clockType),
    },
    dispatch
  );
}

const mergeProps = (stateProps, dispatchProps) => ({
  ...stateProps,
  ...dispatchProps,
  onClickStampButton: () => dispatchProps.onClickStampButton(stateProps),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(StampWidget);
