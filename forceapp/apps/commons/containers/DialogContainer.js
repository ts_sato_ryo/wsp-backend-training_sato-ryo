import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import Dialog from '../components/dialogs/Dialog';

import { hideDialog, DIALOG_TYPE } from '../actions/dialog';

class DialogContainer extends React.Component {
  static get propTypes() {
    return {
      // 親から受け取る props
      style: PropTypes.object.isRequired,
      onClickExpTypeItem: PropTypes.func,
      onSubmitReport: PropTypes.func,
      onClickCostCenterItem: PropTypes.func,
      onClickJobItem: PropTypes.func,
      onClickSettingButton: PropTypes.func,
      onClickSelectRouteButton: PropTypes.func,

      // Redux state を map した props
      dialog: PropTypes.object.isRequired,
      approvalHistoryList: PropTypes.array.isRequired,
      costCenterList: PropTypes.array.isRequired,
      expTypeList: PropTypes.array.isRequired,
      jobList: PropTypes.array.isRequired,
      searchedRoute: PropTypes.object.isRequired,

      children: PropTypes.array,
    };
  }

  static get defaultProps() {
    return {
      children: [],
    };
  }

  constructor(props) {
    super(props);
    this.hideDialog = this.hideDialog.bind(this);
  }

  hideDialog() {
    this.props.dispatch(hideDialog());
  }

  render() {
    return (
      <Dialog
        style={this.props.style}
        dialog={this.props.dialog}
        dialogTypeList={DIALOG_TYPE}
        approvalHistoryList={this.props.approvalHistoryList}
        costCenterList={this.props.costCenterList}
        expTypeList={this.props.expTypeList}
        jobList={this.props.jobList}
        searchedRoute={this.props.searchedRoute}
        hideDialog={this.hideDialog}
        onClickExpTypeItem={this.props.onClickExpTypeItem}
        onSubmitReport={this.props.onSubmitReport}
        onClickCostCenterItem={this.props.onClickCostCenterItem}
        onClickJobItem={this.props.onClickJobItem}
        onClickSettingButton={this.props.onClickSettingButton}
        onClickSelectRouteButton={this.props.onClickSelectRouteButton}
        contents={this.props.children}
      />
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dialog: state.common.dialog,
    approvalHistoryList: state.common.approvalHistoryList,
    costCenterList: state.common.costCenterList,
    expTypeList: state.common.expTypeList,
    jobList: state.common.jobList,
    searchedRoute: state.common.searchedRoute,
  };
};

export default connect(mapStateToProps)(DialogContainer);
