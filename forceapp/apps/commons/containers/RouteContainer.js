import { connect } from 'react-redux';

import Route from '../components/Route';

import {
  onChangeInput,
  cancelConfirm,
  searchStation,
  setSuggestionItem,
  getStationHistory,
  searchRoute,
  setSuggestInput,
} from '../actions/route';

const mapStateToProps = (state) => {
  return {
    empId: state.common.empInfo.record.empId,
    suggestInput: state.common.suggestInput,
    searchedRoute: state.common.searchedRoute,
    stationHistoryList: state.common.stationHistoryList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeInput: (inputType, value) => {
      dispatch(onChangeInput(inputType, value));
    },
    cancelConfirm: (inputType) => {
      dispatch(cancelConfirm(inputType));
    },
    searchStation: (inputType, param) => {
      dispatch(searchStation(inputType, param));
    },
    setSuggestionItem: (inputType, suggestionItem) => {
      dispatch(setSuggestionItem(inputType, suggestionItem));
    },
    getStationHistory: (empId) => {
      dispatch(getStationHistory(empId));
    },
    searchRoute: (inputType, param) => {
      dispatch(searchRoute(inputType, param));
    },
    setSuggestInput: (routeInfo) => {
      dispatch(setSuggestInput(routeInfo));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Route);
