// @flow

import * as React from 'react';
import { connect } from 'react-redux';

import AccessControl, { type Props } from '../components/AccessControl';
import type { State } from '../reducers';
import type {
  Permission,
  TotalTestConditions,
  DynamicTestConditions,
} from '../../domain/models/access-control/Permission';

type OwnProps = {|
  ...$Rest<
    TotalTestConditions,
    {| userPermission: Permission, isByDelegate: boolean |}
  >,
  conditions?: DynamicTestConditions,
  children: React.Node,
|};

const mapStateToProps = (
  state: {
    common: { ...State },
  },
  ownProps: OwnProps
): Props => ({
  ...ownProps,
  userPermission: state.common.accessControl.permission,
  isByDelegate:
    state.common.proxyEmployeeInfo &&
    state.common.proxyEmployeeInfo.isProxyMode,
});

const Component: React.ComponentType<OwnProps> = (connect(mapStateToProps)(
  AccessControl
): React.ComponentType<Object>);

export default Component;
