// @flow

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { clearError } from '../actions/app';
import Global, { type Props } from '../components/Global';
import { selectors as appSelectors } from '../modules/app';

const mapStateToProps = (state) => ({
  loading: appSelectors.loadingSelector(state),
  error: state.common.app.error,
  unexpectedError: state.common.app.unexpectedError,
  confirmDialog: state.common.app.confirmDialog,
  dialog: state.common.dialog,
});

const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
  ...bindActionCreators(
    {
      handleCloseErrorDialog: clearError,
    },
    dispatch
  ),
});

const mergeProps = (stateProps: *, dispatchProps: *, ownProps: *): Props => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  confirmDialog:
    stateProps.confirmDialog !== undefined && stateProps.confirmDialog !== null
      ? {
          ...stateProps.confirmDialog,
          handleClickOkButton: () => stateProps.confirmDialog.callback(true),
          handleClickCancelButton: () =>
            stateProps.confirmDialog.callback(false),
        }
      : null,
});

export default ((connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps
)(Global): React.ComponentType<*>): React.ComponentType<Object>);
