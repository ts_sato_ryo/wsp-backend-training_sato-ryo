/* @flow */

import { type Dispatch } from 'redux';
import { connect } from 'react-redux';

import Component from '../components/Toast';

import { actions } from '../modules/toast';

const mapStateToProps = (state) => ({
  isShow: state.common.toast.isShow,
  message: state.common.toast.message,
  variant: state.common.toast.variant,
});

const mapDispatchToProps = (dispatch: Dispatch<Object>) => ({
  onClick: () => dispatch(actions.hide()),
  onExit: () => dispatch(actions.reset()),
});

export default (connect(
  mapStateToProps,
  mapDispatchToProps
)(Component): React.ComponentType<Object>);
