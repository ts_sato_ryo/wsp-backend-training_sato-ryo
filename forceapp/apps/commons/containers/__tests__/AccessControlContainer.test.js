// @flow

import * as React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { render, cleanup } from 'react-testing-library';

import AccessControl from '../AccessControlContainer';
import type { Permission } from '../../../domain/models/access-control/Permission';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

class Stub extends React.Component<{}> {
  render() {
    return <div data-testid="stub" />;
  }
}

describe('AccessControlContainer', () => {
  describe('権限 を持っている場合', () => {
    let store;
    beforeEach(() => {
      const userPermission: Permission = {
        viewAttTimeSheetByDelegate: false,
        editAttTimeSheetByDelegate: false,
        submitAttDailyRequestByDelegate: false,
        cancelAttDailyRequestByDelegate: false,
        cancelAttDailyApprovalByEmployee: false,
        cancelAttDailyApprovalByDelegate: false,
        submitAttRequestByDelegate: false,
        cancelAttRequestByDelegate: false,
        manageOverallSetting: true,
        switchCompany: false,
        manageDepartment: false,
        manageEmployee: false,
        manageCalendar: true,
        manageJobType: false,
        manageJob: false,
        managePermission: false,
        manageAttLeave: false,
        manageAttShortTimeWorkSetting: false,
        manageAttLeaveOfAbsence: false,
        manageAttWorkingType: false,
        manageAttAgreementAlertSetting: false,
        manageAttLeaveGrant: true,
        manageAttShortTimeSettingApply: false,
        manageAttLeaveOfAbsenceApply: false,
        manageTimeSetting: false,
        manageTimeWorkCategory: false,
      };
      window.empInfo = { permission: userPermission };
      store = mockStore({
        common: {
          proxyEmployeeInfo: { isProxyMode: true },
          accessControl: { permission: { ...userPermission } },
        },
      });
    });

    afterEach(cleanup);

    test('`children` がレンダリングされる', () => {
      const { getByTestId } = render(
        <Provider store={store}>
          <AccessControl
            requireIfByDelegate={[
              'manageOverallSetting',
              'manageCalendar',
              'manageAttLeaveGrant',
            ]}
          >
            <Stub />
          </AccessControl>
        </Provider>
      );

      expect(getByTestId('stub')).not.toBeUndefined();
      expect(getByTestId('stub')).not.toBeNull();
    });
  });

  describe('権限 を持ってない場合', () => {
    let store;
    beforeEach(() => {
      const userPermission: Permission = {
        viewAttTimeSheetByDelegate: false,
        editAttTimeSheetByDelegate: false,
        submitAttDailyRequestByDelegate: false,
        cancelAttDailyRequestByDelegate: false,
        cancelAttDailyApprovalByEmployee: false,
        cancelAttDailyApprovalByDelegate: false,
        submitAttRequestByDelegate: false,
        cancelAttRequestByDelegate: false,
        manageOverallSetting: false,
        switchCompany: false,
        manageDepartment: false,
        manageEmployee: false,
        manageCalendar: false,
        manageJobType: false,
        manageJob: false,
        managePermission: false,
        manageAttLeave: false,
        manageAttShortTimeWorkSetting: false,
        manageAttLeaveOfAbsence: false,
        manageAttWorkingType: false,
        manageAttAgreementAlertSetting: false,
        manageAttLeaveGrant: false,
        manageAttShortTimeSettingApply: false,
        manageAttLeaveOfAbsenceApply: false,
        manageTimeSetting: false,
        manageTimeWorkCategory: false,
      };
      window.empInfo = { permission: userPermission };
      store = mockStore({
        common: {
          proxyEmployeeInfo: { isProxyMode: true },
          accessControl: { permission: { ...userPermission } },
        },
      });
    });

    afterEach(cleanup);

    test('`children` がレンダリングされ無い', () => {
      const { queryByTestId } = render(
        <Provider store={store}>
          <AccessControl
            requireIfByDelegate={[
              'manageOverallSetting',
              'manageCalendar',
              'manageAttLeaveGrant',
            ]}
          >
            <Stub />
          </AccessControl>
        </Provider>
      );

      expect(queryByTestId('stub')).toBeNull();
    });
  });
});
