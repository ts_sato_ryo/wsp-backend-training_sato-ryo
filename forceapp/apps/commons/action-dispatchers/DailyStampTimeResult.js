/* @flow */
import {
  type DailyAttAddProcess,
  PROCESS_TYPE,
  createDailyAttAddProcess,
} from '../models/DailyAttAddProcess';
import * as DailyAttAddProcessActions from './DailyAttAddProcess';
import { type DailyStampTimeResult } from '../../domain/models/attendance/DailyStampTime';

const createDailyStampTimeResultProcess = (
  result: DailyStampTimeResult
): ?DailyAttAddProcess => {
  if (!result) {
    return null;
  }

  if (Number(result.insufficientRestTime) > 0) {
    return createDailyAttAddProcess(PROCESS_TYPE.INSUFFICIENT_REST_TIME, {
      insufficientRestTime: Number(result.insufficientRestTime),
    });
  }

  return null;
};

/**
 * 休憩時間が足りていない場合にダイアログを表示する
 */
// eslint-disable-next-line import/prefer-default-export
export const doAddProcess = (result: DailyStampTimeResult) => (
  dispatch: Dispatch<*>
) => {
  const process = createDailyStampTimeResultProcess(result);
  return dispatch(DailyAttAddProcessActions.doAddProcess(process));
};
