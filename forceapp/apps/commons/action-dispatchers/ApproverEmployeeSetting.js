// @flow
import type { ApproverEmployee } from '../../domain/models/approval/ApproverEmployee';
import * as PersonalSetting from '../../domain/models/PersonalSetting';
import { withLoading, catchApiError } from '../actions/app';
import { getUserSetting } from '../actions/userSetting';
import { actions as ApproverEmployeeSettingUiDialogActions } from '../modules/approverEmployeeSetting/ui/dialog';
import { actions as ApproverEmployeeSettingUiStatusActions } from '../modules/approverEmployeeSetting/ui/status';
import { actions as ApproverEmployeeSettingEntitiesActions } from '../modules/approverEmployeeSetting/entities';
import { type DialogType } from '../modules/approverEmployeeSearch/ui/dialog';

/**
 * ダイアログを閉じます。
 */
export const hideDialog = () => (dispatch: Dispatch<*>) => {
  dispatch(ApproverEmployeeSettingUiDialogActions.close());
};

/**
 * 承認者を変更します。
 */
export const setApproverEmployee = (setting: ApproverEmployee) => (
  dispatch: Dispatch<*>
) => {
  dispatch(ApproverEmployeeSettingEntitiesActions.set(setting));
};

/**
 * 読み込みのみかどうかを指定します。
 */
export const setReadOnly = (isReadOnly: boolean) => (dispatch: Dispatch<*>) => {
  dispatch(ApproverEmployeeSettingUiStatusActions.setReadOnly(isReadOnly));
};

/**
 * 保存できるようにします。
 */
export const save = (setting: ApproverEmployee, onSuccess: () => *) => (
  dispatch: Dispatch<*>
): Promise<*> => {
  return dispatch(
    withLoading(
      () => PersonalSetting.update({ approverBase01Id: setting.id }),
      () => dispatch(getUserSetting())
    )
  )
    .then(() => dispatch(hideDialog()))
    .then(onSuccess)
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
};

/**
 * ダイアログを開きます。
 */
export const showDialog = (
  setting: ApproverEmployee,
  targetDate: string,
  isReadOnly: boolean,
  dialogType: DialogType = ''
) => (dispatch: Dispatch<*>) => {
  dispatch(setApproverEmployee(setting));
  dispatch(ApproverEmployeeSettingUiStatusActions.setTargetDate(targetDate));
  dispatch(setReadOnly(isReadOnly));
  dispatch(ApproverEmployeeSettingUiDialogActions.open(dialogType));
};
