/* @flow */
import { type Dispatch } from 'redux';
import * as commonActions from '../actions/app';
import * as stampWidgetActions from '../actions/stampWidget';
import * as dailyStampTimeResultActions from './DailyStampTimeResult';
import {
  type ClockType,
  type EditingDailyStampTime,
  type PostStampRequest,
  STAMP_SOURCE,
  postStamp,
  fetchDailyStampTime,
} from '../../domain/models/attendance/DailyStampTime';

/**
 * 打刻情報を取得する
 */
export const initDailyStampTime = () => (dispatch: Dispatch<any>) => {
  return fetchDailyStampTime()
    .then((result) => {
      // 打刻情報を適用する
      dispatch(stampWidgetActions.applyDailyStampTime(result));
    })
    .catch((err) =>
      dispatch(commonActions.catchApiError(err, { isContinuable: true }))
    );
};

/**
 * 打刻を実行（送信）する
 */
export const submitStamp = (
  stampWidget: EditingDailyStampTime,
  options: ?{
    withGlobalLoading?: boolean,
    onStampSuccess?: () => void,
  }
) => (dispatch: Dispatch<any>) => {
  // NOTE: 本来発生しえない／mode未定だと打刻ボタンが非活性なので
  if (stampWidget.mode === null || stampWidget.mode === undefined) {
    return Promise.reject();
  }

  const { withGlobalLoading, onStampSuccess } = options || {};

  const postParams: PostStampRequest = {
    mode: stampWidget.mode,
    message: stampWidget.message,
  };

  const commonPostProcess = () => postStamp(postParams, STAMP_SOURCE.WEB);
  const postProcess = withGlobalLoading
    ? commonActions.withLoading(commonPostProcess)
    : commonPostProcess;

  // ユーザー操作を防止する
  dispatch(stampWidgetActions.blockOperation());

  return dispatch(postProcess)
    .then((result) =>
      dispatch(dailyStampTimeResultActions.doAddProcess(result))
    )
    .then(() => {
      if (onStampSuccess && onStampSuccess instanceof Function) {
        onStampSuccess();
      }
    })
    .catch((err) =>
      dispatch(commonActions.catchApiError(err, { isContinuable: true }))
    )
    .then(() => {
      dispatch(initDailyStampTime());
    });
};

/**
 * 出勤・退勤を切り替える
 * @param {String} clockType
 */
export const switchClockType = (clockType: ClockType) => (
  dispatch: Dispatch<any>
) => {
  dispatch(stampWidgetActions.switchClockType(clockType));
};

/**
 * メッセージを更新する
 * @param {String} message
 * @returns {{type: String, payload: Object}}
 */
export const updateMessage = (message: string) => (dispatch: Dispatch<any>) => {
  dispatch(stampWidgetActions.updateMessage(message));
};
