/* @flow */
import * as commonActions from '../actions/app';
import { type DailyAttAddProcess } from '../models/DailyAttAddProcess';
import AskInsufficientRestTime from '../components/dialogs/confirm/AskInsufficientRestTime';
import * as DailyRestTimeFill from '../../domain/models/attendance/DailyRestTimeFill';

/**
 * 日次勤怠登録後に必要な処理（process）が発行されていれば処理を行う。
 */
// eslint-disable-next-line import/prefer-default-export
export const doAddProcess = (process: ?DailyAttAddProcess) => (
  dispatch: Dispatch<any>
) => {
  if (!process) {
    return Promise.resolve();
  }
  const { type, payload } = process;

  switch (type) {
    // 休憩時間が足りて居ない場合
    case 'INSUFFICIENT_REST_TIME':
      return dispatch(
        commonActions.confirm({
          Component: AskInsufficientRestTime,
          params: payload,
        })
      ).then((result) => {
        if (!result) {
          return false;
        }
        return dispatch(
          commonActions.withLoading(() => {
            let targetDate;
            let employeeId;
            if (payload) {
              targetDate = payload.targetDate || null;
              employeeId = payload.employeeId || null;
            }
            return DailyRestTimeFill.post({
              targetDate,
              employeeId,
            })
              .then(() => true)
              .catch((err) => {
                dispatch(
                  commonActions.catchApiError(err, { isContinuable: true })
                );
                return false;
              });
          })
        );
      });
    default:
      return Promise.resolve();
  }
};
