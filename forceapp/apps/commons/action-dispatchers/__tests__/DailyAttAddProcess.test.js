import Api from '../../../../__tests__/mocks/ApiMock';
import DispatcherMock from '../../../../__tests__/mocks/DispatcherMock';
import {
  PROCESS_TYPE,
  createDailyAttAddProcess,
} from '../../models/DailyAttAddProcess';
import * as actions from '../DailyAttAddProcess';

describe('doAddProcess(process)', () => {
  describe('追加の処理がない場合', () => {
    const mockDispatcher = new DispatcherMock();
    const process = createDailyAttAddProcess({});
    mockDispatcher.dispatch(actions.doAddProcess(process));
    expect(mockDispatcher.logged).toHaveLength(1);
  });
  describe('追加の処理がある場合', () => {
    describe('法定休憩時間チェックの場合', () => {
      const process = createDailyAttAddProcess(
        PROCESS_TYPE.INSUFFICIENT_REST_TIME,
        {
          employeeId: null,
          targetDate: null,
          insufficientRestTime: 15,
        }
      );

      Api.setDummyResponse(
        '/att/daily-rest-time/fill',
        {
          targetDate: null,
          employeeId: null,
        },
        {}
      );

      test('[OK] を押下した場合は休暇時間登録用の API が呼ばれます。', async () => {
        // 下記 URL のバグを踏んでしまっているらしく assertions が１つ多い状態になってしまいます。
        // https://github.com/facebook/jest/issues/6592
        expect.assertions(6);
        const mockDispatcher = new DispatcherMock();
        const promise = mockDispatcher.dispatch(actions.doAddProcess(process));
        expect(typeof mockDispatcher.logged[0]).toBe('function');
        expect(typeof mockDispatcher.logged[1]).toBe('function');
        expect(mockDispatcher.logged[2].type).toBe('CONFIRM_DIALOG_OPEN');
        mockDispatcher.logged[2].payload.callback(true);
        expect(mockDispatcher.logged[3].type).toBe('CONFIRM_DIALOG_CLOSE');
        await promise.then((result) => {
          expect(result).toBe(true);
        });
      });

      test('[キャンセル] を押下した場合は何もしません。', async () => {
        expect.assertions(5);
        const mockDispatcher = new DispatcherMock();
        const promise = mockDispatcher.dispatch(actions.doAddProcess(process));
        expect(typeof mockDispatcher.logged[0]).toBe('function');
        expect(typeof mockDispatcher.logged[1]).toBe('function');
        expect(mockDispatcher.logged[2].type).toBe('CONFIRM_DIALOG_OPEN');
        mockDispatcher.logged[2].payload.callback(false);
        expect(mockDispatcher.logged[3].type).toBe('CONFIRM_DIALOG_CLOSE');
        await promise.then((result) => {
          expect(result).toBe(false);
        });
      });
    });
  });
});
