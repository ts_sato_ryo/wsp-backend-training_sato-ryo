// @flow
import { withLoading, catchApiError } from '../actions/app';
import {
  type DialogType,
  actions as ApproverEmployeeSearchUiDialogActions,
} from '../modules/approverEmployeeSearch/ui/dialog';
import { actions as ApproverEmployeeSearchUiOperationActions } from '../modules/approverEmployeeSearch/ui/operation';
import { actions as ApproverEmployeeSearchEntitiesActions } from '../modules/approverEmployeeSearch/entities';
import { actions as ApproverEmployeeSettingEntitiesAction } from '../modules/approverEmployeeSetting/entities';
import {
  searchApproverEmployee,
  type SearchParamType,
} from '../models/ApproverEmployeeSearch';
import SearchStrategy, {
  type SearchStrategyType,
} from '../../../widgets/dialogs/ProxyEmployeeSelectDialog/models/SearchStrategy';
import Employee from '../../../widgets/dialogs/ProxyEmployeeSelectDialog/models/Employee';

const searchByRepositoryRequest = (param: SearchParamType) => (
  dispatch: Dispatch<*>
) => {
  return dispatch(
    withLoading(
      () => searchApproverEmployee(param),
      ({ records }) =>
        dispatch(
          ApproverEmployeeSearchEntitiesActions.setByRepositoryResponseData(
            records
          )
        )
    )
  ).catch((err) => dispatch(catchApiError(err, { isContinuable: true })));
};

export const switchSearchStrategy = (
  searchStrategy: SearchStrategyType,
  targetDate: string,
  targetCompanyId: string,
  targetDepartmentId: string = ''
) => (dispatch: Dispatch<*>) => {
  dispatch(
    ApproverEmployeeSearchUiOperationActions.switchSearchStrategy(
      searchStrategy
    )
  );

  if (searchStrategy === SearchStrategy.SHOW_EMPLOYEES_IN_SAME_DEPARTMENT) {
    dispatch(
      searchByRepositoryRequest({
        companyId: targetCompanyId,
        departmentId: targetDepartmentId,
        targetDate,
      })
    );
  }
};

export const searchByTargetDateAndCompanyIdAndInputValue = (
  targetDate: string,
  targetCompanyId: string,
  query: {|
    departmentCode: string,
    departmentName: string,
    employeeCode: string,
    employeeName: string,
    title: string,
  |}
) => (dispatch: Dispatch<*>) =>
  dispatch(
    searchByRepositoryRequest({
      companyId: targetCompanyId,
      departmentCode: query.departmentCode,
      departmentName: query.departmentName,
      code: query.employeeCode,
      name: query.employeeName,
      title: query.title,
      targetDate,
    })
  );

export const showDialog = (
  targetDate: string,
  targetCompanyId: string,
  targetDepartmentId: string = '',
  dialogType: DialogType = ''
) => (dispatch: Dispatch<*>) => {
  dispatch(ApproverEmployeeSearchUiDialogActions.open(dialogType));
  dispatch(ApproverEmployeeSearchUiOperationActions.setTargetDate(targetDate));
  dispatch(
    searchByRepositoryRequest({
      targetDate,
      companyId: targetCompanyId,
      departmentId: targetDepartmentId,
    })
  );
};

export const decide = (selectedEmployee: Employee) => (
  dispatch: Dispatch<*>
) => {
  dispatch(
    ApproverEmployeeSettingEntitiesAction.set({
      id: selectedEmployee.id,
      employeeName: selectedEmployee.employeeName,
    })
  );
  dispatch(ApproverEmployeeSearchUiDialogActions.close());
  dispatch(ApproverEmployeeSearchUiOperationActions.clear());
  dispatch(ApproverEmployeeSearchEntitiesActions.clear());
};

export const cancel = () => (dispatch: Dispatch<*>) => {
  dispatch(ApproverEmployeeSearchUiDialogActions.close());
  dispatch(ApproverEmployeeSearchUiOperationActions.clear());
  dispatch(ApproverEmployeeSearchEntitiesActions.clear());
};
