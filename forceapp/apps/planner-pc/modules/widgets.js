// @flow

import { combineReducers } from 'redux';

import PersonalMenuPopover from '../../commons/modules/widgets/PersonalMenuPopover';

export default combineReducers<Object, Object>({
  PersonalMenuPopover,
});
