// @flow

// import _ to use chain method on purpose
import _ from 'lodash';
import {
  min,
  max,
  eachDay,
  startOfWeek,
  differenceInCalendarDays,
  addDays,
  isSameDay,
} from 'date-fns';

import { WEEK_START_ON } from '../../constants/calendar';
import type { Event } from '../../../domain/models/time-management/Event';

// State

type State = {
  [date: Date]: {
    ...Event,

    /**
     * The day length of the event.
     *
     * the event less than 24-hour is also counted as 1.
     */
    period: number,

    /**
     * Event start date time
     */
    startDateTime: Date,

    /**
     * Event end date time
     */
    endDateTime: Date,

    /**
     * Date of event
     */
    date: Date,

    /**
     * The day length of the week including `date`
     *
     * the event less than 24-hour is also counted as 1.
     */
    periodOfWeek: number,

    /**
     * The start date of the week including `date`
     */
    startDateTimeOfWeek: Date,

    /**
     * The end date of the week including `date`
     */
    endDateTimeOfWeek: Date,

    /**
     * The flag determines which the event are over multiple days or not.
     */
    isOverMultipleDays: boolean,
  },
};

const initialState = {};

// Actions

type FetchSuccess = {|
  type: '/PLANNER-PC/MODULES/ENTITIES/EVENTS/FETCH_SUCCESS',
  payload: {
    events: $ReadOnlyArray<Event>,
    startDate: Date,
    endDate: Date,
  },
|};

type Action = FetchSuccess;

export const FETCH_SUCCESS =
  '/PLANNER-PC/MODULES/ENTITIES/EVENTS/FETCH_SUCCESS';

export const actions = {
  fetchSuccess: (
    events: $ReadOnlyArray<Event>,
    startDate: Date,
    endDate: Date
  ): FetchSuccess => ({
    type: FETCH_SUCCESS,
    payload: {
      events,
      startDate,
      endDate,
    },
  }),
};

// Reducer

export default (state: State = initialState, action: Action) => {
  switch (action.type) {
    case FETCH_SUCCESS: {
      const minDate = action.payload.startDate;
      const maxDate = action.payload.endDate;

      const events = _(action.payload.events).map((event) => {
        const startDateTime = new Date(event.startDateTime);
        const endDateTime = new Date(event.endDateTime);
        return {
          ...event,
          startDateTime,
          endDateTime,
          period: differenceInCalendarDays(endDateTime, startDateTime) + 1,
          isOverMultipleDays: !isSameDay(startDateTime, endDateTime),
        };
      });

      return events
        .flatMap((event) => {
          const key = (date) => startOfWeek(date, WEEK_START_ON);

          const dates = eachDay(
            max(minDate, event.startDateTime),
            addDays(min(maxDate, event.endDateTime), 1)
          );

          const weeks = _(dates)
            .groupBy((date) => key(date))
            .mapValues((days) => {
              const startDateTimeOfWeek = _.first(days);
              const endDateTimeOfWeek = _.last(days);
              return {
                startDateTimeOfWeek: max(
                  startDateTimeOfWeek,
                  event.startDateTime
                ),
                endDateTimeOfWeek: min(endDateTimeOfWeek, event.endDateTime),
                periodOfWeek: differenceInCalendarDays(
                  endDateTimeOfWeek,
                  startDateTimeOfWeek
                ),
              };
            })
            .value();

          return dates.map((date) => ({
            ...event,
            ...weeks[key(date)],
            date,
          }));
        })
        .groupBy((event) => event.date)
        .mapValues((es: $ReadOnlyArray<$Values<State>>) =>
          _.orderBy(es, [(e) => e.period], ['desc'])
        )
        .value();
    }
    default:
      return state;
  }
};
