// @flow

import { combineReducers } from 'redux';

import events from './events';

const rootReducer = {
  events,
};

export default combineReducers<Object, Object>(rootReducer);
