// @flow

import { combineReducers } from 'redux';

import { calendarMode, empEvents, selectedDay } from './calendar';
import common from '../../commons/reducers';
import eventEditPopup from './eventEditPopup';
import entities from './entities';
import ui from './ui';
import userSetting from '../../commons/reducers/userSetting';
import widgets from './widgets';
import type { $State } from '../../commons/utils/TypeUtil';

// eslint-disable-next-line no-unused-vars
function env(state: Object = {}, action: Object) {
  return state;
}

const rootReducer = {
  entities,
  env,
  common,
  userSetting,
  calendarMode,
  empEvents,
  selectedDay,
  eventEditPopup,
  ui,
  widgets,
};

export type State = $State<typeof rootReducer>;

export default combineReducers<Object, Object>(rootReducer);
