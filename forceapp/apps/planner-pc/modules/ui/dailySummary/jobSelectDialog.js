// @flow

import type { Job } from '../../../../domain/models/time-tracking/Job';
import {
  DAILY_SUMMARY_RECEIVE_JOB_LIST,
  DAILY_SUMMARY_CLEAR,
} from '../../../constants/dailySummary';

// State

type State = Job[];

const initialState = [];

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryReceiveJobListAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_JOB_LIST',
  payload: Job[],
|};

type Action = DailySummaryClearAction | DailySummaryReceiveJobListAction;

// Reducer

export default function jobSelectDialogReducer(
  state: State = initialState,
  action: Action
) {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    case DAILY_SUMMARY_RECEIVE_JOB_LIST:
      return action.payload;
    default:
      return state;
  }
}
