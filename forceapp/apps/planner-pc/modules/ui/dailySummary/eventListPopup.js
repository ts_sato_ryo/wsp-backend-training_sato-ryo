// @flow

import moment from 'moment';

import type { CalendarEvent } from '../../../models/calendar-event/CalendarEvent';

import {
  DAILY_SUMMARY_OPEN_EVENTLIST,
  DAILY_SUMMARY_CLOSE_EVENTLIST,
} from '../../../constants/dailySummary';

// State

type State = {
  day: ?moment,
  events: CalendarEvent[],
  style: {
    top: 0,
    left: 0,
  },
  isOpen: false,
};

const initialState = {
  day: null,
  events: [],
  style: {
    top: 0,
    left: 0,
  },
  isOpen: false,
};

// Actions

type DailySummaryOpenEventlistAction = {|
  type: 'DAILY_SUMMARY_OPEN_EVENTLIST',
  payload: {
    day: moment,
    events: CalendarEvent[],
    style: Object,
  },
|};

type DailySummaryCloseEventlistAction = {|
  type: 'DAILY_SUMMARY_CLOSE_EVENTLIST',
|};

type Action =
  | DailySummaryOpenEventlistAction
  | DailySummaryCloseEventlistAction;

// Reducer

const eventListPopupReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case DAILY_SUMMARY_OPEN_EVENTLIST:
      return Object.assign({}, state, {
        isOpen: true,
        day: action.payload.day,
        events: action.payload.events,
        style: action.payload.style,
      });
    case DAILY_SUMMARY_CLOSE_EVENTLIST:
      return initialState;
    default:
      return state;
  }
};

export default eventListPopupReducer;
