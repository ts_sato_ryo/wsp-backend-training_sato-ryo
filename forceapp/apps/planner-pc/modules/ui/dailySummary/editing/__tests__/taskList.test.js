// @flow

import snapshotDiff from 'snapshot-diff';
import reducer, { actions, initialState } from '../taskList';

import { DAILY_SUMMARY_RECEIVE_TASKLIST } from '../../../../../constants/dailySummary';
import { fetchSuccess } from './taskList.payloads';

describe('/PLANNER-PC/UI/TASKLIST}', () => {
  it(DAILY_SUMMARY_RECEIVE_TASKLIST, () => {
    const next = reducer(
      initialState,
      actions.fetchSuccess(fetchSuccess.taskList, fetchSuccess.realWorkTime)
    );
    expect(snapshotDiff(initialState, next)).toMatchSnapshot();
  });
});
