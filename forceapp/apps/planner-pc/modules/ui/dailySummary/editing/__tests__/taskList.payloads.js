// @flow

// eslint-disable-next-line import/prefer-default-export
export const fetchSuccess = {
  taskList: [
    {
      id: '6e287a40-46ce-4601-9d69-740518054cfe',
      jobId: 'a0h2v00000XB8oCAAT',
      jobCode: '3',
      jobName: '自宅の警備の仕事',
      isDirectCharged: false,
      workCategoryId: null,
      workCategoryName: null,
      workCategoryCode: null,
      color: {
        base: '#0083b6',
        linked: '#00658c',
      },
      isDirectInput: true,
      volume: null,
      ratio: null,
      taskTime: 280,
      eventTaskTime: 9900,
      workCategoryList: [
        {
          id: 'a1F2v00000CwPPzEAN',
          code: '1',
          name: 'ピザの上にペパロニを載せる',
        },
        {
          id: 'a1F2v00000CwPQ4EAN',
          code: '2',
          name: '▲刺身の上にタンポポを乗せる',
        },
        {
          id: 'a1F2v00000CwPQEEA3',
          code: '3',
          name:
            '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れ',
        },
        {
          id: 'a1F2v00000CwPQJEA3',
          code: '4',
          name:
            '吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。 何でも薄暗いじめじめした所でニャ',
        },
        {
          id: 'a1F2v00000CwZRHEA3',
          code: '7',
          name: '7',
        },
      ],
    },
    {
      id: 'e0541749-0b1c-4b80-84d7-7173412a6956',
      jobId: 'a0h2v00000XB8o7AAD',
      jobCode: '2',
      jobName: 'そうめんに2～3本だけ黄色や赤色や緑色の麺を混ぜる仕事',
      isDirectCharged: false,
      workCategoryId: null,
      workCategoryName: null,
      workCategoryCode: null,
      color: {
        base: '#00a3df',
        linked: '#007dab',
      },
      isDirectInput: false,
      volume: 175,
      ratio: 100,
      taskTime: 560,
      eventTaskTime: 0,
      workCategoryList: [
        {
          id: 'a1F2v00000CwPPzEAN',
          code: '1',
          name: 'ピザの上にペパロニを載せる',
        },
        {
          id: 'a1F2v00000CwZRWEA3',
          code: '10',
          name: '10',
        },
        {
          id: 'a1F2v00000CwZRbEAN',
          code: '11',
          name: '11',
        },
        {
          id: 'a1F2v00000CwZRXEA3',
          code: '12',
          name: '12',
        },
        {
          id: 'a1F2v00000CwZRgEAN',
          code: '13',
          name: '13',
        },
        {
          id: 'a1F2v00000CwZRlEAN',
          code: '14',
          name: '14',
        },
        {
          id: 'a1F2v00000CwZRqEAN',
          code: '15',
          name: '15',
        },
        {
          id: 'a1F2v00000CwZRvEAN',
          code: '16',
          name: '16',
        },
        {
          id: 'a1F2v00000CwZS0EAN',
          code: '17',
          name: '17',
        },
        {
          id: 'a1F2v00000CwZSAEA3',
          code: '19',
          name: '19',
        },
        {
          id: 'a1F2v00000CwPQ4EAN',
          code: '2',
          name: '▲刺身の上にタンポポを乗せる',
        },
        {
          id: 'a1F2v00000CwZSKEA3',
          code: '21',
          name: '21',
        },
        {
          id: 'a1F2v00000CwPQEEA3',
          code: '3',
          name:
            '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れ',
        },
        {
          id: 'a1F2v00000CwPQJEA3',
          code: '4',
          name:
            '吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。 何でも薄暗いじめじめした所でニャ',
        },
        {
          id: 'a1F2v00000CwZR7EAN',
          code: '5',
          name: '5',
        },
        {
          id: 'a1F2v00000CwZRCEA3',
          code: '6',
          name: '6',
        },
        {
          id: 'a1F2v00000CwZRHEA3',
          code: '7',
          name: '7',
        },
        {
          id: 'a1F2v00000CwZRMEA3',
          code: '8',
          name: '8',
        },
        {
          id: 'a1F2v00000CwZRREA3',
          code: '9',
          name: '9',
        },
      ],
    },
    {
      id: '1d6d60da-d942-4be1-9353-43228edc9163',
      jobId: 'a0h2v00000Xa3gRAAR',
      jobCode: '7',
      jobName: '7',
      isDirectCharged: true,
      workCategoryId: null,
      workCategoryName: null,
      workCategoryCode: null,
      color: {
        base: '#6fbeda',
        linked: '#5592a8',
      },
      isDirectInput: true,
      volume: null,
      ratio: null,
      taskTime: 0,
      eventTaskTime: 0,
      workCategoryList: [
        {
          id: 'a1F2v00000CwPPzEAN',
          code: '1',
          name: 'ピザの上にペパロニを載せる',
        },
        {
          id: 'a1F2v00000CwPQ4EAN',
          code: '2',
          name: '▲刺身の上にタンポポを乗せる',
        },
        {
          id: 'a1F2v00000CwPQEEA3',
          code: '3',
          name:
            '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れ',
        },
        {
          id: 'a1F2v00000CwPQJEA3',
          code: '4',
          name:
            '吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。 何でも薄暗いじめじめした所でニャ',
        },
        {
          id: 'a1F2v00000CwZRHEA3',
          code: '7',
          name: '7',
        },
      ],
    },
    {
      id: '5e104f73-5715-42ed-9635-396e7d5a797b',
      jobId: 'a0h2v00000XB8oCAAT',
      jobCode: '3',
      jobName: '自宅の警備の仕事',
      isDirectCharged: false,
      workCategoryId: 'a1F2v00000CwPQ4EAN',
      workCategoryName: '▲刺身の上にタンポポを乗せる',
      workCategoryCode: '2',
      color: {
        base: '#79cca6',
        linked: '#5d9d80',
      },
      isDirectInput: true,
      volume: null,
      ratio: null,
      taskTime: 0,
      eventTaskTime: 0,
      workCategoryList: [
        {
          id: 'a1F2v00000CwPPzEAN',
          code: '1',
          name: 'ピザの上にペパロニを載せる',
        },
        {
          id: 'a1F2v00000CwPQ4EAN',
          code: '2',
          name: '▲刺身の上にタンポポを乗せる',
        },
        {
          id: 'a1F2v00000CwPQEEA3',
          code: '3',
          name:
            '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れ',
        },
        {
          id: 'a1F2v00000CwPQJEA3',
          code: '4',
          name:
            '吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。 何でも薄暗いじめじめした所でニャ',
        },
        {
          id: 'a1F2v00000CwZRHEA3',
          code: '7',
          name: '7',
        },
      ],
    },
    {
      id: 'f55beefd-4bba-460f-b2e1-19d46383e225',
      jobId: 'a0h2v00000XB8o7AAD',
      jobCode: '2',
      jobName: 'そうめんに2～3本だけ黄色や赤色や緑色の麺を混ぜる仕事',
      isDirectCharged: false,
      workCategoryId: 'a1F2v00000CwPPzEAN',
      workCategoryName: 'ピザの上にペパロニを載せる',
      workCategoryCode: '1',
      color: {
        base: '#8bcd77',
        linked: '#6b9e5b',
      },
      isDirectInput: true,
      volume: null,
      ratio: null,
      taskTime: 0,
      eventTaskTime: 0,
      workCategoryList: [
        {
          id: 'a1F2v00000CwPPzEAN',
          code: '1',
          name: 'ピザの上にペパロニを載せる',
        },
        {
          id: 'a1F2v00000CwZRWEA3',
          code: '10',
          name: '10',
        },
        {
          id: 'a1F2v00000CwZRbEAN',
          code: '11',
          name: '11',
        },
        {
          id: 'a1F2v00000CwZRXEA3',
          code: '12',
          name: '12',
        },
        {
          id: 'a1F2v00000CwZRgEAN',
          code: '13',
          name: '13',
        },
        {
          id: 'a1F2v00000CwZRlEAN',
          code: '14',
          name: '14',
        },
        {
          id: 'a1F2v00000CwZRqEAN',
          code: '15',
          name: '15',
        },
        {
          id: 'a1F2v00000CwZRvEAN',
          code: '16',
          name: '16',
        },
        {
          id: 'a1F2v00000CwZS0EAN',
          code: '17',
          name: '17',
        },
        {
          id: 'a1F2v00000CwZSAEA3',
          code: '19',
          name: '19',
        },
        {
          id: 'a1F2v00000CwPQ4EAN',
          code: '2',
          name: '▲刺身の上にタンポポを乗せる',
        },
        {
          id: 'a1F2v00000CwZSKEA3',
          code: '21',
          name: '21',
        },
        {
          id: 'a1F2v00000CwPQEEA3',
          code: '3',
          name:
            '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れ',
        },
        {
          id: 'a1F2v00000CwPQJEA3',
          code: '4',
          name:
            '吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。 何でも薄暗いじめじめした所でニャ',
        },
        {
          id: 'a1F2v00000CwZR7EAN',
          code: '5',
          name: '5',
        },
        {
          id: 'a1F2v00000CwZRCEA3',
          code: '6',
          name: '6',
        },
        {
          id: 'a1F2v00000CwZRHEA3',
          code: '7',
          name: '7',
        },
        {
          id: 'a1F2v00000CwZRMEA3',
          code: '8',
          name: '8',
        },
        {
          id: 'a1F2v00000CwZRREA3',
          code: '9',
          name: '9',
        },
      ],
    },
  ],
  realWorkTime: 840,
};
