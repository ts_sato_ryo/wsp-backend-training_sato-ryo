// @flow

import {
  DAILY_SUMMARY_CLEAR,
  DAILY_SUMMARY_RECEIVE_NOTE,
  DAILY_SUMMARY_EDIT_NOTE,
} from '../../../../constants/dailySummary';

// State

type State = string;

const initialState: State = '';

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryReceiveNoteAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_NOTE',
  payload: string,
|};

type DailySummaryEditNoteAction = {|
  type: 'DAILY_SUMMARY_EDIT_NOTE',
  payload: string,
|};

type Action =
  | DailySummaryClearAction
  | DailySummaryReceiveNoteAction
  | DailySummaryEditNoteAction;

// Reducer

const noteReducer = (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    case DAILY_SUMMARY_RECEIVE_NOTE:
      return action.payload;
    case DAILY_SUMMARY_EDIT_NOTE:
      return action.payload;
    default:
      return state;
  }
};

export default noteReducer;
