// @flow

import {
  DAILY_SUMMARY_CLEAR,
  DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW,
} from '../../../../constants/dailySummary';
import * as requestStatus from '../../../../../commons/constants/requestStatus';

// Selector

export const isLockedSelector = (state: Object) => {
  const { status } = state.ui.dailySummary.editing.taskOverview;
  return requestStatus.lockEditStatus.includes(status);
};

// State

type State = {
  realWorkTime: ?number,
  isTemporaryWorkTime: boolean,
  totalDirectChargeTaskTime: ?number,
  status: ?string,
};

const initialState = {
  realWorkTime: null,
  isTemporaryWorkTime: true,
  totalDirectChargeTaskTime: null,
  status: null,
};

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryReceiveTaskOverviewAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW',
  payload: State,
|};

type Action = DailySummaryClearAction | DailySummaryReceiveTaskOverviewAction;

// Reducer

const taskOverviewReducer = (
  state: State = initialState,
  action: Action
): State => {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    case DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW:
      return {
        realWorkTime: action.payload.realWorkTime,
        isTemporaryWorkTime: action.payload.isTemporaryWorkTime,
        totalDirectChargeTaskTime: action.payload.totalDirectChargeTaskTime,
        status: action.payload.status,
      };
    default:
      return state;
  }
};

export default taskOverviewReducer;
