// @flow

import {
  DAILY_SUMMARY_CLEAR,
  DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS,
  DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT,
} from '../../../../constants/dailySummary';

// State

type State = {
  isEnableEndStamp: boolean,
  timestampComment: string,
};

const initialState: State = {
  isEnableEndStamp: false,
  timestampComment: '',
};

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryFetchDailyStampTimeSuccessAction = {|
  type: 'DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS',
  payload: { isEnableEndStamp: boolean },
|};

type DailySummaryChangeTimestampCommentAction = {|
  type: 'DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT',
  payload: string,
|};

type Action =
  | DailySummaryClearAction
  | DailySummaryFetchDailyStampTimeSuccessAction
  | DailySummaryChangeTimestampCommentAction;

// Reducer

export default (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    case DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS:
      const { isEnableEndStamp } = action.payload;
      return {
        ...state,
        isEnableEndStamp,
        timestampComment: isEnableEndStamp ? state.timestampComment : '',
      };
    case DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT:
      return {
        ...state,
        timestampComment: action.payload,
      };

    default:
      return state;
  }
};
