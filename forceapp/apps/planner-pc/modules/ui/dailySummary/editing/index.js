// @flow

import { combineReducers } from 'redux';

import taskList from './taskList';
import taskOverview from './taskOverview';
import note from './note';
import output from './output';
import endTimestamp from './endTimestamp';

export default combineReducers<Object, Object>({
  note,
  output,
  taskOverview,
  taskList,
  endTimestamp,
});
