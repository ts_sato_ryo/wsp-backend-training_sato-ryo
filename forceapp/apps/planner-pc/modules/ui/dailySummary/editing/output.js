// @flow

import {
  DAILY_SUMMARY_CLEAR,
  DAILY_SUMMARY_RECEIVE_OUTPUT,
  DAILY_SUMMARY_EDIT_OUTPUT,
} from '../../../../constants/dailySummary';

// State

type State = string;

const initialState = '';

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryReceiveOutputAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_OUTPUT',
  payload: string,
|};

type DailySummaryEditOutputAction = {|
  type: 'DAILY_SUMMARY_EDIT_OUTPUT',
  payload: string,
|};

type Action =
  | DailySummaryClearAction
  | DailySummaryReceiveOutputAction
  | DailySummaryEditOutputAction;

// Reducer

const noteReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    case DAILY_SUMMARY_RECEIVE_OUTPUT:
      return action.payload;
    case DAILY_SUMMARY_EDIT_OUTPUT:
      return action.payload;
    default:
      return state;
  }
};

export default noteReducer;
