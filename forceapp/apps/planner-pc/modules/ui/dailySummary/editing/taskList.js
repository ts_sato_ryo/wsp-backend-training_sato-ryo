// @flow

import _ from 'lodash';
import { calculateEachRatioAndTaskTimeOfNonDirectInputTasks } from '../../../../../domain/models/time-management/DailySummary';

import type { DailySummaryTask } from '../../../../../domain/models/time-management/DailySummaryTask';
import {
  DAILY_SUMMARY_ADD_TASK,
  DAILY_SUMMARY_DELETE_TASK,
  DAILY_SUMMARY_EDIT_TASK,
  DAILY_SUMMARY_EDIT_TASK_TIME,
  DAILY_SUMMARY_CLEAR,
  DAILY_SUMMARY_RECEIVE_TASKLIST,
  DAILY_SUMMARY_TOGGLE_DIRECT_INPUT,
  DAILY_SUMMARY_REORDER_TASK_LIST,
} from '../../../../constants/dailySummary';

// State

type State = {
  taskList: DailySummaryTask[],
  realWorkTime: ?number,
};

export const initialState = {
  taskList: [],
  realWorkTime: null,
};

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryAddTaskAction = {|
  type: 'DAILY_SUMMARY_ADD_TASK',
  payload: DailySummaryTask,
|};

type DailySummaryDeleteTaskAction = {|
  type: 'DAILY_SUMMARY_DELETE_TASK',
  payload: number,
|};

type DailySummaryEditTaskAction = {|
  type: 'DAILY_SUMMARY_EDIT_TASK',
  payload: {
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>,
  },
|};

type DailySummaryEditTaskTimeAction = {|
  type: 'DAILY_SUMMARY_EDIT_TASK_TIME',
  payload: { index: number, value: number },
|};

type DailySummaryReceiveTaskListAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_TASKLIST',
  payload: { taskList: DailySummaryTask[], realWorkTime: ?number },
|};

type DailySummaryReorderTaskListAction = {|
  type: 'DAILY_SUMMARY_REORDER_TASK_LIST',
  payload: DailySummaryTask[],
|};

type DailySummaryToggleDirectInputAction = {|
  type: 'DAILY_SUMMARY_TOGGLE_DIRECT_INPUT',
  payload: { index: number, task: DailySummaryTask },
|};

type Action =
  | DailySummaryClearAction
  | DailySummaryAddTaskAction
  | DailySummaryDeleteTaskAction
  | DailySummaryEditTaskAction
  | DailySummaryEditTaskTimeAction
  | DailySummaryReceiveTaskListAction
  | DailySummaryReorderTaskListAction
  | DailySummaryToggleDirectInputAction;

export const actions = {
  clear: (): DailySummaryClearAction => ({
    type: DAILY_SUMMARY_CLEAR,
  }),
  add: (task: DailySummaryTask): DailySummaryAddTaskAction => ({
    type: DAILY_SUMMARY_ADD_TASK,
    payload: task,
  }),
  delete: (index: number): DailySummaryDeleteTaskAction => ({
    type: DAILY_SUMMARY_DELETE_TASK,
    payload: index,
  }),
  edit: (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ): DailySummaryEditTaskAction => ({
    type: DAILY_SUMMARY_EDIT_TASK,
    payload: {
      index,
      prop,
      value,
    },
  }),
  editTaskTime: (
    index: number,
    value: number
  ): DailySummaryEditTaskTimeAction => ({
    type: DAILY_SUMMARY_EDIT_TASK_TIME,
    payload: {
      index,
      value,
    },
  }),
  fetchSuccess: (
    taskList: DailySummaryTask[],
    realWorkTime: ?number
  ): DailySummaryReceiveTaskListAction => ({
    type: DAILY_SUMMARY_RECEIVE_TASKLIST,
    payload: {
      taskList,
      realWorkTime,
    },
  }),
  reorder: (
    taskList: DailySummaryTask[]
  ): DailySummaryReorderTaskListAction => ({
    type: DAILY_SUMMARY_REORDER_TASK_LIST,
    payload: taskList,
  }),
  toggleDirectInput: (
    index: number,
    task: DailySummaryTask
  ): DailySummaryToggleDirectInputAction => ({
    type: DAILY_SUMMARY_TOGGLE_DIRECT_INPUT,
    payload: { index, task },
  }),
};

// Reducer

const taskListReducer = (
  state: State = initialState,
  action: Action
): State => {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;

    case DAILY_SUMMARY_ADD_TASK:
      return {
        ...state,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          [...state.taskList, action.payload]
        ),
      };

    case DAILY_SUMMARY_DELETE_TASK:
      return {
        ...state,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          [
            ...state.taskList.slice(0, action.payload),
            ...state.taskList.slice(action.payload + 1),
          ]
        ),
      };

    case DAILY_SUMMARY_EDIT_TASK: {
      const newState = _.cloneDeep(state);
      newState.taskList[action.payload.index][action.payload.prop] =
        action.payload.value;
      return {
        ...newState,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          newState.taskList
        ),
      };
    }

    case DAILY_SUMMARY_EDIT_TASK_TIME: {
      const newState = _.cloneDeep(state);
      newState.taskList[action.payload.index].taskTime = action.payload.value;
      return {
        ...newState,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          newState.taskList
        ),
      };
    }

    case DAILY_SUMMARY_RECEIVE_TASKLIST:
      return {
        realWorkTime: action.payload.realWorkTime,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          action.payload.realWorkTime,
          [...action.payload.taskList]
        ),
      };

    case DAILY_SUMMARY_REORDER_TASK_LIST:
      return {
        ...state,
        taskList: _.cloneDeep(action.payload),
      };

    case DAILY_SUMMARY_TOGGLE_DIRECT_INPUT:
      return {
        ...state,
        taskList: calculateEachRatioAndTaskTimeOfNonDirectInputTasks(
          state.realWorkTime,
          [
            ...state.taskList.slice(0, action.payload.index),
            action.payload.task,
            ...state.taskList.slice(action.payload.index + 1),
          ]
        ),
      };

    default:
      return state;
  }
};

export default taskListReducer;
