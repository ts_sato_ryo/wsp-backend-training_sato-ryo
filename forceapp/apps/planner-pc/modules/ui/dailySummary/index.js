// @flow

import { combineReducers } from 'redux';

import selectedDay from './selectedDay';
import events from './events';
import eventListPopup from './eventListPopup';
import jobSelectDialog from './jobSelectDialog';
import editing from './editing';
import workCategoryLists from './workCategoryLists';

export default combineReducers<Object, Object>({
  editing,
  selectedDay,
  events,
  eventListPopup,
  jobSelectDialog,
  workCategoryLists,
});
