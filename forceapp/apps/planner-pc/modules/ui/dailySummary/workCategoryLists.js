// @flow

import _ from 'lodash';

import type { WorkCategory } from '../../../../domain/models/time-tracking/WorkCategory';
import {
  DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST,
  DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS,
  DAILY_SUMMARY_CLEAR,
} from '../../../constants/dailySummary';

// State

type State = { [string]: WorkCategory[] };

const initialState = {};

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummaryReceiveWorkCategoryListsAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS',
  payload: State,
|};

type DailySummaryReceiveWorkCategoryListAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST',
  payload: {
    jobId: string,
    workCategoryList: WorkCategory[],
  },
|};

type Action =
  | DailySummaryClearAction
  | DailySummaryReceiveWorkCategoryListsAction
  | DailySummaryReceiveWorkCategoryListAction;

// Reducer

const workCategoryListsReducer = (
  state: State = initialState,
  action: Action
) => {
  let newState;
  switch (action.type) {
    case DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS:
      newState = _.cloneDeep(state);
      return Object.assign(newState, action.payload);
    case DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST:
      newState = _.cloneDeep(state);
      newState[action.payload.jobId] = action.payload.workCategoryList;

      return newState;
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    default:
      return state;
  }
};

export default workCategoryListsReducer;
