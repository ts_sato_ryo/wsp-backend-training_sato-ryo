// @flow

import _ from 'lodash';
import moment from 'moment';

import {
  DAILY_SUMMARY_SELECT_DAY,
  DAILY_SUMMARY_CLEAR,
} from '../../../constants/dailySummary';

// State

type State = ?moment;

const initialState = null;

// Actions

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type DailySummarySelectDayAction = {|
  type: 'DAILY_SUMMARY_SELECT_DAY',
  payload: moment,
|};

type Action = DailySummaryClearAction | DailySummarySelectDayAction;

// Reducer

const selectedDayReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    case DAILY_SUMMARY_SELECT_DAY:
      return _.cloneDeep(action.payload);
    default:
      return state;
  }
};

export default selectedDayReducer;
