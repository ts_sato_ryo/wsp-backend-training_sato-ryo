// @flow

import _ from 'lodash';

import type { CalendarEvent } from '../../../models/calendar-event/CalendarEvent';
import {
  DAILY_SUMMARY_RECEIVE_EVENTS,
  DAILY_SUMMARY_CLEAR,
} from '../../../constants/dailySummary';

// State

type State = CalendarEvent[];

const initialState = [];

// Actions

type DailySummaryReceiveEventsAction = {|
  type: 'DAILY_SUMMARY_RECEIVE_EVENTS',
  payload: CalendarEvent[],
|};

type DailySummaryClearAction = {|
  type: 'DAILY_SUMMARY_CLEAR',
|};

type Action = DailySummaryReceiveEventsAction | DailySummaryClearAction;

// Reducer

const eventReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case DAILY_SUMMARY_RECEIVE_EVENTS:
      return _.cloneDeep(action.payload);
    case DAILY_SUMMARY_CLEAR:
      return initialState;
    default:
      return state;
  }
};

export default eventReducer;
