// @flow

import _ from 'lodash';

import { DIALOG_SHOW, DIALOG_HIDE } from '../../constants/dialogs';

import { Z_INDEX_DEFAULT } from '../../../commons/components/dialogs/DialogFrame';

// State

type State = {
  [dialog: string]: boolean,
};

const initialState = {};

// Actions

type DialogShowAction = {|
  type: 'DIALOG_SHOW',
  payload: $Keys<State>,
|};

type DialogHide = {|
  type: 'DIALOG_HIDE',
  payload: string,
|};

type Action = DialogShowAction | DialogHide;

// Reducer

const getNextZIndex = (state: State) => {
  if (Object.keys(state).length === 0) {
    return Z_INDEX_DEFAULT;
  }

  return _.max(_.values(state)) + 2;
};

export default function dialogstReducer(
  state: State = initialState,
  action: Action
) {
  switch (action.type) {
    case DIALOG_SHOW:
      if (Object.keys(state).includes(action.type)) {
        return state;
      }

      const zIndex = getNextZIndex(state);

      return Object.assign({}, state, { [action.payload]: zIndex });
    case DIALOG_HIDE:
      delete state[action.payload];

      return Object.assign({}, state);
    default:
      return state;
  }
}
