// @flow

import { combineReducers } from 'redux';

import dialogs from './dialogs';
import dailySummary from './dailySummary';

export default combineReducers<Object, Object>({
  dialogs,
  dailySummary,
});
