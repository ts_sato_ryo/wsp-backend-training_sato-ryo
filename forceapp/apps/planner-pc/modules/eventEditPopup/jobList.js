// @flow

import {
  FETCH_SUCCESS_ACTIVE_JOB_LIST_EVENT_EDIT_POPUP,
  ADD_JOB_LIST_EVENT_EDIT_POPUP,
  CLEAR_EVENT_EDIT_POPUP,
} from '../../actions/eventEditPopup';

import type { Job } from '../../../domain/models/time-tracking/Job';

// State

type State = {
  jobId: string,
  jobName: string,
  jobCode: string,
}[];

const initialState = [];

// Actions

type ClearEventEditPopupAction = {|
  type: 'CLEAR_EVENT_EDIT_POPUP',
|};

type AddJobListEventEditPopupAction = {|
  type: 'ADD_JOB_LIST_EVENT_EDIT_POPUP',
  payload: Job,
|};

type FetchSuccessActiveJobListEventEditPopupAction = {|
  type: 'FETCH_SUCCESS_ACTIVE_JOB_LIST_EVENT_EDIT_POPUP',
  payload: State,
|};

type Action =
  | ClearEventEditPopupAction
  | AddJobListEventEditPopupAction
  | FetchSuccessActiveJobListEventEditPopupAction;

// Reducer

export default function jobListReducer(
  state: State = initialState,
  action: Action
) {
  switch (action.type) {
    case CLEAR_EVENT_EDIT_POPUP:
      return initialState;
    case ADD_JOB_LIST_EVENT_EDIT_POPUP:
      return [
        ...state,
        {
          jobId: action.payload.id,
          jobName: action.payload.name,
          jobCode: action.payload.code,
        },
      ];
    case FETCH_SUCCESS_ACTIVE_JOB_LIST_EVENT_EDIT_POPUP:
      return action.payload;
    default:
      return state;
  }
}
