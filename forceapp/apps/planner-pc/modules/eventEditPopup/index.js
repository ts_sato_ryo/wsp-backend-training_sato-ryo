// @flow

import { combineReducers } from 'redux';
import event from './event';
import workCategoryList from './workCategoryList';
import jobList from './jobList';

const rootReducer = combineReducers<Object, Object>({
  event,
  workCategoryList,
  jobList,
});

export default rootReducer;
