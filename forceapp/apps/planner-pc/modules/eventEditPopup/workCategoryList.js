// @flow

import _ from 'lodash';

import {
  CLEAR_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP,
  FETCH_SUCCESS_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP,
  CLEAR_EVENT_EDIT_POPUP,
} from '../../actions/eventEditPopup';

import type { WorkCategory } from '../../../domain/models/time-tracking/WorkCategory';

// State

type State = WorkCategory[];

const initialState = [];

// Action

type ClearEventEditPopupAction = {|
  type: 'CLEAR_EVENT_EDIT_POPUP',
|};

type FetchSuccessWorkCategoryListEventEditPopupAction = {|
  type: 'FETCH_SUCCESS_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP',
  payload: WorkCategory[],
|};

type ClearWorkCategoryListEventEditPopup = {|
  type: 'CLEAR_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP',
|};

type Action =
  | ClearEventEditPopupAction
  | FetchSuccessWorkCategoryListEventEditPopupAction
  | ClearWorkCategoryListEventEditPopup;

// Reducer

export default function workCategoryListReducer(
  state: State = initialState,
  action: Action
) {
  switch (action.type) {
    case CLEAR_EVENT_EDIT_POPUP:
      return initialState;
    case FETCH_SUCCESS_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP:
      return _.cloneDeep(action.payload);
    case CLEAR_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP:
      return [];
    default:
      return state;
  }
}
