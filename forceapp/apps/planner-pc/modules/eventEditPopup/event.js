// @flow

import _ from 'lodash';

import {
  SELECT_DATA_EVENT_EDIT_POPUP,
  EDIT_EVENT_EDIT_POPUP,
  SELECT_JOB_EVENT_EDIT_POPUP,
  CLEAR_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP,
  CLEAR_EVENT_EDIT_POPUP,
} from '../../actions/eventEditPopup';
import eventTemplate from '../../constants/eventTemplate';
import type { CalendarEvent } from '../../models/calendar-event/CalendarEvent';

// State

type State = CalendarEvent;

// Action

type ClearEventEditPopupAction = {|
  type: 'CLEAR_EVENT_EDIT_POPUP',
|};

type SelectDataEventEditPopupAction = {|
  type: 'SELECT_DATA_EVENT_EDIT_POPUP',
  payload: CalendarEvent,
|};

type EditEventEditPopupAction = {|
  type: 'EDIT_EVENT_EDIT_POPUP',
  payload: {
    key: $Keys<State>,
    value: $Values<State>,
  },
|};

type SelectJobEventEditPopupAction = {|
  type: 'SELECT_JOB_EVENT_EDIT_POPUP',
  payload: {
    id: string,
    name: string,
    code: string,
  },
|};

type ClearWorkCategoryListEventEditPopupAction = {|
  type: 'CLEAR_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP',
|};

type Action =
  | ClearEventEditPopupAction
  | SelectDataEventEditPopupAction
  | EditEventEditPopupAction
  | SelectJobEventEditPopupAction
  | ClearWorkCategoryListEventEditPopupAction;

// Reducer

// FIXME momentがObject.assignでおかしくなるため、deepCloneしている
// 本来momentをstate管理すべきではない
export default function eventReducer(
  state: State = eventTemplate,
  action: Action
) {
  let cloneState;

  switch (action.type) {
    case CLEAR_EVENT_EDIT_POPUP:
      return eventTemplate;
    case SELECT_DATA_EVENT_EDIT_POPUP:
      return _.cloneDeep(action.payload);
    case EDIT_EVENT_EDIT_POPUP:
      cloneState = _.cloneDeep(state);
      cloneState[action.payload.key] = action.payload.value;

      return cloneState;
    case SELECT_JOB_EVENT_EDIT_POPUP:
      cloneState = _.cloneDeep(state);

      cloneState.job = {
        id: action.payload.id,
        name: action.payload.name,
        code: action.payload.code,
      };

      return cloneState;
    case CLEAR_WORK_CATEGORY_LIST_EVENT_EDIT_POPUP:
      cloneState = _.cloneDeep(state);
      cloneState.workCategoryId = '';
      cloneState.workCategoryName = '';

      return cloneState;
    default:
      return state;
  }
}
