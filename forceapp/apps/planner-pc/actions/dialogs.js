import {
  DIALOG_SHOW,
  DIALOG_HIDE,
} from '../constants/dialogs';

export function showDialog(dialog) {
  return {
    type: DIALOG_SHOW,
    payload: dialog,
  };
}

export function hideDialog(dialog) {
  return {
    type: DIALOG_HIDE,
    payload: dialog,
  };
}
