// @flow

import { type Dispatch } from 'redux';
import moment from 'moment';

import {
  catchApiError,
  catchBusinessError,
  loadingEnd,
  loadingStart,
} from '../../commons/actions/app';
import { convertEventsFromRemote } from '../models/calendar-event/CalendarEvent';
import Api from '../../commons/api';
import CalendarUtil from '../../commons/utils/CalendarUtil';
import msg from '../../commons/languages';
import type { BaseEvent } from '../../commons/models/DailySummary/BaseEvent';
import type { EventParam } from '../models/calendar-event/EventFromRemote';
import type { State } from '../modules';
import PlannerEventRepository from '../../repositories/PlannerEventRepository';

export const SELECT_DAY = 'SELECT_DAY';
export const SWITCH_CALENDAR_MODE = 'SWITCH_CALENDAR_MODE';
export const REQUEST_LOAD_EMP_EVENTS = 'REQUEST_LOAD_EMP_EVENTS';
export const RECEIVE_LOAD_EMP_EVENTS = 'RECEIVE_LOAD_EMP_EVENTS';
export const RECEIVE_SAVE_EMP_EVENT = 'RECEIVE_SAVE_EMP_EVENT';

type GetState = () => State;

export function selectDay(date: moment) {
  return {
    type: SELECT_DAY,
    date,
  };
}

export function switchCalendarMode(calendarMode: 'month' | 'week') {
  return {
    type: SWITCH_CALENDAR_MODE,
    calendarMode,
  };
}

/**
 * 予定一覧
 */
function requestEmpEventsPosts() {
  return {
    type: REQUEST_LOAD_EMP_EVENTS,
  };
}

function receiveEmpEventsPosts(json) {
  return {
    type: RECEIVE_LOAD_EMP_EVENTS,
    empEvents: json,
  };
}

const fetchEmpEvents = (yearMonth: moment, empId?: string) => async (
  dispatch: Dispatch<Object>,
  getState: GetState
): Promise<void> => {
  dispatch(requestEmpEventsPosts());

  const state = getState();
  const startDayOfTheWeek = state.common.empInfo.startDayOfTheWeek;
  const startDate = CalendarUtil.getStartDateOfMonthView(
    yearMonth,
    state.common.empInfo.startDayOfTheWeek
  );
  const endDate = startDate.clone().add(42, 'days'); // 6週間後

  dispatch(loadingStart());

  try {
    const { events, messages } = await PlannerEventRepository.fetch({
      startDate: startDate.format('YYYY-MM-DD'),
      endDate: endDate.format('YYYY-MM-DD'),
      empId,
    });

    // Even if the status is success, it may have failed partially.
    // e.g. couldn't import calendar events from other services
    messages.forEach((o) => {
      dispatch(
        catchBusinessError(
          msg().Cal_Lbl_CalendarEventSyncErrorTitle,
          o.message,
          null,
          {
            isContinuable: true,
          }
        )
      );
    });

    dispatch(
      receiveEmpEventsPosts(convertEventsFromRemote(events, startDayOfTheWeek))
    );
  } catch (err) {
    dispatch(catchApiError(err, { isContinuable: false }));
  } finally {
    dispatch(loadingEnd());
  }
};

export const loadEmpEvents = (yearMonth: moment) => (
  dispatch: Dispatch<Object>
) => dispatch(fetchEmpEvents(yearMonth));

/**
 * 予定保存
 */
function receiveSaveEmpEventPosts(result, event: EventParam) {
  return {
    type: RECEIVE_SAVE_EMP_EVENT,
    event,
  };
}
export const saveEmpEvent = (event: EventParam) => (
  dispatch: Dispatch<Object>,
  getState: GetState
) => {
  const state = getState();
  const selectedDay = state.selectedDay;
  const req = {
    path: '/planner/event/save',
    param: {
      id: event.id,
      title: event.title,
      startDateTime: event.startDate,
      endDateTime: event.endDate,
      jobId: (!event.isAllDay && event.job && event.job.id) || null,
      workCategoryId: (!event.isAllDay && event.workCategoryId) || null,
      isAllDay: event.isAllDay,
      isOuting: event.isOuting,
      location: event.location,
      description: event.remarks,
    },
  };

  dispatch(loadingStart());

  let isSuccess = false;

  return Api.invoke(req)
    .then((res) => {
      isSuccess = true;
      dispatch(receiveSaveEmpEventPosts(res, event));
      dispatch(loadEmpEvents(selectedDay));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()))
    .then(() => ({ isSuccess }));
};

/**
 * 予定削除
 */
export const deleteEmpEvent = (event: BaseEvent) => (
  dispatch: Dispatch<Object>,
  getState: GetState
) => {
  const state = getState();
  const selectedDay = state.selectedDay;
  const req = {
    path: '/planner/event/delete',
    param: {
      id: event.id,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then(() => dispatch(loadEmpEvents(selectedDay)))
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};
