// @flow

import { type Dispatch } from 'redux';
import _ from 'lodash';
import moment from 'moment';
import uuidV4 from 'uuid/v4';

import type { Job } from '../../../domain/models/time-tracking/Job';
import type { WorkCategory } from '../../../domain/models/time-tracking/WorkCategory';
import type { DailySummaryTask } from '../../../domain/models/time-management/DailySummaryTask';
import type { TaskFromRemote } from '../../../domain/models/time-management/DailySummary';
import type { BaseEvent } from '../../../commons/models/DailySummary/BaseEvent';
import type { State } from '../../modules';

import * as DailySummary from '../../../domain/models/time-management/DailySummary';
import * as DailyStampTime from '../../../domain/models/attendance/DailyStampTime';
import {
  DAILY_SUMMARY_SELECT_DAY,
  DAILY_SUMMARY_RECEIVE_EVENTS,
  DAILY_SUMMARY_CLEAR,
  DAILY_SUMMARY_OPEN_EVENTLIST,
  DAILY_SUMMARY_CLOSE_EVENTLIST,
  DAILY_SUMMARY_RECEIVE_JOB_LIST,
  DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST,
  DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS,
  DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW,
  DAILY_SUMMARY_RECEIVE_NOTE,
  DAILY_SUMMARY_EDIT_NOTE,
  DAILY_SUMMARY_RECEIVE_OUTPUT,
  DAILY_SUMMARY_EDIT_OUTPUT,
  DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT,
  DAILY_SUMMARY_CLEAR_DAILY_STAMP_TIME,
  DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS,
} from '../../constants/dailySummary';
import { dialogType } from '../../constants/dialogs';
import { fetchSummary } from '../../action-dispatchers/dailySummary';
import {
  loadingStart,
  catchApiError,
  loadingEnd,
  withLoading,
} from '../../../commons/actions/app';
import { showDialog, hideDialog } from '../dialogs';
import * as DailyStampTimeResultActions from '../../../commons/action-dispatchers/DailyStampTimeResult';
import Api from '../../../commons/api';
import ObjectUtil from '../../../commons/utils/ObjectUtil';
import TaskColorUtil from '../../../commons/utils/TaskColorUtil';
import { actions as TaskListActions } from '../../modules/ui/dailySummary/editing/taskList';
import { TIME_TRACK_UPDATED } from '../../../commons/constants/customEventName';
import dispatchEvent from '../../../commons/utils/EventUtil';

/**
 * デイリーサマリーのデータを全てクリア
 */
export function clear() {
  return {
    type: DAILY_SUMMARY_CLEAR,
  };
}

/**
 * デイリーサマリーを閉じてデータをリセットする
 */
export const hideDailySummaryDialog = () => (dispatch: Dispatch<Object>) => {
  dispatch(hideDialog(dialogType.DAILY_SUMMARY));
  dispatch(clear());
};

/**
 * 作業分類受信
 * @param {string} jobId
 * @param {array} workCategoryList
 */
export const receiveWorkCategoryList = (
  jobId: string,
  workCategoryList: WorkCategory[]
) => {
  return {
    type: DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST,
    payload: {
      jobId,
      workCategoryList,
    },
  };
};

/**
 * まとめて作業分類を追加する
 * 以下形式のobjectを受け取る
 * { jobId: workCategoryList, jobId: workCategoryList... }
 * @param {object} workCategoryLists
 */
export const receiveWorkCategoryLists = (workCategoryLists: {
  [id: string]: WorkCategory[],
}) => {
  return {
    type: DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS,
    payload: workCategoryLists,
  };
};

/**
 * 作業分類を取得する
 * ただし、取得対象が取得済みであった場合は何もしない
 */
export const fetchWorkCategoryList = (jobId: string) => (
  dispatch: Dispatch<Object>,
  getState: () => State
) => {
  const state = getState();
  const { workCategoryLists, selectedDay } = state.ui.dailySummary;

  // すでにリストを持っていたらキャンセル
  if (!_.isEmpty(workCategoryLists[jobId])) {
    return null;
  }

  const dateStr = selectedDay.format('YYYY-MM-DD');
  const req = {
    path: '/time/work-category/get',
    param: {
      jobId,
      targetDate: dateStr,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then((res) =>
      dispatch(receiveWorkCategoryList(jobId, res.workCategoryList))
    )
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

export function receiveNote(note: string) {
  return {
    type: DAILY_SUMMARY_RECEIVE_NOTE,
    payload: note,
  };
}

export function receiveOutput(output: string) {
  return {
    type: DAILY_SUMMARY_RECEIVE_OUTPUT,
    payload: output,
  };
}

/**
 * 予定データをStoreへセットする
 * @param {array} Summary - 予定データ
 */
export function receiveEvents<T: BaseEvent>(events: T[]) {
  return {
    type: DAILY_SUMMARY_RECEIVE_EVENTS,
    payload: events,
  };
}

const colorizeTask = (colorIndex: number, task: DailySummaryTask) => ({
  ...task,
  color: TaskColorUtil.getNextColor(colorIndex),
});

const colorizeTasks = (
  startColorIndex: number,
  nonColorizedTasks: DailySummaryTask[]
) =>
  nonColorizedTasks.map((task, i) => colorizeTask(startColorIndex + i, task));

/**
 * タスクデータをStoreへセットする
 * 同時に色の付与も行う
 * @param {array} Summary - 予定データ
 * @param {?Object} defaultJob Default Job. May be null.
 */
export const receiveTaskList = (
  tasksFromRemote: TaskFromRemote[],
  defaultJob: ?Job,
  realWorkTime: ?number,
  isLocked: boolean
) => (dispatch: Dispatch<Object>) => {
  let taskList = tasksFromRemote.map(DailySummary.convertTaskFromRemote);

  const areTasksLocked = isLocked;
  if (!areTasksLocked && defaultJob) {
    // If Default Job is set, and Time Tracking Summary on the date is not locked,
    // append a task of Default Job.
    taskList = DailySummary.ensureTaskOfDefaultJobExists(
      realWorkTime,
      defaultJob,
      taskList
    );
  }

  // Complete each task-hours of event-related tasks
  taskList = DailySummary.completeTaskTimeOfEventRelatedTasks(taskList);

  // Set a color for each bar of tasks
  taskList = colorizeTasks(0, taskList);

  dispatch(TaskListActions.fetchSuccess(taskList, realWorkTime));
};

/**
 * サマリーデータから作業分類のデータを抽出しオブジェクト形式で返す
 * [{ id: workCategoryList }, { id: workCategoryList }, ...]
 * @param {object} summary
 */
export const convertWorkCategoryListsFromSummary = (
  summary: DailySummary.ContentsFromRemote
): { [id: string]: WorkCategory[] } => {
  if (_.isEmpty(summary) || _.isEmpty(summary.taskList)) {
    return {};
  }

  let workCategoryLists = summary.taskList.map((task) => {
    return {
      jobId: task.jobId,
      list: task.workCategoryList,
    };
  });

  workCategoryLists = _.uniqBy(workCategoryLists, 'jobId');

  const workCategoryMap = {};
  workCategoryLists.forEach((workCategoryList) => {
    workCategoryMap[workCategoryList.jobId] = workCategoryList.list;
  });

  // 有効期間外のため現在は使用できない作業分類も選択肢に加える
  summary.taskList.forEach((task) => {
    const workCategoryList = workCategoryMap[task.jobId];
    if (Array.isArray(workCategoryList) && task.workCategoryId !== null) {
      const notExistsInList = workCategoryList.every((workCategory) => {
        return workCategory.id !== task.workCategoryId;
      });
      if (notExistsInList) {
        workCategoryList.push({
          id: task.workCategoryId,
          code: task.workCategoryCode,
          name: task.workCategoryName,
        });
      }
    }
  });

  return workCategoryMap;
};

export const receiveTaskOverview = (
  summary: DailySummary.ContentsFromRemote
) => {
  return {
    type: DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW,
    payload: summary,
  };
};

/**
 *  サーバーから取得したデータからサマリーデータを抽出する
 *  @param {object} res サーバーから取得したレスポンス
 *  @return {object}
 */
export const convertSummaryData = (res: DailySummary.ContentsFromRemote) => {
  return res;
};

/**
 *  サーバーから取得したサマリーデータから作業分類を抽出する
 *  @param {object} summaryData サマリーデータ
 *  @return {string}
 */
export const convertNote = (summaryData: DailySummary.ContentsFromRemote) => {
  return ObjectUtil.getOrDefault(summaryData, 'note', '');
};

/**
 *  サーバーから取得したサマリーデータからアウトプットを抽出する
 *  @param {object} summaryData サマリーデータ
 *  @return {string}
 */
export const convertOutput = (summaryData: DailySummary.ContentsFromRemote) => {
  return ObjectUtil.getOrDefault(summaryData, 'output', '');
};

/**
 * Dispatch the action to clear the Daily Stamp Time in the state.
 */
export const clearDailyStampTime = () => ({
  type: DAILY_SUMMARY_CLEAR_DAILY_STAMP_TIME,
});

/**
 * Dispatch the action to update the Daily Stamp Time.
 * @param {Object} result The response body from server
 */
const fetchDailyStampTimeSuccess = (result) => ({
  type: DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS,
  payload: result,
});

/**
 * Fetch the Daily Stamp Time and update the state.
 * @returns {Promise<void>}
 */
export const fetchDailyStampTime = () => (dispatch: Dispatch<Object>) => {
  dispatch(loadingStart());

  const req = {
    path: '/att/daily-time/get',
    param: {},
  };

  return Api.invoke(req)
    .then((result) => {
      dispatch(fetchDailyStampTimeSuccess(result));
    })
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
    })
    .then(() => {
      dispatch(loadingEnd());
    });
};

/**
 * 日付を選択してダイアログを展開する
 * @param {moment} momentObj
 */
export function selectDay(momentObj: moment) {
  const m = momentObj.clone();

  return (dispatch: Dispatch<Object>, getState: () => State) => {
    dispatch(showDialog(dialogType.DAILY_SUMMARY));

    const defaultJob = getState().common.personalSetting.defaultJob;
    fetchSummary(
      moment().format('YYYY-MM-DD'),
      defaultJob,
      m.format('YYYY-MM-DD')
    )(dispatch);

    dispatch({
      type: DAILY_SUMMARY_SELECT_DAY,
      payload: m,
    });
  };
}

/**
 * ダイアログ展開後に日付を移動する
 * @param {moment} momentObj
 */
export function moveDay(momentObj: moment) {
  const m = momentObj.clone();

  return (dispatch: Dispatch<Object>, getState: () => State) => {
    dispatch(clear());

    const defaultJob = getState().common.personalSetting.defaultJob;
    fetchSummary(
      moment().format('YYYY-MM-DD'),
      defaultJob,
      m.format('YYYY-MM-DD')
    )(dispatch);

    dispatch({
      type: DAILY_SUMMARY_SELECT_DAY,
      payload: m,
    });
  };
}

/**
 * 終日予定が省略された場合に予定リストポップアップを表示する
 * @param {Event} e イベント
 * @param {moment} day 日付
 * @param {array} events 予定
 */
export function openEventListPopup(
  e: SyntheticMouseEvent<HTMLElement>,
  day: moment,
  events: BaseEvent[]
) {
  return {
    type: DAILY_SUMMARY_OPEN_EVENTLIST,
    payload: {
      day,
      events,
      style: {
        left: e.pageX,
        top: e.pageY,
      },
    },
  };
}

/**
 * EventListPopupを閉じる
 */
export function closeEventListPopup() {
  return {
    type: DAILY_SUMMARY_CLOSE_EVENTLIST,
  };
}

/**
 * ジョブリスト受信
 * @param {array} payload
 */
export function receiveJobList(payload: Job[][]) {
  return {
    type: DAILY_SUMMARY_RECEIVE_JOB_LIST,
    payload,
  };
}

/**
 * ジョブリスト取得
 * @param {string} targetDate YYYY-MM-DD
 * @param {string} parentItem JOB ID
 * @param {array} existingItems
 */
export const fetchJobList = (
  targetDate: string,
  parentItem: null | Job = null,
  existingItems: Job[][] = []
) => (dispatch: Dispatch<Object>) => {
  const parentId = parentItem ? parentItem.id : null;
  const req = {
    path: '/time/job/get',
    param: {
      targetDate,
      parentId,
    },
  };

  dispatch(loadingStart());

  return Api.invoke(req)
    .then((res) => {
      const { jobList } = res;
      const newItems = [...existingItems];
      const resultItems = Object.assign([], jobList);
      newItems.push(resultItems);
      dispatch(receiveJobList(newItems));
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};

/**
 * タスク追加
 * @param {Object} job
 */
export const addTask = (job: Job, taskList: DailySummaryTask[]) => (
  dispatch: Dispatch<Object>
) => {
  dispatch(hideDialog(dialogType.JOB_SELECT));
  // jobIdではないため注意
  dispatch(fetchWorkCategoryList(job.id));

  const taskToAdd = colorizeTask(taskList.length, {
    id: uuidV4(),
    jobId: job.id,
    jobCode: job.code,
    jobName: job.name,
    isDirectCharged: job.isDirectCharged,
    workCategoryId: null,
    workCategoryName: null,
    workCategoryCode: null,
    color: { base: '', linked: '' },
    isDirectInput: true,
    volume: null,
    ratio: null,
    taskTime: 0,
    eventTaskTime: 0,
    // will be updated
    workCategoryList: [],
  });

  dispatch(TaskListActions.add(taskToAdd));
};

/**
 * ジョブの子階層を取得、もしくは子階層がない場合は選択
 * @param {Object} selectedItem
 * @param {array} existingItems
 */
export function fetchJobListChildren(
  selectedItem: Job,
  existingItems: ?(Job[][]),
  selectedDay: moment,
  taskList: DailySummaryTask[]
) {
  return (dispatch: Dispatch<Object>) => {
    const dateMoment = selectedDay;

    if (selectedItem.hasChildren && Array.isArray(existingItems)) {
      // 子階層を描画
      dispatch(
        fetchJobList(
          dateMoment.format('YYYY-MM-DD'),
          selectedItem,
          existingItems
        )
      );
    } else {
      // FIXME selectableが実装されたら有効化
      // } else if (selectedItem.selectabledTimeTrack) {
      dispatch(addTask(selectedItem, taskList));
    }
  };
}

/**
 * ジョブ選択ダイアログ展開
 */
export function showJobSelectDialog() {
  return (dispatch: Dispatch<Object>, getState: () => State) => {
    const state = getState();
    const dateMoment = state.ui.dailySummary.selectedDay;

    dispatch(fetchJobList(dateMoment.format('YYYY-MM-DD')));
    dispatch(showDialog(dialogType.JOB_SELECT));
  };
}

/**
 * ジョブ選択ダイアログ展開
 */
export function closeJobSelectDialog() {
  return hideDialog(dialogType.JOB_SELECT);
}

/**
 * サマリーデータ保存
 * @param {String} plannerDefaultView The default view in Planner; 'Daily' or 'Weekly' or 'Monthly'
 */
export function saveDailySummary(
  plannerDefaultView: 'Daily' | 'Weekly' | 'Monthly'
) {
  return (dispatch: Dispatch<Object>, getState: () => State) => {
    dispatch(loadingStart());

    const dailySummary = getState().ui.dailySummary;
    return DailySummary.postDailySummary({
      editing: {
        ...dailySummary.editing,
        taskList: dailySummary.editing.taskList.taskList,
      },
      selectedDay: dailySummary.selectedDay,
    })
      .then(() => {
        // Don't close the dialog if the default view is 'Daily'
        if (plannerDefaultView !== 'Daily') {
          dispatch(hideDailySummaryDialog());
        }
        dispatchEvent(TIME_TRACK_UPDATED);
        return true;
      })
      .catch((err) => {
        dispatch(catchApiError(err, { isContinuable: true }));
        return false;
      })
      .then((isSuccess) => {
        dispatch(loadingEnd());
        return isSuccess;
      });
  };
}

/**
 * Update the Timestmap Comment in state.
 * @param {String} newTimestampComment Timestamp Comment after change
 */
export const editTimestampComment = (newTimestampComment: string) => ({
  type: DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT,
  payload: newTimestampComment,
});

/**
 * Save the Daily Summary and record an end timestamp.
 *
 * 1. Save the Daily Summary
 * 2. If succeeded, record an end timestamp
 * 3. If succeeded, close the dialog unless the default view is 'Daily'
 * 4. If the dialog remains (failed or 'Daily' is the default view),
 *    refresh the Daily Summary to refresh the end timestamp and real work hours of the day.
 *
 * @param {String} comment Timestamp Comment
 * @param {String} plannerDefaultView The default view in Planner; 'Daily' or 'Weekly' or 'Monthly'
 * @param {Moment} targetDate A moment object of the target date
 * @returns {Promise<void>}
 */
export const saveDailySummaryAndRecordEndTimestamp = (
  comment: string,
  plannerDefaultView: 'Daily' | 'Weekly' | 'Monthly',
  targetDate: moment
) => (dispatch: Dispatch<Object>, getState: () => State) => {
  /* assert(moment(targetDate).isSame(moment(), 'day')); */
  const dailySummary = getState().ui.dailySummary;
  return dispatch(
    withLoading(
      () =>
        DailySummary.postDailySummary({
          editing: {
            ...dailySummary.editing,
            taskList: dailySummary.editing.taskList.taskList,
          },
          selectedDay: dailySummary.selectedDay,
        }),
      () =>
        DailyStampTime.postStamp(
          {
            mode: DailyStampTime.CLOCK_TYPE.CLOCK_OUT,
            message: comment,
          },
          DailyStampTime.STAMP_SOURCE.WEB
        )
    )
  )
    .then((result) =>
      dispatch(DailyStampTimeResultActions.doAddProcess(result))
    )
    .then(() =>
      dispatch(
        withLoading(() => {
          // Don't close the dialog if the default view is 'Daily'
          if (plannerDefaultView !== 'Daily') {
            return dispatch(hideDailySummaryDialog());
          } else {
            // If the default view is 'Daily',
            // it keeps the dialog after Daily Summary save.
            // So refresh the real work hours here
            // and hide 'Save and Record End Timestamp' button here.
            return dispatch(selectDay(targetDate));
          }
        })
      )
    )
    .then(() => true)
    .catch((err) => {
      dispatch(catchApiError(err, { isContinuable: true }));
      return false;
    })
    .finally(() => {
      dispatchEvent(TIME_TRACK_UPDATED);
    });
};

/**
 * サマリーデータ編集
 * @param {number} index
 * @param {string} prop
 * @param {string} value
 */
export const editTask = TaskListActions.edit;

/**
 * 工数時間を変更する。
 * @param {number} index 対象タスクのindex
 * @param {string} prop taskTimeかratioかを指定する
 * @param {number} value 変更する時間
 */
export const editTaskTime = (
  index: number,
  prop: 'taskTime' | 'ratio' | 'volume',
  value: number
) => (dispatch: Dispatch<Object>) => {
  dispatch(editTask(index, prop, value));
};

/**
 * タスクを削除する
 * @param {number} index
 */
export const deleteTask = TaskListActions.delete;

/**
 * 作業報告を編集する
 * @param {string} text
 */
export const editNote = (text: string) => {
  return {
    type: DAILY_SUMMARY_EDIT_NOTE,
    payload: text,
  };
};

/**
 * アウトプットを編集する
 * @param {string} text
 */
export const editOutput = (text: string) => {
  return {
    type: DAILY_SUMMARY_EDIT_OUTPUT,
    payload: text,
  };
};

/**
 * タスクの入力モードを切り替える
 * @param {number} index
 */
export const toggleDirectInput = (
  index: number,
  taskList: DailySummaryTask[]
) => (dispatch: Dispatch<Object>) => {
  const toggleTask = taskList[index];
  toggleTask.isDirectInput = !toggleTask.isDirectInput;

  if (toggleTask.isDirectInput) {
    toggleTask.taskTime = toggleTask.volume;
    toggleTask.volume = null;
    toggleTask.ratio = null;
  } else {
    toggleTask.volume = toggleTask.taskTime;
    toggleTask.taskTime = null;
  }

  dispatch(TaskListActions.toggleDirectInput(index, toggleTask));
};

/**
 * タスクを並び変える
 * @param {Array} reorderedTaskList
 */
export const reorderTaskList = TaskListActions.reorder;
