// @flow

import path from 'path';
import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';

initStoryshots({
  suite: 'planner-pc: Image storyshots',
  configPath: path.resolve(__dirname, 'config.js'),
  test: imageSnapshot({
    storybookUrl: `file://${path.resolve('./storybook-static')}`,
  }),
});
