import { configure } from '@storybook/react';

function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

configure(() => {
  requireAll(require.context('..', true, /.story.js$/));
}, module);
