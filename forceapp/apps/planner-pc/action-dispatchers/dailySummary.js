/* @flow */
import { type Dispatch } from 'redux';
import isNil from 'lodash/isNil';
import moment from 'moment';

import {
  catchApiError,
  catchBusinessError,
  loadingEnd,
  loadingStart,
} from '../../commons/actions/app';
import {
  clearDailyStampTime,
  convertNote,
  convertOutput,
  convertSummaryData,
  convertWorkCategoryListsFromSummary,
  fetchDailyStampTime,
  receiveEvents,
  receiveNote,
  receiveOutput,
  receiveTaskList,
  receiveTaskOverview,
  receiveWorkCategoryLists,
} from '../actions/dailySummary';
import { toViewModel } from '../../commons/models/DailySummary/Converter';
import Api from '../../commons/api';
import msg from '../../commons/languages';
import type { PersonalSetting } from '../../commons/modules/personalSetting';
import PlannerEventRepository from '../../repositories/PlannerEventRepository';
import type { Job } from '../../domain/models/time-tracking/Job';
import type { Event } from '../../domain/models/time-management/Event';
import type { EventMessage } from '../../domain/models/time-management/EventMessage';
import {
  convertFromRemote,
  isLocked,
} from '../../domain/models/time-management/DailySummary';

/**
 * Check if the Default Job is available on the specified date.
 *
 * criteria:
 * - The ID is not null
 * - The "Valid From" is null or before today (inclusive)
 * - The "Valid To" is null or after today (exclusive)
 *
 * @param {?string} todayDate The date represents today. Must be UTC time and formatted as ISO8601.
 * @param {Object} defaultJob The Default Job object in the Personal Setting state
 */
const isDefaultJobAvailable = (
  targetDate: string,
  defaultJob: $PropertyType<PersonalSetting, 'defaultJob'>
): boolean =>
  defaultJob !== null &&
  defaultJob !== undefined &&
  (defaultJob.validDateFrom === null ||
    defaultJob.validDateFrom === undefined ||
    (moment(defaultJob.validDateFrom).isSameOrBefore(targetDate, 'day') &&
      (defaultJob.validDateTo === null ||
        defaultJob.validDateTo === undefined ||
        moment(defaultJob.validDateTo).isAfter(targetDate, 'day'))));

/**
 * Fetch the Daily Summary on the specified date.
 *
 * 1. Show the loading spinner
 * 2. Fetch the Planner Events on the date
 * 3. Fetch the Daily Summary contents on the date
 * 4. Fetch the Default Job details if it's set and available on the date
 * 5. Fetch the Daily Stamp Time(s) if the date is today
 *
 * @param {string} todayDate Today.(YYYY-MM-DD)
 * @param {?Object} defaultJob Default Job object in the Personal Setting state
 * @param {string} targetDate Date. (YYYY-MM-DD)
 */
// eslint-disable-next-line import/prefer-default-export
export const fetchSummary = (
  todayDate: string,
  defaultJob: $PropertyType<PersonalSetting, 'defaultJob'>,
  targetDate: string
) => (dispatch: Dispatch<Object>) => {
  const promises = [
    PlannerEventRepository.fetch({
      startDate: targetDate,
      endDate: targetDate,
    }),
    Api.invoke({
      path: '/daily-summary/get',
      param: {
        targetDate,
      },
    }),
  ];

  // Check if the Default Job is available on the date
  if (isDefaultJobAvailable(targetDate, defaultJob)) {
    // Unwrap optional type
    if (!isNil(defaultJob) && !isNil(defaultJob.id)) {
      promises.push(
        Api.invoke({
          path: '/job/search',
          param: {
            id: defaultJob.id,
          },
        })
      );
    }
  }

  const isToday = moment(todayDate).isSame(targetDate, 'day');
  dispatch((isToday ? fetchDailyStampTime : clearDailyStampTime)());

  dispatch(loadingStart());

  Promise.all(promises)
    .then((res: any) => {
      const [
        plannerEventsResponse,
        dailySummaryResponse,
        defaultJobDetailsResponse,
      ]: [
        {| events: Event[], messages: EventMessage[] |},
        Object,
        { records: Job[] },
      ] = res;

      // Planner Events
      const receiveEventData = toViewModel(
        plannerEventsResponse.events,
        moment(targetDate).startOf('day')
      );

      // Even if the status is success, it may have failed partially.
      // e.g. couldn't import calendar events from other services
      if (plannerEventsResponse && plannerEventsResponse.messages) {
        plannerEventsResponse.messages.forEach((o) => {
          dispatch(
            catchBusinessError(
              msg().Cal_Lbl_CalendarEventSyncErrorTitle,
              o.message,
              null,
              {
                isContinuable: true,
              }
            )
          );
        });
      }

      dispatch(receiveEvents(receiveEventData));

      // Daily Summary
      const summaryData = convertSummaryData(dailySummaryResponse);
      const note = convertNote(summaryData);
      const output = convertOutput(summaryData);
      const { taskList } = summaryData;
      const workCategoryLists = convertWorkCategoryListsFromSummary(
        summaryData
      );
      dispatch(receiveNote(note)); // Notes
      dispatch(receiveOutput(output)); // Outputs
      dispatch(receiveTaskOverview(summaryData)); // Summary

      // Tasks and Default Job details
      let defaultJobWithDetails = null;
      if (
        defaultJobDetailsResponse &&
        defaultJobDetailsResponse.records &&
        defaultJobDetailsResponse.records.length > 0
      ) {
        defaultJobWithDetails = defaultJobDetailsResponse.records[0];
      }

      dispatch(
        receiveTaskList(
          taskList,
          defaultJobWithDetails,
          summaryData.realWorkTime,
          isLocked(convertFromRemote(summaryData))
        )
      ); // Tasks
      dispatch(receiveWorkCategoryLists(workCategoryLists)); // Work Categories
    })
    .catch((err) => dispatch(catchApiError(err, { isContinuable: true })))
    .then(() => dispatch(loadingEnd()));
};
