// @flow

import type { Dispatch } from 'redux';
import first from 'lodash/first';
import last from 'lodash/last';

import PlannerEventRepository from '../../repositories/PlannerEventRepository';
import { actions } from '../modules/entities/events';
import CalendarUtil from '../../commons/utils/CalendarUtil';

export default (dispatch: Dispatch<Object>) => ({
  loadEvents: async (targetDate: Date, empId?: string) => {
    const dates = CalendarUtil.getCalendarAsOf(targetDate);
    const startDate = first(dates);
    const endDate = last(dates);

    const { events } = await PlannerEventRepository.fetch({
      startDate: startDate.toISOString(),
      endDate: endDate.toISOString(),
      empId,
    });
    dispatch(actions.fetchSuccess(events, startDate, endDate));
  },
});
