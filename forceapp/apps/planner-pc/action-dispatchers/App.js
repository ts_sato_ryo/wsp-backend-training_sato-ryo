// @flow

import { type Dispatch } from 'redux';
import moment from 'moment';
import { fetch as fetchPersonalSetting } from '../../commons/modules/personalSetting';
import { selectDay } from '../actions/dailySummary';
import { getUserSetting } from '../../commons/actions/userSetting';
import { loadEmpEvents } from '../actions/events';
import Events from './Events';

export default (dispatch: Dispatch<Object>) => {
  const events = Events(dispatch);
  return {
    /**
     * Application initializes its first view.
     */
    initialize: async (): Promise<void> => {
      // TODO
      // Handle access permissions

      const today = moment();

      dispatch(getUserSetting());
      const setting = await dispatch(fetchPersonalSetting());
      const plannerDefaultView = setting.plannerDefaultView;
      if (plannerDefaultView === 'Daily') {
        dispatch(selectDay(today));
      }

      // TODO
      // Remove loadEmpEvents
      // loadEmpEvents is not recommended and planned to be removed by epic/GENIE-11663
      await dispatch(loadEmpEvents(today.startOf('day')));
      await events.loadEvents(new Date());
    },
  };
};
