import '../commons/config/public-path';

/* eslint-disable import/imports-first */
import * as React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
/* eslint-enable */

import configureStore from './store/configureStore';

import PlannerContainer from './containers/PlannerContainer';
import App from './action-dispatchers/App';

import '../commons/styles/base.scss';
import '../commons/config/moment';

function renderApp(store, Component) {
  ReactDOM.render(
    <Provider store={store}>
      <Component />
    </Provider>,
    document.getElementById('container')
  );
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = () => {
  const configuredStore = configureStore();
  renderApp(configuredStore, PlannerContainer);
  App(configuredStore.dispatch).initialize();
};
