import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import moment from 'moment';

import DateTimeField from '../components/DateTimeField';

storiesOf('planner-pc/DateTimeField', module)
  .add(
    'Enable isAllDay',
    () => (
      <DateTimeField
        value={moment('2019-08-31T15:00:00Z')}
        onChange={action('onChange')}
        isAllDay
      />
    ),
    { info: { propTables: [DateTimeField], inline: true, source: true } }
  )
  .add(
    'Disable isAllDay',
    () => (
      <DateTimeField
        value={moment('2019-08-31T15:00:00Z')}
        onChange={action('onChange')}
        isAllDay={false}
      />
    ),
    { info: { propTables: [DateTimeField], inline: true, source: true } }
  );
