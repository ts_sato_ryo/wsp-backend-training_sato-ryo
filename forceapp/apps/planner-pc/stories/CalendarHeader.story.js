// @flow
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import CalendarHeader from '../components/CalendarHeader';
import Button from '../../core/elements/Button';

storiesOf('planner-pc', module).add('CalendarHeader', () => (
  <CalendarHeader
    dateList={[
      { value: '2018/05' },
      { value: '2018/04' },
      { value: '2018/03' },
    ]}
    date="2018/05"
    calendarMode="week"
    onClickToday={action('onClickToday')}
    onClickNext={action('onClickNext')}
    onClickPrevious={action('onClickPrevious')}
    onSelectDate={action('onSelectDate')}
    onSelectCalendarMode={action('onSelectCalendarMode')}
    renderDailySummaryButton={(props) => <Button {...props} />}
  />
));
