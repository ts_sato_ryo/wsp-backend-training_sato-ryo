/* @flow */

import React from 'react';
import moment from 'moment';

import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import {
  CALENDAR_CREATED_SERVICE_BY_OFFICE365,
  CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT,
} from '../../../constants/event';
import CalendarMonthlyDayEvents from '../../../components/MonthlyView/CalendarMonthlyWeek/CalendarMonthlyDayEvents';
import '../../../components/MonthlyView/index.scss';
import CalendarUtil from '../../../../commons/utils/CalendarUtil';

const dummyAllDayEvent = {
  id: 'a0Q7F000008HfBaUAK',
  title: 'All Day Event',
  start: moment('2018-06-04'),
  end: moment('2018-06-05'),
  isAllDay: true,
  isOrganizer: false,
  location: '',
  remarks: '',
  isOuting: false,
  createdServiceBy: CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT,
  job: {
    id: '',
    code: '',
    name: '',
  },
  workCategoryId: '',
  workCategoryName: '',
  layout: {
    containsAllDay: true,
    startMinutesOfDay: 0,
    endMinutesOfDay: 0,
    colIndex: 0,
    colSpan: 1,
    visibleInMonthlyView: true,
  },
  externalEventId: '',
};

const dummyAllDayEventFromO365 = {
  id: '',
  title: 'All Day Event from O365',
  start: moment('2018-06-04'),
  end: moment('2018-06-05'),
  isAllDay: true,
  isOrganizer: false,
  location: '',
  remarks: '',
  isOuting: false,
  createdServiceBy: CALENDAR_CREATED_SERVICE_BY_OFFICE365,
  job: {
    id: '',
    code: '',
    name: '',
  },
  workCategoryId: '',
  workCategoryName: '',
  layout: {
    containsAllDay: true,
    startMinutesOfDay: 0,
    endMinutesOfDay: 0,
    colIndex: 0,
    colSpan: 1,
    visibleInMonthlyView: true,
  },
  externalEventId: '',
};

const dummyEvent = {
  id: 'a0Q7F000008HfBQUA0',
  title: 'Specification MTG',
  start: moment([2018, 6, 4, 14]),
  end: moment([2018, 6, 4, 16]),
  isAllDay: false,
  isOrganizer: false,
  location: 'MTG Table 3',
  remarks: '',
  isOuting: false,
  createdServiceBy: CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT,
  job: {
    id: '',
    code: '',
    name: '',
  },
  workCategoryId: '',
  workCategoryName: '',
  left: 0,
  width: 150,
  layout: {
    containsAllDay: false,
    startMinutesOfDay: ((60 * 14 + 30) * 2) / 3,
    endMinutesOfDay: ((60 * 16 + 30) * 2) / 3,
    colIndex: 0,
    colSpan: 1,
    visibleInMonthlyView: true,
  },
  externalEventId: '',
};

const dummyEventFromO365 = {
  id: 'a0Q7F000008HfBQUA0',
  title: 'Morning Assembly',
  start: moment([2018, 6, 4, 10]),
  end: moment([2018, 6, 4, 12]),
  isAllDay: false,
  isOrganizer: false,
  location: 'Seminar Room',
  remarks: '',
  isOuting: false,
  createdServiceBy: CALENDAR_CREATED_SERVICE_BY_OFFICE365,
  job: {
    id: '',
    code: '',
    name: '',
  },
  workCategoryId: '',
  workCategoryName: '',
  left: 0,
  width: 150,
  layout: {
    containsAllDay: false,
    startMinutesOfDay: ((60 * 10 + 30) * 2) / 3,
    endMinutesOfDay: ((60 * 12 + 30) * 2) / 3,
    colIndex: 0,
    colSpan: 1,
    visibleInMonthlyView: true,
  },
  externalEventId: '',
};

let date = CalendarUtil.getStartDateOfMonthView(moment('2018-06-20'), 1);
const days = [];
const lastDate = date.clone().add(42, 'days');
while (!date.isSame(lastDate, 'day')) {
  const isToday = date.isSame(new Date(), 'day');
  days.push({
    name: date.format('dd').substring(0, 1),
    date,
    number: date.date(),
    month: date.month() + 1,
    isCurrentMonth: true,
    isToday,
  });
  date = date.clone();
  date.add(1, 'd');
}

storiesOf('planner-pc/MonthlyView/CalendarMonthlyWeek', module).add(
  'CalendarMonthlyDayEvents',
  () => (
    <div style={{ position: 'relative', width: '100%', height: '192px' }}>
      <CalendarMonthlyDayEvents
        openEventEdit={action('openEventEdit')}
        openMenu={action('openMenu')}
        openEventListPopup={action('openEventListPopup')}
        renderEvents={[
          [dummyAllDayEvent],
          [dummyAllDayEventFromO365],
          [dummyEventFromO365],
          [dummyEvent],
        ]}
        days={days}
        index={1}
      />
    </div>
  )
);
