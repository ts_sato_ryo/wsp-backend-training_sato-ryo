/* @flow */
import type { Job } from '../../../domain/models/time-tracking/Job';

export type EventParam = {
  id: string,
  title: string,
  startDate: string,
  endDate: string,
  job: ?Job,
  workCategoryId: string | null,
  isAllDay: boolean,
  isOuting: boolean,
  location: string,
  description: string,
  remarks: string,
};
