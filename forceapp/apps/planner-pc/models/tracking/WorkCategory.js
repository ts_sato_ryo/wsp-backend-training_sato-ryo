/* @flow */
export type WorkCategory = {
  id: string,
  name: string,
};
