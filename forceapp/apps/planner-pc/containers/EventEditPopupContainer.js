// @flow

import * as React from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';

import {
  editEventEditPopup,
  refreshActiveJobList,
  selectJobEventEditPopup as selectJobEventEditPopupAction,
} from '../actions/eventEditPopup';
import EventEditPopup from '../components/EventEditPopup';
import JobSelect from '../components/EventEditPopup/JobSelect';
import WorkCategorySelect from '../components/EventEditPopup/WorkCategorySelect';

import type { State } from '../modules';
import DateUtil from '../../commons/utils/DateUtil';

const { useCallback, useEffect } = React;

type OwnProps = $ReadOnly<{
  eventStyle: Object,
  closeEventEdit: () => void,
  onClickJobFinderButton: () => void,
}>;

const mapStateToProps = (state: State) => {
  return {
    empInfo: state.common.empInfo,
    workCategoryList: state.eventEditPopup.workCategoryList,
    event: state.eventEditPopup.event,
    jobList: state.eventEditPopup.jobList,
    useWorkTime: state.userSetting.useWorkTime,
  };
};

export default function EventEditPopupContainer(props: OwnProps) {
  const { useWorkTime, ...stateProps } = useSelector(
    mapStateToProps,
    shallowEqual
  );
  const dispatch = useDispatch();
  const selectJobEventEditPopup = useCallback(
    (id) => dispatch(selectJobEventEditPopupAction(id)),
    [dispatch]
  );
  const onChange = useCallback(
    (key, e) => dispatch(editEventEditPopup(key, (e.target: any).value)),
    [dispatch]
  );

  const input =
    stateProps.event.start === null
      ? ''
      : DateUtil.formatISO8601Date(stateProps.event.start.valueOf());
  useEffect(() => {
    if (stateProps.event.start === null) {
      return;
    }

    // 予定の開始日時を変更したら、ジョブリストを取得し直す
    // FIXME: 開始日変更以外のタイミングでも実行されるため、場所を移したい

    // NOTE:
    // 保存されていない予定のnextProps.event.jobはcodeとnameを持たないため、
    // ジョブを指定した後で開始日を変更すると、プルダウン要素を正しく構築できない
    // これを回避するため、this.props.jobListの内容で不足を補填する
    const targetEvent = stateProps.event;
    const detailedEventJob = stateProps.jobList.find(
      (job) => job.jobId === targetEvent.job.id
    );
    const supplementedEvent: Object = {
      ...targetEvent,
      job: {
        id: targetEvent.job.id,
        code: detailedEventJob ? detailedEventJob.jobCode : '',
        name: detailedEventJob ? detailedEventJob.jobName : '',
      },
    };

    dispatch(refreshActiveJobList(supplementedEvent));
  }, [dispatch, input]);

  return (
    <EventEditPopup
      {...stateProps}
      {...props}
      dispatch={dispatch}
      renderJobSelect={() =>
        useWorkTime && (
          <JobSelect
            value={stateProps.event.job}
            options={stateProps.jobList}
            onChange={(e) => selectJobEventEditPopup((e.target: any).value)}
            onClickSearch={props.onClickJobFinderButton}
          />
        )
      }
      renderWorkCategory={() =>
        useWorkTime && (
          <WorkCategorySelect
            value={stateProps.event}
            options={stateProps.workCategoryList}
            onChange={onChange}
          />
        )
      }
    />
  );
}
