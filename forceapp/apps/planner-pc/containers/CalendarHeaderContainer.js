// @flow
import React, { useCallback, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import CalendarHeader from '../components/CalendarHeader';
import { selectDay, switchCalendarMode } from '../actions/events';
import { selectDay as dailySummarySelectedDay } from '../actions/dailySummary';

import DateUtil from '../../commons/utils/DateUtil';

import type { State } from '../modules';
import DailySummaryButton from '../components/Buttons/DailySummaryButton';

const CalendarHeaderContainer = () => {
  const calendarMode = useSelector((state: State) => state.calendarMode);
  const selectedDay = useSelector((state: State) => state.selectedDay);
  const useWorkTime = useSelector(
    (state: State) => state.userSetting.useWorkTime
  );
  const dispatch = useDispatch();

  const date = useMemo(() => selectedDay.format('YYYY-MM'), [selectedDay]);

  const onClickToday = useCallback(() => dispatch(selectDay(moment())), [
    dispatch,
  ]);

  const onClickNext = useCallback(() => {
    const next = selectedDay.clone();
    if (calendarMode === 'month') {
      next.add(1, 'M');
    } else {
      next.add(1, 'w');
    }
    dispatch(selectDay(next));
  }, [dispatch, calendarMode, selectedDay]);

  const onClickPrevious = useCallback(() => {
    const next = selectedDay.clone();
    if (calendarMode === 'month') {
      next.add(-1, 'M');
    } else {
      next.add(-1, 'w');
    }
    dispatch(selectDay(next));
  }, [dispatch, calendarMode, selectedDay]);

  const onSelectDate = useCallback(
    ({ value }: { value: string }) => {
      dispatch(selectDay(moment(value).date(selectedDay.date())));
    },
    [dispatch, selectedDay]
  );

  const onSelectCalendarMode = useCallback(
    ({ value }: { value: 'week' | 'month' }) => {
      dispatch(switchCalendarMode(value));
    },
    [dispatch, calendarMode]
  );

  const onClickDailySummary = useCallback(() => {
    dispatch(dailySummarySelectedDay(moment()));
  });

  const dateList = useMemo(() => {
    const list = [];
    for (let i = 12; i > -12; i--) {
      const month = selectedDay.clone().add(i, 'month');
      list.push({
        label: DateUtil.formatYLongM(month.toISOString()),
        value: month.format('YYYY-MM'),
      });
    }

    return list;
  }, [selectedDay]);

  return (
    <CalendarHeader
      onClickToday={onClickToday}
      onClickNext={onClickNext}
      onClickPrevious={onClickPrevious}
      onClickDailySummary={onClickDailySummary}
      onSelectDate={onSelectDate}
      onSelectCalendarMode={onSelectCalendarMode}
      dateList={dateList}
      date={date}
      calendarMode={calendarMode}
      renderDailySummaryButton={(props) =>
        useWorkTime && (
          <DailySummaryButton onClick={onClickDailySummary} {...props} />
        )
      }
    />
  );
};

export default CalendarHeaderContainer;
