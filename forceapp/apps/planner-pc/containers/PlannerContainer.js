// @flow

import { connect } from 'react-redux';

import Planner from '../components/Planner';

import type { State } from '../modules';

function mapStateToProps(state: State) {
  return {
    calendarMode: state.calendarMode,
    empEvents: state.empEvents.records,
    userSetting: state.userSetting,
    selectedDay: state.selectedDay,
    jobList: state.common.jobList,
    dialog: state.common.dialog,
    eventEditPopup: state.eventEditPopup,
    dialogs: state.ui.dialogs,
  };
}

export default (connect(mapStateToProps)(Planner): React.ComponentType<Object>);
