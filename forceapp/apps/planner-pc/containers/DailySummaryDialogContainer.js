// @flow

import * as React from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import isNil from 'lodash/isNil';

import * as actions from '../actions/dailySummary';
import { isLockedSelector } from '../modules/ui/dailySummary/editing/taskOverview';

import DailySummaryDialog from '../components/dialogs/DailySummaryDialog';

import type { Job } from '../../domain/models/time-tracking/Job';
import type { State } from '../modules';
import type { DailySummaryTask } from '../../domain/models/time-management/DailySummaryTask';

type OwnProps = {|
  zIndex: number,
|};

const mapStateToProps = (state: State) => {
  return {
    userPermission: state.common.accessControl.permission,
    isProxyMode: state.common.proxyEmployeeInfo.isProxyMode,
    selectedDay: state.ui.dailySummary.selectedDay,
    events: state.ui.dailySummary.events,
    eventListPopupIsOpen: state.ui.dailySummary.eventListPopup.isOpen,
    eventListPopupDay: state.ui.dailySummary.eventListPopup.day,
    eventListPopupEvents: state.ui.dailySummary.eventListPopup.events,
    eventListPopupStyle: state.ui.dailySummary.eventListPopup.style,
    note: state.ui.dailySummary.editing.note,
    // output: state.ui.dailySummary.editing.output,
    timestampComment:
      state.ui.dailySummary.editing.endTimestamp.timestampComment,
    workCategoryLists: state.ui.dailySummary.workCategoryLists,
    defaultJobId:
      state.common.personalSetting.defaultJob &&
      state.common.personalSetting.defaultJob.id,
    realWorkTime: state.ui.dailySummary.editing.taskOverview.realWorkTime,
    isTemporaryWorkTime:
      state.ui.dailySummary.editing.taskOverview.isTemporaryWorkTime,
    isLocked: isLockedSelector(state),
    isEnableEndStamp:
      state.ui.dailySummary.editing.endTimestamp.isEnableEndStamp,
    plannerDefaultView: state.common.personalSetting.plannerDefaultView,
    // needed to update translations
    language: state.common.userSetting.language,
    taskList: state.ui.dailySummary.editing.taskList.taskList,
    isOpeningJobSelectDialog: !isNil(state.ui.dialogs.JOB_SELECT),
    jobTree: state.ui.dailySummary.jobSelectDialog,
  };
};

export default function DailySummaryDialogContainer(props: OwnProps) {
  const {
    taskList,
    selectedDay,
    timestampComment,
    plannerDefaultView,
    ...stateProps
  } = useSelector(mapStateToProps, shallowEqual);

  const dispatch = useDispatch();

  const fetchJobTree = (selectedItem: Job, existingItems: ?(Job[][])) =>
    dispatch(
      actions.fetchJobListChildren(
        selectedItem,
        existingItems,
        selectedDay,
        taskList
      )
    );
  const toggleDirectInput = (index: number) =>
    dispatch(actions.toggleDirectInput(index, taskList));

  const saveDailySummary = () =>
    dispatch(actions.saveDailySummary(plannerDefaultView));

  const saveDailySummaryAndRecordEndTimestamp = () =>
    dispatch(
      actions.saveDailySummaryAndRecordEndTimestamp(
        timestampComment,
        plannerDefaultView,
        selectedDay
      )
    );

  const hideDailySummaryDialog = () =>
    dispatch(actions.hideDailySummaryDialog());

  const showJobSelectDialog = () => dispatch(actions.showJobSelectDialog());

  const deleteTask = (index: number) => dispatch(actions.deleteTask(index));

  const editTask = (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ) => dispatch(actions.editTask(index, prop, value));

  const editTaskTime = (
    index: number,
    prop: 'taskTime' | 'ratio' | 'volume',
    value: number
  ) => dispatch(actions.editTaskTime(index, prop, value));

  const editNote = (text: string) => dispatch(actions.editNote(text));

  const editTimestampComment = (newTimestampComment: string) =>
    dispatch(actions.editTimestampComment(newTimestampComment));

  const reorderTaskList = (list: Array<DailySummaryTask>) =>
    dispatch(actions.reorderTaskList(list));

  const closeJobSelectDialog = () => dispatch(actions.closeJobSelectDialog());

  return (
    <DailySummaryDialog
      {...props}
      {...stateProps}
      dispatch={dispatch}
      selectedDay={selectedDay}
      taskList={taskList}
      timestampComment={timestampComment}
      fetchJobTree={fetchJobTree}
      toggleDirectInput={toggleDirectInput}
      saveDailySummary={saveDailySummary}
      saveDailySummaryAndRecordEndTimestamp={
        saveDailySummaryAndRecordEndTimestamp
      }
      hideDailySummaryDialog={hideDailySummaryDialog}
      showJobSelectDialog={showJobSelectDialog}
      deleteTask={deleteTask}
      editTask={editTask}
      editTaskTime={editTaskTime}
      editNote={editNote}
      editTimestampComment={editTimestampComment}
      reorderTaskList={reorderTaskList}
      closeJobSelectDialog={closeJobSelectDialog}
    />
  );
}
