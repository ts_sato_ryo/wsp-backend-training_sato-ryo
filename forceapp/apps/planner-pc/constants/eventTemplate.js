const eventTemplate = {
  id: null,
  start: null,
  end: null,
  title: '',
  location: '',
  isOuting: false,
  remarks: '',
  isAllDay: false,
  isOrganizer: true,
  job: {
    id: '',
    name: '',
  },
  workCategoryId: '',
  workCategoryName: '',
};

export default eventTemplate;
