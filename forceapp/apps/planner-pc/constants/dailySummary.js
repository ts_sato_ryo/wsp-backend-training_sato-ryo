// @flow

export const DAILY_SUMMARY_SELECT_DAY = 'DAILY_SUMMARY_SELECT_DAY';
export const DAILY_SUMMARY_RECEIVE_EVENTS = 'DAILY_SUMMARY_RECEIVE_EVENTS';
export const DAILY_SUMMARY_RECEIVE_TASKLIST = 'DAILY_SUMMARY_RECEIVE_TASKLIST';
export const DAILY_SUMMARY_CLEAR = 'DAILY_SUMMARY_CLEAR';
export const DAILY_SUMMARY_OPEN_EVENTLIST = 'DAILY_SUMMARY_OPEN_EVENTLIST';
export const DAILY_SUMMARY_CLOSE_EVENTLIST = 'DAILY_SUMMARY_CLOSE_EVENTLIST';
export const DAILY_SUMMARY_RECEIVE_JOB_LIST = 'DAILY_SUMMARY_RECEIVE_JOB_LIST';
export const DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST =
  'DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LIST';
export const DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS =
  'DAILY_SUMMARY_RECEIVE_WORK_CATEGORY_LISTS';
export const DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW =
  'DAILY_SUMMARY_RECEIVE_TASK_OVERVIEW';
export const DAILY_SUMMARY_ADD_TASK = 'DAILY_SUMMARY_ADD_TASK';
export const DAILY_SUMMARY_DELETE_TASK = 'DAILY_SUMMARY_DELETE_TASK';
export const DAILY_SUMMARY_EDIT_TASK = 'DAILY_SUMMARY_EDIT_TASK';
export const DAILY_SUMMARY_EDIT_TASK_TIME = 'DAILY_SUMMARY_EDIT_TASK_TIME';
export const DAILY_SUMMARY_RECEIVE_NOTE = 'DAILY_SUMMARY_RECEIVE_NOTE';
export const DAILY_SUMMARY_EDIT_NOTE = 'DAILY_SUMMARY_EDIT_NOTE';
export const DAILY_SUMMARY_RECEIVE_OUTPUT = 'DAILY_SUMMARY_RECEIVE_OUTPUT';
export const DAILY_SUMMARY_EDIT_OUTPUT = 'DAILY_SUMMARY_EDIT_OUTPUT';
export const DAILY_SUMMARY_TOGGLE_DIRECT_INPUT =
  'DAILY_SUMMARY_TOGGLE_DIRECT_INPUT';
export const DAILY_SUMMARY_REORDER_TASK_LIST =
  'DAILY_SUMMARY_REORDER_TASK_LIST';
export const DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT =
  'DAILY_SUMMARY_CHANGE_TIMESTAMP_COMMENT';
export const DAILY_SUMMARY_CLEAR_DAILY_STAMP_TIME =
  'DAILY_SUMMARY_CLEAR_DAILY_STAMP_TIME';
export const DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS =
  'DAILY_SUMMARY_FETCH_DAILY_STAMP_TIME_SUCCESS';
