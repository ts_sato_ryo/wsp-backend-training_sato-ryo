// @flow

export const DIALOG_SHOW = 'DIALOG_SHOW';
export const DIALOG_HIDE = 'DIALOG_HIDE';

export const dialogType = {
  DAILY_SUMMARY: 'DAILY_SUMMARY',
  JOB_SELECT: 'JOB_SELECT',
};
