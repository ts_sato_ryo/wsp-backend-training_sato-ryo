// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import PlannerContainer from './containers/PlannerContainer';
import App from './action-dispatchers/App';

import onDevelopment from '../commons/config/development';

import '../commons/styles/base.scss';
import '../commons/config/moment';

function renderApp(store, Component) {
  const container = document.getElementById('container');
  if (container) {
    onDevelopment(() => {
      ReactDOM.render(
        <Provider store={store}>
          <Component />
        </Provider>,
        container
      );
    });
  }
}

// eslint-disable-next-line import/prefer-default-export
export const startApp = () => {
  const configuredStore = configureStore();
  renderApp(configuredStore, PlannerContainer);
  App(configuredStore.dispatch).initialize();
};
