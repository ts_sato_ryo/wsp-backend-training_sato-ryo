// @flow

import * as React from 'react';
import Textarea from 'react-textarea-autosize';
import classNames from 'classnames';
import { type Dispatch } from 'redux';
import moment from 'moment';

import { type CalendarEvent } from '../models/calendar-event/CalendarEvent';
import { type UserSetting } from '../../domain/models/UserSetting';

import { confirm } from '../../commons/actions/app';
import { saveEmpEvent, deleteEmpEvent } from '../actions/events';
import { editEventEditPopup } from '../actions/eventEditPopup';
import {
  CALENDAR_CREATED_SERVICE_BY_SALESFORCE,
  CALENDAR_CREATED_SERVICE_BY_OFFICE365,
  CALENDAR_CREATED_SERVICE_BY_GOOGLE,
  CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT,
} from '../constants/event';

import TextField from '../../commons/components/fields/TextField';
import IconButton from '../../commons/components/buttons/IconButton';
import msg from '../../commons/languages';
import { validateEvent } from '../validators';
import DateTimeField from './DateTimeField';

import './EventEditPopup.scss';
// FIXME 将来削除
import './CalendarMenu.scss';

import ImgIconCalGoogleCalender from '../images/Icon_cal_Googlecalender.png';
import ImgIconCalO365 from '../images/Icon_cal_O365.png';
import ImgIconCalSF from '../images/Icon_cal_SF.png';
import ImgPopClose from '../images/popClose.png';
import ImgStsCalGaisyutu from '../images/sts_Cal_gaisyutu.png';

const LINK_ACTION = {
  PREAPPROVALS: 'PREAPPROVALS',
  EXPENSES: 'EXPENSES',
};

const ROOT = 'planner-pc-event-edit-popup';

type Props = $ReadOnly<{
  empInfo: UserSetting,
  closeEventEdit: () => void,
  eventStyle: Object,
  event: CalendarEvent,
  dispatch: Dispatch<any>,
  renderJobSelect: React.ComponentType<{}>,
  renderWorkCategory: React.ComponentType<{}>,
}>;

type State = {|
  eventStyle: Object,
  selectedTabIndex: number,
|};

export default class EventEdit extends React.PureComponent<Props, State> {
  closeEventEdit: () => void;
  openQuad: () => void;
  onSubmit: () => void;
  closeQuadrants: () => void;
  onDeleteButtonClick: () => void;
  renderEventForm: () => void;
  onClickLinkButton: ($Values<typeof LINK_ACTION>) => void;
  changeText: (string, SyntheticEvent<HTMLInputElement>) => void;
  changeDate: (string, string | moment) => void;

  constructor(props: Props) {
    super(props);
    this.state = {
      eventStyle: this.props.eventStyle,
      selectedTabIndex: 0,
    };

    this.closeEventEdit = this.closeEventEdit.bind(this);
    this.openQuad = this.openQuad.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.closeQuadrants = this.closeQuadrants.bind(this);
    this.onDeleteButtonClick = this.onDeleteButtonClick.bind(this);
    this.renderEventForm = this.renderEventForm.bind(this);
    this.onClickLinkButton = this.onClickLinkButton.bind(this);
    this.changeText = this.changeText.bind(this);
    this.changeDate = this.changeDate.bind(this);
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState({
      eventStyle: nextProps.eventStyle,
    });
  }

  onSubmit(e: SyntheticEvent<HTMLFormElement>) {
    e.preventDefault();

    const errors = validateEvent(this.props.event);
    if (errors.length > 0) {
      const messages = errors.map((error) => error.message);
      alert(messages.join('\n'));
      return;
    }

    let startDate = this.props.event.start.clone();
    let endDate = this.props.event.end.clone();

    // 終日の場合utcに変換する前に該当日の開始時刻へ移動
    // utcモードにしてからstartOfを呼び出すと意図しない時間のズレが発生する
    if (this.props.event.isAllDay) {
      startDate.startOf('day');
      endDate.startOf('day');
    }

    startDate = startDate.utc().format();
    endDate = endDate.utc().format();

    // FIXME: サーバーへ送るための変換は action にまとめたい
    const event: Object = {
      id: this.props.event.id,
      title: this.props.event.title,
      startDate, // utc
      endDate, // utc
      isAllDay: this.props.event.isAllDay,
      isOrganizer: this.props.event.isOrganizer,
      isOuting: this.props.event.isOuting,
      location: this.props.event.location,
      remarks: this.props.event.remarks,
      job: this.props.event.job,
      workCategoryId: this.props.event.workCategoryId,
    };

    const { dispatch } = this.props;
    dispatch(saveEmpEvent(event)).then((result) => {
      if (result.isSuccess) {
        this.props.closeEventEdit();
      }
    });
  }

  onChangeTabMenu(tabIndex: number) {
    if (this.isNewEvent()) {
      return null;
    }
    this.setState({
      selectedTabIndex: tabIndex,
    });
    return null;
  }

  onClickLinkButton(action: $Values<typeof LINK_ACTION>) {
    let destination = '';
    const event = this.props.event;
    const id = event.id;
    const title = encodeURIComponent(event.title);
    const startDate = event.start.format('YYYY-MM-DD');
    const startTime = event.start.format('HH:mm');
    const endDate = event.end.format('YYYY-MM-DD');
    const endTime = event.end.format('HH:mm');

    const queryString = `?eventId=${id ||
      ''}&title=${title}&startDate=${startDate}&startTime=${startTime}&endDate=${endDate}&endTime=${endTime}`;

    switch (action) {
      case LINK_ACTION.PREAPPROVALS:
        destination = 'applications';
        break;
      case LINK_ACTION.EXPENSES:
        destination = 'expenses';
        break;
      default:
    }

    // 開発環境とClassic/LEXで飛び先変える必要あり。SF1はLEXと同じでよい？
    if (typeof sforce !== 'undefined' && sforce !== null) {
      // http://salesforce.stackexchange.com/questions/44148/salesforce1-navigatetourl-method
      // Salesforce1 navigation
      sforce.one.navigateToURL(`/apex/${destination}_pc${queryString}`);
    } else {
      const href = window.location.href;
      if (href.split('.')[1] === 'html') {
        // 簡易的な判定
        // ローカル
        window.location.href = `./${destination}-pc/index.html${queryString}`;
      } else {
        // Classic環境
        window.location.href = `./${destination}_pc${queryString}`;
      }
    }
  }

  onDeleteButtonClick() {
    const { dispatch } = this.props;
    dispatch(
      confirm(msg().Cal_Msg_DeleteEvent, (yes) => {
        if (yes) {
          dispatch(deleteEmpEvent(this.props.event));
          this.props.closeEventEdit();
        }
      })
    );
  }

  /**
   *  チェックボックスの状態を変更する
   */
  changeSelection(key: string, e: SyntheticEvent<HTMLInputElement>) {
    this.props.dispatch(editEventEditPopup(key, (e.target: any).checked));
  }

  openQuad(e: SyntheticEvent<HTMLElement>) {
    e.preventDefault();
  }

  closeQuadrants() {}

  closeEventEdit() {
    this.setState({
      selectedTabIndex: 0,
    });
    this.props.closeEventEdit();
  }

  isNewEvent() {
    if (!this.props.event.id) {
      return true;
    }
    return false;
  }

  tabStyleActive(tabIndex: number) {
    if (this.state.selectedTabIndex === tabIndex) {
      return 'active';
    }
    return '';
  }

  tabStyleDisabled() {
    if (this.isNewEvent()) {
      return 'disabled';
    }
    return '';
  }

  changeText(key: string, e: SyntheticEvent<HTMLInputElement>) {
    this.props.dispatch(editEventEditPopup(key, (e.target: any).value));
  }

  changeDate(key: string, value: moment) {
    this.props.dispatch(editEventEditPopup(key, value));
  }

  /**
   * 時刻の変更をdispatchする
   * @param {Srting} key
   * @param {moment} time
   */
  changeTime(key: string, time: moment) {
    this.props.dispatch(editEventEditPopup(key, time));
  }

  renderSourceIcon() {
    switch (this.props.event.createdServiceBy) {
      case CALENDAR_CREATED_SERVICE_BY_SALESFORCE:
        return (
          <img
            className="ts-event-edit__header-icon-link"
            src={ImgIconCalSF}
            alt="Salesforce"
          />
        );
      case CALENDAR_CREATED_SERVICE_BY_OFFICE365:
        return (
          <img
            className="ts-event-edit__header-icon-link"
            src={ImgIconCalO365}
            alt="Office 365"
          />
        );
      case CALENDAR_CREATED_SERVICE_BY_GOOGLE:
        return (
          <img
            className="ts-event-edit__header-icon-link"
            src={ImgIconCalGoogleCalender}
            alt="Google Calendar"
          />
        );
      case CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT:
      default:
        return null;
    }
  }

  renderHeader() {
    const isAllDay = this.props.event.isAllDay;

    return (
      <header className="ts-event-edit__header">
        <IconButton
          onClick={this.closeEventEdit}
          className="ts-event-edit__header-close"
          src={ImgPopClose}
        />

        <div className="ts-event-edit__header-row ts-event-edit__header-row--top">
          <label
            className="ts-event-edit__header-row-label"
            htmlFor={`${ROOT}-title`}
          >
            {msg().Cal_Lbl_Title}
          </label>

          <TextField
            className="ts-event-edit__header-input"
            id={`${ROOT}-title`}
            value={this.props.event.title}
            onChange={(event) => this.changeText('title', event)}
          />

          {this.renderSourceIcon()}
        </div>

        <div className="ts-event-edit__header-row">
          <label
            className="ts-event-edit__header-row-label"
            htmlFor={`${ROOT}-start-date`}
          >
            {msg().Cal_Lbl_Date}
          </label>

          <div className="ts-event-edit__header-date-wrap">
            <DateTimeField
              onChange={(value) => this.changeDate('start', value)}
              isAllDay={isAllDay}
              value={this.props.event.start}
              required
            />

            <div className="ts-event-edit__header-date-separate">–</div>

            <DateTimeField
              onChange={(value) => this.changeDate('end', value)}
              isAllDay={isAllDay}
              value={this.props.event.end}
              required
            />
          </div>

          <div className="ts-event-edit__header-allday">
            <input
              type="checkbox"
              className="ts-event-edit__header-allday-input"
              id="allday"
              value={isAllDay}
              checked={isAllDay ? 'checked' : ''}
              onChange={(e) => this.changeSelection('isAllDay', e)}
            />
            <label htmlFor="allday">{msg().Cal_Lbl_AllDay}</label>
          </div>
        </div>
      </header>
    );
  }

  renderTabMenuImage(tabIndex: number) {
    let key = 'off';
    if (this.state.selectedTabIndex === tabIndex) {
      key = 'on';
    }
    // FIXME
    // Replace paths with literal string.
    //
    // Maybe, the blow code does not work because
    // an argument of `require` must be literal string (modules deps is resolved statically).
    switch (tabIndex) {
      case 0:
        // $FlowFixMe
        return <img src={require(`../images/IconSubmenu001_${key}.png`)} />;
      case 1:
        // $FlowFixMe
        return <img src={require(`../images/IconSubmenu008_${key}.png`)} />;
      case 2:
        // $FlowFixMe
        return <img src={require(`../images/IconSubmenu008_${key}.png`)} />;
      case 3:
        // $FlowFixMe
        return <img src={require(`../images/IconSubmenu004_${key}.png`)} />;
      default:
        return null;
    }
  }

  renderTab() {
    return (
      <div className="ts-event-edit__body__tab">
        <div className="ts-event-edit__body__tab__row slds-grid">
          <div
            className="slds-col slds-size--4-of-12 slds-align-middle"
            onClick={() => this.onChangeTabMenu(0)}
          >
            <div
              className={`ts-event-edit__body__tab__menu ${this.tabStyleActive(
                0
              )}`}
            >
              <div className="ts-event-edit__body__tab__menu-icon slds-align-middle">
                {this.renderTabMenuImage(0)}
              </div>
              <div className="ts-event-edit__body__tab__menu-title">
                {msg().Cal_Lbl_Event}
              </div>
            </div>
          </div>

          <div className="slds-col slds-size--4-of-12" />

          {/* TODO: 勤怠に対応するまでは非表示
          <div
            className="slds-col slds-size--3-of-12 slds-align-middle"
            onClick={() => this.onChangeTabMenu(1)}
          >
            <div className={`ts-event-edit__body__tab__menu ${this.tabStyleActive(1)} ${this.tabStyleDisabled()}`}>
              <div className="ts-event-edit__body__tab__menu-icon slds-align-middle">
                {this.renderTabMenuImage(1)}
              </div>
              <div className="ts-event-edit__body__tab__menu-title">
                勤怠申請
              </div>
            </div>
            */}
          {/* TODO: Uncomment below to show expense applications */}
          {/* (this.renderTabMenuItem(2, this.props.empInfo.usePreApplication, msg().Cal_Btn_TERequest)) */}
          {/* (this.renderTabMenuItem(3, this.props.empInfo.useExpense, msg().Cal_Lbl_Expense)) */}
        </div>
      </div>
    );
  }

  renderTabMenuItem(menuNum: number, available: boolean, title: string) {
    if (!available) {
      return null;
    }

    return (
      <div
        className="slds-col slds-size--4-of-12 slds-align-middle"
        onClick={() => this.onChangeTabMenu(menuNum)}
      >
        <div
          className={`ts-event-edit__body__tab__menu ${this.tabStyleActive(
            menuNum
          )} ${this.tabStyleDisabled()}`}
        >
          <div className="ts-event-edit__body__tab__menu-icon slds-align-middle">
            {this.renderTabMenuImage(menuNum)}
          </div>
          <div className="ts-event-edit__body__tab__menu-title">{title}</div>
        </div>
      </div>
    );
  }

  renderAttendanceStatus(status: number) {
    if (status === 1) {
      return <img src={require('../images/Keihi_sts_Shinseizumi.png')} />;
    }
    return '';
  }

  renderAttendance() {
    const attendancesList = [
      {
        type: '残業申請',
      },
      {
        type: '早退申請',
      },
      {},
      {},
      {},
      {},
    ];

    return (
      <div className="ts-event-edit__body__attendance">
        <div className="ts-event-edit__body__attendance__wrap">
          <div className="slds-grid">
            <div className="slds-col slds-size--12-of-12 slds-align-middle">
              <div className="ts-event-edit__body__attendance__title">
                {msg().Cal_Lbl_RequestItems}
              </div>
            </div>
          </div>
          <div className="ts-event-edit__body__attendance__table">
            <table>
              <tbody>
                {attendancesList.map((item) => {
                  return (
                    <tr>
                      <td className="type">{item.type}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }

  renderPreapprovals() {
    const preapprovalsList = [
      {
        type: msg().Exp_Sel_OverseasTravel,
        active: 'true',
      },
      {
        type: msg().Exp_Sel_DomesticTravel,
      },
      {
        type: msg().Exp_Sel_Entertainment,
      },
      {
        type: msg().Exp_Sel_GeneralExpense,
      },
      {
        type: msg().Exp_Sel_CashAdvance,
      },
      {},
      {},
      {},
      {},
    ];

    return (
      <div className="ts-event-edit__body__preapprovals">
        <div className="ts-event-edit__body__preapprovals__wrap">
          <div className="slds-grid">
            <div className="slds-col slds-size--12-of-12 slds-align-middle">
              <div className="ts-event-edit__body__preapprovals__title">
                {msg().Cal_Lbl_RequestItems}
              </div>
            </div>
          </div>
          <div className="ts-event-edit__body__preapprovals__table">
            <table>
              <tbody>
                {preapprovalsList.map((item) => {
                  if (item.active) {
                    return (
                      <tr>
                        <td
                          className="type text-link"
                          onClick={() =>
                            this.onClickLinkButton(LINK_ACTION.PREAPPROVALS)
                          }
                        >
                          {item.type}
                        </td>
                      </tr>
                    );
                  } else {
                    return (
                      <tr>
                        <td className="type">{item.type}</td>
                      </tr>
                    );
                  }
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }

  renderExpenses() {
    const expensesList = [
      {
        type: msg().Cal_Lbl_SettleExpenses,
        active: true,
      },
      {},
      {},
      {},
      {},
      {},
      {},
    ];

    return (
      <div className="ts-event-edit__body__expenses">
        <div className="slds-grid">
          <div className="slds-col slds-size--12-of-12 slds-align-middle ts-event-edit__body__expenses__header__wrap">
            <div className="ts-event-edit__body__expenses__header">
              <div className="ts-event-edit__body__expenses__header__tag">
                <img src={require('../images/Keihi_sts_FromJizen.png')} />
                <div>{msg().Exp_Lbl_TERequest}</div>
              </div>
              <div className="ts-event-edit__body__expenses__header__title">
                <span>{msg().Cal_Lbl_Title}：</span>
                <span className="value">シンガポール出張</span>
              </div>
              <div className="ts-event-edit__body__expenses__header__total">
                <span>{msg().Appr_Lbl_Total}：</span>
                <span className="value">¥300,000</span>
              </div>
            </div>
          </div>
        </div>

        <div className="ts-event-edit__body__expenses__wrap">
          <div className="slds-grid">
            <div className="slds-col slds-size--12-of-12 slds-align-middle">
              <div className="ts-event-edit__body__expenses__title">
                {msg().Cal_Lbl_RequestItems}
              </div>
            </div>
          </div>
          <div className="ts-event-edit__body__expenses__table">
            <table>
              <tbody>
                {expensesList.map((item) => {
                  if (item.active) {
                    return (
                      <tr>
                        <td
                          className="type text-link"
                          onClick={() =>
                            this.onClickLinkButton(LINK_ACTION.EXPENSES)
                          }
                        >
                          {item.type}
                        </td>
                      </tr>
                    );
                  } else {
                    return (
                      <tr>
                        <td className="type">{item.type}</td>
                      </tr>
                    );
                  }
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }

  renderTask() {
    const JobSelect = this.props.renderJobSelect;
    const WorkCategorySelect = this.props.renderWorkCategory;
    if (this.props.event.isAllDay) {
      return null;
    } else {
      return (
        <>
          <JobSelect />
          <WorkCategorySelect />
        </>
      );
    }
  }

  renderEventForm() {
    return (
      <div className="ts-event-edit__body__input">
        <div className="ts-event-edit__body__input__row slds-grid">
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            <label htmlFor={`${ROOT}-locaiton`} className="key">
              {msg().Cal_Lbl_Location}
            </label>
            <div className="separate">:</div>
          </div>
          <div className="slds-col slds-size--9-of-12 slds-align-middle">
            <div className="value">
              <input
                id={`${ROOT}-locaiton`}
                value={this.props.event.location}
                type="text"
                onChange={(event) => this.changeText('location', event)}
              />
            </div>
          </div>
        </div>

        {this.renderTask()}

        <div className="ts-event-edit__body__input__row slds-grid">
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            <label htmlFor={`${ROOT}-description`} className="key">
              {msg().Cal_Lbl_Description}
            </label>
            <div className="separate">:</div>
          </div>
          <div className="slds-col slds-size--9-of-12 slds-align-middle">
            <div className="value">
              <Textarea
                id={`${ROOT}-description`}
                minRows={2}
                value={this.props.event.remarks}
                onChange={(event) => this.changeText('remarks', event)}
              />
            </div>
          </div>
        </div>

        <div className="ts-event-edit__body__input__row slds-grid">
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            <label htmlFor={`${ROOT}-is-go-out`} className="key">
              {msg().Cal_Lbl_OutOf}
            </label>
            <div className="separate">:</div>
          </div>
          <div className="slds-col slds-size--9-of-12 slds-align-middle">
            <div className="value ts-event-edit__event-outing">
              <input
                id={`${ROOT}-is-go-out`}
                type="checkbox"
                checked={this.props.event.isOuting}
                onChange={(e) => this.changeSelection('isOuting', e)}
              />
              {this.props.event.isOuting ? (
                <img
                  className="ts-event-edit__event-outing-icon"
                  src={ImgStsCalGaisyutu}
                  alt={msg().Cal_Lbl_OutOf}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderMainContent() {
    switch (this.state.selectedTabIndex) {
      case 0:
        return <div>{this.renderEventForm()}</div>;
      case 1:
        return <div>{this.renderAttendance()}</div>;
      case 2:
        return <div>{this.renderPreapprovals()}</div>;
      case 3:
        return <div>{this.renderExpenses()}</div>;
      default:
        return null;
    }
  }

  renderFooter() {
    return (
      <div className="ts-event-edit__footer">
        <div className="ts-event-edit__footer__row slds-grid">
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            {(() => {
              if (!this.isNewEvent()) {
                return (
                  <button
                    type="button"
                    className="delete"
                    onClick={this.onDeleteButtonClick}
                  >
                    {msg().Cal_Btn_Delete}
                  </button>
                );
              }
              return null;
            })()}
          </div>
          <div className="slds-col slds-size--3-of-12 slds-align-middle">
            &nbsp;
          </div>
          <div className="slds-col slds-size--3-of-12 slds-align-middle right">
            <button
              type="button"
              className="cancel"
              onClick={this.closeEventEdit}
            >
              {msg().Com_Btn_Cancel}
            </button>
          </div>
          <div className="slds-col slds-size--3-of-12 slds-align-middle right">
            <button type="submit" className="save">
              {msg().Cal_Btn_Save}
            </button>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const componentCss = classNames('ts-event-edit slds', {
      'ts-calendar-menu-none': this.state.eventStyle.isClose,
    });

    const wrapCss = classNames(
      'ts-event-edit__wrap',
      this.state.eventStyle.classNames
    );

    return (
      <div className={componentCss}>
        <form onSubmit={this.onSubmit}>
          <div
            className="ts-event-edit__overlay"
            onClick={this.closeEventEdit}
          />
          <div className={wrapCss} style={this.state.eventStyle.style}>
            <div className="ts-event-edit__inner ts-event-edit__body">
              <div className="ts-event-edit__header-wrap">
                {this.renderHeader()}
              </div>

              <div className="ts-event-edit__content-wrap">
                {this.renderTab()}
                {this.renderMainContent()}
              </div>

              {this.renderFooter()}
            </div>
          </div>
        </form>
      </div>
    );
  }
}
