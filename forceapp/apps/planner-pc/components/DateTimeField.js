// @flow

import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import classNames from 'classnames';

import DateField from '../../commons/components/fields/DateField';
import TimePicker from '../../commons/components/fields/TimePicker';

import './DateTimeField.scss';

const ROOT = 'planner-pc-date-time-field';

type Props = $ReadOnly<{
  value: string | moment,
  onChange: (string | moment) => void,
  isAllDay: boolean,
  required?: boolean,
}>;

/**
 * EventEditPopupで使う日付及び時刻の編集フィールド
 * 終日予定向けに時刻は非表示可能
 */
export default class DateTimeField extends React.Component<Props> {
  onChangeDate: (string) => void;
  onChangeTime: (string) => void;

  static get defaultProps() {
    return {
      required: false,
    };
  }

  constructor(props: Props) {
    super(props);

    this.onChangeDate = this.onChangeDate.bind(this);
    this.onChangeTime = this.onChangeTime.bind(this);
  }

  // 日付が変更された
  onChangeDate(value: string) {
    let update = moment(`${value} ${this.formatTime(this.props.value)}`);

    if (!update.isValid()) {
      update = '';
    }

    this.props.onChange(update);
  }

  // 時刻が変更された
  onChangeTime(value: string) {
    let update = moment(`${this.formatDate(this.props.value)} ${value}`);

    if (!update.isValid()) {
      update = '';
    }

    this.props.onChange(update);
  }

  formatDate(momentObj: string | Object): string {
    if (
      typeof momentObj === 'object' &&
      !_.isEmpty(momentObj) &&
      momentObj.isValid()
    ) {
      return momentObj.format('YYYY-MM-DD');
    } else {
      return '';
    }
  }

  formatTime(momentObj: string | Object): string {
    if (
      typeof momentObj === 'object' &&
      !_.isEmpty(momentObj) &&
      momentObj.isValid()
    ) {
      return momentObj.format('HH:mm');
    } else {
      return '';
    }
  }

  render() {
    const dateValue = this.formatDate(this.props.value);
    const timeValue = this.formatTime(this.props.value);

    const allDayClass = `${ROOT}--all-day`;

    const cssClass = classNames(ROOT, {
      [allDayClass]: this.props.isAllDay,
    });

    return (
      <span className={`${cssClass} ${ROOT}__container`}>
        <DateField
          className="slds-input ts-event-edit__header-input ts-event-edit__header-input--date"
          value={dateValue}
          onChange={this.onChangeDate}
          required={this.props.required}
        />

        {this.props.isAllDay ? null : (
          <TimePicker
            id="slds-input ts-event-edit__header-input ts-event-edit__header-input--time"
            className="slds-input ts-event-edit__header-input ts-event-edit__header-input--time"
            value={timeValue}
            onTimeChange={this.onChangeTime}
            required={this.props.required}
          />
        )}
      </span>
    );
  }
}
