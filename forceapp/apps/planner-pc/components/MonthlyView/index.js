// @flow

import React from 'react';
import _ from 'lodash';
import moment from 'moment';

import {
  type CalendarEvent,
  type CalendarEventMap,
} from '../../models/calendar-event/CalendarEvent';

import CalendarUtil from '../../../commons/utils/CalendarUtil';

import CalendarMonthlyDayNames from './CalendarMonthlyDayNames';
import CalendarMonthlyWeek from './CalendarMonthlyWeek';

import eventTemplate from '../../constants/eventTemplate';

import './index.scss';

type Day = {
  name: string,
  date: moment,
  number: number,
  month: number,
  isCurrentMonth: boolean,
  isToday: boolean,
};

type Props = $ReadOnly<{
  available: boolean,
  events: CalendarEventMap,
  selectedDay: moment,
  startDayOfTheWeek: number,

  openEventEdit: (
    null | CalendarEvent,
    SyntheticEvent<HTMLButtonElement>
  ) => void,
  openEventListPopup: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void,
}>;

type State = {
  days: Day[],
};

/**
 * 月ビュー
 */
export default class MonthlyView extends React.Component<Props, State> {
  openEventEdit: (
    null | CalendarEvent,
    Day,
    SyntheticEvent<HTMLButtonElement>
  ) => void;

  constructor(props: Props) {
    super(props);
    this.state = {
      days: [],
    };

    this.openEventEdit = this.openEventEdit.bind(this);
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props);
  }

  componentWillReceiveProps(nextProps: Props) {
    let date = CalendarUtil.getStartDateOfMonthView(
      nextProps.selectedDay,
      nextProps.startDayOfTheWeek
    );
    const days = [];
    const lastDate = date.clone().add(42, 'days');

    while (!date.isSame(lastDate, 'day')) {
      const isCurrentMonth = date.month() === nextProps.selectedDay.month();
      const isToday = date.isSame(new Date(), 'day');
      const day = {
        name: date.format('dd').substring(0, 1),
        date,
        number: date.date(),
        month: date.month() + 1,
        isCurrentMonth,
        isToday,
      };

      days.push(day);

      date = date.clone();
      date.add(1, 'd');
    }

    this.setState({ days });
  }

  openEventEdit(
    selectedEvent: null | CalendarEvent,
    day: Day,
    mouseEvent: SyntheticEvent<HTMLButtonElement>
  ) {
    if (selectedEvent === null) {
      const newEvent = _.cloneDeep(eventTemplate);
      newEvent.start = day.date;
      newEvent.end = day.date;
      newEvent.isAllDay = true;

      this.props.openEventEdit(newEvent, mouseEvent);
    } else {
      this.props.openEventEdit(selectedEvent, mouseEvent);
    }
  }

  renderWeeks() {
    const weeks = [];
    let done = false;
    const date = this.state.days[0].date.clone();
    let monthIndex = date.month();
    let count = 0;
    const days = this.state.days.concat();

    while (!done) {
      weeks.push(
        <CalendarMonthlyWeek
          available={this.props.available}
          key={date.toString()}
          date={date.clone()}
          days={days}
          selectedDate={this.props.selectedDay}
          index={count}
          openEventEdit={this.openEventEdit}
          events={this.props.events}
          openEventListPopup={this.props.openEventListPopup}
        />
      );
      date.add(1, 'w');
      done = count > 2 && monthIndex !== date.month();
      count += 1;
      monthIndex = date.month();
    }

    return weeks;
  }

  render() {
    return (
      <div className="ts-calendar-monthly">
        <CalendarMonthlyDayNames
          startDayOfTheWeek={this.props.startDayOfTheWeek}
        />
        <div className="ts-calendar-monthly__body">{this.renderWeeks()}</div>
      </div>
    );
  }
}
