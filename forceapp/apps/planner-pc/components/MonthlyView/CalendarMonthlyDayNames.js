// @flow

import React from 'react';
import classNames from 'classnames';

import msg from '../../../commons/languages';

import './CalendarMonthlyDayNames.scss';

type Props = $ReadOnly<{
  startDayOfTheWeek: number,
}>;

/**
 * 月ビューの曜日部分のヘッダ
 */
export default class CalendarMonthlyDayNames extends React.Component<Props> {
  render() {
    // FIXME: moment 使って解決できそう
    let dayNames = [
      msg().Cal_Lbl_Sun,
      msg().Cal_Lbl_Mon,
      msg().Cal_Lbl_Tue,
      msg().Cal_Lbl_Wed,
      msg().Cal_Lbl_Thu,
      msg().Cal_Lbl_Fri,
      msg().Cal_Lbl_Sat,
    ];

    /** 並び替え */
    const tmp = dayNames.splice(
      this.props.startDayOfTheWeek,
      7 - this.props.startDayOfTheWeek
    );
    dayNames = tmp.concat(dayNames);

    return (
      <table className="ts-calendar-monthly-day-names">
        <tbody>
          <tr>
            {dayNames.map((name, i) => {
              const monthlyDayClass = classNames({
                'ts-calendar-monthly-day': true,
                'ts-calendar-monthly-day--name': true,
                sunday: name === dayNames[0],
                saturday: name === dayNames[6],
              });
              return (
                <td key={i} className={monthlyDayClass}>
                  {name}
                </td>
              );
            })}
          </tr>
        </tbody>
      </table>
    );
  }
}
