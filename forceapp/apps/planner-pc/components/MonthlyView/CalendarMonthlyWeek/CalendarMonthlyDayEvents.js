// @flow

import React from 'react';
import classNames from 'classnames';
import moment from 'moment';

import { type CalendarEvent } from '../../../models/calendar-event/CalendarEvent';
import { CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT } from '../../../constants/event';
import DateUtil from '../../../../commons/utils/DateUtil';
import ImgStsCalGaisyutu from '../../../images/sts_Cal_gaisyutu.png';
import msg from '../../../../commons/languages';

import './CalendarMonthlyDayEvents.scss';

type Day = {
  name: string,
  date: moment,
  number: number,
  month: number,
  isCurrentMonth: boolean,
  isToday: boolean,
};

type Props = $ReadOnly<{
  openEventEdit: (
    null | CalendarEvent,
    Day,
    SyntheticEvent<HTMLButtonElement>
  ) => void,
  openMenu: (SyntheticEvent<HTMLElement>) => void,
  renderEvents: CalendarEvent[][],
  openEventListPopup: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void,
  days: Day[],
  index: number,
}>;

/**
 * 予定表示用コンポーネント
 * CalendarMonthlyWeek の上に別レイヤーとして予定を描画するイメージ
 * TODO: スタイルを MonthlyView/index.scss から分離する
 */
export default class CalendarMothlyDayEvents extends React.Component<Props> {
  openEventEdit: (
    selectedEvent: null | CalendarEvent,
    day: Day,
    mouseEvent: SyntheticEvent<HTMLButtonElement>
  ) => void;

  numberOfMaximumEventsByDay: number;

  static defaultProps = {
    openMenu: () => {},
  };

  constructor(props: Props) {
    super(props);
    this.openEventEdit = this.openEventEdit.bind(this);
  }

  openMenu(mouseEvent: SyntheticEvent<HTMLElement>) {
    mouseEvent.stopPropagation();
    this.props.openMenu(mouseEvent);
  }

  openEventEdit(
    selectedEvent: CalendarEvent,
    day: Day,
    mouseEvent: SyntheticEvent<HTMLButtonElement>
  ) {
    mouseEvent.stopPropagation();
    this.props.openEventEdit(selectedEvent, day, mouseEvent);
  }

  stopMouseEventPropagation(mouseEvent: SyntheticEvent<HTMLButtonElement>) {
    mouseEvent.stopPropagation();
  }

  renderMessageRow(days: Day[], index: number) {
    const grids = [];
    for (let i = 0; i < 7; i++) {
      const day = days[index * 7 + i];
      let numberOfHiddenEvents = 0;
      for (
        let j = this.numberOfMaximumEventsByDay;
        j < this.props.renderEvents.length;
        j++
      ) {
        if (this.props.renderEvents[j][i]) {
          numberOfHiddenEvents += 1;
        }
      }
      if (numberOfHiddenEvents === 0) {
        grids.push(
          <td
            key={i}
            onClick={(mouseEvent) =>
              this.props.openEventEdit(null, day, mouseEvent)
            }
          />
        );
      } else {
        grids.push(
          <td
            key={i}
            onClick={(mouseEvent) =>
              this.props.openEventListPopup(
                day.date,
                this.props.renderEvents,
                mouseEvent
              )
            }
          >
            <div className="ts-calendar-monthly-day__detail ts-calendar-monthly-day__detail--hidden">
              {msg().Cal_Lbl_MoreEvents1}
              {numberOfHiddenEvents}
              {msg().Cal_Lbl_MoreEvents2}
            </div>
          </td>
        );
      }
    }
    return grids;
  }

  renderIconsOnEventGrid(event: CalendarEvent) {
    const icons = [];

    if (event.isOuting) {
      icons.push(
        <img
          className="ts-calendar-monthly-day__detail__icon ts-calendar-monthly-day__detail__icon--outing"
          src={ImgStsCalGaisyutu}
          alt={msg().Cal_Lbl_OutOf}
        />
      );
    }

    return icons;
  }

  renderEventGrid(event: CalendarEvent, colSpan: number, date: Day) {
    const hasLink =
      event.createdServiceBy !== CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT;
    const allowEdit = !hasLink;

    const detailClass = classNames('ts-calendar-monthly-day__detail', {
      event: !event.layout.containsAllDay,
      allday: event.layout.containsAllDay,
      'ts-calendar-monthly-day__detail--has-link': hasLink,
      'ts-calendar-monthly-day__detail--allow-edit': allowEdit,
    });

    const iconsClass = classNames('ts-calendar-monthly-day__icons', {
      'ts-calendar-monthly-day__icons--allday': event.layout.containsAllDay,
    });

    const eventName = event.title || msg().Cal_Lbl_NoTitle;
    let eventTitle = eventName;
    if (!event.isAllDay) {
      if (moment(event.start).isSame(date.date, 'day')) {
        // NOTE: 予定開始日ではない場合には時刻を表示させない
        eventTitle = `${DateUtil.format(event.start, 'HH:mm')} ${eventName}`;
      }
    }

    // FIXME Should not create an event handler here
    const onClickEvent = (mouseEvent) =>
      allowEdit
        ? this.openEventEdit(event, date, mouseEvent)
        : this.stopMouseEventPropagation(mouseEvent);

    return (
      <td
        key={`event-${date.month}-${date.number}`}
        colSpan={colSpan}
        onClick={onClickEvent}
      >
        <div className={detailClass}>
          {eventTitle}
          <div className={iconsClass}>{this.renderIconsOnEventGrid(event)}</div>
        </div>
      </td>
    );
  }

  renderEmptyGrid(date: Day) {
    return (
      <td
        key={`empty-${date.month}-${date.number}`}
        onClick={(mouseEvent) =>
          this.props.openEventEdit(null, date, mouseEvent)
        }
      >
        &nbsp;
      </td>
    );
  }

  renderRow(days: Day[], index: number, events: CalendarEvent[]) {
    const result = [];
    for (let i = 0; i < 7; i++) {
      const day = days[index * 7 + i];
      if (events === undefined || events[i] === undefined) {
        result.push(this.renderEmptyGrid(day));
      } else {
        const event = events[i];
        const colSpan = event.layout.colSpan;
        i = i + colSpan - 1;
        result.push(this.renderEventGrid(event, colSpan, day));
      }
    }

    return result;
  }

  render() {
    const days = this.props.days;
    const index = this.props.index;
    this.numberOfMaximumEventsByDay = 3;

    return (
      <table className="ts-calendar-monthly__table__week-detail">
        <tbody>
          <tr>
            <td colSpan="7">&nbsp;</td>
          </tr>
          <tr>{this.renderRow(days, index, this.props.renderEvents[0])}</tr>
          <tr>{this.renderRow(days, index, this.props.renderEvents[1])}</tr>
          <tr>{this.renderRow(days, index, this.props.renderEvents[2])}</tr>
          <tr>{this.renderMessageRow(days, index)}</tr>
        </tbody>
      </table>
    );
  }
}
