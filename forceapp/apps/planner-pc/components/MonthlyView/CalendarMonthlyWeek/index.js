// @flow

import React from 'react';
import moment from 'moment';

import {
  type CalendarEvent,
  type CalendarEventMap,
} from '../../../models/calendar-event/CalendarEvent';

import { DATETIME_FORMAT_FOR_EVENT_DATA_KEY } from '../../../constants/event';
import CalendarMothlyDayBox from './CalendarEventDayBox';
import CalendarMothlyDayEvents from './CalendarMonthlyDayEvents';

import './index.scss';

type Day = {
  name: string,
  date: moment,
  number: number,
  month: number,
  isCurrentMonth: boolean,
  isToday: boolean,
};

type Props = $ReadOnly<{
  events: CalendarEventMap,
  renderEvents?: CalendarEvent[][],
  openEventEdit: (
    null | CalendarEvent,
    Day,
    SyntheticEvent<HTMLButtonElement>
  ) => void,
  days: Day[],
  index: number,
  selectedDate: moment,
  openEventListPopup: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void,
  date: moment,
  available: boolean,
}>;

/**
 * 月ビューの1週間分の行を表すコンポーネント
 */
export default class CalendarMonthlyWeek extends React.Component<Props> {
  vacancyTable: boolean[][];

  getRowIndex(eventsInTheWeek: CalendarEvent[][], colIndex: number) {
    let rowIndex = 0;
    while (true) {
      if (!eventsInTheWeek[rowIndex]) {
        eventsInTheWeek[rowIndex] = [];
        this.vacancyTable[rowIndex] = [
          true,
          true,
          true,
          true,
          true,
          true,
          true,
        ];
      }
      // NOTE: 指定グリッドにすでにイベントが入っているかどうかを検索
      if (this.vacancyTable[rowIndex][colIndex]) {
        return rowIndex;
      }
      rowIndex += 1;
    }

    // Never reach here. The blow code is needed to pass type checking.
    // eslint-disable-next-line no-unreachable
    return 0;
  }

  // NOTE: 週ごとの予定リストを作成
  getEventsInTheWeek(date: moment) {
    const eventsInTheWeek: CalendarEvent[][] = []; // NOTE: Gridを表す2次元配列として使う
    this.vacancyTable = [];
    this.vacancyTable[0] = [true, true, true, true, true, true, true];
    for (let i = 0; i < 7; i++) {
      const dateKey = date
        .clone()
        .add(i, 'days')
        .format(DATETIME_FORMAT_FOR_EVENT_DATA_KEY);
      const events = this.props.events[dateKey];
      if (events !== undefined) {
        events.forEach((event) => {
          if (event.layout.visibleInMonthlyView) {
            const rowIndex = this.getRowIndex(
              eventsInTheWeek,
              event.layout.colIndex
            );
            eventsInTheWeek[rowIndex][event.layout.colIndex] = event;

            let startCol = event.layout.colIndex;
            const endCol = event.layout.colIndex + event.layout.colSpan;
            for (startCol; startCol < endCol; startCol++) {
              this.vacancyTable[rowIndex][startCol] = false;
            }
          }
        });
      }
    }
    return eventsInTheWeek;
  }

  render() {
    const dayBox = [];
    const days = this.props.days;
    const index = this.props.index;
    const eventsInTheWeek = this.getEventsInTheWeek(days[index * 7].date);

    for (let i = 0; i < 7; i++) {
      /** let day = days.shift(); // ここのshift()動きがおかしい？ */
      const day = days[index * 7 + i];
      if (day) {
        dayBox.push(
          <CalendarMothlyDayBox
            available={this.props.available}
            key={day.date.toString()}
            day={day}
            openEventEdit={this.props.openEventEdit}
            openEventListPopup={this.props.openEventListPopup}
          />
        );
      }
    }

    return (
      <div className="ts-calendar-monthly-week">
        <table className="ts-calendar-monthly__table__week-box">
          <tbody>
            <tr>{dayBox}</tr>
          </tbody>
        </table>
        <CalendarMothlyDayEvents
          date={this.props.date}
          days={this.props.days}
          index={this.props.index}
          selectedDate={this.props.selectedDate}
          openEventEdit={this.props.openEventEdit}
          renderEvents={eventsInTheWeek}
          vacancyTable={this.vacancyTable}
          openEventListPopup={this.props.openEventListPopup}
        />
      </div>
    );
  }
}
