// @flow

import React from 'react';
import classNames from 'classnames';
import moment from 'moment';

import { type CalendarEvent } from '../../../models/calendar-event/CalendarEvent';

type Day = {
  name: string,
  date: moment,
  number: number,
  month: number,
  isCurrentMonth: boolean,
  isToday: boolean,
};

type Props = {
  day: Day,
  openEventEdit: (
    CalendarEvent,
    Day,
    SyntheticEvent<HTMLButtonElement>
  ) => void,
};

/**
 * カレンダーの 1 日分のセル
 * TODO: スタイルを MonthlyView/index.scss から分離する
 */
export default class CalendarMothlyDayBox extends React.Component<Props> {
  openEventEdit: (
    selectedEvent: null | CalendarEvent,
    mouseEvent: SyntheticEvent<HTMLButtonElement>
  ) => void;

  constructor(props: Props) {
    super(props);

    this.openEventEdit = this.openEventEdit.bind(this);
  }

  openEventEdit(
    selectedEvent: CalendarEvent,
    mouseEvent: SyntheticEvent<HTMLButtonElement>
  ) {
    mouseEvent.stopPropagation();
    this.props.openEventEdit(selectedEvent, this.props.day, mouseEvent);
  }

  renderDayNumber(day: Day) {
    if (day.number === 1) {
      return `${day.month}/${day.number}`;
    }
    return `${day.number}`;
  }

  render() {
    const day = this.props.day;
    const monthlyDayClass = classNames({
      'ts-calendar-monthly-day': true,
      'different-month': !day.isCurrentMonth,
      sunday: day.date.format('d') === '0',
      saturday: day.date.format('d') === '6',
      today: day.date.isSame(moment(), 'day'),
      box: true,
    });

    return (
      <td
        id={`day_${day.number}`}
        className={monthlyDayClass}
        onClick={(mouseEvent) => {
          this.openEventEdit(null, mouseEvent);
        }}
      >
        <div
          className={
            day.isToday ? 'ts-calendar-monthly-day__number--today' : ' '
          }
        />
        <div
          className={`ts-calendar-monthly-day__number ${String(
            day.isCurrentMonth
          )}`}
        >
          {this.renderDayNumber(day)}
        </div>
        <div
          id={`detail_${day.number}`}
          className="ts-calendar-monthly-day__details"
        />
      </td>
    );
  }
}
