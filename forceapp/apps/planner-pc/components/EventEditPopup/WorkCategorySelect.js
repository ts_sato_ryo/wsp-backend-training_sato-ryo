// @flow

import React from 'react';
import isEmpty from 'lodash/isEmpty';

import msg from '../../../commons/languages';
import type { WorkCategory } from '../../../domain/models/time-tracking/WorkCategory';

const ROOT = 'planner-pc-event-edit-popup';

type Props = $ReadOnly<{|
  options?: WorkCategory[],
  value: { workCategoryId: string, workCategoryName: string },
  onChange: (string, SyntheticEvent<HTMLInputElement>) => void,
|}>;

const renderWorkCategoryItems = ({ value, options = [] }: Props) => {
  if (isEmpty(options)) {
    // Set selected work category as default item
    return (
      <option value={value.workCategoryId} key={value.workCategoryId}>
        {value.workCategoryName}
      </option>
    );
  } else {
    const emptyItem = <option value="" key={0} />;

    const optionItems = options.map((item) => (
      <option value={item.id} key={item.id}>
        {item.name}
      </option>
    ));
    const items = [emptyItem, ...optionItems];
    return <>{items}</>;
  }
};

const WorkCategorySelect = (props: Props) => {
  return (
    <div className={ROOT}>
      <div className="ts-event-edit__body__input__row slds-grid">
        <div className="slds-col slds-size--3-of-12 slds-align-middle">
          <label htmlFor={`${ROOT}-work-category`} className="key">
            {msg().Cal_Lbl_WorkCategory}
          </label>
          <div className="separate">:</div>
        </div>
        <div className="slds-col slds-size--9-of-12 slds-align-middle">
          <div className="value">
            <select
              id={`${ROOT}-work-category`}
              value={props.value.workCategoryId}
              onChange={(event) => props.onChange('workCategoryId', event)}
            >
              {renderWorkCategoryItems(props)}
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WorkCategorySelect;
