// @flow

import React from 'react';

import SelectField from '../../../commons/components/fields/SelectField';
import ImgSearch from '../../images/Search.png';

import msg from '../../../commons/languages';

type Props = $ReadOnly<{|
  options?: {| jobId: string, jobCode: string, jobName: string |}[],
  value?: { id: string },
  onChange: (e: SyntheticEvent<HTMLSelectElement>) => void,
  onClickSearch: () => void,
|}>;

const ROOT = 'planner-pc-event-edit-popup';

const JobSelect = ({ value, options = [], onChange, onClickSearch }: Props) => {
  const buildJobListOptions = () => {
    const jobListOption = options.map((job) => {
      return {
        value: job.jobId,
        text: `${job.jobCode} ${job.jobName}`,
      };
    });

    return [{ value: '', text: '' }, ...jobListOption];
  };

  return (
    <div className={ROOT}>
      <div className="ts-event-edit__body__input__row slds-grid">
        <div className="slds-col slds-size--3-of-12 slds-align-middle">
          <label htmlFor={`${ROOT}-job`} className="key">
            {msg().Cal_Lbl_Job}
          </label>
          <div className="separate">:</div>
        </div>
        <div className="slds-col slds-size--8-of-12 slds-align-middle">
          <div className="value">
            <SelectField
              onChange={onChange}
              id={`${ROOT}-job`}
              options={buildJobListOptions()}
              value={value ? value.id : undefined}
            />
          </div>
        </div>
        <div className="slds-col slds-size--1-of-12 slds-align-middle">
          <div className="search">
            <button onClick={onClickSearch} type="button">
              <img src={ImgSearch} />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default JobSelect;
