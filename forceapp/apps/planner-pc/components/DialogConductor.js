// @flow

import React from 'react';

import { dialogType } from '../constants/dialogs';
import DailySummaryDialogContainer from '../containers/DailySummaryDialogContainer';

type Props = $ReadOnly<{
  dialogs: Object,
}>;

export default class DialogConductor extends React.Component<Props> {
  renderDialog: ($Values<typeof dialogType>) => void;

  constructor(props: Props) {
    super(props);

    this.renderDialog = this.renderDialog.bind(this);
  }

  renderDialog(type: $Values<typeof dialogType>) {
    switch (type) {
      case dialogType.DAILY_SUMMARY:
        return (
          <DailySummaryDialogContainer
            key={type}
            zIndex={this.props.dialogs[type]}
          />
        );
      default:
        return null;
    }
  }

  render() {
    return <div>{Object.keys(this.props.dialogs).map(this.renderDialog)}</div>;
  }
}
