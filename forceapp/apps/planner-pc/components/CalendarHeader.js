// @flow
import * as React from 'react';
import styled from 'styled-components';

import CommonSelect, { type Option } from '../../core/elements/Select';
import CommonButton from '../../core/elements/Button';
import ArrowRightButton from '../../core/blocks/buttons/ArrowRightButton';
import ArrowLeftButton from '../../core/blocks/buttons/ArrowLeftButton';
import msg from '../../commons/languages';
import variables from '../../commons/styles/wsp.scss';

type Props = {
  onClickToday: () => void,
  onClickNext: () => void,
  onClickPrevious: () => void,
  onSelectDate: (value: Option) => void,
  onSelectCalendarMode: (value: Option) => void,
  dateList: $ReadOnlyArray<Option>,
  date: string,
  calendarMode: 'week' | 'month',
  renderDailySummaryButton: React.ComponentType<{ label: string }>,
};

const Container = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: transparent;
  width: 100%;
  height: 32px;
`;

const Content = styled.div`
  display: flex;
  align-items: center;
  flex-flow: row nowrap;
`;

const MonthSelect = styled(CommonSelect)`
  background: transparent;
  border: none;
  border-radius: 0;
  color: ${variables['color-text-2']};
  font-size: 16px;
  line-height: 24px;
  font-weight: bold;
  width: 160px;
  height: 24px;
  padding: 0;

  &:hover {
    background: transparent;
    border-bottom: 1px solid ${variables['color-text-2']};
  }
`;

const TodayButton = styled(CommonButton)`
  width: 70px;
  height: 32px;
  padding: 7px 0 8px 0;
  margin-left: 32px;
  color: ${variables['color-text-1']};
  font-size: 13px;
  line-height: 17px;
`;

const Switching = styled(CommonSelect)`
  font-size: 13px;
  line-height: 17px;
  width: 95px;
  height: 32px;
  margin-left: 20px;
`;

const Previous = styled.div`
  margin-left: 22px;
`;

const Next = styled.div`
  margin-left: 6px;
`;

const DailySummary = styled.div`
  align-content: flex-end;
  margin: 0 20px 0 0;
`;

const CalendarHeader = (props: Props) => {
  const DailySummaryButton = props.renderDailySummaryButton;
  return (
    <Container>
      <Content>
        <MonthSelect
          onSelect={props.onSelectDate}
          options={props.dateList}
          value={props.date}
        />
        <TodayButton onClick={props.onClickToday}>
          {msg().Cal_Lbl_Today}
        </TodayButton>
        <Switching
          onSelect={props.onSelectCalendarMode}
          options={[
            { label: msg().Cal_Lbl_Month, value: 'month' },
            { label: msg().Cal_Lbl_Week, value: 'week' },
          ]}
          value={props.calendarMode}
        />
        <Previous>
          <ArrowLeftButton onClick={props.onClickPrevious} />
        </Previous>
        <Next>
          <ArrowRightButton onClick={props.onClickNext} />
        </Next>
      </Content>

      <Content>
        <DailySummary>
          <DailySummaryButton label={msg().Cal_Lbl_DailySummary} />
        </DailySummary>
      </Content>
    </Container>
  );
};

export default CalendarHeader;
