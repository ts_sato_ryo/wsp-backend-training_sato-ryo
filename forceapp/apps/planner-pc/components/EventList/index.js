// @flow

import * as React from 'react';
import styled from 'styled-components';

import CalendarUtil from '../../../commons/utils/CalendarUtil';
import Text from '../../../core/elements/Text';
import { Color } from '../../../core/styles';
import CloseButton from '../../../core/blocks/buttons/CloseButton';
import Event from './Event';

type Props = $ReadOnly<{
  date: Date,
  events: $ReadOnlyArray<{
    id: string,
    title: string,
    startDateTime: Date,
    isOverMultipleDays: boolean,
  }>,

  onClickClose: () => void,
  onClickOpen: (id: string) => void,
}>;

const Card = styled.div`
  width: 200px;
  height: 100%;
  overflow: hidden;
  border-radius: 4px;
  background: #ffffff;
  border: 1px solid ${Color.border3};
  box-sizing: border-box;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.2);
`;

const Body = styled.div`
  height: 200px;
  overflow: hidden auto;
`;

const Header = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  height: 35px;
  padding: 0 3px 0 9px;
`;

const H2 = styled.h2`
  width: 33%;
  flex: 1 1 0%;
`;

const Heading = (props: { children: string }) => (
  <Text as={H2} {...props} bold size="xl" />
);

const Hr = styled.hr`
  width: 100%;
  height: 1px;
  padding: 0;
  margin: 0;
`;

const List = styled.ul`
  padding: 8px 0 14px 0;
`;

const ListItem = styled.li`
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: flex-start;

  height: ${({ isOverMultipleDays }) => (isOverMultipleDays ? 18 : 20)}px;
  padding: 0 ${({ isOverMultipleDays }) => (isOverMultipleDays ? 9 : 13)}px;
  margin: ${({ isOverMultipleDays }) => (isOverMultipleDays ? 2 : 0)}px 0 0 0;
  :first-child {
    margin: 0;
  }

  :hover,
  :focus {
    cursor: pointer;
    outline: none;
    background: ${Color.hover};
    color: ${Color.font1};
  }
`;

const ListItemContent = styled(Text)`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const EventList = ({ date, events, onClickClose, onClickOpen }: Props) => (
  <Card>
    <Header role="presentation">
      <Heading>{CalendarUtil.format(date, { weekday: 'short' })}</Heading>
      <Heading>{date.getDate().toString()}</Heading>
      <CloseButton tabIndex={0} onClick={onClickClose} />
    </Header>
    <Hr />
    <Body>
      <List role="menu">
        {events.map((event) =>
          event.isOverMultipleDays ? (
            <ListItem key={event.id} tabIndex={0} {...event}>
              <Event onClick={() => onClickOpen(event.id)} role="menuitem">
                {event.title}
              </Event>
            </ListItem>
          ) : (
            <ListItem
              key={event.id}
              tabIndex={0}
              role="menuitem"
              onClick={() => onClickOpen(event.id)}
            >
              <ListItemContent size="small">
                {CalendarUtil.format(event.startDateTime, {
                  hour: '2-digit',
                  minute: '2-digit',
                })}{' '}
                {event.title}
              </ListItemContent>
            </ListItem>
          )
        )}
      </List>
    </Body>
  </Card>
);

export default EventList;
