// @flow

import * as React from 'react';
import styled from 'styled-components';

import CommonText from '../../../core/elements/Text';

type Props = $ReadOnly<{
  children: string,
}>;

const Block = styled.span`
  appearance: none;
  border: none;
  outline: none;
  background: #006dcc;
  border-radius: 2px;
  height: 18px;
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  padding: 0 4px;
  display: flex;
  align-items: center;

  :hover,
  :focus {
    background: #004b99;
  }
`;

const Text = styled(CommonText)`
  color: #fff;
`;

const Event = ({ children, ...props }: Props) => (
  <>
    <Block {...props}>
      <Text size="small">{children}</Text>
    </Block>
  </>
);

export default Event;
