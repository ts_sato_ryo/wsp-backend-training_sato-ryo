// @flow

import * as React from 'react';
import _ from 'lodash';
import moment from 'moment';
import classnames from 'classnames';
import { type Dispatch } from 'redux';

import type { UserSetting } from '../../domain/models/UserSetting';
import type {
  CalendarEvent,
  CalendarEventMap,
} from '../models/calendar-event/CalendarEvent';
import type { Job } from '../../domain/models/time-tracking/Job';

import {
  selectDay as dailySummarySelectedDay,
  closeEventListPopup,
} from '../actions/dailySummary';
import { fetchJobListByParent } from '../../commons/actions/jobList';
import {
  selectDay,
  switchCalendarMode,
  loadEmpEvents,
} from '../actions/events';
import {
  selectEventEditPopup,
  selectJobFromDialog,
  fetchWorkCategoryList,
  fetchWorkCategoryListFromSavedEvent,
  clearWorkCategoryList,
} from '../actions/eventEditPopup';
import {
  showDialog,
  hideDialog,
  DIALOG_TYPE,
} from '../../commons/actions/dialog';
import Text from '../../core/elements/Text';
import PersonalMenuPopoverButtonContainer from '../../commons/containers/PersonalMenuPopoverButtonContainer';
import CalendarMonthly from './MonthlyView';
import CalendarWeekly from './WeeklyView';
import TrackSummary from '../../time-tracking/TrackSummary';
import DateUtil from '../../commons/utils/DateUtil';
import DialogConductor from './DialogConductor';
import EventEditPopupContainer from '../containers/EventEditPopupContainer';
import EventListPopup from '../../commons/components/popups/EventListPopup';
import GlobalContainer from '../../commons/containers/GlobalContainer';
import NavigationBar from '../../core/elements/NavigationBar';
import CalendarHeader from '../containers/CalendarHeaderContainer';
import Modal from '../../commons/components/Modal';
import MultiColumnFinder from '../../commons/components/dialogs/MultiColumnFinder';
import PersonalMenuPopoverContainer from '../../commons/containers/PersonalMenuPopoverContainer';
import PersonalSettingDialogContainer from '../../commons/containers/PersonalSettingDialogContainer';
import ToastContainer from '../../commons/containers/ToastContainer';
import IconPlanner from '../images/icons/icon_planner.svg';
import msg from '../../commons/languages';

import '../../commons/styles/modal-transition-slideup.css';
import './Planner.scss';
import { DATETIME_FORMAT_FOR_EVENT_DATA_KEY } from '../constants/event';

type Props = $ReadOnly<{
  calendarMode: string,
  dialog: Object,
  dialogs: Object,
  dispatch: Dispatch<Object>,
  empEvents: CalendarEventMap,
  eventEditPopup: Object,
  jobList: Job[],
  selectedDay: moment,
  userSetting: UserSetting,
}>;

type State = {
  eventStyle: {
    isClose: boolean,
    style?: {
      top: string,
      left: string,
    },
    classNames?: string,
  },
  eventListDate: moment,
  eventListPosition: {
    position: 'absolute',
    top: string,
    left: string,
  },
  modalStyle: {
    width: number,
  },
  isEventListOpen: boolean,
};

export default class Planner extends React.Component<Props, State> {
  closeEventEdit: Function;
  closeEventListPopup: Function;
  closeMenu: Function;
  hideDialog: Function;

  onClickEventMore: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void;

  onClickJobItem: Function;
  openEventEdit: Function;
  openJobFinder: Function;
  showDailySummary: Function;
  switchCalendarMode: Function;
  userSetting: UserSetting;

  constructor(props: Props) {
    super(props);

    this.state = {
      eventStyle: {
        isClose: true,
        style: {
          top: '0px',
          left: '0px',
        },
        classNames: '',
      },
      eventListDate: moment(),
      eventListPosition: {
        position: 'absolute',
        top: '0px',
        left: '0px',
      },
      modalStyle: {
        width: 865,
      },
      isEventListOpen: false,
    };
    this.closeEventEdit = this.closeEventEdit.bind(this);
    this.closeEventListPopup = this.closeEventListPopup.bind(this);
    this.hideDialog = this.hideDialog.bind(this);
    this.onClickEventMore = this.onClickEventMore.bind(this);
    this.onClickJobItem = this.onClickJobItem.bind(this);
    this.openEventEdit = this.openEventEdit.bind(this);
    this.openJobFinder = this.openJobFinder.bind(this);
    this.showDailySummary = this.showDailySummary.bind(this);
    this.switchCalendarMode = this.switchCalendarMode.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(switchCalendarMode('week'));
  }

  componentWillReceiveProps(nextProps: Props) {
    if (
      nextProps.selectedDay.year() !== this.props.selectedDay.year() ||
      nextProps.selectedDay.month() !== this.props.selectedDay.month() ||
      nextProps.selectedDay.day() !== this.props.selectedDay.day()
    ) {
      const { dispatch } = nextProps;
      dispatch(loadEmpEvents(nextProps.selectedDay));
    }
  }

  componentWillUpdate(nextProps: Props) {
    if (
      this.props.userSetting !== nextProps.userSetting &&
      this.userSetting !== nextProps.userSetting &&
      Object.keys(nextProps.userSetting).length > 0
    ) {
      // FIXME: API側のエラーにて無限ループが発生するため、thisに持たせて判断しているが、
      // 出来るだけこの判断を使わないように修正したい
      this.userSetting = nextProps.userSetting;
      const { dispatch } = this.props;
      dispatch(selectDay(moment()));
    }
  }

  onClickEventMore(
    selectedDay: moment,
    _events: CalendarEvent[][],
    event: SyntheticMouseEvent<HTMLElement>
  ) {
    const eventListHeight = 180;
    const eventListWidth = 180;

    const container = document.getElementsByClassName('ts-container')[0];
    const windowScrollX = window.pageXOffset;
    const windowScrollY = window.pageYOffset;

    const pageHeight = container.clientHeight;
    const offsetTop = container.getBoundingClientRect().top;
    const top =
      (eventListHeight + event.pageY > pageHeight
        ? event.pageY - offsetTop - 170
        : event.pageY - offsetTop) - windowScrollY;

    const left =
      (window.innerWidth - event.pageX < eventListWidth &&
      eventListWidth < event.pageX
        ? container.getBoundingClientRect().left - 180
        : event.pageX - container.getBoundingClientRect().left) - windowScrollX;

    this.setState({
      isEventListOpen: true,
      eventListPosition: {
        position: 'absolute',
        top: `${top}px`,
        left: `${left}px`,
      },
      // FIXME: If use props.selected, Calendar transition to next month when 'more' of next month click.
      // Actually I want to use props.selectedDay.
      eventListDate: selectedDay,
    });
  }

  onClickJobItem(selectedItem: Job, existingItems: Job[][]) {
    const { dispatch } = this.props;
    const eventStart = this.props.eventEditPopup.event.start.format(
      'YYYY-MM-DD'
    );

    if (selectedItem.hasChildren && existingItems !== undefined) {
      // 子階層を描画
      dispatch(fetchJobListByParent(eventStart, selectedItem, existingItems));
    } else {
      // FIXME selectableが実装されたら有効化
      // } else if (selectedItem.selectabledTimeTrack) {

      // 選択[]
      this.hideDialog();

      dispatch(
        selectJobFromDialog(
          selectedItem.id,
          selectedItem.name,
          selectedItem.code
        )
      );

      const targetDate = DateUtil.formatISO8601Date(
        this.props.eventEditPopup.event.start.valueOf()
      );
      dispatch(clearWorkCategoryList());
      dispatch(fetchWorkCategoryList(selectedItem.id, targetDate));
    }
  }

  switchCalendarMode(mode: 'month' | 'week') {
    const { dispatch } = this.props;
    if (this.props.calendarMode === mode) {
      return;
    }
    dispatch(switchCalendarMode(mode));
  }

  closeEventListPopup() {
    this.setState({
      isEventListOpen: false,
    });
    this.props.dispatch(closeEventListPopup());
  }

  openEventEdit(
    selectedEvent: CalendarEvent,
    { pageX, pageY }: SyntheticMouseEvent<HTMLElement>
  ) {
    const container = document.getElementsByClassName('ts-container')[0];
    const { top, right } = container.getBoundingClientRect();
    const { pageYOffset } = window;

    const popupWindow = {
      height: 402,
      width: 510,
      offsetOfCallouts: 25,
      offsetOfRightCallouts: 60,
    };

    const absPosY = pageY + pageYOffset + container.scrollTop;
    const absPosX = pageX - popupWindow.offsetOfCallouts;

    const verticalPosition =
      top + popupWindow.height < absPosY
        ? {
            arrowDirection: 'down',
            top: absPosY - popupWindow.height - popupWindow.offsetOfCallouts,
          }
        : {
            arrowDirection: 'up',
            top: absPosY,
          };
    const horizontalPosition =
      right - popupWindow.width < absPosX
        ? {
            arrowDirection: 'right',
            left:
              absPosX - popupWindow.width + popupWindow.offsetOfRightCallouts,
          }
        : {
            arrowDirection: 'left',
            left: absPosX,
          };

    const classNames = `${horizontalPosition.arrowDirection} ${verticalPosition.arrowDirection}`;
    const eventStyle = {
      isClose: false,
      style: {
        top: `${verticalPosition.top}px`,
        left: `${horizontalPosition.left}px`,
      },
      classNames,
    };

    const { dispatch } = this.props;
    const eventData = _.cloneDeep(selectedEvent);
    dispatch(selectEventEditPopup(eventData));

    this.setState({
      eventStyle,
    });

    // FIXME Dialog展開処理が散らばっているためまとめたい
    if (!_.isEmpty(eventData.job.id)) {
      // Jobが指定済みであれば、作業分類のリストを取りにいく
      dispatch(fetchWorkCategoryListFromSavedEvent(eventData));
    }
  }

  closeEventEdit() {
    // FIXME actionに移動したい
    const eventStyle = {
      isClose: true,
    };
    this.setState({ eventStyle });
    this.closeEventListPopup();

    this.props.dispatch(clearWorkCategoryList());
  }

  openJobFinder() {
    this.setState({
      modalStyle: {
        width: 865,
      },
    });
    const { dispatch } = this.props;
    const day = this.props.eventEditPopup.event.start.clone();

    dispatch(showDialog(DIALOG_TYPE.JOB_FINDER));
    dispatch(fetchJobListByParent(day.format('YYYY-MM-DD')));
  }

  hideDialog() {
    this.props.dispatch(hideDialog());
  }

  /**
   * デイリーサマリー展開
   * @param {moment} day - 展開する日付
   */
  showDailySummary(day: moment = moment()) {
    const { dispatch } = this.props;

    return () => {
      dispatch(dailySummarySelectedDay(day));
    };
  }

  renderModalComponent() {
    return (
      <MultiColumnFinder
        items={this.props.jobList}
        typeName={msg().Com_Lbl_Job}
        showFavorites={false}
        showHistory={false}
        parentSelectable
        onClickItem={this.onClickJobItem}
        onClickCloseButton={this.hideDialog}
      />
    );
  }

  renderCalendar() {
    if (this.props.calendarMode === 'month') {
      return (
        <CalendarMonthly
          {...this.props}
          events={this.props.empEvents}
          selectedDay={this.props.selectedDay}
          // TODO 下記は設定から取得する
          startDayOfTheWeek={0}
          openEventEdit={this.openEventEdit}
          openEventListPopup={this.onClickEventMore}
          available={false}
        />
      );
    }

    return (
      <CalendarWeekly
        events={this.props.empEvents}
        selectedDay={this.props.selectedDay}
        // TODO 下記は設定から取得する
        startDayOfTheWeek={0}
        showDailySummary={
          this.props.userSetting.useWorkTime ? this.showDailySummary : null
        }
        openEventEdit={this.openEventEdit}
        openEventListPopup={this.onClickEventMore}
        available={false}
        dispatch={this.props.dispatch}
      />
    );
  }

  render() {
    // 社員情報取得後に表示
    if (_.isEmpty(this.props.userSetting)) {
      return null;
    }

    return (
      <GlobalContainer>
        <NavigationBar icon={IconPlanner}>
          <div className="ts-calendar__header">
            <div>
              <Text size="xxl" color="secondary" bold>
                {msg().Cal_Lbl_Planning}
              </Text>
            </div>
            <div className="ts-calendar__header__personal-setting-button">
              <PersonalMenuPopoverButtonContainer />
            </div>
          </div>
        </NavigationBar>

        <div className="ts-calendar">
          <div className="planner-pc-planner">
            {this.props.userSetting.useWorkTime && (
              <div>
                <TrackSummary.Request margin="20px" />
              </div>
            )}
            <div
              className={classnames({
                'planner-pc-planner__calendar__header': true,
                'is-narrow': !this.props.userSetting.useWorkTime,
              })}
            >
              <CalendarHeader />
            </div>
            <div className="planner-pc-planner__calendar">
              {this.renderCalendar()}
            </div>
          </div>

          <EventEditPopupContainer
            eventStyle={this.state.eventStyle}
            closeEventEdit={this.closeEventEdit}
            onClickJobFinderButton={this.openJobFinder}
          />

          <EventListPopup
            style={this.state.eventListPosition}
            events={(
              this.props.empEvents[
                this.state.eventListDate.format(
                  DATETIME_FORMAT_FOR_EVENT_DATA_KEY
                )
              ] || []
            ).map((e) =>
              /* Enforce casting CalendarEvent to BaseEvent */
              ({ ...e, layout: { ...e.layout } })
            )}
            targetDate={this.state.eventListDate}
            isOpened={this.state.isEventListOpen}
            onClickClose={this.closeEventListPopup}
            onClickEvent={this.openEventEdit}
          />

          <Modal
            onClosed={this.hideDialog}
            isOpen={this.props.dialog.show}
            component={this.renderModalComponent()}
            modalStyle={this.state.modalStyle}
          />
        </div>

        <PersonalMenuPopoverContainer
          showProxyEmployeeSelectButton={false}
          showChangeApproverButton={false}
          showPersonalSettingButton
        />
        <PersonalSettingDialogContainer />

        <ToastContainer />

        <DialogConductor dialogs={this.props.dialogs} />
      </GlobalContainer>
    );
  }
}
