// @flow

import React from 'react';

import { type UserSetting } from '../../domain/models/UserSetting';

import './CalendarMenu.scss';
import msg from '../../commons/languages';

type Props = $ReadOnly<{
  menuStyle: Object,
  closeMenu: () => void,
  empInfo: UserSetting,
}>;

type State = {
  menuStyle: Object,
};

export default class CalendarMenu extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      menuStyle: this.props.menuStyle,
    };
  }

  componentWillReceiveProps(nextProps: Props) {
    this.setState({ menuStyle: nextProps.menuStyle });
  }

  onItemSelected(page: string, key: string, value: string) {
    console.log(key, value);
    // 開発環境とClassic/LEXで飛び先変える必要あり。SF1はLEXと同じでよい？
    if (typeof sforce !== 'undefined' && sforce !== null) {
      // http://salesforce.stackexchange.com/questions/44148/salesforce1-navigatetourl-method
      // Salesforce1 navigation
      sforce.one.navigateToURL(`/apex/${page}_pc?${key}=${value}`);
    } else {
      const href = window.location.href;
      if (href.split('.')[1] === 'html') {
        // 簡易的な判定
        // ローカル
        window.location.href = `./${page}-pc.html?${key}=${value}`;
      } else {
        // Classic環境
        window.location.href = `./${page}_pc?${key}=${value}`;
      }
    }
  }

  renderTeRequest() {
    return (
      <div className="ts-calendar-menu__body__others">
        <div className="ts-calendar-menu__body__title">
          {msg().Cal_Btn_TERequest}
        </div>
        <div
          className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
          onClick={() =>
            this.onItemSelected('applications', 'new', 'overseas_travel')
          }
        >
          <img
            className="ts-calendar-menu__icon-question"
            src={require('../images/heddaIcon_Help.png')}
          />
          <span>&nbsp;{msg().Exp_Sel_OverseasTravel}</span>
        </div>
        <div
          className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
          onClick={() =>
            this.onItemSelected('applications', 'new', 'domestic_travel')
          }
        >
          <img
            className="ts-calendar-menu__icon-question"
            src={require('../images/heddaIcon_Help.png')}
          />
          <span>&nbsp;{msg().Exp_Sel_DomesticTravel}</span>
        </div>
        <div
          className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
          onClick={() =>
            this.onItemSelected('applications', 'new', 'general_expense')
          }
        >
          <img
            className="ts-calendar-menu__icon-question"
            src={require('../images/heddaIcon_Help.png')}
          />
          <span>&nbsp;{msg().Exp_Sel_Entertainment}</span>
        </div>
        <div
          className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
          onClick={() =>
            this.onItemSelected('applications', 'new', 'general_expense')
          }
        >
          <img
            className="ts-calendar-menu__icon-question"
            src={require('../images/heddaIcon_Help.png')}
          />
          <span>&nbsp;{msg().Exp_Sel_GeneralExpense}</span>
        </div>
        <div
          className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
          onClick={() =>
            this.onItemSelected('applications', 'new', 'general_expense')
          }
        >
          <img
            className="ts-calendar-menu__icon-question"
            src={require('../images/heddaIcon_Help.png')}
          />
          <span>&nbsp;{msg().Exp_Sel_CashAdvance}</span>
        </div>
      </div>
    );
  }

  renderExpense() {
    return (
      <div className="ts-calendar-menu__body__others">
        <div className="ts-calendar-menu__body__title">
          {msg().Cal_Lbl_ExpenseGroup}
        </div>
        <div className="ts-calendar-menu__body__item ts-calendar-menu__pointer">
          <img
            className="ts-calendar-menu__icon-question"
            src={require('../images/heddaIcon_Help.png')}
          />
          <span>&nbsp;{msg().Cal_Lbl_SettleExpenses}</span>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div
        className={`ts-calendar-menu ${
          this.state.menuStyle.isClose ? 'ts-calendar-menu-none' : ' '
        }`}
      >
        <div
          className="ts-calendar-menu__overlay"
          onClick={this.props.closeMenu}
        />
        <div
          className={`ts-calendar-menu__wrap ${
            this.state.menuStyle.classNames
          }`}
          style={this.state.menuStyle.style}
        >
          <div className="ts-calendar-menu__header">
            <div className="ts-calendar-menu__header__row">
              <img
                className="ts-calendar-menu__icon-baloon"
                src={require('../images/popIcon.png')}
              />
              <span>&nbsp;{msg().Cal_Btn_Menu}</span>
              <img
                onClick={this.props.closeMenu}
                className="ts-calendar-menu__icon-close"
                src={require('../images/popClose.png')}
              />
            </div>
          </div>
          <div className="ts-calendar-menu__body">
            {/* TODO: 勤怠に対応するまでは非表示
            <div className="ts-calendar-menu__body__applications">
              <div className="ts-calendar-menu__body__title">
                {msg().Appr_Lbl_AttendanceRequest}
              </div>
              <div className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
                onClick={(event) => this.onItemSelected('timesheet', 'modal', 'daily_closing')}
              >
                <img
                  className="ts-calendar-menu__icon-question"
                  src={require('../images/heddaIcon_Help.png')}
                />
                <span>
                  &nbsp;{msg().Cal_Lbl_RequestsOvertime}
                </span>
              </div>
              <div className="ts-calendar-menu__body__item ts-calendar-menu__pointer"
                onClick={(event) => this.onItemSelected('timesheet', 'modal', 'leave_request')}
              >
                <img
                  className="ts-calendar-menu__icon-question"
                  src={require('../images/heddaIcon_Help.png')}
                />
                <span>
                  &nbsp;{msg().Cal_Lbl_RequestsLeaveEarly}
                </span>
              </div>
            </div>

            <div className="ts-calendar-menu__body__separate" />
            */}
            {/* {(this.props.empInfo.usePreApplication ? this.renderTeRequest() : null)} */}
            <div className="ts-calendar-menu__body__separate" />
            {this.props.empInfo.useExpense ? this.renderExpense() : null}
          </div>
        </div>
      </div>
    );
  }
}
