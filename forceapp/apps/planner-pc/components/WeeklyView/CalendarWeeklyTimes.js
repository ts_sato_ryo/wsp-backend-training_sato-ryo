// @flow

import * as React from 'react';

import './CalendarWeeklyTimes.scss';

type Props = $ReadOnly<{
  heightOffset: number,
}>;

/**
 * 時刻(09:00 など)が表示されている列
 */
export default class CalendarWeeklyTimes extends React.Component<Props> {
  node: ?React.ElementRef<'div'>;

  paddingZero(i: number) {
    const time = `0${i}`.slice(-2);
    return `${time}:00`;
  }

  render() {
    const style = {
      height: `calc(100% - (45px + ${this.props.heightOffset}px))`,
      top: 45 + this.props.heightOffset,
    };

    // FIXME: ref は親から参照するために入れている。より良い方法があれば修正する。
    return (
      <div
        className="ts-calendar-weekly-times"
        style={style}
        ref={(ref) => {
          this.node = ref;
        }}
      >
        <table className="ts-calendar-weekly-times__table">
          <tbody>
            {[...Array(24)].map((x, i) => (
              <tr key={i}>
                <td
                  className={`ts-calendar-weekly-times__td ${
                    i % 2 === 0 ? 'odd' : 'even'
                  }`}
                >
                  <div>{this.paddingZero(i)}</div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
