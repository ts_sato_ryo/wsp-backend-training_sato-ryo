// @flow

import React from 'react';
import moment from 'moment';
import _ from 'lodash';
import classNames from 'classnames';

import { type CalendarEvent } from '../../models/calendar-event/CalendarEvent';

import msg from '../../../commons/languages';
import DateUtil from '../../../commons/utils/DateUtil';
import eventTemplate from '../../constants/eventTemplate';
import AllDayEvent from './AllDayEvent';

import './CalendarWeeklyDays.scss';

type Props = $ReadOnly<{
  openEventEdit: (CalendarEvent, SyntheticEvent<HTMLButtonElement>) => void,
  openEventListPopup: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void,
  showDailySummary: null | ((moment) => void),
  vacancyTable: boolean[][],
  allDayEvents: CalendarEvent[][],
  height: number,
  startDate: moment,
}>;

function backgroundColorClass(day: moment, baseClassName: string = '') {
  const classname = classNames({
    [baseClassName]: baseClassName || false,
    sunday: parseInt(day.format('d')) === 0,
    saturday: parseInt(day.format('d')) === 6,
  });
  return classname;
}

/**
 * 日付ヘッダ
 */
export default class CalendarWeeklyDays extends React.Component<Props> {
  renderDaysHeader: (moment, index: number) => void;
  days: moment[];
  numberOfMaximumEventsByDay: number;

  constructor(props: Props) {
    super(props);
    this.configureDays(props);

    this.renderDaysHeader = this.renderDaysHeader.bind(this);
  }

  componentWillReceiveProps(nextProps: Props) {
    this.configureDays(nextProps);
  }

  openNewEventEdit(day: moment, mouseEvent: SyntheticEvent<HTMLButtonElement>) {
    const selectedEvent = _.cloneDeep(eventTemplate);
    const start = day.clone().minutes(0);
    selectedEvent.start = start;
    selectedEvent.end = start.clone();
    selectedEvent.isAllDay = true;
    this.props.openEventEdit(selectedEvent, mouseEvent);
  }

  configureDays(props: Props) {
    this.days = [];
    for (let i = 0; i < 7; i++) {
      this.days.push(props.startDate.clone().add(i, 'days'));
    }
  }

  renderMessageRow() {
    const grids = [];
    for (let i = 0; i < 7; i++) {
      const day = this.days[i];
      let numberOfHiddenEvents = 0;
      for (
        let j = this.numberOfMaximumEventsByDay;
        j < this.props.vacancyTable.length;
        j++
      ) {
        if (!this.props.vacancyTable[j][i]) {
          numberOfHiddenEvents += 1;
        }
      }
      if (numberOfHiddenEvents === 0) {
        grids.push(
          <td
            key={i}
            className="ts-calendar-weekly-days__header-all-day"
            onClick={(mouseEvent) => this.openNewEventEdit(day, mouseEvent)}
          />
        );
      } else {
        grids.push(
          <td
            key={i}
            className="ts-calendar-weekly-days__header-all-day"
            onClick={(mouseEvent) =>
              this.props.openEventListPopup(
                day,
                this.props.allDayEvents,
                mouseEvent
              )
            }
          >
            <div className="ts-calendar-weekly-days__header-all-day--hidden">
              {msg().Cal_Lbl_MoreEvents1}
              {numberOfHiddenEvents}
              {msg().Cal_Lbl_MoreEvents2}
            </div>
          </td>
        );
      }
    }
    return grids;
  }

  renderBlankRow() {
    const grids = [];
    for (let i = 0; i < 7; i++) {
      const day = this.days[i];
      grids.push(
        <td
          key={i}
          className="ts-calendar-weekly-days__header-all-day blank"
          onClick={(mouseEvent) => this.openNewEventEdit(day, mouseEvent)}
        />
      );
    }
    return grids;
  }

  renderRow(events: CalendarEvent[]) {
    const grids = [];
    for (let i = 0; i < 7; i++) {
      const day = this.days[i];
      if (!events[i]) {
        grids.push(this.renderEmptyGrid(day));
      } else {
        const colSpan = events[i].layout.colSpan;
        const event = events[i];
        i = i + colSpan - 1;
        grids.push(this.renderEventGrid(day, colSpan, event));
      }
    }
    return grids;
  }

  renderEmptyGrid(day: moment) {
    return (
      <td
        key={`empty-${day.toISOString()}`}
        className="ts-calendar-weekly-days__header-all-day"
        onClick={(mouseEvent) => this.openNewEventEdit(day, mouseEvent)}
      />
    );
  }

  renderEventGrid(day: moment, colSpan: number, event: CalendarEvent) {
    return (
      // $FlowFixMe v0.89
      <AllDayEvent
        key={`empty-${day.toISOString()}`}
        event={event}
        day={day}
        colSpan={colSpan}
        openEventEdit={this.props.openEventEdit}
      />
    );
  }

  renderDaysHeader(day: moment, i: number) {
    const weekday = day.weekday();
    const isHoliday = [0, 6].includes(weekday);

    const cssClass = classNames('ts-calendar-weekly-days__header', {
      'ts-calendar-weekly-days__header--holiday': isHoliday,
    });

    // FIXME aタグのonClick, Buttonに差し替え(a11y)
    return (
      <td className={cssClass} key={i}>
        <div className="ts-calendar-weekly-days__header__top">
          {this.props.showDailySummary ? (
            <button
              className="ts-calendar-weekly-days__day-number"
              onClick={
                this.props.showDailySummary && this.props.showDailySummary(day)
              }
            >
              <div>{DateUtil.formatW(day.toISOString())}</div>
              <div className="ts-calendar-weekly-days__day">
                {DateUtil.formatD(day.toISOString())}
              </div>
            </button>
          ) : (
            <div className="ts-calendar-weekly-days__day-number">
              <div>{DateUtil.formatW(day.toISOString())}</div>
              <div className="ts-calendar-weekly-days__day">
                {DateUtil.formatD(day.toISOString())}
              </div>
            </div>
          )}
        </div>
        <div
          className={`ts-calendar-weekly-days__header__wrap
          ${day.isSame(moment(), 'day') ? ' today' : ''}`}
        />
      </td>
    );
  }

  render() {
    const style = {
      width: 'calc(100% - 78px)',
      overflowY: 'scroll',
      height: `${this.props.height}px`,
    };

    this.numberOfMaximumEventsByDay = 5;

    return (
      <div className="ts-calendar-weekly-days" style={style}>
        <table className="ts-calendar-weekly-days__table-border">
          <tbody>
            <tr>
              {this.days.map((day, i) => (
                <td key={i} className={`${backgroundColorClass(day)}`} />
              ))}
            </tr>
          </tbody>
        </table>
        <table className="ts-calendar-weekly-days__table">
          <tbody>
            <tr>{this.days.map(this.renderDaysHeader)}</tr>
            {this.props.allDayEvents.map((events, i) => {
              if (i > this.numberOfMaximumEventsByDay) {
                return null;
              }
              if (i === this.numberOfMaximumEventsByDay) {
                return <tr key={i}>{this.renderMessageRow()}</tr>;
              }
              return <tr key={i}>{this.renderRow(events)}</tr>;
            })}
            <tr>{this.renderBlankRow()}</tr>
          </tbody>
        </table>
      </div>
    );
  }
}
