// @flow

import React from 'react';
import classNames from 'classnames';
import moment from 'moment';

import { CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT } from '../../constants/event';
import DateUtil from '../../../commons/utils/DateUtil';
import ImgIconHasJob from '../../images/iconHasJob.png';
import ImgStsCalGaisyutu from '../../images/sts_Cal_gaisyutu.png';
import msg from '../../../commons/languages';

import './CalendarWeeklyDays.scss';
import type { CalendarEvent } from '../../models/calendar-event/CalendarEvent';

type Props = $ReadOnly<{
  event: CalendarEvent,
  day: moment,
  colSpan?: number,
  openEventEdit: (CalendarEvent, SyntheticEvent<HTMLButtonElement>) => void,
}>;

/**
 * 終日予定
 * tdをroot要素として生成するので注意
 */
export default class AllDayEvent extends React.Component<Props> {
  static defaultProps = {
    colSpan: 1,
    openEventEdit: null,
  };

  renderJobStatus(event: CalendarEvent) {
    if (event.job.id) {
      return (
        <img
          className="ts-calendar-weekly-days__all-day-event__job-status"
          src={ImgIconHasJob}
          alt={event.job.name}
        />
      );
    } else {
      return null;
    }
  }

  renderIcons(event: CalendarEvent) {
    const icons = [];

    if (event.isOuting) {
      icons.push(
        <img
          className="ts-calendar-weekly-days__all-day-event__icon"
          src={ImgStsCalGaisyutu}
          alt={msg().Cal_Lbl_OutOf}
        />
      );
    }

    return icons;
  }

  render() {
    // TODO: 予定のタイトルに何を表示させるかもactionの中であらかじめ決められないか？
    const eventName = this.props.event.title || msg().Cal_Lbl_NoTitle;
    let eventTitle = eventName;

    const hasLink =
      this.props.event.createdServiceBy !==
      CALENDAR_CREATED_SERVICE_BY_TEAMSPIRIT;
    const allowEdit = !hasLink && this.props.openEventEdit;

    if (!this.props.event.isAllDay) {
      if (moment(this.props.event.start).isSame(this.props.day, 'day')) {
        // NOTE: 予定開始日ではない場合には時刻を表示させない
        eventTitle = `${DateUtil.format(
          this.props.event.start,
          'HH:mm'
        )} ${eventName}`;
      }
    }

    // openEventEditが付与されていない場合は非クリッカブル要素とする
    const divProps: {
      className: string,
      onClick?: (SyntheticEvent<HTMLButtonElement>) => void,
      role?: string,
      tabIndex?: number,
    } = {
      className: classNames('ts-calendar-weekly-days__all-day-event__title', {
        'ts-calendar-weekly-days__all-day-event__title--has-link': hasLink,
        'ts-calendar-weekly-days__all-day-event__title--no-clickable': !allowEdit,
      }),
    };

    // Make the event clickable if it were provided by WSP,
    // and openEventEdit prop were provided.
    if (allowEdit) {
      divProps.onClick = (mouseEvent) =>
        this.props.openEventEdit(this.props.event, mouseEvent);
      divProps.role = 'button';
      divProps.tabIndex = 0;
    }

    return (
      <td
        colSpan={this.props.colSpan}
        className="ts-calendar-weekly-days__header-all-day ts-calendar-weekly-days__all-day-event"
      >
        <div {...divProps}>
          {this.renderJobStatus(this.props.event)}
          {eventTitle}
          {this.renderIcons(this.props.event)}
        </div>
      </td>
    );
  }
}
