// @flow

import * as React from 'react';
import classNames from 'classnames';
import _ from 'lodash';
import Measure from 'react-measure';
import moment from 'moment';
import { type Dispatch } from 'redux';

import {
  type CalendarEvent,
  type CalendarEventMap,
} from '../../models/calendar-event/CalendarEvent';

import msg from '../../../commons/languages';
import CalendarUtil from '../../../commons/utils/CalendarUtil';
import { addPositionAndWidth } from '../../models/calendar-event/BaseEvent';

import CalendarWeeklyTimes from './CalendarWeeklyTimes';
import CalendarWeeklyDays from './CalendarWeeklyDays';
import CalendarWeeklyEvent from './CalendarWeeklyEvent';

import eventTemplate from '../../constants/eventTemplate';

import './index.scss';

type CalendarWeeklyEventListProps = $ReadOnly<{
  available: boolean,
  events: CalendarEventMap,
  selectedDay: moment,
  startDayOfTheWeek: number,
  width: number,
  openEventEdit: (CalendarEvent, SyntheticEvent<HTMLButtonElement>) => void,
  openEventListPopup: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void,
  showDailySummary: null | ((moment) => void),
}>;

type CalendarWeeklyEventListState = {
  events: Object,
  startDate: moment,
};

// TODO: 他のところでも使われているので helpers/ に移動する
function backgroundColorClass(day: moment, baseClassName: string) {
  const classname = classNames({
    [baseClassName]: true,
    sunday: parseInt(day.format('d')) === 0,
    saturday: parseInt(day.format('d')) === 6,
  });
  return classname;
}

const timesPerDay = (targetDate: moment): $ReadOnlyArray<moment> => {
  const hoursPerDay = 24;
  const times = [];
  const now = targetDate
    .clone()
    .set('hour', 0)
    .set('minute', 0)
    .set('second', 0)
    .set('millisecond', 0);
  for (let i = 0; i < hoursPerDay; i++) {
    const t = now.clone().add(i, 'hours');
    times.push(t);
  }
  return times;
};

/**
 * 週ビュー
 */
class CalendarWeeklyEventList extends React.Component<
  CalendarWeeklyEventListProps,
  CalendarWeeklyEventListState
> {
  onMeasure: (Object) => void;
  actualWidthOfEventsTable: number;
  numberOfRows: number;
  vacancyTable: boolean[][];
  timesRef: React.ElementRef<any>;
  eventsRef: ?React.ElementRef<'div'>;

  constructor(props: CalendarWeeklyEventListProps) {
    super(props);

    this.state = {
      events: this.expandEventsByDays(this.props.events),
      startDate: this.configureStartDate(this.props.selectedDay),
    };

    this.actualWidthOfEventsTable = 0;

    this.onMeasure = this.onMeasure.bind(this);
  }

  componentDidMount() {
    const timesDiv = this.timesRef.node;
    this.addScrollSynchronization(timesDiv, this.eventsRef, 'vertical');
    if (this.eventsRef) {
      this.eventsRef.scrollTop = 60 * 7; // 初期表示7時を先頭に
    }
  }

  componentWillReceiveProps(nextProps: CalendarWeeklyEventListProps) {
    const startDate = this.configureStartDate(nextProps.selectedDay);
    const events = this.expandEventsByDays(nextProps.events);
    this.setState({
      startDate,
      events,
    });
  }

  onMeasure(dimension) {
    if (this.actualWidthOfEventsTable !== dimension.width) {
      this.actualWidthOfEventsTable = dimension.width;
      this.setState({
        events: this.expandEventsByDays(this.props.events),
      });
    }
  }

  getOnScrollFunction(oElement) {
    return (event: SyntheticEvent<HTMLElement>) => {
      if (
        oElement._scrollSyncDirection === 'horizontal' ||
        oElement._scrollSyncDirection === 'both'
      ) {
        oElement.scrollLeft = (event.target: any).scrollLeft;
      }
      if (
        oElement._scrollSyncDirection === 'vertical' ||
        oElement._scrollSyncDirection === 'both'
      ) {
        oElement.scrollTop = (event.target: any).scrollTop;
      }
    };
  }

  configureStartDate(date: moment) {
    let startDate = date.clone().day(this.props.startDayOfTheWeek);
    if (startDate.isAfter(date)) {
      startDate = date
        .clone()
        .add(-1, 'w')
        .day(this.props.startDayOfTheWeek);
    }
    return startDate;
  }

  generateCalendarEvents(events: CalendarEvent[]) {
    if (!events) {
      return [];
    }

    // TODO: calculate the width of the target (for reuse)
    const targetWidth = this.actualWidthOfEventsTable / 7 - 10;

    return addPositionAndWidth(events, targetWidth);
  }

  expandEventsByDays(events: CalendarEventMap) {
    const daysInMonthCalendar = 42;
    const date = CalendarUtil.getStartDateOfMonthView(
      this.props.selectedDay,
      this.props.startDayOfTheWeek
    );

    const calendarEvents = {};
    for (let i = 0; i < daysInMonthCalendar; i++) {
      const event = events[date.format('YYYYMMDD')];
      calendarEvents[date.format('YYYYMMDD')] = this.generateCalendarEvents(
        event
      );
      date.add(1, 'days');
    }
    return calendarEvents;
  }

  // This function adds scroll syncronization for the fromElement to the toElement
  // this means that the fromElement will be updated when the toElement is scrolled
  addScrollSynchronization(
    fromElement: any,
    toElement: any,
    direction: string
  ) {
    this.removeScrollSynchronization(fromElement);

    fromElement._syncScroll = this.getOnScrollFunction(fromElement);
    fromElement._scrollSyncDirection = direction;
    fromElement._syncTo = toElement;
    toElement.addEventListener('scroll', fromElement._syncScroll);
  }

  // removes the scroll synchronization for an element
  removeScrollSynchronization(fromElement) {
    if (fromElement._syncTo) {
      fromElement._syncTo.removeEventListener(
        'onscroll',
        fromElement._syncScroll
      );
    }

    fromElement._syncTo = null;
    fromElement._syncScroll = null;
    fromElement._scrollSyncDirection = null;
  }

  openNewEventEdit(
    time: moment,
    event: SyntheticMouseEvent<HTMLButtonElement>
  ) {
    const selectedEvent = _.cloneDeep(eventTemplate);
    const start = time.clone();
    selectedEvent.start = start;
    selectedEvent.end = start.clone().add(1, 'hours');
    this.props.openEventEdit(selectedEvent, event);
  }

  openEventEdit(selectedEvent, mouseEvent) {
    this.props.openEventEdit(selectedEvent, mouseEvent);
  }

  getRowIndex(allDayEvents: CalendarEvent[][], colIndex: number): number {
    let rowIndex = 0;
    while (true) {
      if (!allDayEvents[rowIndex]) {
        allDayEvents[rowIndex] = [];
        this.vacancyTable[rowIndex] = [
          true,
          true,
          true,
          true,
          true,
          true,
          true,
        ];
      }
      // NOTE: 指定グリッドにすでにイベントが入っているかどうかを検索
      if (this.vacancyTable[rowIndex][colIndex]) {
        return rowIndex;
      }
      rowIndex += 1;
    }

    // Never reach here. The blow code is needed to pass type checking.
    // eslint-disable-next-line no-unreachable
    return 0;
  }

  getAllDayEvents(monthEvents) {
    const allDayEvents = [];
    allDayEvents[0] = [];
    this.vacancyTable = [];
    this.vacancyTable[0] = [true, true, true, true, true, true, true];

    for (let i = 0; i < 7; i++) {
      const dateKey = this.state.startDate
        .clone()
        .add(i, 'days')
        .format('YYYYMMDD');
      const events = monthEvents[dateKey];
      if (events !== undefined) {
        events.forEach((event) => {
          if (event.layout.containsAllDay) {
            const rowIndex = this.getRowIndex(
              allDayEvents,
              event.layout.colIndex
            );
            allDayEvents[rowIndex][event.layout.colIndex] = event;

            let startCol = event.layout.colIndex;
            const endCol = event.layout.colIndex + event.layout.colSpan;
            for (startCol; startCol < endCol; startCol++) {
              this.vacancyTable[rowIndex][startCol] = false;
            }
          }
        });
      }
    }

    return allDayEvents;
  }

  render() {
    const date = this.state.startDate.clone();
    const events = this.state.events;
    const allDayEvents = this.getAllDayEvents(events);
    this.numberOfRows = allDayEvents.length;
    if (this.numberOfRows === 0) {
      this.numberOfRows = 1;
    } else if (this.numberOfRows > 6) {
      this.numberOfRows = 6;
    }
    const blankSpaceOffset = 10; // NOTE: 終日予定を作成するための空白領域
    const heightOffset = this.numberOfRows * 26 + blankSpaceOffset;
    const dayGridStyle = {
      height: heightOffset,
    };
    const timeGridStyle = {
      height: `calc(100% - (45px + ${heightOffset}px))`,
      top: 45 + heightOffset,
    };

    return (
      <div className="ts-calendar-weekly-event-list">
        <div className="ts-calendar-weekly-time-corner">
          <table>
            <tbody>
              <tr>
                <td className="ts-calendar-weekly-time-corner-td">
                  <span />
                </td>
              </tr>
              <tr>
                <td
                  className="ts-calendar-weekly-time-corner-td__all-day"
                  style={dayGridStyle}
                >
                  <span>{msg().Cal_Lbl_AllDay}</span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <CalendarWeeklyTimes
          ref={(ref) => {
            this.timesRef = ref;
          }}
          heightOffset={heightOffset}
        />
        <CalendarWeeklyDays
          available={this.props.available}
          startDate={this.state.startDate}
          openEventEdit={this.props.openEventEdit}
          showDailySummary={this.props.showDailySummary}
          events={events}
          width={this.props.width}
          height={heightOffset + 44}
          allDayEvents={allDayEvents}
          vacancyTable={this.vacancyTable}
          openEventListPopup={this.props.openEventListPopup}
        />
        <div
          className="ts-calendar-weekly-event-list__events"
          ref={(ref) => {
            this.eventsRef = ref;
          }}
          style={timeGridStyle}
        >
          <table className="ts-calendar-weekly-event-list__events-border-table">
            <tbody>
              {[...Array(24)].map((value, i) => (
                <tr key={i}>
                  <td className="ts-calendar-weekly-event-list__events-border-td">
                    <hr />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <Measure onMeasure={(dimension) => this.onMeasure(dimension)}>
            <table className="ts-calendar-weekly-event-list__events-table">
              <tbody>
                <tr>
                  {[...Array(7)].map((_days) => {
                    const targetDate = date.clone();
                    let eventComps = [];
                    if (events[date.format('YYYYMMDD')]) {
                      eventComps = events[date.format('YYYYMMDD')].map(
                        (event, key) => {
                          if (!event.layout.containsAllDay) {
                            return (
                              <CalendarWeeklyEvent
                                event={event}
                                key={key}
                                openEventEdit={this.props.openEventEdit}
                              />
                            );
                          }
                          return null;
                        }
                      );
                    }
                    date.add(1, 'days');
                    return (
                      <td
                        className={backgroundColorClass(
                          date.clone().add(-1, 'day'),
                          'ts-calendar-weekly-event-list__day'
                        )}
                        key={date.format('YYYYMMDD')}
                      >
                        <div className="ts-calendar-weekly-event-list__click_item">
                          {timesPerDay(targetDate).map((time: moment) => (
                            <div
                              key={time.toISOString()}
                              className="ts-calendar-weekly-event-list__click_item__button-group"
                            >
                              <div
                                className="ts-calendar-weekly-event-list__click_item__button"
                                onClick={(e) => this.openNewEventEdit(time, e)}
                              />
                              <div
                                className="ts-calendar-weekly-event-list__click_item__button"
                                onClick={(e) =>
                                  this.openNewEventEdit(
                                    time.clone().add(30, 'minutes'),
                                    e
                                  )
                                }
                              />
                            </div>
                          ))}
                        </div>
                        {eventComps}
                      </td>
                    );
                  })}
                </tr>
              </tbody>
            </table>
          </Measure>
        </div>
      </div>
    );
  }
}

type Props = $ReadOnly<{
  available: boolean,
  events: CalendarEventMap,
  selectedDay: moment,
  startDayOfTheWeek: number,
  openEventEdit: (CalendarEvent, SyntheticEvent<HTMLButtonElement>) => void,
  openEventListPopup: (
    moment,
    CalendarEvent[][],
    SyntheticMouseEvent<HTMLElement>
  ) => void,
  showDailySummary: null | ((moment) => void),
  dispatch: Dispatch<any>,
}>;

type State = {
  width: number,
};

class WeeklyView extends React.Component<Props, State> {
  handleResize: () => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      width: window.innerWidth < 1024 ? 1024 : window.innerWidth,
    };
    this.handleResize = this.handleResize.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize() {
    this.setState({
      width: window.innerWidth < 1024 ? 1024 : window.innerWidth,
    });
  }

  render() {
    return (
      <div className="ts-calendar-weekly slds">
        <div className="ts-calendar-weekly__body">
          <div className="planner-pc-weekly-view__calendar">
            <CalendarWeeklyEventList
              available={this.props.available}
              events={this.props.events}
              selectedDay={this.props.selectedDay}
              dispatch={this.props.dispatch}
              openEventEdit={this.props.openEventEdit}
              width={this.state.width}
              startDayOfTheWeek={this.props.startDayOfTheWeek}
              openEventListPopup={this.props.openEventListPopup}
              showDailySummary={this.props.showDailySummary}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default WeeklyView;
