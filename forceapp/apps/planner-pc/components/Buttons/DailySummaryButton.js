// @flow

import * as React from 'react';
import styled from 'styled-components';
import CommonButton from '../../../core/elements/Button';
import { Color } from '../../../core/styles';

type Props = $ReadOnly<{|
  className?: string,
  label: string,
  onClick: () => void,
|}>;

const Button = styled(CommonButton)`
  width: 130px;
  height: 32px;
  padding: 7px 0 8px 0;
  margin-left: 32px;
  color: ${Color.font1};
  font-size: 13px;
  line-height: 17px;
`;

const DailySummaryButton = ({ label, ...props }: Props) => (
  <Button {...props}>{label}</Button>
);

export default DailySummaryButton;
