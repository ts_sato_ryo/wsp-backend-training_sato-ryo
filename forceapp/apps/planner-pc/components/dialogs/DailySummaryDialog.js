// @flow

import moment from 'moment';

import React from 'react';

import type { Dispatch } from 'redux';
import type { CalendarEvent } from '../../models/calendar-event/CalendarEvent';
import type { DailySummaryTask } from '../../../domain/models/time-management/DailySummaryTask';
import type { BaseEvent } from '../../../commons/models/DailySummary/BaseEvent';
import type { Job } from '../../../domain/models/time-tracking/Job';
import type { Permission } from '../../../domain/models/access-control/Permission';
import type { WorkCategory } from '../../../domain/models/time-tracking/WorkCategory';

import DailySummaryDialogInternal from '../../../commons/components/dialogs/DailySummaryDialog';
import {
  moveDay,
  openEventListPopup,
  closeEventListPopup,
} from '../../actions/dailySummary';

type Props = $ReadOnly<{
  closeJobSelectDialog: () => void,
  defaultJobId: string,
  deleteTask: (index: number) => void,
  dispatch: Dispatch<Object>,
  editNote: (value: string) => void,
  editTask: (
    index: number,
    prop: string,
    value: $Values<DailySummaryTask>
  ) => void,
  editTaskTime: (
    index: number,
    prop: 'taskTime' | 'ratio' | 'volume',
    value: number
  ) => void,
  editTimestampComment: (string) => void,
  eventListPopupDay: moment,
  eventListPopupEvents: CalendarEvent[],
  eventListPopupIsOpen: boolean,
  eventListPopupStyle: Object,
  events: BaseEvent[],
  fetchJobTree: (selectedItem: Job, existingItems: ?(Job[][])) => void,
  hideDailySummaryDialog: () => void,
  isEnableEndStamp?: boolean,
  isLocked: boolean,
  isOpeningJobSelectDialog: boolean,
  isProxyMode: boolean,
  isTemporaryWorkTime: boolean,
  jobTree: { ...Job, isGroup: boolean }[][],
  note: string,
  realWorkTime: number,
  reorderTaskList: Function,
  saveDailySummary: Function,
  saveDailySummaryAndRecordEndTimestamp: Function,
  selectedDay: moment,
  showJobSelectDialog: () => void,
  taskList: DailySummaryTask[],
  timestampComment: string,
  toggleDirectInput: Function,
  userPermission: Permission,
  workCategoryLists: { [jobId: string]: WorkCategory[] },
  zIndex: number,
}>;

export default class DailySummaryDialog extends React.Component<Props> {
  moveDay: (moment) => void;
  openEventListPopup: (
    SyntheticMouseEvent<HTMLElement>,
    moment,
    BaseEvent[]
  ) => void;

  closeEventListPopup: () => void;

  constructor(props: Props) {
    super(props);

    this.moveDay = this.moveDay.bind(this);
    this.openEventListPopup = this.openEventListPopup.bind(this);
    this.closeEventListPopup = this.closeEventListPopup.bind(this);
  }

  moveDay(momentObj: moment) {
    const { dispatch } = this.props;
    dispatch(moveDay(momentObj));
  }

  openEventListPopup(
    e: SyntheticMouseEvent<HTMLElement>,
    day: moment,
    events: BaseEvent[]
  ) {
    this.props.dispatch(openEventListPopup(e, day, events));
  }

  closeEventListPopup() {
    this.props.dispatch(closeEventListPopup());
  }

  extractAllDayLayout(events: CalendarEvent[]) {
    return (events.filter((event) => {
      return event.layout.containsAllDay === true;
    }): CalendarEvent[]);
  }

  render() {
    return (
      <div style={{ zIndex: this.props.zIndex }}>
        <DailySummaryDialogInternal
          userPermission={this.props.userPermission}
          isProxyMode={this.props.isProxyMode}
          hideDialog={this.props.hideDailySummaryDialog}
          events={this.props.events}
          moveDay={this.moveDay}
          selectedDay={this.props.selectedDay}
          openEventListPopup={this.openEventListPopup}
          showJobSelectDialog={this.props.showJobSelectDialog}
          saveDailySummaryAndRecordEndTimestamp={
            this.props.saveDailySummaryAndRecordEndTimestamp
          }
          saveDailySummary={this.props.saveDailySummary}
          note={this.props.note}
          timestampComment={this.props.timestampComment}
          taskList={this.props.taskList}
          workCategoryLists={this.props.workCategoryLists}
          defaultJobId={this.props.defaultJobId}
          deleteTask={this.props.deleteTask}
          editTask={this.props.editTask}
          editNote={this.props.editNote}
          editTaskTime={this.props.editTaskTime}
          editTimestampComment={this.props.editTimestampComment}
          toggleDirectInput={this.props.toggleDirectInput}
          reorderTaskList={this.props.reorderTaskList}
          isTemporaryWorkTime={this.props.isTemporaryWorkTime}
          realWorkTime={this.props.realWorkTime}
          isLocked={this.props.isLocked}
          isEnableEndStamp={this.props.isEnableEndStamp}
          opening
          isOpeningJobSelectDialog={this.props.isOpeningJobSelectDialog}
          jobTree={this.props.jobTree}
          closeJobSelectDialog={this.props.closeJobSelectDialog}
          fetchJobTree={this.props.fetchJobTree}
        />
      </div>
    );
  }
}
