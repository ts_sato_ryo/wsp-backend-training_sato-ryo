/* eslint-disable import/no-extraneous-dependencies */
import webpack from 'webpack';
import path from 'path';
import log from 'fancy-log';

const APP_NAME = 'tsnext';
const SFDX_BASE_PATH = 'sfdx/force-app/main/default';

export default function createWebpackConfig(env, componentList) {
  // NOTE: gulp タスク内で NODE_ENV を設定するので、この定数は関数外に定義できなかった
  const PRODUCTION = process.env.NODE_ENV === 'production';

  let entries = {
    admin_pc: `./apps/admin-pc/index-${env}.js`,
    expenses_pc: `./apps/expenses-pc/index-${env}.js`,
    requests_pc: `./apps/requests-pc/index-${env}.js`,
    finance_approval_pc: `./apps/finance-approval-pc/index-${env}.js`,
    approvals_pc: `./apps/approvals-pc/index-${env}.js`,
    oauth_result_view: `./apps/oauth-result-view/index-${env}.js`,
    planner_pc: `./apps/planner-pc/index-${env}.js`,
    team_pc: `./apps/team-pc/index-${env}.js`,
    timesheet_pc: `./apps/timesheet-pc/index-${env}.js`,
    timesheet_pc_leave: `./apps/timesheet-pc-leave/index-${env}.js`,
    timesheet_pc_summary: `./apps/timesheet-pc-summary/index-${env}.js`,
    tracking_pc: `./apps/time-tracking/tracking-pc/index-${env}.js`,
    test_api: './apps/test-api/index.js',
    mobile_app: `./apps/mobile-app/index-${env}.js`,
    expenses_pc_print: `./apps/expenses-pc-print/index-${env}.js`,
    subapps_debug: './apps/subapps-debug/index.js',
  };
  // filter entries to only include selected component
  if (componentList && componentList.length > 0) {
    log(`Selected Component = ${componentList}`);
    Object.keys(entries).forEach((key) => {
      if (!componentList.includes(key)) {
        delete entries[key];
      }
    });
  }
  if (process.env.ENTRY) {
    const fileName = `./apps/${process.env.ENTRY.replace(
      /_/g,
      '-'
    )}/index-${env}.js`;
    entries = {
      [process.env.ENTRY]: fileName,
    };
  }

  const config = {
    entry: entries,
    output: {
      filename: '[name].js',
      chunkFilename: 'chunks/[id].[name].chunk.js',
      library: APP_NAME,
      libraryTarget: 'umd',
      path: path.resolve(__dirname, 'build', 'js'),
      publicPath: '/js/',
    },
    devServer: {
      historyApiFallback: {
        rewrites: [
          {
            from: /^\/mobile-app/,
            to: '/mobile-app/index.html',
          },
          {
            from: /^\/subapps-debug/,
            to: '/subapps-debug/index.html',
          },
        ],
      },
      contentBase: path.resolve(__dirname, 'build'),
      port: 3000,
    },
    devtool: PRODUCTION ? undefined : 'cheap-module-eval-source-map',
    module: {
      rules: [
        {
          test: path.resolve(__dirname, 'apps/commons/api'),
          use: [
            {
              loader: 'string-replace-loader',
              options: {
                search: '__SF_NAMESPACE__',
                replace: process.env.SF_NAMESPACE
                  ? `${process.env.SF_NAMESPACE}.`
                  : '',
              },
            },
          ],
          exclude: /(node_modules)/,
        },
        {
          test: /\.jsx?$/,
          use: [
            {
              loader: 'babel-loader?cacheDirectory=true',
              options:
                env === 'local' ? { plugins: ['react-hot-loader/babel'] } : {},
            },
          ],
          exclude: /(node_modules)/,
        },
        {
          test: /\.(css)$/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                minimize: PRODUCTION,
              },
            },
            'postcss-loader',
          ],
        },
        {
          test: /\.(scss)$/,
          use: [
            'style-loader',
            {
              loader: 'css-loader',
              options: {
                minimize: PRODUCTION,
              },
            },
            'postcss-loader',
            {
              loader: 'sass-loader',
              options: {
                includePaths: [
                  path.resolve(__dirname, 'apps/commons/styles'),
                  path.resolve(__dirname, 'apps/mobile-app/styles'),
                ],
                data: "@import 'global-imports.scss';",
              },
            },
          ],
        },
        {
          test: /\.(png|jpg)$/,
          // NOTE: vfp 環境向けのみ url-loader を使う
          use: [
            env === 'vfp'
              ? 'url-loader'
              : {
                  loader: 'file-loader',
                  options: { name: '[name][sha512:hash:base64:7].[ext]' },
                },
          ],
        },
        {
          test: /\.(eot|woff|woff2)$/,
          use: ['url-loader'],
        },
        {
          test: /\.svg$/,
          exclude: /node_modules/,
          use: [
            { loader: 'babel-loader' },
            {
              loader: 'react-svg-loader',
              query: {
                svgo: {
                  pretty: false,
                  plugins: [{ removeStyleElement: true }],
                },
              },
            },
          ],
        },
        {
          test: /\.svg$/,
          include: /node_modules/,
          // FIXME LDS assetsのsvgを使うための暫定対応
          use: [
            {
              loader: 'svg-react-loader',
              options: {
                filters: [
                  function(value) {
                    // LDSのアイコンをロードするとfill属性が必ず#fffになってしまう。
                    // fill属性が設定されるとcssから色をコントロールできなくなるため、
                    // 暫定処置として、fillを空に設定することにした。
                    if (value.children && Array.isArray(value.children)) {
                      value.children.forEach((child) => {
                        if (child.props && child.props.fill) {
                          child.props.fill = '';
                        }
                      });
                    }

                    this.update(value);
                  },
                ],
                classIdPrefix: '[name]-[hash:8]__',
                xmlnsTest: /^xmlns.*$/,
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          SF_ENV: JSON.stringify(env),
        },
      }),
    ],
  };

  return config;
}
